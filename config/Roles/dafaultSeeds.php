<?php
return
  [
  	'users' => 	[
  					'name' =>
  								[
  									'superAdmin'=>'Super Administrator',
  									'admin'=>'Administrator',
  									'user'=>'User',
  									'specialUser'=>'Special User',
  								],
  					'email' =>
  								[
  									'superAdmin'=>'superadmin@email.com',
  									'admin'=>'admin@email.com',
  									'user'=>'user@email.com',
  									'specialUser'=>'specialuser@email.com',
  								],
  					'password' =>
  								[
  									'superAdmin'=>'superadmin@welcome123',
  									'admin'=>'admin@welcome123',
  									'user'=>'user@welcome123',
  									'specialUser'=>'specialuser@welcome123',
  								],
  				],

  	'roles' => 	[
  					'name'=>
			  					[
			  						'superAdmin'=>'SuperAdmin',
                                    'admin'=>'Admin',
                                    'user'=>'User',
                                    'employee'=>'Employee',
                                    'manager'=>'Manager',
			  					],
            'description'=>
                  [
                    'superAdmin'=>'Super Admin Role',
                    'admin'=>'Admin Role',
                    'user'=>'User Role',
                    'employee'=>'Employee Role',
                    'manager' => 'Manager Role',
                  ],
  				],

  	'permissions' =>
  				[

  				],

  ];
