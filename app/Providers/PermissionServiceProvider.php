<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Blade;
use App\Models\Permission;

class PermissionServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    

    public function boot()
    {
        if (Schema::hasTable('permissions'))
        {

            //new

         // $permissions =  Cache::rememberForever('permissions', function (){ return Permission::get();});
         //    $permissions->map(function ($permission) 
         //        {
         //            Gate::define($permission->name, function ($user) use ($permission) 
         //            {
         //                return $user->hasPermission($permission);
         //            });
         //        });



           //old


            Permission::get()->map(function ($permission) 
            {
                Gate::define($permission->name, function ($user) use ($permission) 
                {
                    return $user->hasPermission($permission);
                });
            });


        }

        

        Blade::directive('role', function ($role)
            {
               return "<?php if(Auth::user()->hasRole({$role})): ?>";
            });
        Blade::directive('endrole', function ($role)
            {
                return "<?php endif; ?>";
            });
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
