<?php

namespace App\Models\Comman\Traits;
use Illuminate\Support\Facades\DB;

trait DropDown
{
    public static function toDropDown($tableName='',$nameField='',$idField='')
    {
        if($idField==null)
            {
                $idField="id";
            }
        $listFiledValues = DB::table($tableName)->select($idField,$nameField)->get();

        $selectArray=[];
        $selectArray[null] = '--Select--';
        foreach ($listFiledValues as $listFiledValue) 
        {
            $selectArray[$listFiledValue->$idField] = $listFiledValue->$nameField;
        }
        return $selectArray;
    }
}
