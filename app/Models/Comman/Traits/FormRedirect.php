<?php

namespace App\Models\Comman\Traits;


trait FormRedirect
{
    public static  function redirectForm($SubmitButtonName='',$edittingId='',$resourceName='',$eventType='')
    {

        switch($SubmitButtonName)
        {
            case 'Save':
                    return redirect()
                            ->route($resourceName.'.index')
                            ->with('success',ucfirst($resourceName).' '.$eventType.' '.'successfully');
                break;

            case 'Save And Edit':
                    return redirect()
                            ->route($resourceName.'.edit', Crypt::encrypt($edittingId))
                            ->with('success',ucfirst($resourceName).' '.$eventType.' '.'successfully And Redirected To Edit'.' '.$resourceName);
                break;

            case 'Save And Add New':
                    return redirect()
                            ->route($resourceName.'.create')
                            ->with('success',ucfirst($resourceName).' '.$eventType.' '."successfully And Redirected To Create");
                break;
        }
    }
}
