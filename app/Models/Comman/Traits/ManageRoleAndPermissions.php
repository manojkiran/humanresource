<?php

namespace App\Models\Comman\Traits;
use App\Models\Role;
use App\Models\Permission;

trait ManageRoleAndPermissions
{
    public function giveRoleById(...$role)
    {
        $roles = $this->getRolesById(array_flatten($role));

        if($roles === null)
        {
            return $this;
        }
        $this->roles()->saveMany($roles);

        return $this; 
    }
    public function getRolesById(array $roles)
    {
        return Role::whereIn('id',$roles)->get();
    }

    public function removeRoleById(...$role)
    {
        $roles = $this->getRolesById(array_flatten($role));

        $this->roles()->detach($roles);

        return $this;
    }
    public function modifyRoleById(...$roles)
    {

        $this->roles()->detach();

        return $this->giveRoleById($roles);

    }

    public function giveRoleByName(...$role)
    {
        $roles = $this->getRolesByName(array_flatten($role));

        if($roles === null)
        {
            return $this;
        }
        $this->roles()->saveMany($roles);

        return $this; 
    }
    public function getRolesByName(array $roles)
    {
        return Role::whereIn('name',$roles)->get();
    }

    public function removeRoleByName(...$role)
    {
        $roles = $this->getRolesByName(array_flatten($role));

        $this->roles()->detach($roles);

        return $this;
    }
    public function modifyRoleByName(...$roles)
    {

        $this->roles()->detach();

        return $this->giveRoleByName($roles);

    }

    public function givePermissionById(...$permission)
    {
        $permissions = $this->getPermissionsById(array_flatten($permission));

        if($permissions === null)
        {
            return $this;
        }
        $this->permissions()->saveMany($permissions);

        return $this; 
    }
    public function getPermissionsById(array $permissions)
    {
        return Permission::whereIn('id',$permissions)->get();
    }

    public function removePermissionById(...$permission)
    {
        $permissions = $this->getPermissionsById(array_flatten($permission));

        $this->permissions()->detach($permissions);

        return $this;
    }
    public function modifyPermissionById(...$permissions)
    {

        $this->permissions()->detach();

        return $this->givePermissionById($permissions);

    }

    

    public function givePermissionByName(...$permission)
    {
        $permissions = $this->getPermissionsByName(array_flatten($permission));

        if($permissions === null)
        {
            return $this;
        }
        $this->permissions()->saveMany($permissions);

        return $this; 
    }
    public function getPermissionsByName(array $permissions)
    {
        return Permission::whereIn('name',$permissions)->get();
    }

    public function removePermissionByName(...$permission)
    {
        $permissions = $this->getPermissionsByName(array_flatten($permission));

        $this->permissions()->detach($permissions);

        return $this;
    }
    public function modifyPermissionByName(...$permissions)
    {

        $this->permissions()->detach();

        return $this->givePermissionByName($permissions);

    }
}
