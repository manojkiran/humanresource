<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use App\Models\Comman\Traits\ManageRoleAndPermissions;

use App\Models\Comman\Traits\DropDown;

use App\Models\Comman\Traits\FormRedirect;



class Permission extends Model
{
    use SoftDeletes;

    use ManageRoleAndPermissions;
    
    use DropDown;
    use FormRedirect;
	/**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'permissions';
   /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'description',
        'module_id',
        'created_at',
        'created_by',
        'updated_at',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'name' => 'string',
        'description' => 'string',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at','created_at','updated_at'];
    
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * Belongs-to-Many relations with Role.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function roles()
    {
        return $this->belongsToMany(Role::class);        
    }
    /**
     * Belongs-to-Many relations with User.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function users()
    {
        return $this->belongsToMany(User::class);
    }
    /**
     * Belongs-to-Many relations with Modules.
     *
     * @return \Illuminate\Database\Eloquent\Relations\belongsToMany
     */
    public function moduleName()
    {
        return $this->belongsTo(Module::class,'module_id');
    }


}
