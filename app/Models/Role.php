<?php

namespace App\Models;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Comman\Traits\ManageRoleAndPermissions;
use App\Models\Comman\Traits\FormRedirect;


class Role extends Model
{

    use SoftDeletes;

    use ManageRoleAndPermissions;

    use FormRedirect;
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'roles';
	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'description',
        'created_at',
        'created_by',
        'updated_at',
    ];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'name' => 'string',
        'description' => 'string',
    ];
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * Many-to-Many relations with Permissions.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function permissions()
    {
        return $this->belongsToMany(Permission::class);
    }
    /**
     * Many-to-Many relations with Users.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function users()
    {
        return $this->belongsToMany(User::class);
    }

    public static function getAllPermissionsOfModule($id)
    {
        $permissions = Permission::where('module_id', '=',$id)->get();
        return $permissions;
    }

    public static function generateCheckBox($permissionValueId='',$permissionValueName='',$permissionDescription='')
    {

        $danger = str_contains($permissionValueName, "delete") ? "text-danger" : '';
        $checkbox='';
        $checkbox.='<div class="checkbox">';
        $checkbox.='<label class="'.$danger.'">';
        $checkbox.='<div class="col-md-12">';
        $checkbox.= \Form::checkbox("permissions[]", $permissionValueId);
        $checkbox.= $permissionDescription;
        $checkbox.='</div>';
        $checkbox.='</label>';
        $checkbox.='</div>';
        return $checkbox;
    }
    public static function generatePermissonGrouping($modules='')
    {
        foreach($modules as $moduleKey => $moduleValue)
        {
            echo $moduleValue->name;

            $eachPermissons = Role::getAllPermissionsOfModule($moduleValue->id);

            foreach ($eachPermissons as $eachPermissonKey => $eachPermissonValue) 
            {
                echo Role::generateCheckBox($eachPermissonValue->id,$eachPermissonValue->name,$eachPermissonValue->description);
                    
            }
        }
    }

    



}
