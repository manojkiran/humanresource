<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

use App\Models\Comman\Traits\ManageRoleAndPermissions;

use App\Models\Comman\Traits\FormRedirect;



class User extends Authenticatable
{
    use Notifiable;
    use SoftDeletes;
    use ManageRoleAndPermissions;
    

    use FormRedirect;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','created_at',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at','created_at','updated_at'];

    /**
     * Many-to-Many relations with Role.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function roles()
    {
        return $this->belongsToMany(Role::class);
    }
    /**
     * Many-to-Many relations with Permission.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function permissions()
    {
        return $this->belongsToMany(Permission::class);
    }


    

    // public function hasRole(...$roles)
    // {

    //     foreach($roles as $role)
    //     {
    //         if($this->roles->contains('name',$role))
    //         {
    //             return true;
    //         }
    //     }
    //     return false;
    // }

   public function hasPermission($permission)
    {
        return $this->hasPermissionThroughRole($permission) || (bool) $this->permissions->where('name',$permission->name)->count();
    }

    public function hasPermissionThroughRole($permission)
    {
        foreach($permission->roles as $role)
        {
            if($this->roles->contains($role))
            {
                return true;
            }
        }
        return false;
    }

    public function hasRoles($roles)
    {
        $roles = is_array($roles) ? $roles : func_get_args();
        foreach ($roles as $role) 
        {
            if ($this->hasRole($role)) 
            {
                return true;
            }
        }
        return false;
    }
    /**
     * Returns if the given user has an specific role.
     *
     * @param string $role
     *
     * @return bool
     */
    public function hasRole($role)
    {
        return $this->roles
            ->where('name', $role)
            ->first() != null;
    }

}
