<?php

namespace App;

use App\HrmsBaseModel;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use App\Models\Comman\Traits\UserStampsTrait;

/**
 * Class HrmsEmployeeExperience
 *
 * @package App
*/

class HrmsEmployeeExperience extends HrmsBaseModel
{
	use SoftDeletes;
    use UserStampsTrait;

    protected $guarded = ['id'];

     /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'hrms_employee_experiences';

    /**
     * The primary key associated with the model.
     *
     * @var integer
     */

    protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'employee_id',
                             'employee_previous_company_name',
                            'employee_previous_designation',
                            'employee_previous_employment_start_date',
                            'employee_previous_employment_end_date',
                            'employee_previous_employment_reason_for_leaving',
                            'employee_previous_employment_contact_person_name',
                            'employee_previous_employment_contact_person_email',
                            'employee_previous_employment_contact_person_phone',
                            'employee_documents_collected',
                            'employee_relieving_letter',
                            'employee_experience_letter',
                            'employee_payslips',
                            'created_by',
                            'updated_by'
                            ];

     /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [ ];

    /**
     * @var array
     */
    protected $dates = ['created_at','updated_at','deleted_at'];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * The storage format of the model's date columns.
     *
     * @var string
     */
    protected $dateFormat ='';

    /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection ='';


}
