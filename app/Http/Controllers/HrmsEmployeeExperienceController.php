<?php

namespace App\Http\Controllers;

use App\HrmsEmployeeExperience;
use App\Http\Requests\HrmsEmployeeExperienceStoreRequest;
use Illuminate\Http\Request;

class HrmsEmployeeExperienceController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    protected $HrmsEmployeeExperience;

    public function __construct(HrmsEmployeeExperience $HrmsEmployeeExperience,Request $request)
    {
        $this->middleware('auth');
        $this->HrmsEmployeeExperience = $HrmsEmployeeExperience;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        $hrmsemployeeexperiences = HrmsEmployeeExperience::latest()->paginate(5);
        return view('HrmsEmployeeExperience.index', compact('hrmsemployeeexperiences'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('HrmsEmployeeExperience.create');
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\HrmsEmployeeExperienceStoreRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(HrmsEmployeeExperienceStoreRequest $request)
    {
        HrmsEmployeeExperience::create($request->all());
        return redirect()->route('HrmsEmployeeExperience.index')->with('success','HrmsEmployeeExperience Created Successfully');
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @param  \App\HrmsEmployeeExperience  $HrmsEmployeeExperience
     * @return \Illuminate\Http\Response
     */
    public function show($id,HrmsEmployeeExperience $HrmsEmployeeExperience)
    {
        $hrmsemployeeexperiences = HrmsEmployeeExperience::findOrFail($id);
        return view('HrmsEmployeeExperience.show',compact('hrmsemployeeexperiences'));
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\HrmsEmployeeExperience  $HrmsEmployeeExperience
     * @return \Illuminate\Http\Response
     */
    public function edit($id,HrmsEmployeeExperience $HrmsEmployeeExperience)
    {
        $hrmsemployeeexperiences = HrmsEmployeeExperience::findOrFail($id);
        return view('HrmsEmployeeExperience.edit',compact('hrmsemployeeexperiences'));
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\HrmsEmployeeExperience  $HrmsEmployeeExperience
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        HrmsEmployeeExperience::find($id)->update(Request::all());
        return redirect()->route('HrmsEmployeeExperience.index')->with('success','HrmsEmployeeExperience Updated Successfully');
    }    
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\HrmsEmployeeExperience  $HrmsEmployeeExperience
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id,HrmsEmployeeExperience $HrmsEmployeeExperience)
    {
        $hrmsemployeeexperiences = HrmsEmployeeExperience::findOrFail($id);
        if (!empty($hrmsemployeeexperiences))
        {

            $hrmsemployeeexperiences->delete();

        }else{

        }
        return redirect()->route();
    }
}
