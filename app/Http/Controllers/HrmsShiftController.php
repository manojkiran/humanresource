<?php

namespace App\Http\Controllers;

use App\HrmsShift;
use App\Http\Requests\HrmsShiftStoreRequest;
use Illuminate\Http\Request;

class HrmsShiftController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    protected $HrmsShift;

    public function __construct(HrmsShift $HrmsShift,Request $request)
    {
        $this->middleware('auth');
        $this->HrmsShift = $HrmsShift;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        $hrmsshifts = HrmsShift::latest()->paginate(5);
        return view('HrmsShift.index', compact('hrmsshifts'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('HrmsShift.create');
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\HrmsShiftStoreRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(HrmsShiftStoreRequest $request)
    {
        HrmsShift::create($request->all());
        return redirect()->route('HrmsShift.index')->with('success','HrmsShift Created Successfully');
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @param  \App\HrmsShift  $HrmsShift
     * @return \Illuminate\Http\Response
     */
    public function show($id,HrmsShift $HrmsShift)
    {
        $hrmsshifts = HrmsShift::findOrFail($id);
        return view('HrmsShift.show',compact('hrmsshifts'));
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\HrmsShift  $HrmsShift
     * @return \Illuminate\Http\Response
     */
    public function edit($id,HrmsShift $HrmsShift)
    {
        $hrmsshifts = HrmsShift::findOrFail($id);
        return view('HrmsShift.edit',compact('hrmsshifts'));
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\HrmsShift  $HrmsShift
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        HrmsShift::find($id)->update(Request::all());
        return redirect()->route('HrmsShift.index')->with('success','HrmsShift Updated Successfully');
    }    
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\HrmsShift  $HrmsShift
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id,HrmsShift $HrmsShift)
    {
        $hrmsshifts = HrmsShift::findOrFail($id);
        if (!empty($hrmsshifts))
        {

            $hrmsshifts->delete();

        }else{

        }
        return redirect()->route();
    }
}
