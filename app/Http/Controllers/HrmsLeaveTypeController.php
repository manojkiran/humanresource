<?php

namespace App\Http\Controllers;

use App\HrmsLeaveType;
use App\Http\Requests\HrmsLeaveTypeStoreRequest;
use Illuminate\Http\Request;

class HrmsLeaveTypeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    protected $HrmsLeaveType;

    public function __construct(HrmsLeaveType $HrmsLeaveType,Request $request)
    {
        $this->middleware('auth');
        $this->HrmsLeaveType = $HrmsLeaveType;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        $hrmsleavetypes = HrmsLeaveType::paginate(5);
        return view('HrmsLeaveType.index', compact('hrmsleavetypes'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('HrmsLeaveType.create');
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\HrmsLeaveTypeStoreRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(HrmsLeaveTypeStoreRequest $request)
    {

        HrmsLeaveType::create($request->all());
        return redirect()->route('leavetypes.index')->with('success','LeaveType Created Successfully');
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @param  \App\HrmsLeaveType  $HrmsLeaveType
     * @return \Illuminate\Http\Response
     */
    public function show($id,HrmsLeaveType $HrmsLeaveType)
    {
        $hrmsleavetypes = HrmsLeaveType::findOrFail($id);
        return view('HrmsLeaveType.show',compact('hrmsleavetypes'));
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\HrmsLeaveType  $HrmsLeaveType
     * @return \Illuminate\Http\Response
     */
    public function edit($id,HrmsLeaveType $HrmsLeaveType)
    {
        $hrmsleavetypes = HrmsLeaveType::findOrFail($id);
        return view('HrmsLeaveType.edit',compact('hrmsleavetypes'));
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\HrmsLeaveType  $HrmsLeaveType
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        HrmsLeaveType::find($id)->update(Request::all());
        return redirect()->route('HrmsLeaveType.index')->with('success','HrmsLeaveType Updated Successfully');
    }    
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\HrmsLeaveType  $HrmsLeaveType
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id,HrmsLeaveType $HrmsLeaveType)
    {
        $hrmsleavetypes = HrmsLeaveType::findOrFail($id);
        if (!empty($hrmsleavetypes))
        {

            $hrmsleavetypes->delete();

        }else{

        }
        return redirect()->route('leavetypes.index');
    }
}
