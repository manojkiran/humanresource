<?php

namespace App\Http\Controllers;

use App\HrmsDesignation;
use App\Http\Requests\HrmsDesignationStoreRequest;
use Illuminate\Http\Request;
use App\HrmsCompanyProfile;

use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;

class HrmsDesignationController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    protected $HrmsDesignation;

    public function __construct(HrmsDesignation $HrmsDesignation,Request $request)
    {
        $this->middleware('auth');
        $this->HrmsDesignation = $HrmsDesignation;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        $hrmsdesignations = HrmsDesignation::latest()->paginate(5);
        return view('HrmsDesignation.index', compact('hrmsdesignations'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $companies = HrmsCompanyProfile::dropDownTwoColumns('hrms_company_profiles', 'company_name', 'company_address_city', null, '--Select Company--');
        $departments = HrmsDesignation::toDropDown('hrms_departments','department_name',null,'--Select Department');
        $viewShare = ['departments','companies'];
        return view('HrmsDesignation.create' ,compact($viewShare));
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\HrmsDesignationStoreRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(HrmsDesignationStoreRequest $request)
    {
        $created_at = now();
        $created_by = Auth::user()->id;
        HrmsDesignation::create(array_merge($request->all(), ['created_at' => $created_at, 'created_by', $created_by]));
        return redirect()->route('designations.index')->with('success','HrmsDesignation Created Successfully');
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @param  \App\HrmsDesignation  $HrmsDesignation
     * @return \Illuminate\Http\Response
     */
    public function show($id,HrmsDesignation $HrmsDesignation)
    {
        $hrmsdesignations = HrmsDesignation::findOrFail($id);
        return view('HrmsDesignation.show',compact('hrmsdesignations'));
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\HrmsDesignation  $HrmsDesignation
     * @return \Illuminate\Http\Response
     */
    public function edit($id,HrmsDesignation $HrmsDesignation)
    {
        $hrmsdesignations = HrmsDesignation::findOrFail($id);
        return view('HrmsDesignation.edit',compact('hrmsdesignations'));
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\HrmsDesignation  $HrmsDesignation
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        HrmsDesignation::find($id)->update(Request::all());
        return redirect()->route('designations.index')->with('success','HrmsDesignation Updated Successfully');
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\HrmsDesignation  $HrmsDesignation
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id,HrmsDesignation $HrmsDesignation)
    {
        $hrmsdesignations = HrmsDesignation::findOrFail($id);
        if (!empty($hrmsdesignations))
        {

            $hrmsdesignations->delete();

        }else{

        }
        return redirect()->route('designations.index');
    }
}
