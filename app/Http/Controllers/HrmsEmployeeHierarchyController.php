<?php

namespace App\Http\Controllers;

use App\HrmsEmployeeHierarchy;
use App\HrmsDepartment;
use App\Http\Requests\HrmsEmployeeHierarchyStoreRequest;
use Illuminate\Http\Request;
use App\HrmsSalary;
use App\HrmsDesignation;

class HrmsEmployeeHierarchyController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    protected $HrmsEmployeeHierarchy;

    public function __construct(HrmsEmployeeHierarchy $HrmsEmployeeHierarchy,Request $request)
    {
        $this->middleware('auth');
        $this->HrmsEmployeeHierarchy = $HrmsEmployeeHierarchy;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        // $nameTemplate="prefix_";

        // $tableNames = \Schema::getConnection()->getDoctrineSchemaManager()->listTableNames();
        // // $seach = array_search("hrms",$tableNames);

        // foreach ($tableNames as $tableNameValue) 
        // {
        //     if (strpos($tableNameValue, $nameTemplate) !== false) 
        //     {
        //         dd('table found in serach criteria ');
        //     }
        //     else
        //     {
        //         dd('table not found in serach criteria ');
        //     }
            
        // }

        // dd($seach);
        $hrmsemployeehierarchies = HrmsEmployeeHierarchy::latest()->paginate(5);
        return view('HrmsEmployeeHierarchy.index', compact('hrmsemployeehierarchies'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $departments = HrmsDepartment::toDropDown('hrms_departments','department_name',null,'--Choose Department-');
        $employees = HrmsSalary::dropDownThreeColumns('hrms_employees','employee_first_name','employee_middle_name','employee_last_name',null,'Choose  Employee');
        $designations = HrmsDesignation::toDropDown('hrms_designations','designation_name',null,'--Choose Designation');
        
        $viewShare = ['departments','employees','designations'];

        return view('HrmsEmployeeHierarchy.create' , compact($viewShare));
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\HrmsEmployeeHierarchyStoreRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(HrmsEmployeeHierarchyStoreRequest $request)
    {

        HrmsEmployeeHierarchy::create($request->all());
        return redirect()->route('HrmsEmployeeHierarchy.index')->with('success','HrmsEmployeeHierarchy Created Successfully');
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @param  \App\HrmsEmployeeHierarchy  $HrmsEmployeeHierarchy
     * @return \Illuminate\Http\Response
     */
    public function show($id,HrmsEmployeeHierarchy $HrmsEmployeeHierarchy)
    {
        $hrmsemployeehierarchies = HrmsEmployeeHierarchy::findOrFail($id);
        return view('HrmsEmployeeHierarchy.show',compact('hrmsemployeehierarchies'));
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\HrmsEmployeeHierarchy  $HrmsEmployeeHierarchy
     * @return \Illuminate\Http\Response
     */
    public function edit($id,HrmsEmployeeHierarchy $HrmsEmployeeHierarchy)
    {
        $hrmsemployeehierarchies = HrmsEmployeeHierarchy::findOrFail($id);
        return view('HrmsEmployeeHierarchy.edit',compact('hrmsemployeehierarchies'));
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\HrmsEmployeeHierarchy  $HrmsEmployeeHierarchy
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        HrmsEmployeeHierarchy::find($id)->update(Request::all());
        return redirect()->route('HrmsEmployeeHierarchy.index')->with('success','HrmsEmployeeHierarchy Updated Successfully');
    }    
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\HrmsEmployeeHierarchy  $HrmsEmployeeHierarchy
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id,HrmsEmployeeHierarchy $HrmsEmployeeHierarchy)
    {
        $hrmsemployeehierarchies = HrmsEmployeeHierarchy::findOrFail($id);
        if (!empty($hrmsemployeehierarchies))
        {

            $hrmsemployeehierarchies->delete();

        }else{

        }
        return redirect()->route();
    }
}
