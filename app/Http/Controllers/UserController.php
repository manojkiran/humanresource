<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Role;
use App\Models\Permission;
use App\Models\Module;

use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    protected $user;
    public function __construct(User $user,Request $request)
    {
        $this->middleware('auth');
        $this->user = $user;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {        
        if(Gate::allows('view.users'))
        {
            $result = User::where('email', '!=',config('Roles.dafaultSeeds.users.email.superAdmin'))->latest()->paginate(2); 
            return view('backend.users.index', compact('result'));
        }else
        {
            abort(401);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(Gate::allows('create.users'))
        {

            $modules = DB::table('modules')->get();
            $roles = Role::where('name', '!=','superadmin')->get();
            $modelName = get_class($this->user);

            
            return view('backend.users.new',compact('roles','permissions','modules'));
        }else{
            abort(401);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         $this->validate($request, [
            'name' => 'bail|required|max:80',
            'email' => 'required|string|email|max:255|unique:users',
            'roles' => 'bail|required',
            'password'=>'required',
        ]);

        $created_at  = now();
        $password = $this->hashPassword($request->password);

        $userStore  = User::create(array_merge($request->all(), ['created_at' => $created_at,'password' => $password]));

        if ($request->has('roles'))
        {
            $user = User::find($userStore->id);
            $user->giveRoleById($request->roles);
        }
        if ($request->has('permissions'))
        {
            $user = User::find($userStore->id);
            $user->givePermissionById($request->permissions);
        }

        $redirectUrl = User::redirectForm($request->submitbutton,$userStore->id,'users','Created');

        return $redirectUrl;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (Gate::allows('edit.users'))
        {
            $modules = DB::table('modules')->get();
            $users = User::findOrFail($this->decryptId($id));
            $roles = DB::table('roles')->where('name', '!=','superadmin')->get();
            return view('backend.users.edit',compact('users','permissions','roles','modules'));
        }
        else
        {
            abort(401);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       
        $this->validate($request, [
            'name' => 'bail|required|min:2',
            'email' => 'required|email|unique:users,email,' . $id,
            'roles' => 'required|min:1'
        ]);

        // Get the user
        $user = User::findOrFail($id);

        // Update user
        

        // check for password change
        if($request->get('password')) 
        {
            $user->password = bcrypt($request->get('password'));
        }


      
        if ($request->has('roles')) 
        {
            
            $user->modifyRoleById($request->roles);
        }
        elseif(!$request->has('roles')) 
        {
            $user->roles()->sync(null);
        }

        if($request->has('permissions')) 
        {
            
            $user->modifyPermissionById($request->permissions);
        }elseif (!$request->has('permissions')) 
        {
            $user->permissions()->sync(null);
        }

        $redirectUrl = Permission::redirectForm($request->submitbutton,$this->getId($user),'users','Updated');

        return $redirectUrl;

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        
        $id = $this->decryptId($id);
        if (authUserId() == $id || $this->getSuperadminRoleId() == $id)  
        {
            return redirect()->route('users.index')->with('error','You Cannot delete Your Account');
        }elseif (condition) 
        {
            User::findOrFail($id)->delete();
            return redirect()->back()->with('success','User Deleted Successfully!');
        }
    }

    private function hashPassword($normalText='')
    {
        $encryptedPassword  = bcrypt($normalText);
        return $encryptedPassword;
    }

    private function authUserId()
    {
        return Auth::user()->id;
    }

    private function decryptId($encryptedID)
    {
        $decryptedId = Crypt::decrypt($encryptedID);
        return $decryptedId;
    }
    private function getId($array = [])
    {
        $id ="";
        $id = $array->id;
        if (!empty($id)) 
        {
            return $id;
        }else
        {
            return "No Id Found in the Given Array";
        }
    }

    private function getSuperadminRoleId()
    {
        $superadmin = DB::table('roles')->where('name','SuperAdmin')->first();
        return $this->getId($superadmin);
    }
}
