<?php

namespace App\Http\Controllers;

use App\HrmsApplyLeave;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\HrmsApplyLeaveStoreRequest;
use Illuminate\Http\Request;

class HrmsApplyLeaveController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    protected $HrmsApplyLeave;

    public function __construct(HrmsApplyLeave $HrmsApplyLeave,Request $request)
    {
        $this->middleware('auth');
        $this->HrmsApplyLeave = $HrmsApplyLeave;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        $hrmsapplyleaves = HrmsApplyLeave::latest()->paginate(5);
        return view('HrmsApplyLeave.index', compact('hrmsapplyleaves'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $leaveTypes  =HrmsApplyLeave::dropDownTwoColumns('hrms_leave_types','leave_type','leave_no_of_days',null,'-Select Your Leave Type-');
        $leaveLength = ['1' => 'Forenoon','2' =>'Afternoon'];
        $viewShare =['leaveTypes','leaveLength'];
        return view('HrmsApplyLeave.create' , compact($viewShare));
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\HrmsApplyLeaveStoreRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(HrmsApplyLeaveStoreRequest $request)
    {
        
        
        $userId = Auth::user()->id;
        HrmsApplyLeave::create(array_merge($request->all(),['user_id'=>$userId]) );
        return redirect()->route('applyleaves.index')->with('success','HrmsApplyLeave Created Successfully');
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @param  \App\HrmsApplyLeave  $HrmsApplyLeave
     * @return \Illuminate\Http\Response
     */
    public function show($id,HrmsApplyLeave $HrmsApplyLeave)
    {
        $hrmsapplyleaves = HrmsApplyLeave::findOrFail($id);
        return view('HrmsApplyLeave.show',compact('hrmsapplyleaves'));
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\HrmsApplyLeave  $HrmsApplyLeave
     * @return \Illuminate\Http\Response
     */
    public function edit($id,HrmsApplyLeave $HrmsApplyLeave)
    {
        $hrmsapplyleaves = HrmsApplyLeave::findOrFail($id);
        return view('HrmsApplyLeave.edit',compact('hrmsapplyleaves'));
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\HrmsApplyLeave  $HrmsApplyLeave
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        HrmsApplyLeave::find($id)->update(Request::all());
        return redirect()->route('HrmsApplyLeave.index')->with('success','HrmsApplyLeave Updated Successfully');
    }    
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\HrmsApplyLeave  $HrmsApplyLeave
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id,HrmsApplyLeave $HrmsApplyLeave)
    {
        $hrmsapplyleaves = HrmsApplyLeave::findOrFail($id);
        if (!empty($hrmsapplyleaves))
        {

            $hrmsapplyleaves->delete();

        }else{

        }
        return redirect()->route();
    }
}
