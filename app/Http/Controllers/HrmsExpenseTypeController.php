<?php

namespace App\Http\Controllers;

use App\HrmsExpenseType;
use App\Http\Requests\HrmsExpenseTypeStoreRequest;
use Illuminate\Http\Request;

class HrmsExpenseTypeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    protected $HrmsExpenseType;

    public function __construct(HrmsExpenseType $HrmsExpenseType,Request $request)
    {
        $this->middleware('auth');
        $this->HrmsExpenseType = $HrmsExpenseType;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        $hrmsexpensetypes = HrmsExpenseType::latest()->paginate(5);
        return view('HrmsExpenseType.index', compact('hrmsexpensetypes'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('HrmsExpenseType.create');
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\HrmsExpenseTypeStoreRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(HrmsExpenseTypeStoreRequest $request)
    {
        HrmsExpenseType::create($request->all());
        return redirect()->route('HrmsExpenseType.index')->with('success','HrmsExpenseType Created Successfully');
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @param  \App\HrmsExpenseType  $HrmsExpenseType
     * @return \Illuminate\Http\Response
     */
    public function show($id,HrmsExpenseType $HrmsExpenseType)
    {
        $hrmsexpensetypes = HrmsExpenseType::findOrFail($id);
        return view('HrmsExpenseType.show',compact('hrmsexpensetypes'));
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\HrmsExpenseType  $HrmsExpenseType
     * @return \Illuminate\Http\Response
     */
    public function edit($id,HrmsExpenseType $HrmsExpenseType)
    {
        $hrmsexpensetypes = HrmsExpenseType::findOrFail($id);
        return view('HrmsExpenseType.edit',compact('hrmsexpensetypes'));
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\HrmsExpenseType  $HrmsExpenseType
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        HrmsExpenseType::find($id)->update(Request::all());
        return redirect()->route('HrmsExpenseType.index')->with('success','HrmsExpenseType Updated Successfully');
    }    
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\HrmsExpenseType  $HrmsExpenseType
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id,HrmsExpenseType $HrmsExpenseType)
    {
        $hrmsexpensetypes = HrmsExpenseType::findOrFail($id);
        if (!empty($hrmsexpensetypes))
        {

            $hrmsexpensetypes->delete();

        }else{

        }
        return redirect()->route();
    }
}
