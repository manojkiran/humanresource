<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\HrmsSalary;
use App\HrmsEmployeeGrade;
use App\Http\Requests\HrmsSalaryStoreRequest;
use Illuminate\Http\Request;

class HrmsSalaryController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    protected $HrmsSalary;

    public function __construct(HrmsSalary $HrmsSalary,Request $request)
    {
        $this->middleware('auth');
        $this->HrmsSalary = $HrmsSalary;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        $hrmssalaries = HrmsSalary::latest()->paginate(5);
        return view('HrmsSalary.index', compact('hrmssalaries'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $employees = HrmsSalary::dropDownThreeColumns('hrms_employees','employee_first_name','employee_middle_name','employee_last_name',null,'Choose  Employee');
        $employeeGrade  = HrmsEmployeeGrade::toDropDown('hrms_employee_grades','employee_grade_name',null,'--Select Employee Grade');
        
        $viewShare = ['employees','employeeGrade'];
        return view('HrmsSalary.create' , compact($viewShare));
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\HrmsSalaryStoreRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(HrmsSalaryStoreRequest $request)
    {
        

        HrmsSalary::create($request->all());
        return redirect()->route('salaries.index')->with('success','HrmsSalary Created Successfully');
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @param  \App\HrmsSalary  $HrmsSalary
     * @return \Illuminate\Http\Response
     */
    public function show($id,HrmsSalary $HrmsSalary)
    {
        $hrmssalaries = HrmsSalary::findOrFail($id);
        return view('HrmsSalary.show',compact('hrmssalaries'));
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\HrmsSalary  $HrmsSalary
     * @return \Illuminate\Http\Response
     */
    public function edit($id,HrmsSalary $HrmsSalary)
    {
        $hrmssalaries = HrmsSalary::findOrFail($id);
        return view('HrmsSalary.edit',compact('hrmssalaries'));
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\HrmsSalary  $HrmsSalary
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        HrmsSalary::find($id)->update(Request::all());
        return redirect()->route('HrmsSalary.index')->with('success','HrmsSalary Updated Successfully');
    }    
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\HrmsSalary  $HrmsSalary
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id,HrmsSalary $HrmsSalary)
    {
        $hrmssalaries = HrmsSalary::findOrFail($id);
        if (!empty($hrmssalaries))
        {

            $hrmssalaries->delete();

        }else{

        }
        return redirect()->route();
    }
    public function calculateSalary(Request $request)
    {

        $employees = HrmsSalary::dropDownThreeColumns('hrms_employees','employee_first_name','employee_middle_name','employee_last_name',null,'Choose  Employee');
        $employeeGrade  = HrmsEmployeeGrade::toDropDown('hrms_employee_grades','employee_grade_name',null,'--Select Employee Grade');


        $selctedGrade = $request->employee_grade;

        $selctedEmployee = $request->employee_id;

        $enterdNetSalary  = $request->employee_netsalary;


        

        
        
        $medicalReimbursement = $this->getValuesOfTable('employee_grade_salary_medical_reimbursement',$selctedGrade);

        $mobilePhoneAllowance = $this->getValuesOfTable('employee_grade_salary_mobile_phone_allowance',$selctedGrade);

        $fieldAllowance = $this->getValuesOfTable('employee_grade_salary_field_allowance',$selctedGrade);

        $monthlyReimbursement = $this->getValuesOfTable('employee_grade_salary_monthly_reimbursement',$selctedGrade);

        $conveyanceAllowance = $this->generateEmployeeSalary($enterdNetSalary,'conveyanceAllowance');

        $basicPay =$this->generateEmployeeSalary($enterdNetSalary,'basicPay');

        $hra = $this->generateEmployeeSalary($enterdNetSalary,'hra');

        $cca = $this->generateEmployeeSalary($enterdNetSalary,'cca');

        $providentFund = $this->generateEmployeeSalary($enterdNetSalary,'pf');

        $esi = $this->generateEmployeeSalary($enterdNetSalary,'esi');

        $professionalTax = $this->generateEmployeeSalary($enterdNetSalary , 'professional_tax');

        $earningsTotal = $this->addArrayNumbers([$basicPay,$hra,$cca,$medicalReimbursement,$mobilePhoneAllowance,$fieldAllowance,$monthlyReimbursement,$conveyanceAllowance]);

        $deductionsTotal = $this->addArrayNumbers([$providentFund,$esi,$professionalTax]);

        $netSalary = $this->subtractArrayNumbers([$earningsTotal,$deductionsTotal]);


        

        

        $viewShare = [
                        'medicalReimbursement','mobilePhoneAllowance','fieldAllowance','monthlyReimbursement',
                        'employees','selctedEmployee','employeeGrade','selctedGrade','enterdNetSalary','basicPay','hra','cca','providentFund',
                        'esi','professionalTax','earningsTotal','deductionsTotal','netSalary','conveyanceAllowance',
                    ];
        return view('HrmsSalary.generated' , compact($viewShare));


        
        
    }
    private function getValuesOfTable($fieldName='',$selctedGrade='')
    {
        $tableQuery = DB::table('hrms_employee_salary_grades')
                        ->select($fieldName)
                        ->where('employee_grade_id','=', $selctedGrade)
                        ->get();
                          

                        if ($tableQuery->isEmpty()) 
                        {
                            return 0;
                        }
                        else
                        {

                            foreach ($tableQuery as $tableQueryKey => $tableQueryValue) 
                                {
                                 
                                }
                                return $tableQueryValue->$fieldName;

                        }
        


        
        
    }

    private  function generateEmployeeSalary($grossSalary='',$returnValue='')
                            {

                                
                               
                                
                                $basicPayConstant = 50/100;

                                

                                $basciPayResult  = $grossSalary * $basicPayConstant;

                                $hRAConstant = 50/100;


                                $hRAResult  = $basciPayResult * $hRAConstant;

                                $cCAConstant = 10/100;

                                $cCAResult  = $basciPayResult * $cCAConstant;


                                $conveyanceAllowanceConstant = 800;

                                $conveyanceAllowanceResult = $conveyanceAllowanceConstant;

                                $providentFundConstant = 12/100;

                                $providentFundResult = $basciPayResult * $providentFundConstant;



                                $esiConstantSALBEL_21000 = 1.75 / 100;

                                if ($grossSalary < 21000) 
                                {
                                    $esiResult = $grossSalary * $esiConstantSALBEL_21000;
                                    
                                }else
                                {
                                    $esiResult = 0;
                                }


                                

                                $professionalTaxConstantSALBEL_5000 = 16.50;

                                $professionalTaxConstantSALBEL_5000_7500 = 30;

                                $professionalTaxConstantSALBEL_7501_10000 = 85;

                                $professionalTaxConstantSALBEL_10001_12500 = 126.50;

                                $professionalTaxConstantSALBEL_12501 = 182.50;


                                if ($grossSalary <= 5000) 
                                {
                                    $professionalTaxResult = $professionalTaxConstantSALBEL_5000;
                                }

                                if ($grossSalary > 5000 && $grossSalary <= 7500) 
                                {
                                    $professionalTaxResult = $professionalTaxConstantSALBEL_5000_7500;
                                }

                                if ($grossSalary >= 7501 && $grossSalary <= 10000) 
                                {
                                    $professionalTaxResult = $professionalTaxConstantSALBEL_7501_10000;
                                }

                                if ($grossSalary >= 10001 && $grossSalary <= 12500) 
                                {
                                    $professionalTaxResult = $professionalTaxConstantSALBEL_10001_12500;
                                }

                                if ($grossSalary >= 12501) 
                                {
                                    $professionalTaxResult = $professionalTaxConstantSALBEL_12501;
                                }










                                


                                $finalResultArray = 
                                                [
                                                    'basicPay' => $basciPayResult,
                                                    'hra' => $hRAResult,
                                                    'cca' => $cCAResult,
                                                    'conveyanceAllowance'=> $conveyanceAllowanceResult,
                                                    'pf' => $providentFundResult,
                                                    'esi' => $esiResult,
                                                    'professional_tax' =>$professionalTaxResult,
                                                ];

                                $resultObj = (object)$finalResultArray;
                                
                                

                                return $resultObj->$returnValue;
                                }

                              private  function addArrayNumbers($numbersArray='')
                                {
                                    $errorMessage = "Minimum Two Numbers is required";
                                    $isAddable = count($numbersArray);
                                    if ($isAddable < 2 ) 
                                    {
                                       return $errorMessage;
                                    }else
                                    {
                                        $result = 0;
                                       foreach ($numbersArray as $numbersArray)
                                        {
                                            $result += $numbersArray;
                                        }
                                        return $result;
                                    }
                                }

                              private  function subtractArrayNumbers($numbersArray='')
                                {
                                    
                                    //defining the error messages
                                    $errorMessage = "Minimum Two Numbers is required";
                                    //check if atleast two numbers in array
                                    $isMultiplicable = count($numbersArray);
                                    if ($isMultiplicable < 2 ) 
                                    {
                                        //if fails return the errormessage
                                       return $errorMessage;
                                    }
                                    else
                                    {
                                        //initializing the result as one for the first time
                                        $result = reset($numbersArray);
                                        
                                       foreach ($numbersArray as $numbersArray)
                                        {
                                            if ($numbersArray != $result) 
                                            {
                                                $result -= $numbersArray;
                                            }

                                            //on the each multiple an store it in the result varialbe
                                            //$result -= $numbersArray;
                                        }
                                        //return the result
                                        return $result;
                                    }
                                }
                            
}
