<?php

namespace App\Http\Controllers;

use App\HrmsApplyPermission;
use App\Http\Requests\HrmsApplyPermissionStoreRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
class HrmsApplyPermissionController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    protected $HrmsApplyPermission;

    public function __construct(HrmsApplyPermission $HrmsApplyPermission,Request $request)
    {
        $this->middleware('auth');
        $this->HrmsApplyPermission = $HrmsApplyPermission;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        // $t  = '4:15 PM' 
        // dd(date('h:i A'));
        $hrmsapplypermissions = HrmsApplyPermission::latest()->paginate(5);
        return view('HrmsApplyPermission.index', compact('hrmsapplypermissions'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('HrmsApplyPermission.create');
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\HrmsApplyPermissionStoreRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(HrmsApplyPermissionStoreRequest $request)
    {
        HrmsApplyPermission::create(array_merge($request->all(),['user_id' => Auth::user()->id,]));
        return redirect()->route('applypermissions.index')->with('success','HrmsApplyPermission Created Successfully');
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @param  \App\HrmsApplyPermission  $HrmsApplyPermission
     * @return \Illuminate\Http\Response
     */
    public function show($id,HrmsApplyPermission $HrmsApplyPermission)
    {
        $hrmsapplypermissions = HrmsApplyPermission::findOrFail($id);
        return view('HrmsApplyPermission.show',compact('hrmsapplypermissions'));
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\HrmsApplyPermission  $HrmsApplyPermission
     * @return \Illuminate\Http\Response
     */
    public function edit($id,HrmsApplyPermission $HrmsApplyPermission)
    {
        $hrmsapplypermissions = HrmsApplyPermission::findOrFail($id);
        return view('HrmsApplyPermission.edit',compact('hrmsapplypermissions'));
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\HrmsApplyPermission  $HrmsApplyPermission
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        HrmsApplyPermission::find($id)->update(Request::all());
        return redirect()->route('applypermissions.index')->with('success','HrmsApplyPermission Updated Successfully');
    }    
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\HrmsApplyPermission  $HrmsApplyPermission
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id,HrmsApplyPermission $HrmsApplyPermission)
    {
        $hrmsapplypermissions = HrmsApplyPermission::findOrFail($id);
        if (!empty($hrmsapplypermissions))
        {

            $hrmsapplypermissions->delete();

        }else{

        }
        return redirect()->route('applypermissions.index');
    }
}
