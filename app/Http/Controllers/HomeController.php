<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Role;
use App\Models\Permission;
use App\HrmsLeaveType;
use App\HrmsBaseModel;

class HomeController extends Controller
{
    public function changeStatus($id,Request $request)
    {
        
        $modelName = $request->ModelName;
        $fieldName = $request->DbFieldName;
        $toChangeStatus = $request->DbStatus;
        $updateFiledId = $request->updateFiledId;
       
        $leaveType= $modelName::find($id);
        $leaveType->leave_type_status='InActive';
        $leaveType->save();
        return redirect()->route('leavetypes.index')->with('success','HrmsDepartment Created Successfully');

    }
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }
    public function test()
    {
        dd('hai');
    }
    public function give()
    {
        //    $test= ['create.users','delete.users','edit.roles'];
        // $role = Role::find(3);
        // $role->modifyPermissionByName($test);


        $user = User::find(3);
        $user->modifyRoleById('2', '3');

        $permission = Permission::find(17);
        $permission->modifyRoleById(2,3);

        //$user->modifyRoleByName('Admin');

//        $user->modifyPermissionByName('view.permissions');
        //$user->modifyPermissionById('10');
    }
}

//2  4  7