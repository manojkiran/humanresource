<?php

namespace App\Http\Controllers;

use App\HrmsEmployeeSalaryGrade;
use App\Http\Requests\HrmsEmployeeSalaryGradeStoreRequest;
use Illuminate\Http\Request;

class HrmsEmployeeSalaryGradeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    protected $HrmsEmployeeSalaryGrade;

    public function __construct(HrmsEmployeeSalaryGrade $HrmsEmployeeSalaryGrade,Request $request)
    {
        $this->middleware('auth');
        $this->HrmsEmployeeSalaryGrade = $HrmsEmployeeSalaryGrade;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        $hrmsemployeesalarygrades = HrmsEmployeeSalaryGrade::latest()->paginate(20);
        return view('HrmsEmployeeSalaryGrade.index', compact('hrmsemployeesalarygrades'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $grades = HrmsEmployeeSalaryGrade::toDropDown('hrms_employee_grades','employee_grade_name',null,'--Select Grade--');

        $viewShare  = ['grades'];
        return view('HrmsEmployeeSalaryGrade.create' ,compact($viewShare));
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\HrmsEmployeeSalaryGradeStoreRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(HrmsEmployeeSalaryGradeStoreRequest $request)
    {
        
        HrmsEmployeeSalaryGrade::create($request->all());
        return redirect()->route('salarygradesettings.index')->with('success','HrmsEmployeeSalaryGrade Created Successfully');
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @param  \App\HrmsEmployeeSalaryGrade  $HrmsEmployeeSalaryGrade
     * @return \Illuminate\Http\Response
     */
    public function show($id,HrmsEmployeeSalaryGrade $HrmsEmployeeSalaryGrade)
    {
        $hrmsemployeesalarygrades = HrmsEmployeeSalaryGrade::findOrFail($id);
        return view('HrmsEmployeeSalaryGrade.show',compact('hrmsemployeesalarygrades'));
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\HrmsEmployeeSalaryGrade  $HrmsEmployeeSalaryGrade
     * @return \Illuminate\Http\Response
     */
    public function edit($id,HrmsEmployeeSalaryGrade $HrmsEmployeeSalaryGrade)
    {
        $hrmsemployeesalarygrades = HrmsEmployeeSalaryGrade::findOrFail($id);
        return view('HrmsEmployeeSalaryGrade.edit',compact('hrmsemployeesalarygrades'));
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\HrmsEmployeeSalaryGrade  $HrmsEmployeeSalaryGrade
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        HrmsEmployeeSalaryGrade::find($id)->update(Request::all());
        return redirect()->route('HrmsEmployeeSalaryGrade.index')->with('success','HrmsEmployeeSalaryGrade Updated Successfully');
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\HrmsEmployeeSalaryGrade  $HrmsEmployeeSalaryGrade
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id,HrmsEmployeeSalaryGrade $HrmsEmployeeSalaryGrade)
    {
        $hrmsemployeesalarygrades = HrmsEmployeeSalaryGrade::findOrFail($id);
        if (!empty($hrmsemployeesalarygrades))
        {

            $hrmsemployeesalarygrades->delete();

        }else{

        }
        return redirect()->route();
    }
}
