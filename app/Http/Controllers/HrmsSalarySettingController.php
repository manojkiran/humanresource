<?php

namespace App\Http\Controllers;

use App\HrmsSalarySetting;
use App\Http\Requests\HrmsSalarySettingStoreRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HrmsSalarySettingController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    protected $HrmsSalarySetting;

    public function __construct(HrmsSalarySetting $HrmsSalarySetting,Request $request)
    {
        $this->middleware('auth');
        $this->HrmsSalarySetting = $HrmsSalarySetting;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        $hrmssalarysettings = HrmsSalarySetting::latest()->paginate(5);
        return view('HrmsSalarySetting.index', compact('hrmssalarysettings'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $componentType =  ['1'=>'Earning','2'=>'Deduction'];
        $addTo  = ['1'=>'Total Payable','2' => 'Cost to Company'];
        $valueType = ['1'=> 'Amount', '2'=>'Percentage'];
        $viewShare = ['componentType','addTo','valueType'];
        return view('HrmsSalarySetting.create',compact($viewShare));
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\HrmsSalarySettingStoreRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        

        $created_at = now();
        $created_by = Auth::user()->id;
         HrmsSalarySetting::create(array_merge($request->all(), [
                                    'created_at' => $created_at,
                                    'created_by' => $created_by,
                                ]));

        return redirect()->route('salarysettings.index')->with('success','Salarysettings Created Successfully');
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @param  \App\HrmsSalarySetting  $HrmsSalarySetting
     * @return \Illuminate\Http\Response
     */
    public function show($id,HrmsSalarySetting $HrmsSalarySetting)
    {
        $hrmssalarysettings = HrmsSalarySetting::findOrFail($id);
        return view('HrmsSalarySetting.show',compact('hrmssalarysettings'));
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\HrmsSalarySetting  $HrmsSalarySetting
     * @return \Illuminate\Http\Response
     */
    public function edit($id,HrmsSalarySetting $HrmsSalarySetting)
    {
        $hrmssalarysettings = HrmsSalarySetting::findOrFail($id);
        $componentType =  ['1'=>'Earning','2'=>'Deduction'];
        $addTo  = ['1'=>'Total Payable','2' => 'Cost to Company'];
        $valueType = ['1'=> 'Amount', '2'=>'Percentage'];
        $viewShare = ['componentType','addTo','valueType','hrmssalarysettings'];

        return view('HrmsSalarySetting.edit',compact($viewShare));
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\HrmsSalarySetting  $HrmsSalarySetting
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        HrmsSalarySetting::find($id)->update(Request::all());
        return redirect()->route('HrmsSalarySetting.index')->with('success','HrmsSalarySetting Updated Successfully');
    }    
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\HrmsSalarySetting  $HrmsSalarySetting
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id,HrmsSalarySetting $HrmsSalarySetting)
    {
        $hrmssalarysettings = HrmsSalarySetting::findOrFail($id);
        if (!empty($hrmssalarysettings))
        {

            $hrmssalarysettings->delete();

        }else{

        }
        return redirect()->route();
    }
}
