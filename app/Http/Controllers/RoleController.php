<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Role;
use App\Models\Permission;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;


class RoleController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    protected $role;
    public function __construct(Role $role)
    {
        $this->middleware('auth');
        $this->role = $role;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Gate::allows('view.roles'))
        {
            $result = Role::where('name', '!=','SuperAdmin')->latest()->paginate();
            return view('backend.roles.index', compact('result'));
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(Gate::allows('create.roles')){
            $modules = DB::table('modules')->get();
            $permissions = DB::table('permissions')->get();
            return view('backend.roles.new',compact('permissions','modules'));
        }
        else
        {
            abort(401);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'bail|required|unique:roles|max:15',
        ]);
        $created_at  = now();$created_by  = Auth::user()->id;
        $roleStore  = Role::create(array_merge($request->all(), ['created_at' => $created_at,'created_by' => $created_by]));    
        if ($request->has('permissions')) 
        {
            $role = Role::find($roleStore->id);
            $role->givePermissionById($request->permissions);
        }
        $redirectUrl = Permission::redirectForm($request->submitbutton,$roleStore->id,'roles','Created');
        return $redirectUrl;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function show(Role $role)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if(Gate::allows('edit.roles'))
        {
            $id = $this->decryptId($id);
            $permissions = DB::table('permissions')->get();
            $modules = DB::table('modules')->get();
            $roles = Role::findOrFail($id);
            return view('backend.roles.edit',compact('roles','permissions','modules'));

            return view('backend.roles.edit',compact('roles'));
        }else{
            abort(401);
        }
        

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {   
        $role = Role::findOrFail($id);

        $roleID = $this->getId($role);


        $this->validate($request, [
            'name' => 'required|min:3|max:30|unique:roles,name,'.$id.',id',
        ]);
        $roleUpdate  = $role->update($request->all());

        if ($request->has('permissions')) 
        {
            $role->modifyPermissionById($request->permissions);
        }
        elseif(!$request->has('permissions')) 
        {
            $role->permissions()->sync(null);
        }
        $redirectUrl = Permission::redirectForm($request->submitbutton,$roleID,'roles','Updated');
        return $redirectUrl;
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $id = $this->decryptId($id);

        $role = Role::findOrFail($id);
        if ($role->users->isEmpty())
         {
            Role::findOrFail($id)->delete();
            return redirect()->back()->with('success','Role Deleted Successfully!');
        }
        elseif ($role->users->isNotEmpty()) 
        {
            return redirect()->back()->with('error','Some Roles Are Associated with this User!');
        }
    }

    private function decryptId($encryptedID)
    {
        $decryptedId = Crypt::decrypt($encryptedID);
        return $decryptedId;
    }
    private function authUserId()
    {
        return Auth::user()->id;
    }

    private function getId($array = [])
    {
        $id ="";
        $id = $array->id;
        if (!empty($id)) 
        {
            return $id;
        }else
        {
            return "No Id Found in the Given Array";
        }
    }
}
