<?php

namespace App\Http\Controllers;

use App\HrmsCountry;
use App\Http\Requests\HrmsCountryStoreRequest;
use Illuminate\Http\Request;

class HrmsCountryController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    protected $HrmsCountry;

    public function __construct(HrmsCountry $HrmsCountry,Request $request)
    {
        $this->middleware('auth');
        $this->HrmsCountry = $HrmsCountry;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        $hrmscountries = HrmsCountry::latest()->paginate(5);
        return view('HrmsCountry.index', compact('hrmscountries'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('HrmsCountry.create');
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\HrmsCountryStoreRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(HrmsCountryStoreRequest $request)
    {
        HrmsCountry::create($request->all());
        return redirect()->route('HrmsCountry.index')->with('success','HrmsCountry Created Successfully');
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @param  \App\HrmsCountry  $HrmsCountry
     * @return \Illuminate\Http\Response
     */
    public function show($id,HrmsCountry $HrmsCountry)
    {
        $hrmscountries = HrmsCountry::findOrFail($id);
        return view('HrmsCountry.show',compact('hrmscountries'));
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\HrmsCountry  $HrmsCountry
     * @return \Illuminate\Http\Response
     */
    public function edit($id,HrmsCountry $HrmsCountry)
    {
        $hrmscountries = HrmsCountry::findOrFail($id);
        return view('HrmsCountry.edit',compact('hrmscountries'));
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\HrmsCountry  $HrmsCountry
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        HrmsCountry::find($id)->update(Request::all());
        return redirect()->route('HrmsCountry.index')->with('success','HrmsCountry Updated Successfully');
    }    
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\HrmsCountry  $HrmsCountry
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id,HrmsCountry $HrmsCountry)
    {
        $hrmscountries = HrmsCountry::findOrFail($id);
        if (!empty($hrmscountries))
        {

            $hrmscountries->delete();

        }else{

        }
        return redirect()->route();
    }
}
