<?php

namespace App\Http\Controllers;

use App\HrmsCompanyProfile;
use App\Http\Requests\HrmsCompanyProfileStoreRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HrmsCompanyProfileController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    protected $HrmsCompanyProfile;

    public function __construct(HrmsCompanyProfile $HrmsCompanyProfile,Request $request)
    {
        $this->middleware('auth');
        $this->HrmsCompanyProfile = $HrmsCompanyProfile;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        $hrmscompanyprofiles = HrmsCompanyProfile::latest()->paginate(5);
        return view('HrmsCompanyProfile.index', compact('hrmscompanyprofiles'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('HrmsCompanyProfile.create');
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\HrmsCompanyProfileStoreRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(HrmsCompanyProfileStoreRequest $request)
    {

       

        $created_at  = now();
        $created_by  = Auth::user()->id;       
        



        HrmsCompanyProfile::create(array_merge($request->all(), 
                                                ['created_at' => $created_at,
                                                'created_by' => $created_by,                                                ]
                                            ));
        return redirect()->route('companyprofiles.index')->with('success','HrmsCompanyProfile Created Successfully');
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @param  \App\HrmsCompanyProfile  $HrmsCompanyProfile
     * @return \Illuminate\Http\Response
     */
    public function show($id,HrmsCompanyProfile $HrmsCompanyProfile)
    {
        $hrmscompanyprofiles = HrmsCompanyProfile::findOrFail($id);
        return view('HrmsCompanyProfile.show',compact('hrmscompanyprofiles'));
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\HrmsCompanyProfile  $HrmsCompanyProfile
     * @return \Illuminate\Http\Response
     */
    public function edit($id,HrmsCompanyProfile $HrmsCompanyProfile)
    {
        $hrmscompanyprofiles = HrmsCompanyProfile::findOrFail($id);
        return view('HrmsCompanyProfile.edit',compact('hrmscompanyprofiles'));
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\HrmsCompanyProfile  $HrmsCompanyProfile
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        HrmsCompanyProfile::find($id)->update(Request::all());
        return redirect()->route('HrmsCompanyProfile.index')->with('success','HrmsCompanyProfile Updated Successfully');
    }    
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\HrmsCompanyProfile  $HrmsCompanyProfile
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id,HrmsCompanyProfile $HrmsCompanyProfile)
    {
        $hrmscompanyprofiles = HrmsCompanyProfile::findOrFail($id);
        if (!empty($hrmscompanyprofiles))
        {

            $hrmscompanyprofiles->delete();

        }else{

        }
        return redirect()->route();
    }
}
