<?php

namespace App\Http\Controllers;

use App\HrmsLeaveSetting;
use App\HrmsLeaveSettingLeaveType;
use App\Http\Requests\HrmsLeaveSettingStoreRequest;
use Illuminate\Http\Request;

class HrmsLeaveSettingController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    protected $HrmsLeaveSetting;

    public function __construct(HrmsLeaveSetting $HrmsLeaveSetting,Request $request)
    {
        $this->middleware('auth');
        $this->HrmsLeaveSetting = $HrmsLeaveSetting;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        $hrmsleavesettings = HrmsLeaveSetting::latest()->paginate(5);
        return view('HrmsLeaveSetting.index', compact('hrmsleavesettings'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $leaveRepeatsAnnually = ['1' => 'YES', '2' => 'NO'];
        $leaveIncludeAllLocation = ['1' => 'YES', '2' => 'NO'];
        $leaveSettingsType = HrmsLeaveSettingLeaveType::toDropDown('hrms_leave_setting_leave_types','leave_setting_name',null,'--Select Leave Type--');
        $viewShare = ['leaveRepeatsAnnually','leaveSettingsType','leaveIncludeAllLocation'];
        return view('HrmsLeaveSetting.create' , compact($viewShare));
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\HrmsLeaveSettingStoreRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(HrmsLeaveSettingStoreRequest $request)
    {
        HrmsLeaveSetting::create($request->all());
        return redirect()->route('leavesettings.index')->with('success','HrmsLeaveSetting Created Successfully');
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @param  \App\HrmsLeaveSetting  $HrmsLeaveSetting
     * @return \Illuminate\Http\Response
     */
    public function show($id,HrmsLeaveSetting $HrmsLeaveSetting)
    {
        $hrmsleavesettings = HrmsLeaveSetting::findOrFail($id);
        return view('HrmsLeaveSetting.show',compact('hrmsleavesettings'));
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\HrmsLeaveSetting  $HrmsLeaveSetting
     * @return \Illuminate\Http\Response
     */
    public function edit($id,HrmsLeaveSetting $HrmsLeaveSetting)
    {
        $hrmsleavesettings = HrmsLeaveSetting::findOrFail($id);
        return view('HrmsLeaveSetting.edit',compact('hrmsleavesettings'));
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\HrmsLeaveSetting  $HrmsLeaveSetting
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        HrmsLeaveSetting::find($id)->update(Request::all());
        return redirect()->route('HrmsLeaveSetting.index')->with('success','HrmsLeaveSetting Updated Successfully');
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\HrmsLeaveSetting  $HrmsLeaveSetting
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id,HrmsLeaveSetting $HrmsLeaveSetting)
    {
        $hrmsleavesettings = HrmsLeaveSetting::findOrFail($id);
        if (!empty($hrmsleavesettings))
        {

            $hrmsleavesettings->delete();

        }else{

        }
        return redirect()->route();
    }
}
