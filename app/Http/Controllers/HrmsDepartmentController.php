<?php

namespace App\Http\Controllers;

use App\HrmsDepartment;
use App\HrmsCompanyProfile;
use App\Http\Requests\HrmsDepartmentStoreRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HrmsDepartmentController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    protected $HrmsDepartment;

    public function __construct(HrmsDepartment $HrmsDepartment,Request $request)
    {
        $this->middleware('auth');
        $this->HrmsDepartment = $HrmsDepartment;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        $hrmsdepartments = HrmsDepartment::latest()->paginate(5);
        return view('HrmsDepartment.index', compact('hrmsdepartments'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
        $companies  =HrmsCompanyProfile::dropDownTwoColumns('hrms_company_profiles','company_name','company_address_city',null,'--Select Company--');
        $viewShare = ['companies'];
        return view('HrmsDepartment.create', compact($viewShare));
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\HrmsDepartmentStoreRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(HrmsDepartmentStoreRequest $request)
    {
        HrmsDepartment::create($request->all());
        return redirect()->route('departments.index')->with('success','Department Created Successfully');
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @param  \App\HrmsDepartment  $HrmsDepartment
     * @return \Illuminate\Http\Response
     */
    public function show($id,HrmsDepartment $HrmsDepartment)
    {
        $hrmsdepartments = HrmsDepartment::findOrFail($id);
        return view('HrmsDepartment.show',compact('hrmsdepartments'));
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\HrmsDepartment  $HrmsDepartment
     * @return \Illuminate\Http\Response
     */
    public function edit($id,HrmsDepartment $HrmsDepartment)
    {
        $hrmsdepartments = HrmsDepartment::findOrFail($id);
        return view('HrmsDepartment.edit',compact('hrmsdepartments'));
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\HrmsDepartment  $HrmsDepartment
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        HrmsDepartment::find($id)->update(Request::all());
        return redirect()->route('HrmsDepartment.index')->with('success','HrmsDepartment Updated Successfully');
    }    
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\HrmsDepartment  $HrmsDepartment
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id,HrmsDepartment $HrmsDepartment)
    {
        $hrmsdepartments = HrmsDepartment::findOrFail($id);
        if (!empty($hrmsdepartments))
        {
            $hrmsdepartments->delete();

        }else{

        }
        return redirect()->route('departments.index');
    }
}
