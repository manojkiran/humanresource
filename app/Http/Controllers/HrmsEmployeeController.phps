<?php

namespace App\Http\Controllers;

use App\HrmsEmployee;
use App\HrmsCountry;
use App\HrmsDepartment;
use App\HrmsShift;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;
use App\Http\Requests\HrmsEmployeeStoreRequest;

class HrmsEmployeeController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       $employees = HrmsEmployee::latest()->paginate('6');
       return view('employees.index', compact('employees'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $documentCollectedArray = ['Relieving Letter','Experience Letter','Payslips','Certificates(if any)'];

        $maritalStatus  =[''=>'--Select--','Married' => 'Married','Single' => 'Single'];

        $nationality = HrmsCountry::toDropDown('hrms_countries','country_nationality_name',null,'--Select Your Nationality--');

        $country = HrmsCountry::toDropDown('hrms_countries','country_name',null,'--Select Your Nationality--');

        $departments  = HrmsDepartment::toDropDown('hrms_departments','department_name',null,'--Select your Department');

        $shiftTimings  =HrmsShift::dropDownMumtipleColumns('hrms_shifts','shift_name','shift_timing',null,'--Select ShiftTimings--');

        $autoIncrement = HrmsEmployee::autoIncrementWithPrefix('hrms_employees','employee_code','1000','Emp-');


        $experienceYears = ['1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16','17','18','19','20','21','22','23','24','25','26','27','28','29'];

        $experienceMonths = ['1','2','3','4','5','6','7','8','9','10','11','12'];

        $viewShare = ['documentCollectedArray','maritalStatus','country','nationality','departments','experienceYears','experienceMonths','shiftTimings','autoIncrement'];



        return view('employees.new' , compact($viewShare));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(HrmsEmployeeStoreRequest $request)
    {

        $created_at  = now();
        $created_by  = Auth::user()->id;
        $employeeCreate = HrmsEmployee::create(array_merge($request->all(), ['created_at' => $created_at,'created_by' => $created_by,'employee_documents_collected'=>json_encode($request->employee_documents_collected)]));
        return redirect()->route('employees.index')->with('success','Employee Created Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\HrmsEmployee  $HrmsEmployee
     * @return \Illuminate\Http\Response
     */
    public function show(HrmsEmployee $HrmsEmployee)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\HrmsEmployee  $HrmsEmployee
     * @return \Illuminate\Http\Response
     */
    public function edit($id,HrmsEmployee $HrmsEmployee)
    {
        dd($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\HrmsEmployee  $HrmsEmployee
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, HrmsEmployee $HrmsEmployee)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\HrmsEmployee  $HrmsEmployee
     * @return \Illuminate\Http\Response
     */
    public function destroy(HrmsEmployee $HrmsEmployee)
    {
        //
    }





}
