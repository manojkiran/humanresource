<?php

namespace App\Http\Controllers;

use App\HrmsEmployeeEducation;
use App\Http\Requests\HrmsEmployeeEducationStoreRequest;
use Illuminate\Http\Request;

class HrmsEmployeeEducationController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    protected $HrmsEmployeeEducation;

    public function __construct(HrmsEmployeeEducation $HrmsEmployeeEducation,Request $request)
    {
        $this->middleware('auth');
        $this->HrmsEmployeeEducation = $HrmsEmployeeEducation;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        $hrmsemployeeeducations = HrmsEmployeeEducation::latest()->paginate(5);
        return view('HrmsEmployeeEducation.index', compact('hrmsemployeeeducations'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('HrmsEmployeeEducation.create');
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\HrmsEmployeeEducationStoreRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(HrmsEmployeeEducationStoreRequest $request)
    {
        HrmsEmployeeEducation::create($request->all());
        return redirect()->route('HrmsEmployeeEducation.index')->with('success','HrmsEmployeeEducation Created Successfully');
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @param  \App\HrmsEmployeeEducation  $HrmsEmployeeEducation
     * @return \Illuminate\Http\Response
     */
    public function show($id,HrmsEmployeeEducation $HrmsEmployeeEducation)
    {
        $hrmsemployeeeducations = HrmsEmployeeEducation::findOrFail($id);
        return view('HrmsEmployeeEducation.show',compact('hrmsemployeeeducations'));
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\HrmsEmployeeEducation  $HrmsEmployeeEducation
     * @return \Illuminate\Http\Response
     */
    public function edit($id,HrmsEmployeeEducation $HrmsEmployeeEducation)
    {
        $hrmsemployeeeducations = HrmsEmployeeEducation::findOrFail($id);
        return view('HrmsEmployeeEducation.edit',compact('hrmsemployeeeducations'));
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\HrmsEmployeeEducation  $HrmsEmployeeEducation
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        HrmsEmployeeEducation::find($id)->update(Request::all());
        return redirect()->route('HrmsEmployeeEducation.index')->with('success','HrmsEmployeeEducation Updated Successfully');
    }    
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\HrmsEmployeeEducation  $HrmsEmployeeEducation
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id,HrmsEmployeeEducation $HrmsEmployeeEducation)
    {
        $hrmsemployeeeducations = HrmsEmployeeEducation::findOrFail($id);
        if (!empty($hrmsemployeeeducations))
        {

            $hrmsemployeeeducations->delete();

        }else{

        }
        return redirect()->route();
    }
}
