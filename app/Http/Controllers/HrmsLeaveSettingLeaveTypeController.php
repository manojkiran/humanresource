<?php

namespace App\Http\Controllers;

use App\HrmsLeaveSettingLeaveType;
use App\Http\Requests\HrmsLeaveSettingLeaveTypeStoreRequest;
use Illuminate\Http\Request;

class HrmsLeaveSettingLeaveTypeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    protected $HrmsLeaveSettingLeaveType;

    public function __construct(HrmsLeaveSettingLeaveType $HrmsLeaveSettingLeaveType,Request $request)
    {
        $this->middleware('auth');
        $this->HrmsLeaveSettingLeaveType = $HrmsLeaveSettingLeaveType;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        $hrmsleavesettingleavetypes = HrmsLeaveSettingLeaveType::latest()->paginate(5);
        return view('HrmsLeaveSettingLeaveType.index', compact('hrmsleavesettingleavetypes'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $viewShare = [''];
        return view('HrmsLeaveSettingLeaveType.create' , compact($viewShare));
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\HrmsLeaveSettingLeaveTypeStoreRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(HrmsLeaveSettingLeaveTypeStoreRequest $request)
    {
        HrmsLeaveSettingLeaveType::create($request->all());
        return redirect()->route('HrmsLeaveSettingLeaveType.index')->with('success','HrmsLeaveSettingLeaveType Created Successfully');
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @param  \App\HrmsLeaveSettingLeaveType  $HrmsLeaveSettingLeaveType
     * @return \Illuminate\Http\Response
     */
    public function show($id,HrmsLeaveSettingLeaveType $HrmsLeaveSettingLeaveType)
    {
        $hrmsleavesettingleavetypes = HrmsLeaveSettingLeaveType::findOrFail($id);
        return view('HrmsLeaveSettingLeaveType.show',compact('hrmsleavesettingleavetypes'));
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\HrmsLeaveSettingLeaveType  $HrmsLeaveSettingLeaveType
     * @return \Illuminate\Http\Response
     */
    public function edit($id,HrmsLeaveSettingLeaveType $HrmsLeaveSettingLeaveType)
    {
        $hrmsleavesettingleavetypes = HrmsLeaveSettingLeaveType::findOrFail($id);
        return view('HrmsLeaveSettingLeaveType.edit',compact('hrmsleavesettingleavetypes'));
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\HrmsLeaveSettingLeaveType  $HrmsLeaveSettingLeaveType
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        HrmsLeaveSettingLeaveType::find($id)->update(Request::all());
        return redirect()->route('HrmsLeaveSettingLeaveType.index')->with('success','HrmsLeaveSettingLeaveType Updated Successfully');
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\HrmsLeaveSettingLeaveType  $HrmsLeaveSettingLeaveType
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id,HrmsLeaveSettingLeaveType $HrmsLeaveSettingLeaveType)
    {
        $hrmsleavesettingleavetypes = HrmsLeaveSettingLeaveType::findOrFail($id);
        if (!empty($hrmsleavesettingleavetypes))
        {

            $hrmsleavesettingleavetypes->delete();

        }else{

        }
        return redirect()->route();
    }
}
