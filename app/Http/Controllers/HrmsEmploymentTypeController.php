<?php

namespace App\Http\Controllers;

use App\HrmsEmploymentType;
use App\Http\Requests\HrmsEmploymentTypeStoreRequest;
use Illuminate\Http\Request;

class HrmsEmploymentTypeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    protected $HrmsEmploymentType;

    public function __construct(HrmsEmploymentType $HrmsEmploymentType,Request $request)
    {
        $this->middleware('auth');
        $this->HrmsEmploymentType = $HrmsEmploymentType;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        $hrmsemploymenttypes = HrmsEmploymentType::latest()->paginate(5);
        return view('HrmsEmploymentType.index', compact('hrmsemploymenttypes'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('HrmsEmploymentType.create');
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\HrmsEmploymentTypeStoreRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(HrmsEmploymentTypeStoreRequest $request)
    {
        HrmsEmploymentType::create($request->all());
        return redirect()->route('HrmsEmploymentType.index')->with('success','HrmsEmploymentType Created Successfully');
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @param  \App\HrmsEmploymentType  $HrmsEmploymentType
     * @return \Illuminate\Http\Response
     */
    public function show($id,HrmsEmploymentType $HrmsEmploymentType)
    {
        $hrmsemploymenttypes = HrmsEmploymentType::findOrFail($id);
        return view('HrmsEmploymentType.show',compact('hrmsemploymenttypes'));
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\HrmsEmploymentType  $HrmsEmploymentType
     * @return \Illuminate\Http\Response
     */
    public function edit($id,HrmsEmploymentType $HrmsEmploymentType)
    {
        $hrmsemploymenttypes = HrmsEmploymentType::findOrFail($id);
        return view('HrmsEmploymentType.edit',compact('hrmsemploymenttypes'));
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\HrmsEmploymentType  $HrmsEmploymentType
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        HrmsEmploymentType::find($id)->update(Request::all());
        return redirect()->route('HrmsEmploymentType.index')->with('success','HrmsEmploymentType Updated Successfully');
    }    
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\HrmsEmploymentType  $HrmsEmploymentType
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id,HrmsEmploymentType $HrmsEmploymentType)
    {
        $hrmsemploymenttypes = HrmsEmploymentType::findOrFail($id);
        if (!empty($hrmsemploymenttypes))
        {

            $hrmsemploymenttypes->delete();

        }else{

        }
        return redirect()->route();
    }
}
