<?php

namespace App\Http\Controllers;

use App\HrmsEmployee;
use App\HrmsCountry;
use App\HrmsDepartment;
use App\HrmsShift;
use App\HrmsEmploymentType;
use App\Models\Role;
use App\Models\User;
use App\HrmsEmployeeEducation;
use App\HrmsEmployeeCertificate;
use App\HrmsEmployeePgEducation;

use Illuminate\Http\Request;
use App\HrmsEmployeeExperience;


use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;
use App\Http\Requests\HrmsEmployeeStoreRequest;
use App\Helpers\BladeHelper;


class HrmsEmployeeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    protected $HrmsEmployee;

    public function __construct(HrmsEmployee $HrmsEmployee,Request $request)
    {
        $this->middleware('auth');
        $this->HrmsEmployee = $HrmsEmployee;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        $hrmsemployees = HrmsEmployee::latest()->paginate(5);
        return view('HrmsEmployee.index', compact('hrmsemployees'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $roles = Role::where('name', '!=','superadmin')->get();

        $documentCollectedArray = ['Relieving Letter','Experience Letter','Payslips','Certificates(if any)'];

        $maritalStatus  =[''=>'--Select--','Married' => 'Married','Single' => 'Single'];

        $nationality = HrmsCountry::toDropDown('hrms_countries','country_nationality_name',null,'--Select Your Nationality--');

        $country = HrmsCountry::toDropDown('hrms_countries','country_name',null,'--Select Your Nationality--');

        $departments  = HrmsDepartment::toDropDown('hrms_departments','department_name',null,'--Select your Department','--Select Your Department--');

        $shiftTimings  =HrmsShift::dropDownTwoColumns('hrms_shifts','shift_name','shift_timing',null,'--Select ShiftTimings--');

        $autoIncrement = HrmsEmployee::autoIncrementWithPrefix('hrms_employees','employee_code','1000','Emp-');


        $experienceYears = [
                            '0' =>'0','1' =>'1','2' =>'2','3' =>'3','4' =>'4','5' =>'5','6' =>'6','7' =>'7','8' =>'8','9' =>'9','10' =>'10','11' =>'11','12' =>'12','13' =>'13','14' =>'14','15' =>'15','16' =>'16','17' =>'17','18' =>'18','19' =>'19','20' =>'20','21' =>'21','22' =>'22','23' =>'23','24' =>'24','25' =>'25','26' =>'26','27' =>'27','28' =>'28','29' =>'29','30' =>'30'
                        ];

        $experienceMonths = ['0' =>'0','1' =>'1','2' =>'2','3' =>'3','4' =>'4','5' =>'5','6' =>'6','7' =>'7','8' =>'8','9' =>'9','10' =>'10','11' =>'11','12' =>'12'];



        $hrmsEmploymentType  = HrmsEmploymentType::toDropDown('hrms_employment_types','employment_type',null,'--Selct Employment Type--');

        $viewShare = ['documentCollectedArray','maritalStatus','country','nationality','departments','experienceYears','experienceMonths','shiftTimings','autoIncrement','hrmsEmploymentType','roles'];

        return view('HrmsEmployee.create' , compact($viewShare));
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\HrmsEmployeeStoreRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

         




// dd($request->all());
       

         
         
         
    //     // dd($employeeCertificationName);

        
        $folderNameforEmployee  = $this->determineFolderNameforEmployee($request->employee_first_name,$request->employee_middle_name,$request->employee_last_name,$request->employee_code);

        if($request->hasfile('employee_payslips'))
        {
           $paySlipsFileName =  $this->uploadFiles($request->employee_payslips,config('hrmsconstants.fileUploadPath.employeeFileUploadPath').DIRECTORY_SEPARATOR.$folderNameforEmployee.DIRECTORY_SEPARATOR.'paySlips');

           $fileInput['employee_payslips'] = $paySlipsFileName;
        }

        if($request->hasfile('employee_recent_photo'))
        {
           $photoFileName =  $this->uploadFile($request->employee_recent_photo,config('hrmsconstants.fileUploadPath.employeeFileUploadPath').DIRECTORY_SEPARATOR.$folderNameforEmployee.DIRECTORY_SEPARATOR.'photos');

           $fileInput['employee_recent_photo'] = $photoFileName;
        }
        

        

        if($request->hasFile('employee_relieving_letter'))
        {
            
            $relievingLetterFileName = $this->uploadFile($request->file('employee_relieving_letter'),config('hrmsconstants.fileUploadPath.employeeFileUploadPath').DIRECTORY_SEPARATOR.$folderNameforEmployee.DIRECTORY_SEPARATOR.'relievingLetter');

            $fileInput['employee_relieving_letter'] = $relievingLetterFileName;
        }

        if($request->hasFile('employee_experience_letter'))
        {

            $experienceLetterFileName = $this->uploadFile($request->file('employee_experience_letter'),config('hrmsconstants.fileUploadPath.employeeFileUploadPath').DIRECTORY_SEPARATOR.$folderNameforEmployee.DIRECTORY_SEPARATOR.'experienceLetter');

            $fileInput['employee_experience_letter'] = $experienceLetterFileName;
        }
        // dd($fileInput);
        

        




        

        


        $created_at  = now();
        $created_by  = Auth::user()->id;

        $password = $this->hashPassword($request->password);

        $loginName = $request->name;
        $loginEmail = $request->email;
        $loginPassword = $password;


        $userStore  = User::create(array_merge(['name' => $loginName , 'email' => $loginEmail,'password' => $password],['created_at' => $created_at]));

        if ($request->has('roles'))
        {
            $user = User::find($userStore->id);
            $user->giveRoleById($request->roles);
        }


       



       
        


    $employeeArray = [
                        'created_at' => $created_at,
                        'created_by' => $created_by,
                        'user_id' => $userStore->id,
                        'employee_recent_photo' => 'test',

                        

                    ];

    $storedEmployee =  HrmsEmployee::create(array_merge($request->all(),$employeeArray));

    $educationArray = [
                        'employee_id'=>$storedEmployee->id,
                        'employee_sslc_school_name' => $request->employee_sslc_school_name,
                        'employee_sslc_year_of_passing' => $request->employee_sslc_year_of_passing,
                        'employee_sslc_percentage_of_marks' => $request->employee_sslc_percentage_of_marks,

                        'employee_hsc_school_name' => $request->employee_hsc_school_name,
                        'employee_hsc_year_of_passing' => $request->employee_hsc_year_of_passing,
                        'employee_hsc_percentage_of_marks' => $request->employee_hsc_percentage_of_marks,


                        'employee_ug_degree_name' => $request->employee_ug_degree_name,
                        'employee_ug_college_name' => $request->employee_ug_college_name,
                        'employee_ug_year_of_passing' => $request->employee_ug_year_of_passing,
                        'employee_ug_percentage_of_marks' => $request->employee_ug_percentage_of_marks,

                    ];
                    HrmsEmployeeEducation::create($educationArray);
    //                 // dd($educationArray);




if ($this->isArrayEmptyOrnull($request->employee_certification_name) == false) 
{
    $certLoopCount = count($request->employee_certification_name) - 1;


     for ($certCtrlVrb=0; $certCtrlVrb <= $certLoopCount ; $certCtrlVrb++) 
     { 
        $certficationArray = [
            'employee_id' => $storedEmployee->id,
            'employee_certification_name' => $request->employee_certification_name[$certCtrlVrb],
            'employee_certification_valid_till' => $request->employee_certification_valid_till[$certCtrlVrb],
            'employee_certification_grade' => $request->employee_certification_grade[$certCtrlVrb],
            'employee_certification_institution_name' => $request->employee_certification_institution_name[$certCtrlVrb],
            
            
            
        ];
            
            HrmsEmployeeCertificate::create($certficationArray);
     }  
}
    


    if ($this->isArrayEmptyOrnull($request->employee_certification_name) == false) 
    {

         $certficationLoopCount = count($request->employee_certification_name) - 1;


     for ($certCtrlVrb=0; $certCtrlVrb <= $certficationLoopCount ; $certCtrlVrb++) 
     { 
        $certficationArray = [
            'employee_id' => $storedEmployee->id,
            'employee_certification_name' => $request->employee_certification_name[$certCtrlVrb],
            'employee_certification_valid_till' => $request->employee_certification_valid_till[$certCtrlVrb],
            'employee_certification_grade' => $request->employee_certification_grade[$certCtrlVrb],
            'employee_certification_institution_name' => $request->employee_certification_institution_name[$certCtrlVrb],
            
            
            
        ];
            
            HrmsEmployeeCertificate::create($certficationArray);
     }  

        
    }

    if ($this->isArrayEmptyOrnull($request->employee_pg_college_name) == false) 
    {

         $pgDegreeLoopCount = count($request->employee_pg_college_name) - 1;




     for ($pgCtrlVerb=0; $pgCtrlVerb <= $pgDegreeLoopCount ; $pgCtrlVerb++) 
     { 
        $pgDegreeArray = [
            'employee_id' => $storedEmployee->id,
            'employee_pg_college_name' => $request->employee_pg_college_name[$pgCtrlVerb],
            'employee_pg_degree_name' => $request->employee_pg_degree_name[$pgCtrlVerb],
            'employee_pg_year_of_passing' => $request->employee_pg_year_of_passing[$pgCtrlVerb],
            'employee_pg_percentage_of_marks' => $request->employee_pg_percentage_of_marks[$pgCtrlVerb],
            
            
            
        ];
            
            HrmsEmployeePgEducation::create($pgDegreeArray);
     }  
        
    }

    if ($this->isArrayEmptyOrnull($request->employee_previous_company_name) == false) 
    {

        $experienceLoopCount = count($request->employee_previous_company_name) - 1;




     for ($expCtrlVerb=0; $expCtrlVerb <= $experienceLoopCount ; $expCtrlVerb++) 
     { 

        $experienceArray = [
            'employee_id' => $storedEmployee->id,

            'employee_previous_company_name' => $request->employee_previous_company_name[$expCtrlVerb],
            'employee_previous_designation' => $request->employee_previous_designation[$expCtrlVerb],
            'employee_previous_employment_start_date' => $request->employee_previous_employment_start_date[$expCtrlVerb],
            'employee_previous_employment_end_date' => $request->employee_previous_employment_end_date[$expCtrlVerb],
            'employee_previous_employment_reason_for_leaving' => $request->employee_previous_employment_reason_for_leaving[$expCtrlVerb],
            'employee_previous_employment_contact_person_name' => $request->employee_previous_employment_contact_person_name[$expCtrlVerb],
            'employee_previous_employment_contact_person_email' => $request->employee_previous_employment_contact_person_email[$expCtrlVerb],
            'employee_previous_employment_contact_person_phone' => $request->employee_previous_employment_contact_person_phone[$expCtrlVerb],
            'employee_documents_collected' => json_encode($request->employee_documents_collected[$expCtrlVerb]),
            'employee_relieving_letter' => $this->uploadFile($request->employee_relieving_letter[$expCtrlVerb],$folderNameforEmployee),
            'employee_experience_letter' => $this->uploadFile($request->employee_experience_letter[$expCtrlVerb],$folderNameforEmployee),
            'employee_payslips' => $this->uploadFile($request->employee_payslips[$expCtrlVerb],$folderNameforEmployee),            
            
        ];
            
            HrmsEmployeeExperience::create($experienceArray);
     }  
        
    }



    


   

     
     

   
        
        return redirect()->route('employees.index')->with('success','Employee Created Successfully');
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @param  \App\HrmsEmployee  $HrmsEmployee
     * @return \Illuminate\Http\Response
     */
    public function show($id,HrmsEmployee $HrmsEmployee)
    {
        $hrmsemployees = HrmsEmployee::findOrFail($id);
        $employeeId = $hrmsemployees->id;
        $hrmsEmployeeEducation = HrmsEmployeeEducation::where('employee_id',$employeeId)->get();
        $viewShare = ['hrmsemployees','hrmsEmployeeEducation'];
        return view('HrmsEmployee.show',compact($viewShare));
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\HrmsEmployee  $HrmsEmployee
     * @return \Illuminate\Http\Response
     */
    public function edit($id,HrmsEmployee $HrmsEmployee)
    {

        $hrmsemployees = HrmsEmployee::findOrFail($id);

        $documentCollectedArray = ['Relieving Letter','Experience Letter','Payslips','Certificates(if any)'];

        $maritalStatus  =[''=>'--Select--','Married' => 'Married','Single' => 'Single'];

        $nationality = HrmsCountry::toDropDown('hrms_countries','country_nationality_name',null,'--Select Your Nationality--');

        $country = HrmsCountry::toDropDown('hrms_countries','country_name',null,'--Select Your Nationality--');

        $departments  = HrmsDepartment::toDropDown('hrms_departments','department_name',null,'--Select your Department');

        $shiftTimings  =HrmsShift::dropDownTwoColumns('hrms_shifts','shift_name','shift_timing',null,'--Select ShiftTimings--');

        $autoIncrement = HrmsEmployee::autoIncrementWithPrefix('hrms_employees','employee_code','1000','Emp-');


        $experienceYears = ['1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16','17','18','19','20','21','22','23','24','25','26','27','28','29'];

        $experienceMonths = ['1','2','3','4','5','6','7','8','9','10','11','12'];

        $hrmsEmploymentType  = HrmsEmploymentType::toDropDown('hrms_employment_types','employment_type',null,'--Selct Employment Type--');
        $roles = DB::table('roles')->where('name', '!=','superadmin')->get();

        $viewShare = ['documentCollectedArray','maritalStatus','country','nationality','departments','experienceYears','experienceMonths','shiftTimings','autoIncrement','hrmsemployees','hrmsEmploymentType','roles'];

        return view('HrmsEmployee.edit',compact($viewShare));
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\HrmsEmployee  $HrmsEmployee
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $updated_at  = now();
        $updated_by  = Auth::user()->id;
        HrmsEmployee::find($id)->update(array_merge($request->all(), ['updated_at' => $updated_at,'updated_by' => $updated_by,'employee_documents_collected'=>json_encode($request->employee_documents_collected)]));
        return redirect()->route('employees.index')->with('success','HrmsEmployee Updated Successfully');
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\HrmsEmployee  $HrmsEmployee
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id,HrmsEmployee $HrmsEmployee)
    {
        $hrmsemployees = HrmsEmployee::findOrFail($id);
        if (!empty($hrmsemployees))
        {

            $hrmsemployees->delete();

        }else{

        }
        return redirect()->route('employees.index');
    }
    private function hashPassword($normalText='')
    {
        $encryptedPassword  = bcrypt($normalText);
        return $encryptedPassword;
    }

    private  function uploadFile($fileName='',$destinationPath='')
    {
        
        if (is_array($fileName))
        {
            return $this->uploadFiles($fileName,$destinationPath);
        }
         
        $fileOriginalName = $fileName->getClientOriginalName();
        $fileName->move($destinationPath, $fileOriginalName);
        return $fileOriginalName;
    }

    private  function uploadFiles($fileNames='',$destinationPath='')
    {
        foreach($fileNames as $fileName)
            {
                $fileOriginalName = $fileName->getClientOriginalName();
                $fileName->move($destinationPath, $fileOriginalName);  
                $allFileNames[] = $fileOriginalName;
            }
            return json_encode($allFileNames);
    }

    private function determineFolderNameforEmployee($firstName='',$middleName='',$lastName='',$empCode='')
    {
        return (empty($firstName) ? '' : $firstName).(empty($middleName) ? '' : '-'.$middleName).(empty($lastName) ? '' : '-'.$lastName).(empty($empCode) ? '' : '-'.$empCode);
        
    }

    private function isArrayEmptyOrnull($array='')
    {

        $isEmpty = array_filter($array, 'strlen') == [];      

        if ($isEmpty == true) 
        {
            return true;
        }else
        {
            return false;
        }
    }

}
