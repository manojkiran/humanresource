<?php

namespace App\Http\Controllers;

use App\HrmsEmployeeGrade;
use App\Http\Requests\HrmsEmployeeGradeStoreRequest;
use Illuminate\Http\Request;

class HrmsEmployeeGradeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    protected $HrmsEmployeeGrade;

    public function __construct(HrmsEmployeeGrade $HrmsEmployeeGrade,Request $request)
    {
        $this->middleware('auth');
        $this->HrmsEmployeeGrade = $HrmsEmployeeGrade;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        $hrmsemployeegrades = HrmsEmployeeGrade::paginate(20);
        return view('HrmsEmployeeGrade.index', compact('hrmsemployeegrades'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('HrmsEmployeeGrade.create');
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\HrmsEmployeeGradeStoreRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(HrmsEmployeeGradeStoreRequest $request)
    {
        
        HrmsEmployeeGrade::create($request->all());
        return redirect()->route('employeegrades.index')->with('success','HrmsEmployeeGrade Created Successfully');
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @param  \App\HrmsEmployeeGrade  $HrmsEmployeeGrade
     * @return \Illuminate\Http\Response
     */
    public function show($id,HrmsEmployeeGrade $HrmsEmployeeGrade)
    {
        $hrmsemployeegrades = HrmsEmployeeGrade::findOrFail($id);
        return view('HrmsEmployeeGrade.show',compact('hrmsemployeegrades'));
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\HrmsEmployeeGrade  $HrmsEmployeeGrade
     * @return \Illuminate\Http\Response
     */
    public function edit($id,HrmsEmployeeGrade $HrmsEmployeeGrade)
    {
        $hrmsemployeegrades = HrmsEmployeeGrade::findOrFail($id);
        return view('HrmsEmployeeGrade.edit',compact('hrmsemployeegrades'));
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\HrmsEmployeeGrade  $HrmsEmployeeGrade
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        HrmsEmployeeGrade::find($id)->update($request->all());
        return redirect()->route('employeegrades.index')->with('success','HrmsEmployeeGrade Updated Successfully');
    }    
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\HrmsEmployeeGrade  $HrmsEmployeeGrade
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id,HrmsEmployeeGrade $HrmsEmployeeGrade)
    {
        $hrmsemployeegrades = HrmsEmployeeGrade::findOrFail($id);
        if (!empty($hrmsemployeegrades))
        {

            $hrmsemployeegrades->delete();

        }else{

        }
        return redirect()->route();
    }
}
