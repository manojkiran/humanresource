<?php

namespace App\Http\Controllers;

use App\HrmsEmployeeSalarySetting;
use App\Http\Requests\HrmsEmployeeSalarySettingStoreRequest;
use Illuminate\Http\Request;

class HrmsEmployeeSalarySettingController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    protected $HrmsEmployeeSalarySetting;

    public function __construct(HrmsEmployeeSalarySetting $HrmsEmployeeSalarySetting,Request $request)
    {
        $this->middleware('auth');
        $this->HrmsEmployeeSalarySetting = $HrmsEmployeeSalarySetting;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        $hrmsemployeesalarysettings = HrmsEmployeeSalarySetting::latest()->paginate(5);
        return view('HrmsEmployeeSalarySetting.index', compact('hrmsemployeesalarysettings'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('HrmsEmployeeSalarySetting.create');
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\HrmsEmployeeSalarySettingStoreRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(HrmsEmployeeSalarySettingStoreRequest $request)
    {
        dd($request->all());
        HrmsEmployeeSalarySetting::create($request->all());
        return redirect()->route('HrmsEmployeeSalarySetting.index')->with('success','HrmsEmployeeSalarySetting Created Successfully');
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @param  \App\HrmsEmployeeSalarySetting  $HrmsEmployeeSalarySetting
     * @return \Illuminate\Http\Response
     */
    public function show($id,HrmsEmployeeSalarySetting $HrmsEmployeeSalarySetting)
    {
        $hrmsemployeesalarysettings = HrmsEmployeeSalarySetting::findOrFail($id);
        return view('HrmsEmployeeSalarySetting.show',compact('hrmsemployeesalarysettings'));
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\HrmsEmployeeSalarySetting  $HrmsEmployeeSalarySetting
     * @return \Illuminate\Http\Response
     */
    public function edit($id,HrmsEmployeeSalarySetting $HrmsEmployeeSalarySetting)
    {
        $hrmsemployeesalarysettings = HrmsEmployeeSalarySetting::findOrFail($id);
        return view('HrmsEmployeeSalarySetting.edit',compact('hrmsemployeesalarysettings'));
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\HrmsEmployeeSalarySetting  $HrmsEmployeeSalarySetting
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        HrmsEmployeeSalarySetting::find($id)->update(Request::all());
        return redirect()->route('HrmsEmployeeSalarySetting.index')->with('success','HrmsEmployeeSalarySetting Updated Successfully');
    }    
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\HrmsEmployeeSalarySetting  $HrmsEmployeeSalarySetting
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id,HrmsEmployeeSalarySetting $HrmsEmployeeSalarySetting)
    {
        $hrmsemployeesalarysettings = HrmsEmployeeSalarySetting::findOrFail($id);
        if (!empty($hrmsemployeesalarysettings))
        {

            $hrmsemployeesalarysettings->delete();

        }else{

        }
        return redirect()->route();
    }
}
