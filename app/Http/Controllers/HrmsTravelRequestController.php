<?php

namespace App\Http\Controllers;

use App\HrmsTravelRequest;
use App\Http\Requests\HrmsTravelRequestStoreRequest;
use Illuminate\Http\Request;

class HrmsTravelRequestController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    protected $HrmsTravelRequest;

    public function __construct(HrmsTravelRequest $HrmsTravelRequest,Request $request)
    {
        $this->middleware('auth');
        $this->HrmsTravelRequest = $HrmsTravelRequest;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        $hrmstravelrequests = HrmsTravelRequest::latest()->paginate(5);
        return view('HrmsTravelRequest.index', compact('hrmstravelrequests'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('HrmsTravelRequest.create');
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\HrmsTravelRequestStoreRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(HrmsTravelRequestStoreRequest $request)
    {
        HrmsTravelRequest::create($request->all());
        return redirect()->route('HrmsTravelRequest.index')->with('success','HrmsTravelRequest Created Successfully');
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @param  \App\HrmsTravelRequest  $HrmsTravelRequest
     * @return \Illuminate\Http\Response
     */
    public function show($id,HrmsTravelRequest $HrmsTravelRequest)
    {
        $hrmstravelrequests = HrmsTravelRequest::findOrFail($id);
        return view('HrmsTravelRequest.show',compact('hrmstravelrequests'));
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\HrmsTravelRequest  $HrmsTravelRequest
     * @return \Illuminate\Http\Response
     */
    public function edit($id,HrmsTravelRequest $HrmsTravelRequest)
    {
        $hrmstravelrequests = HrmsTravelRequest::findOrFail($id);
        return view('HrmsTravelRequest.edit',compact('hrmstravelrequests'));
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\HrmsTravelRequest  $HrmsTravelRequest
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        HrmsTravelRequest::find($id)->update(Request::all());
        return redirect()->route('HrmsTravelRequest.index')->with('success','HrmsTravelRequest Updated Successfully');
    }    
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\HrmsTravelRequest  $HrmsTravelRequest
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id,HrmsTravelRequest $HrmsTravelRequest)
    {
        $hrmstravelrequests = HrmsTravelRequest::findOrFail($id);
        if (!empty($hrmstravelrequests))
        {

            $hrmstravelrequests->delete();

        }else{

        }
        return redirect()->route();
    }
}
