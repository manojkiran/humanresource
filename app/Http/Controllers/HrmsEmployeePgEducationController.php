<?php

namespace App\Http\Controllers;

use App\HrmsEmployeePgEducation;
use App\Http\Requests\HrmsEmployeePgEducationStoreRequest;
use Illuminate\Http\Request;

class HrmsEmployeePgEducationController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    protected $HrmsEmployeePgEducation;

    public function __construct(HrmsEmployeePgEducation $HrmsEmployeePgEducation,Request $request)
    {
        $this->middleware('auth');
        $this->HrmsEmployeePgEducation = $HrmsEmployeePgEducation;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        $hrmsemployeepgeducations = HrmsEmployeePgEducation::latest()->paginate(5);
        return view('HrmsEmployeePgEducation.index', compact('hrmsemployeepgeducations'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('HrmsEmployeePgEducation.create');
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\HrmsEmployeePgEducationStoreRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(HrmsEmployeePgEducationStoreRequest $request)
    {
        HrmsEmployeePgEducation::create($request->all());
        return redirect()->route('HrmsEmployeePgEducation.index')->with('success','HrmsEmployeePgEducation Created Successfully');
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @param  \App\HrmsEmployeePgEducation  $HrmsEmployeePgEducation
     * @return \Illuminate\Http\Response
     */
    public function show($id,HrmsEmployeePgEducation $HrmsEmployeePgEducation)
    {
        $hrmsemployeepgeducations = HrmsEmployeePgEducation::findOrFail($id);
        return view('HrmsEmployeePgEducation.show',compact('hrmsemployeepgeducations'));
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\HrmsEmployeePgEducation  $HrmsEmployeePgEducation
     * @return \Illuminate\Http\Response
     */
    public function edit($id,HrmsEmployeePgEducation $HrmsEmployeePgEducation)
    {
        $hrmsemployeepgeducations = HrmsEmployeePgEducation::findOrFail($id);
        return view('HrmsEmployeePgEducation.edit',compact('hrmsemployeepgeducations'));
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\HrmsEmployeePgEducation  $HrmsEmployeePgEducation
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        HrmsEmployeePgEducation::find($id)->update(Request::all());
        return redirect()->route('HrmsEmployeePgEducation.index')->with('success','HrmsEmployeePgEducation Updated Successfully');
    }    
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\HrmsEmployeePgEducation  $HrmsEmployeePgEducation
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id,HrmsEmployeePgEducation $HrmsEmployeePgEducation)
    {
        $hrmsemployeepgeducations = HrmsEmployeePgEducation::findOrFail($id);
        if (!empty($hrmsemployeepgeducations))
        {

            $hrmsemployeepgeducations->delete();

        }else{

        }
        return redirect()->route();
    }
}
