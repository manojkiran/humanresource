<?php

namespace App\Http\Controllers;

use App\Models\Comman\Traits\CredAuth;
use App\Models\Permission;
use App\Models\Role;
use App\Models\Module;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Auth;

class PermissionController extends Controller
{
    //use CredAuth;
     /**
     * Create a new controller instance.
     *
     * @return void
     */
    protected $permission;
    public function __construct(Permission $permission)
    {
        $this->middleware('auth');
        $this->permission = $permission;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        if(Gate::allows('view.permissions'))
        {
            $modelName = get_class($this->permission);

            $result = $this->permission::latest()->with('moduleName')->paginate(5)->onEachSide(3);
            return view('backend.permissions.index', compact('result'));
        }else{
            abort(401);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(Gate::allows('create.permissions'))
        {

            $roles = DB::table('roles')->where('name', '!=','superadmin')->get();
            $modules = Permission::toDropDown('modules','name');
            return view('backend.permissions.new',compact('roles','modules'));
        }else{
            abort(401);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, 
            [
            'name' => 'bail|required|unique:permissions|max:255',
            'description' => 'bail|required',
            'module_id' => 'required',
        ]); 
        $created_at  = now();
        $created_by  = $this->authUserId();
        $permissionStore  = Permission::create(array_merge($request->all(), ['created_at' => $created_at,'created_by' => $created_by]));

        if ($request->has('roles')) 
        {
            $permission = Permission::find($permissionStore->id);
            $permission->giveRoleById(array_merge(explode(',', $this->getSuperadminRoleId()),$request->roles));
        }elseif (!$request->has('roles')) 
        {
            $permission = Permission::find($permissionStore->id);
            $permission->giveRoleById($this->getSuperadminRoleId());
            
        }
        $redirectUrl = Permission::redirectForm($request->submitbutton,$permissionStore->id,'permissions','Created');
        return $redirectUrl;



    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Permission  $permission
     * @return \Illuminate\Http\Response
     */
    public function show(Permission $permission)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Permission  $permission
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if(Gate::allows('edit.permissions'))
        {

            $roles = DB::table('roles')->where('name', '!=','superadmin')->get();
            $modules = Permission::toDropDown('modules','name');
            $permissions = Permission::findOrFail($this->decryptId($id));

            return view('backend.permissions.edit',compact('roles','modules','permissions'));
        }else{
            abort(401);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Permission  $permission
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $permission = Permission::findOrFail($id);
        $this->validate($request, [
            'name' => 'required|min:3|max:30|unique:permissions,name,'.$id.',id',
            'description' => 'bail|required',
            'module_id' => 'required',
        ]);
        $permissionUpdate  = Permission::find($id)->update($request->all());

        if ($request->has('roles')) 
        {
            $permission = Permission::find($this->getId($permission));
            $permission->modifyRoleById(array_merge(explode(',', $this->getSuperadminRoleId()),$request->roles));
        }
        elseif (!$request->has('roles')) 
        {
            $permission = Permission::find($this->getId($permission));
            $permission->modifyRoleById($this->getSuperadminRoleId());
        }

        $redirectUrl = Permission::redirectForm($request->submitbutton,$this->getId($permission),'permissions','Updated');

        return $redirectUrl;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Permission  $permission
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $id = $this->decryptId($id);
        $permission = Permission::findOrFail($id);
        $permisisonToUser  = $permission->users();
        $permisisonToRole  = $permission->roles();

       if ($permission->users->isEmpty() && $permission->roles->isEmpty()) 
       {
            Permission::findOrFail($id)->delete();
            return redirect()->back()->with('success','Permission Deleted Successfully!');
           dd('empty');
       }else 
       {
           return redirect()->back()->with('error','Some Roles or User  Associated with this Permission!');
       }
    }


    private function authUserId()
    {
        return Auth::user()->id;
    }

    private function decryptId($encryptedID)
    {
        $decryptedId = Crypt::decrypt($encryptedID);
        return $decryptedId;
    }
    private function getId($array = [])
    {
        $id ="";
        $id = $array->id;
        if (!empty($id)) 
        {
            return $id;
        }else
        {
            return "No Id Found in the Given Array";
        }
    }

    private function getSuperadminRoleId()
    {
        $superadmin = DB::table('roles')->where('name','SuperAdmin')->first();
        return $this->getId($superadmin);
    }
}
