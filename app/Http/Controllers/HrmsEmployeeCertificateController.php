<?php

namespace App\Http\Controllers;

use App\HrmsEmployeeCertificate;
use App\Http\Requests\HrmsEmployeeCertificateStoreRequest;
use Illuminate\Http\Request;

class HrmsEmployeeCertificateController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    protected $HrmsEmployeeCertificate;

    public function __construct(HrmsEmployeeCertificate $HrmsEmployeeCertificate,Request $request)
    {
        $this->middleware('auth');
        $this->HrmsEmployeeCertificate = $HrmsEmployeeCertificate;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        $hrmsemployeecertificates = HrmsEmployeeCertificate::latest()->paginate(5);
        return view('HrmsEmployeeCertificate.index', compact('hrmsemployeecertificates'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('HrmsEmployeeCertificate.create');
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\HrmsEmployeeCertificateStoreRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(HrmsEmployeeCertificateStoreRequest $request)
    {
        HrmsEmployeeCertificate::create($request->all());
        return redirect()->route('HrmsEmployeeCertificate.index')->with('success','HrmsEmployeeCertificate Created Successfully');
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @param  \App\HrmsEmployeeCertificate  $HrmsEmployeeCertificate
     * @return \Illuminate\Http\Response
     */
    public function show($id,HrmsEmployeeCertificate $HrmsEmployeeCertificate)
    {
        $hrmsemployeecertificates = HrmsEmployeeCertificate::findOrFail($id);
        return view('HrmsEmployeeCertificate.show',compact('hrmsemployeecertificates'));
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\HrmsEmployeeCertificate  $HrmsEmployeeCertificate
     * @return \Illuminate\Http\Response
     */
    public function edit($id,HrmsEmployeeCertificate $HrmsEmployeeCertificate)
    {
        $hrmsemployeecertificates = HrmsEmployeeCertificate::findOrFail($id);
        return view('HrmsEmployeeCertificate.edit',compact('hrmsemployeecertificates'));
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\HrmsEmployeeCertificate  $HrmsEmployeeCertificate
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        HrmsEmployeeCertificate::find($id)->update(Request::all());
        return redirect()->route('HrmsEmployeeCertificate.index')->with('success','HrmsEmployeeCertificate Updated Successfully');
    }    
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\HrmsEmployeeCertificate  $HrmsEmployeeCertificate
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id,HrmsEmployeeCertificate $HrmsEmployeeCertificate)
    {
        $hrmsemployeecertificates = HrmsEmployeeCertificate::findOrFail($id);
        if (!empty($hrmsemployeecertificates))
        {

            $hrmsemployeecertificates->delete();

        }else{

        }
        return redirect()->route();
    }
}
