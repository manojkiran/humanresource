<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Gate;

class HrmsApplyPermissionStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        // if (Gate::allows('create.hrmsapplypermission'))
        // {
        //     return true;
        // }else {
        //     abort(403, 'Unauthorized.');
        // }
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'permission_date' => 'required|date|after_or_equal:today|date_format:Y/m/d',
            'permissions_time_from' => 'required',
            'permissions_time_to' =>'required',
            'permissions_reson' =>'required',
        ];
    }

    /**
     * Custom message for validation
     *
     * @return array
     */
    public function messages()
    {
        return [
            'permission_date.required' =>'Choose Date of Permisson',
            'permissions_time_from.required' =>'Choose Time From',
            'permissions_time_to.required' =>'Choose Time To',
            'permission_date.after_or_equal' =>'The permission date must be a date after or equal to today',
            'permissions_reson.required' => 'Enter Description '
            
            
        ];
    }

}
