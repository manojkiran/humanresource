<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Gate;

class HrmsSalarySettingStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        // if (Gate::allows('create.hrmssalarysetting'))
        // {
        //     return true;
        // }else {
        //     abort(403, 'Unauthorized.');
        // }
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
             'salary_component_name' => 'required|unique:hrms_salary_settings|max:150',
            'salary_component_type' => 'required',
            'salary_component_add_to' => 'required',
            'salary_component_value_type' => 'required',
            'salary_component_value' => 'required',
        ];
    }

    /**
     * Custom message for validation
     *
     * @return array
     */
    public function messages()
    {
        return [
            'salary_component_name.required' => 'Enter Component Name',
            'salary_component_name.unique' => 'You have Already Created the component With This Name',
            'salary_component_type.required' => 'Choose Component Type',
            'salary_component_add_to.required' => 'Choose Component That Needed to Added',
            'salary_component_value_type.required' => 'Choose Component Value Type',
            'salary_component_value.required' => 'Enter the Value',
            
            
            
        ];
    }

}
