<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Gate;

class HrmsApplyLeaveStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        // if (Gate::allows('create.hrmsapplyleave'))
        // {
        //     return true;
        // }else {
        //     abort(403, 'Unauthorized.');
        // }
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'employee_leave_type' => 'required',
            // 'employee_leave_starting_from' => 'required|date|after_or_equal:today|date_format:Y/m/d|before:employee_leave_ending_to',
            // 'employee_leave_ending_to' => 'required|date|date_format:Y/m/d|after:employee_leave_starting_from',  
            'employee_leave_starting_from' => 'required|date|after_or_equal:today|date_format:Y/m/d',
            'employee_leave_ending_to' => 'required|date|date_format:Y/m/d',  
            'employee_reason_for_leave' => 'required',
            'employee_leave_half_day' => 'nullable',
             
        ];
    }

    /**
     * Custom message for validation
     *
     * @return array
     */
    public function messages()
    {
        return [
            'employee_leave_type.required' => 'Selct Type of leave',

            'employee_leave_starting_from.required' => 'Select Start Date of Leave',
            'employee_leave_ending_to.required' => 'Select End Date of Leave',

            'employee_leave_starting_from.date' => 'Not A Valid Date',
            'employee_leave_ending_to.date' => 'Not A Valid Date',

            'employee_leave_starting_from.date_format' => 'Not A Valid Date Format',
            'employee_leave_ending_to.date_format' => 'Not A Valid Date Format',

            'employee_leave_starting_from.after_or_equal' =>'You Can Apply Leave Only From Today',

            

            'employee_leave_starting_from.before'=>'The  Date must be  before Leave To',
            'employee_leave_ending_to.after'=>'The  Date must be  after Leave From',

            'employee_reason_for_leave.required' =>'Enter Description',
        ];
    }

}
