<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Gate;

class HrmsCompanyProfileStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        // if (Gate::allows('create.hrmscompanyprofile'))
        // {
        //     return true;
        // }else {
        //     abort(403, 'Unauthorized.');
        // }
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'company_name' =>'required',
            'company_registration_number' =>'required',
            'company_gst_in' => 'required',
            'company_address_line_one' => 'required',
            'company_address_city' => 'required',
            'company_address_state' => 'required',
            'company_address_country' => 'required',
            'company_address_zip_code' => 'required|numeric|digits_between:6,6',
            'company_email' => 'required|email',
            'company_phone_number' => 'required|numeric',
            'company_website_url' =>'required|active_url',
            'company_contact_person_full_name' =>'required',
            'company_contact_person_designation' =>'required',
            'company_contact_person_email_id' =>'required|email',
            'company_contact_person_phone_number' =>'required|numeric',
            
            
            
            
        ];
    }

    /**
     * Custom message for validation
     *
     * @return array
     */
    public function messages()
    {
        return [
            'company_name.required' => 'Enter Your Comapny Name',
            'company_registration_number.required' => 'Enter Your Comapny Registration Number',
            'company_gst_in.required' => 'Enter Your Comapny GST Number',
            'company_address_line_one.required' => 'Enter Your Comapny Address',
            'company_address_city.required' => 'Select City For Your Comapny',
            'company_address_state.required' => 'Select State For Your Comapny',
            'company_address_country.required' => 'Select Country For Your Comapny',
            'company_address_zip_code.required' => 'Enter Zip Code',
            'company_address_zip_code.digits_between' =>'Pinocde Must Be 6 Character Length',
            'company_address_zip_code.numeric' =>'Pinocde Must Be Only Numbers',
            'company_email.required' =>'Enter the Email',
            'company_email.email' =>'Enter the Valid Email',
            'company_phone_number.required' => 'Enter the Company Phone Number',
            'company_phone_number.numeric' => 'Only Numbers are accepted',
            'company_website_url.required' => 'Enter the url',
            'company_website_url.active_url' => 'The Website Doesn'."'t Matches With Any DNS Record" ,
            'company_contact_person_full_name.required' => 'Enter the Contact Person Name',
            'company_contact_person_designation.required' => 'Enter the Contact Person Designation',
            'company_contact_person_email_id.required' => 'Enter the Contact Email id',
            'company_contact_person_email_id.email' => 'Enter the Valid Email id', 
            'company_contact_person_phone_number.required' => 'Enter the Phone Number',
            'company_contact_person_phone_number.numeric' => 'Only Numbers are Accepted',            
            
            
            
        ];
    }

}
