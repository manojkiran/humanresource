<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Gate;

class HrmsEmployeeStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        if (Gate::allows('create.employees'))
        {
            return true;
        }else {
            abort(403, 'Unauthorized.');
        }
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $allowedDomains = implode('|', ['postiefs','mail.postiefs']);
        return [
            'name' => 'required|string|max:255',
            'email' => ['required', 'max:255', 'email', 'regex:/(.*)@('.$allowedDomains.')\.com/i', 'unique:users'],
            'password' => 'required|string|min:6|confirmed',
            'password' => 'required|string|min:6|confirmed',
            'roles' => 'required',
            'employee_department_id' => 'required',
            'employee_reporting_to' => 'required',
            
            
            'employee_employment_type_id' => 'required',
            'employee_work_shift_id' => 'required',
            'employee_designation' => 'required',

            'employee_contract_start_date' => 'required|date|date_format:Y/m/d|before:employee_contract_end_date',
            'employee_contract_end_date' => 'nullable|date|date_format:Y/m/d|after:employee_contract_start_date', 
            
            

            'employee_primary_skills' => 'required',

             'employee_first_name' => 'required',

            'employee_last_name' => 'required',
            'employee_code' => 'required',

            'employee_date_of_birth' => 'required|date|date_format:Y/m/d',
            'employee_gender' => 'required',
            'employee_aadhar_number' => 'required|numeric',
            'employee_marital_status' => 'required',
            'employee_nationality' => 'required',

            

            'employee_recent_photo' => 'required|image|mimes:jpeg,png,jpg|max:4096',


            'employee_sslc_school_name' => 'required',
            'employee_sslc_year_of_passing' => 'required',
            'employee_sslc_percentage_of_marks' => 'required',

            'employee_hsc_school_name' => 'required',
            'employee_hsc_year_of_passing' => 'required',
            'employee_hsc_percentage_of_marks' => 'required',

            'employee_ug_college_name' => 'required',
            'employee_ug_degree_name' => 'required',
            'employee_ug_year_of_passing' => 'required',
            'employee_ug_percentage_of_marks' => 'required',

            


            'employee_pg_college_name.*' => 'sometimes|nullable',
            'employee_pg_degree_name.*' => 'required_with:employee_pg_college_name.*|distinct',
            'employee_pg_year_of_passing.*' => 'required_with:employee_pg_college_name.*|distinct',
            'employee_pg_percentage_of_marks.*' => 'required_with:employee_pg_college_name.*',

            'employee_certification_name.*' => 'sometimes|nullable',
            'employee_certification_valid_till.*' => 'required_with:employee_certification_name.*|distinct',
            'employee_certification_grade.*' => 'required_with:employee_certification_name.*|distinct',
            'employee_certification_institution_name.*' => 'required_with:employee_certification_name.*',


            'employee_certification_name.*' => 'sometimes|nullable|distinct',
            'employee_certification_valid_till.*' => 'required_with:employee_certification_name.*',
            'employee_certification_grade.*' => 'required_with:employee_certification_name.*',
            'employee_certification_institution_name.*' => 'required_with:employee_certification_name.*',


            'employee_door_no' => 'required',
            'employee_street_name' => 'required',
            'employee_city' => 'required',
            'employee_state' => 'required',
            'employee_country' => 'required',
            'employee_zip_code' => 'required',
            'employee_home_telephone' =>'nullable|numeric',
            'employee_mobile_number' =>'required|numeric',
            'employee_email_id' => 'required|email',


        ];
    }

    /**
     * Custom message for validation
     *
     * @return array
     */
    public function messages()
    {
        $allowedDomains = implode('|', ['postiefs.com','mail.postiefs.com']);

        $messages = 
         [

        'name.required' => 'Enter Name of the Employee',

        'email.required' => 'Enter Official Email of the Employee',
        'email.required' => 'Enter Valid Email',
        'email.email' => 'Enter is not in valid Format',
        'email.regex' => 'Enter the Domains Such as '.$allowedDomains,
        'password.confirmed' => 'Confirm Password Does'."'t".'Matches',
        'roles.required' => 'Select Role For User',
        'employee_department_id.required' => 'Select Department For the User',
        'employee_reporting_to.required' => 'Enter Reporting to ',
        'employee_employment_type_id.required' => 'Choose Employement Type ',
        'employee_work_shift_id.required' => 'Choose Employement Work Shift ',
        'employee_designation.required' => 'Enter Employee Designation ',

        'employee_contract_start_date.required' => 'Choose Date of Joining',
        'employee_contract_start_date.before'=>'The  Date must be  before Contract End Date',
        'employee_contract_end_date.after'=>'The  Date must be  after Contract Start Date', 

        

            'employee_primary_skills.required' =>'Enter Primary Skills',
            'employee_first_name.required' =>'Enter First Name',
            'employee_last_name.required' =>'Enter Last Name',
            'employee_code.required' => 'Enter Employee Code',
            'employee_date_of_birth.required' => 'Enter Employee Date of Birth',
            'employee_gender.required' => 'Enter Employee Gender',
            'employee_aadhar_number.required' => 'Enter Employee Aadhar Number',
            'employee_marital_status.required' => 'Select Marital Status',
            'employee_nationality.required' => 'Select Nationality', 
            'employee_recent_photo.required' => 'Choose Recent Photo of Employee', 
            'employee_recent_photo.mimes' => 'Choose the Photo With png,jpeg,jpg', 
            'employee_recent_photo.max' => 'Only 4Mb Size of image is required', 

            'employee_ug_college_name.required' => 'Enter College Name',
            'employee_ug_degree_name.required' => 'Enter Degree Name',
            'employee_ug_year_of_passing.required' => 'Enter Year of Passing ',
            'employee_ug_percentage_of_marks.required' => 'Enter Percentage of Marks',

            'employee_pg_college_name.required.*' => 'Enter Percentage of Marks',


            'employee_door_no.required' => 'Enter Door Number',
            'employee_street_name.required' =>'Enter Street Name',

            'employee_city.required' =>'Enter City Name',
            'employee_state.required' =>'Enter State Name',
            'employee_country.required' =>'Select Country Name',
            'employee_zip_code.required' =>'Enter Zip Code',

            'employee_home_telephone.required' => 'Enter Telephone Number',
            'employee_home_telephone.numeric' => 'Only Numbers are accepted',

            'employee_mobile_number.required' => 'Enter Mobile Number',
            'employee_mobile_number.numeric' => 'Only Numbers are accepted',

            'employee_email_id.required' => 'Enter Email id',
            'employee_email_id.email' => 'Enter Valid Email',

            'employee_email_id.unique' => 'Email Has Already Added',

            


        ];

        return $messages;
    }

}
