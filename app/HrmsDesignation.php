<?php

namespace App;

use App\HrmsBaseModel;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

/**
 * Class HrmsDesignation
 *
 * @package App
*/

class HrmsDesignation extends HrmsBaseModel
{
	use SoftDeletes;

    protected $guarded = ['id'];

     /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'hrms_designations';

    /**
     * The primary key associated with the model.
     *
     * @var integer
     */

    protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['designation_name','department_id','reporting_to','company_id','created_by','updated_by'];

     /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [ ];

    /**
     * @var array
     */
    protected $dates = ['created_at','updated_at','deleted_at'];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * The storage format of the model's date columns.
     *
     * @var string
     */
    protected $dateFormat ='';

    /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection ='';


}
