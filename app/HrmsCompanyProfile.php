<?php

namespace App;
use App\HrmsBaseModel;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

/**
 * Class HrmsCompanyProfile
 *
 * @package App
*/

class HrmsCompanyProfile extends HrmsBaseModel
{
	use SoftDeletes;

    protected $guarded = ['id'];

     /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'hrms_company_profiles';

    /**
     * The primary key associated with the model.
     *
     * @var integer
     */

    protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [ 'company_name',
                            'company_registration_number',
                            'company_gst_in',
                            'company_address_line_one',
                            'company_address_line_two',
                            'company_address_city',
                            'company_address_state',
                            'company_address_country',
                            'company_address_zip_code',
                            'company_email',
                            'company_mobile_number',
                            'company_phone_number',
                            'company_fax_number',
                            'company_website_url',
                            'company_contact_person_full_name',
                            'company_contact_person_designation',
                            'company_contact_person_email_id',
                            'company_contact_person_phone_number',
                            'created_by',
                            'updated_by'
                        ];

     /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [ ];

    /**
     * @var array
     */
    protected $dates = ['created_at','updated_at','deleted_at'];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * The storage format of the model's date columns.
     *
     * @var string
     */
    protected $dateFormat ='';

    /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection ='';


}
