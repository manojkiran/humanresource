<?php

namespace App;

use App\HrmsBaseModel;
use App\HrmsEmployeeGrade;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use App\Models\Comman\Traits\UserStampsTrait;

/**
 * Class HrmsEmployeeSalaryGrade
 *
 * @package App
*/

class HrmsEmployeeSalaryGrade extends HrmsBaseModel
{
	use SoftDeletes;
    use UserStampsTrait;

    protected $guarded = ['id'];

     /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'hrms_employee_salary_grades';

    /**
     * The primary key associated with the model.
     *
     * @var integer
     */

    protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
                            'employee_grade_id',
                            'employee_grade_salary_medical_reimbursement',
                            'employee_grade_salary_mobile_phone_allowance',
                            'employee_grade_salary_field_allowance',
                            'employee_grade_salary_monthly_reimbursement',
                            'created_by',
                            'updated_by'
                        ];

     /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [ ];

    /**
     * @var array
     */
    protected $dates = ['created_at','updated_at','deleted_at'];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * The storage format of the model's date columns.
     *
     * @var string
     */
    protected $dateFormat ='';

    /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection ='';

    public function gradeName()
    {
        return $this->belongsTo(HrmsEmployeeGrade::class,'employee_grade_id');
    }


}
