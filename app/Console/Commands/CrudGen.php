<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Str;

class CrudGen extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'crud:gen {controlName : Class (singular) for example User}';


    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create CRUD operations';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {


        $controlName = $this->argument('controlName');

        $this->generateModel($controlName);

        $this->generateView($controlName);

        $this->generateController($controlName);

        $this->generateMigration($controlName);

        $this->generateFaker($controlName);




        $this->generateStoreRequest($controlName);

        $this->publishApiRoutes($controlName);

        $this->publishWebRoutes($controlName);
    }

    protected function prepareMigrationFileName($controlName='')
    {
        $fileName = "";
        $fileName .=date('Y_m_d_His');
        $fileName .="_create_";
        $fileName .=$this->fixForTableName($controlName);
        $fileName .="_table";
        return $fileName;
    }


    protected function generateMigration($controlName)
    {
        $migrationFileName = $this->prepareMigrationFileName($controlName);

        $migrationTemplateStubNames = ['{{tableName}}','{{className}}'];

        $migrationTemplateRealNames = [$this->fixForTableName($controlName),"Create".Str::plural($controlName)."Table"];

        $migrationTemplate = $this->replaceInStub($migrationTemplateStubNames,$migrationTemplateRealNames,$this->getStubName('Migration'));

        file_put_contents(database_path("migrations/{$migrationFileName}.php"), $migrationTemplate);
    }


    protected function generateFromBlade($controlName)
    {
        file_put_contents(resource_path("views/{$controlName}/_form.blade.php"),$this->getStubName('Form'));
    }

    protected function generateShowBlade($controlName)
    {
        $modelTemplateStubNames = ['{{Title}}','{{modelNamePluralLowerCase}}'];


        $modelTemplateRealNames = [$controlName,$this->fixForRouteUrl($controlName)];


        $modelTemplate = $this->replaceInStub($modelTemplateStubNames,$modelTemplateRealNames,$this->getStubName('Show'));


        file_put_contents(resource_path("views/{$controlName}/show.blade.php"),$modelTemplate);
    }

    protected function generateFaker($controlName)
    {
        $modelTemplateStubNames = ['{{className}}'];


        $modelTemplateRealNames = [ucfirst($controlName)];


        $modelTemplate = $this->replaceInStub($modelTemplateStubNames,$modelTemplateRealNames,$this->getStubName('Faker'));

        $fakerFileName =  ucfirst($controlName)."Factory";


        file_put_contents(database_path("factories/{$fakerFileName}.php"), $modelTemplate);
    }


    protected function generateModel($controlName)
    {
        $modelTemplateStubNames = ['{{modelName}}','{{tableName}}'];


        $modelTemplateRealNames = [ucfirst($controlName),$this->fixForTableName($controlName)];


        $modelTemplate = $this->replaceInStub($modelTemplateStubNames,$modelTemplateRealNames,$this->getStubName('Model'));


        file_put_contents(app_path("/{$this->fixForModel($controlName)}.php"), $modelTemplate);
    }

    protected function generateView($controlName='')
    {

        $this->generateIndexBlade($controlName);
        $this->generateCreateBlade($controlName);
        $this->generateEditBlade($controlName);
        $this->generateShowBlade($controlName);
        $this->generateFromBlade($controlName);


    }



    protected function generateController($controlName)
    {
        $controllerTemplateStubNames  =[ '{{modelName}}', '{{modelNamePluralLowerCase}}', '{{modelNameSingularLowerCase}}' ];

        $controllerTemplateRealNames  =[$controlName,strtolower(str_plural($controlName)),strtolower($controlName)];

        $controllerTemplate = $this->replaceInStub($controllerTemplateStubNames,$controllerTemplateRealNames,$this->getStubName('Controller'));



        file_put_contents(app_path("/Http/Controllers/{$controlName}Controller.php"), $controllerTemplate);
    }



    protected function generateStoreRequest($controlName)
    {


        $storeRequestTemplateStubNames = ['{{className}}','{{gateName}}'];
        $storeRequestTemplateRealNames = [$controlName,strtolower($controlName)];

        $storeRequestTemplate = $this->replaceInStub($storeRequestTemplateStubNames,$storeRequestTemplateRealNames,$this->getStubName('StoreRequest'));


        if(!File::isDirectory($this->requestFolder()))
        {
            File::makeDirectory($this->requestFolder(), 0777, true, true);
        }

        file_put_contents(app_path("/Http/Requests/{$controlName}StoreRequest.php"), $storeRequestTemplate);
    }

    protected function getStubName($type)
    {
        return file_get_contents(resource_path("stubs/$type.stub"));
    }

    protected function replaceInStub($findFor='',$replaceWith='',$paragraphText='')
    {
        $newPhrase = str_replace($findFor, $replaceWith, $paragraphText);
        return $newPhrase;
    }
    protected function publishApiRoutes($controlName='')
    {
        $lineBreak="\n";

        File::append(base_path('routes/api.php'), 'Route::resource(\'' . str_plural(strtolower($controlName)) . "', '{$controlName}Controller');$lineBreak");
    }
    protected function publishWebRoutes($controlName='')
    {

        $this->startComment($controlName);
        $this->generateIndexRoute($controlName);
        $this->generateCreateRoute($controlName);
        $this->generateSaveRoute($controlName);
        $this->generateShowRoute($controlName);
        $this->generateEditRoute($controlName);
        $this->generateUpdateRoute($controlName);
        $this->generateDeleteRoute($controlName);
        $this->endComment($controlName);
    }
    protected function startComment($controlName='')
    {
        $lineBreak="\n";
        $commentStart = "//Start Routes For ".$controlName;
        File::append(base_path('routes/web.php'), $commentStart.$lineBreak);
    }

    protected function endComment($controlName='')
    {
        $lineBreak="\n";
        $commentStart = "//End Routes For ".$controlName;
        File::append(base_path('routes/web.php'), $commentStart.$lineBreak);
    }

    protected function generateIndexRoute($controlName='')
    {


        $indexRoute ='';
        $indexRoute .= "Route::";
        $indexRoute .= "get";
        $indexRoute .= "('/";
        $indexRoute .= $this->fixForRouteUrl($controlName);
        $indexRoute .="',  ";
        $indexRoute .="'";
        $indexRoute .= $controlName."Controller@";
        $indexRoute .="index";
        $indexRoute .="')";
        $indexRoute .= "->name('";
        $indexRoute .= $this->fixForRouteUrl($controlName).".";
        $indexRoute .="index";
        $indexRoute .="'";
        $indexRoute .=");";
        $this->appendToFile(base_path('routes/web.php'),$indexRoute);


    }

    protected function generateCreateRoute($controlName='')
    {


        $createRoute ='';
        $createRoute .= "Route::";
        $createRoute .= "get";
        $createRoute .= "('/";
        $createRoute .= $this->fixForRouteUrl($controlName);
        $createRoute .="/create',  ";
        $createRoute .="'";
        $createRoute .= $controlName."Controller@";
        $createRoute .="create";
        $createRoute .="')";
        $createRoute .= "->name('";
        $createRoute .= $this->fixForRouteUrl($controlName).".";
        $createRoute .="create";
        $createRoute .="'";
        $createRoute .=");";

        $this->appendToFile(base_path('routes/web.php'),$createRoute);


    }

    protected function generateSaveRoute($controlName='')
    {


        $saveRoute ='';
        $saveRoute .= "Route::";
        $saveRoute .= "post";
        $saveRoute .= "('/";
        $saveRoute .= $this->fixForRouteUrl($controlName);
        $saveRoute .="',  ";
        $saveRoute .="'";
        $saveRoute .= $controlName."Controller@";
        $saveRoute .="store";
        $saveRoute .="')";
        $saveRoute .= "->name('";
        $saveRoute .= $this->fixForRouteUrl($controlName).".";
        $saveRoute .="store";
        $saveRoute .="'";
        $saveRoute .=");";
        $this->appendToFile(base_path('routes/web.php'),$saveRoute);
    }

    protected function generateShowRoute($controlName='')
    {

        $showRoute ='';

        $showRoute .= "Route::";
        $showRoute .= "get";
        $showRoute .= "('/";
        $showRoute .= $this->fixForRouteUrl($controlName);
        $showRoute .="/{";
        $showRoute .= $this->fixForRouteID($controlName);
        $showRoute .="}',  '";
        $showRoute .= $controlName."Controller@";
        $showRoute .="show";
        $showRoute .="')";
        $showRoute .= "->name('";
        $showRoute .= $this->fixForRouteUrl($controlName).".";
        $showRoute .="show";
        $showRoute .="'";
        $showRoute .=");";
        $this->appendToFile(base_path('routes/web.php'),$showRoute);

    }

    protected function generateEditRoute($controlName='')
    {


        $editRoute ='';

        $editRoute .= "Route::";
        $editRoute .= "get";
        $editRoute .= "('/";
        $editRoute .= $this->fixForRouteUrl($controlName);
        $editRoute .="/{";
        $editRoute .= $this->fixForRouteID($controlName);
        $editRoute .="}/edit',  '";
        $editRoute .= $controlName."Controller@";
        $editRoute .="edit";
        $editRoute .="')";
        $editRoute .= "->name('";
        $editRoute .= $this->fixForRouteUrl($controlName).".";
        $editRoute .="edit";
        $editRoute .="'";
        $editRoute .=");";

        $this->appendToFile(base_path('routes/web.php'),$editRoute);
    }

    protected function generateUpdateRoute($controlName='')
    {

        $updateRoute ='';

        $updateRoute .= "Route::";
        $updateRoute .= "put";
        $updateRoute .= "('/";
        $updateRoute .= $this->fixForRouteUrl($controlName);
        $updateRoute .="/{";
        $updateRoute .= $this->fixForRouteID($controlName);
        $updateRoute .="}',  '";
        $updateRoute .= $controlName."Controller@";
        $updateRoute .="update";
        $updateRoute .="')";
        $updateRoute .= "->name('";
        $updateRoute .= $this->fixForRouteUrl($controlName).".";
        $updateRoute .="update";
        $updateRoute .="'";
        $updateRoute .=");";

        $this->appendToFile(base_path('routes/web.php'),$updateRoute);
    }

    protected function generateDeleteRoute($controlName='')
    {


        $deleteRoute ='';

        $deleteRoute .= "Route::";
        $deleteRoute .= "delete";
        $deleteRoute .= "('/";
        $deleteRoute .= $this->fixForRouteUrl($controlName);
        $deleteRoute .="/{";
        $deleteRoute .= $this->fixForRouteID($controlName);
        $deleteRoute .="}',  '";
        $deleteRoute .= $controlName."Controller@";
        $deleteRoute .="destroy";
        $deleteRoute .="')";
        $deleteRoute .= "->name('";
        $deleteRoute .= $this->fixForRouteUrl($controlName).".";
        $deleteRoute .="destroy";
        $deleteRoute .="'";
        $deleteRoute .=");";

        $this->appendToFile(base_path('routes/web.php'),$deleteRoute);
    }


    protected function generateIndexBlade($controlName)
    {
        $folderWithFileName = resource_path("views\\$controlName");

        if(!File::isDirectory($folderWithFileName))
        {
            File::makeDirectory($folderWithFileName, 0777, true, true);
        }
        $indexTemplateStubNames = ['{{Title}}','{{modelNamePluralLowerCase}}'];


        $indexTemplateRealNames = [$controlName,strtolower(str_plural($controlName))];


        $modelTemplate = $this->replaceInStub($indexTemplateStubNames,$indexTemplateRealNames,$this->getStubName('Index'));


        file_put_contents(resource_path("views/{$controlName}/index.blade.php"), $modelTemplate);
    }

    protected function generateCreateBlade($controlName)
    {
        $indexTemplateStubNames = ['{{Title}}','{{modelNamePluralLowerCase}}'];


        $indexTemplateRealNames = [$controlName,strtolower(str_plural($controlName))];


        $modelTemplate = $this->replaceInStub($indexTemplateStubNames,$indexTemplateRealNames,$this->getStubName('Create'));


        file_put_contents(resource_path("views/{$controlName}/create.blade.php"), $modelTemplate);
    }

    protected function generateEditBlade($controlName)
    {
        $indexTemplateStubNames = ['{{Title}}','{{modelNamePluralLowerCase}}'];


        $indexTemplateRealNames = [$controlName,$this->fixForRouteUrl($controlName)];


        $modelTemplate = $this->replaceInStub($indexTemplateStubNames,$indexTemplateRealNames,$this->getStubName('Edit'));


        file_put_contents(resource_path("views/{$controlName}/edit.blade.php"), $modelTemplate);
    }


    protected function fixForModel($value='')
    {
        return ucfirst($value);
    }

    protected function fixForTableName($value='')
    {

        return Str::plural(strtolower(Str::snake($value)));

    }
    protected function requestFolder()
    {
        return app_path('/Http/Requests');
    }
    protected function fixForRouteID($controlName='')
    {
        return  Str::singular(strtolower($controlName));

    }
    protected function fixForRouteUrl($controlName='')
    {
        return str_plural(strtolower($controlName));
    }
    protected function appendToFile($filePath='',$text='')
    {
        File::append($filePath,$text."\n");
    }

}
