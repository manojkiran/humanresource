<?php

namespace App;

use App\HrmsBaseModel;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use App\Models\Comman\Traits\UserStampsTrait;

/**
 * Class HrmsLeaveSettingLeaveType
 *
 * @package App
*/

class HrmsLeaveSettingLeaveType extends HrmsBaseModel
{
	use SoftDeletes;
    use UserStampsTrait;

    protected $guarded = ['id'];

     /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'hrms_leave_setting_leave_types';

    /**
     * The primary key associated with the model.
     *
     * @var integer
     */

    protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['leave_setting_name','created_by','updated_by'];

     /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [ ];

    /**
     * @var array
     */
    protected $dates = ['created_at','updated_at','deleted_at'];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * The storage format of the model's date columns.
     *
     * @var string
     */
    protected $dateFormat ='';

    /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection ='';


}
