<?php

namespace App;

use App\HrmsBaseModel;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use App\Models\Comman\Traits\UserStampsTrait;

/**
 * Class HrmsEmployeeEducation
 *
 * @package App
*/

class HrmsEmployeeEducation extends HrmsBaseModel
{
	use SoftDeletes;
    use UserStampsTrait;

    protected $guarded = ['id'];

     /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'hrms_employee_educations';

    /**
     * The primary key associated with the model.
     *
     * @var integer
     */

    protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
                        'employee_id',
                        'employee_sslc_school_name',
                        'employee_sslc_year_of_passing',
                        'employee_sslc_percentage_of_marks',
                        'employee_hsc_school_name',
                        'employee_hsc_year_of_passing',
                        'employee_hsc_percentage_of_marks',
                        'employee_ug_degree_name',
                        'employee_ug_college_name',
                        'employee_ug_year_of_passing',
                        'employee_ug_percentage_of_marks',
                        'created_by',
                        'updated_by'
                    ];

     /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [ ];

    /**
     * @var array
     */
    protected $dates = ['created_at','updated_at','deleted_at'];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * The storage format of the model's date columns.
     *
     * @var string
     */
    protected $dateFormat ='';

    /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection ='';


}
