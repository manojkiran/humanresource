<?php

namespace App\Helpers;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Gate;
use App\HrmsEmployeePgEducation;
use App\HrmsEmployee;
use App\HrmsEmployeeExperience;

class BladeHelper
{
    /**
     * @param int $userId userId
     *
     * @return string
     */
    public static function generateFullName($userId='')
    {
        $empColl = HrmsEmployee::where('id',$userId)
        ->select('employee_first_name','employee_middle_name','employee_last_name')
        ->get();
        

          if ($empColl->isEmpty()) 
            {
                return"";   
            }
            foreach ($empColl as $empCollValue)
            {
                $firstName =  $empCollValue->employee_last_name;
                $middleName = $empCollValue->employee_middle_name;
                $lastName = $empCollValue->employee_last_name;

            }


        

        $fullName ='';
        $fullName .= empty($firstName) || $firstName == null ? '' : $firstName;
        $fullName .= empty($middleName) || $middleName == null ? '' : $middleName.'.';
        $fullName .= empty($lastName) || $lastName == null ? '' : $lastName;
        return $fullName;
    }
    /**
    * @function     tableActionButtons
    * @author       Manojkiran <manojkiran10031998@gmail.com>
    * @param        string  $fullUrl
    * @param        integer  $id
    * @param        string  $titleValue
    * @param        array  $buttonActions
    * @usage        Generates the buttons
    * @version      1.0
    **/
    /*
    NOTE:

    if you want to show tooltip you need the JQUERY JS and tooltip Javascript

    if you are not well in JavaScript Just Use My Function toolTipScript()



    |--------------------------------------------------------------------------
    | Generates the buttons
    |--------------------------------------------------------------------------
    |Generates the buttons while displaying the table data in laravel
    |when the project is bigger and if you are laravel expert you this.
    |But if you are the learner just go with basic
    |
    |Basically It Will generate the buttons for show edit delete record with the default
    |Route::resource('foo',FooController);
    |
    |//requirements
    |
    |//bootstrap --version (4.1.3)
    |//  <link rel="stylesheet"href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="" crossorigin="">
    |//fontawesome --version (5.6.0(all))
    |//<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.0/css/all.css" integrity="" crossorigin="">
    |
    |if you want to show tooltip you nee the jquery and tooltip you need these js and toottipscript javascript or use my function toolTipScript
    |
    |//jquery
    |// <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    |//popper js
    |// <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
    |//bootstrap js
    |// <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
    |
    |usage
    |option1:
    |tableActionButtons(url()->full(),$item->id,$item->name);
    |this will generate all the buttons
    |
    |option2:
    |tableActionButtons(url()->full(),$item->id,$item->name,['edit',delete]);
    |this will generate edit and delete the buttons
    |
    |option3:
    |tableActionButtons(url()->full(),$item->id,$item->name,['edit',delete,delete],'group');
    |this will generate all the buttons with button grouping
    |
    |option4:
    |tableActionButtons(url()->full(),$item->id,$item->name,['show','edit','delete'],'dropdown');
    |this will generate all the buttons with button dropdown
    |
    */

    public static  function tableActionButtons($fullUrl,$id,$titleValue,$buttonActions = ['show', 'edit', 'delete'],$buttonOptions='')
    {


            //Value of the post Method
            $postMethod = 'POST';
            //if the application is laravel then csrf is used

            $token = csrf_token();

            //NON laravel application
            // if (function_exists('csrf_token'))
            // {
            //   $token = csrf_token();
            // }elseif (!function_exists('csrf_token'))
            // //else if the mcrypt id is used if the function exits
            //     {
            //         if (function_exists('mcrypt_create_iv'))
            //         {
            //             // if the mcrypt_create_iv id is used if the function exits the set the token
            //             $token = bin2hex(mcrypt_create_iv(32, MCRYPT_DEV_URANDOM));
            //         }
            //         else{
            //             // elseopenssl_random_pseudo_bytes is used if the function exits the set the token
            //             $token = bin2hex(openssl_random_pseudo_bytes(32));
            //         }
            //     }

            //action button Value
            //(url()->full()) will pass the current browser url to the function[only aplicable in laravel]
            $urlWithId  =$fullUrl.'/'.$id;
            //Charset UsedByFrom
            $charset = 'UTF-8';

            // Start Delete Button Arguments
            //title for delete functions
            $deleteFunctionTitle = 'Delete';
            //class name for the deletebutton
            $deleteButtonClass = 'btn-delete btn btn-xs btn-danger';
            //Icon for the delete Button
            $deleteButtonIcon = 'fa fa-trash';
            //text for the delete button
            $deleteButtonText  = '';
            //dialog Which needs to be displayes while deleting the record
            $deleteConfirmationDialog = 'Are You Sure you wnat to delete ' . $titleValue;

            $deleteButtonTooltopPostion = 'top';
            // End Delete Button Arguments


             // Start Edit Button Arguments
            //title for Edit functions
            $editFunctionTitle = 'Edit';
            $editButtonClass = 'btn-delete btn btn-xs btn-primary';
            //Icon for the Edit Button
            $editButtonIcon = 'fa fa-edit';
            //text for the Edit button
            $editButtonText  = '';
            $editButtonTooltopPostion = 'top';
            // End Edit Button Arguments


            // Start Show Button Arguments
            //title for Edit functions
            $showFunctionTitle = 'Show';
            $showButtonClass = 'btn-delete btn btn-xs btn-primary';
            //Icon for the Show Button
            $showButtonIcon = 'fa fa-eye';
            //text for the Show button
            $showButtonText  = '';
            $showButtonTooltopPostion = 'top';
            // End Show Button Arguments
            //Start Arguments for DropDown Buttons
            $dropDownButtonName = 'Actions';
            //End Arguments for DropDown Buttons




            $showButton = '';
            $showButton .='
                <a href="'.$fullUrl.'/'.$id.'"class="'.$showButtonClass.'"data-toggle="tooltip"data-placement="'.$showButtonTooltopPostion.'"title="'.$showFunctionTitle.'-'.$titleValue.'">
                    <i class="'.$showButtonIcon.'"></i> '.$showButtonText.'
                </a>
            ';

            $editButton ='';
            $editButton .='
                    <a href="'.$urlWithId.'/edit'.'"class="'.$editButtonClass.'"data-toggle="tooltip"data-placement="'.$editButtonTooltopPostion.'" title="'.$editFunctionTitle.'-'.$titleValue.'">
                        <i class="'.$editButtonIcon.'"></i> '.$editButtonText.'
                    </a>
                ';


            $deleteButton='';
            $deleteButton .='
                    <form id="form-delete-row' . $id . '"  method="'.$postMethod.'" action="'.$urlWithId.'" accept-charset="'.$charset.'"style="display: inline" onSubmit="return confirm(&quot;'.$deleteConfirmationDialog.'&quot;)">
                        <input name="_method" type="hidden" value="DELETE">
                        <input name="_token" type="hidden" value="'.$token.'">
                        <input name="_id" type="hidden" value="'.$id.'">
                        <button type="submit"class="'.$deleteButtonClass.'"data-toggle="tooltip"data-placement="'.$deleteButtonTooltopPostion.'" title="'.$deleteFunctionTitle.'-'.$titleValue.'">
                            <i class="'.$deleteButtonIcon.'"></i>'.$deleteButtonText.'
                        </button>
                    </form>
                ';


        // $deleteButton = "<a href='index.php?page=de_activate_organization&action_id=$id' onClick=\"return confirm('Are you Sure to De Activate?')\"><span class='label label-success'>" ."Test" . "</span></a>";



            $actionButtons = '';

            foreach ($buttonActions as $buttonAction)
            {
                if ($buttonAction == 'show')
                {
                    $actionButtons .= $showButton;
                }
                if ($buttonAction == 'edit')
                {
                    $actionButtons .= $editButton;
                }
                if ($buttonAction == 'delete')
                {
                    $actionButtons .= $deleteButton;
                }
            }
            if (empty($buttonOptions))
            {
                return  $actionButtons;
            }
            elseif (!empty($buttonOptions))
            {
                if ($buttonOptions == 'group')
                {

                    $buttonGroup = '<div class="btn-group" role="group" aria-label="">
                    '.$actionButtons.'
                    </div>';
                    return $buttonGroup;
                }elseif($buttonOptions == 'dropdown')
                {
                    $dropDownButton  =
                        '<div class="dropdown">
                          <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            '.$dropDownButtonName.'
                          </button>
                          <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                          '.$actionButtons.'
                          </div>
                        </div>
                        ';
                        return $dropDownButton;
                }else
                {
                    return  'only <code>group</code> and <code>dropdown</code> is Available ';
                }

            }
        }

    /**
    created on 24/12/2018
    Updatd on on
    * @function     tableActionButtonsWithGate
    * @author       Manojkiran <manojkiran10031998@gmail.com>
    * @param        string  $fullUrl
    * @param        integer  $id
    * @param        string  $titleValue
    * @param        array  $buttonActions
    * @usage        Generates the buttons
    * @version      1.0
    * @createdOn     24/12/2018
    **/
    /*
    NOTE:

    if you want to show tooltip you need the JQUERY JS and tooltip Javascript

    if you are not well in JavaScript Just Use My Function toolTipScript()



    |--------------------------------------------------------------------------
    | Generates the buttons
    |--------------------------------------------------------------------------
    |Generates the buttons while displaying the table data in laravel
    |when the project is bigger and if you are laravel expert you this.
    |But if you are the learner just go with basic
    |
    |Basically It Will generate the buttons for show edit delete record with the default
    |Route::resource('foo',FooController);
    |
    |//requirements
    |
    |//bootstrap --version (4.1.3)
    |//  <link rel="stylesheet"href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="" crossorigin="">
    |//fontawesome --version (5.6.0(all))
    |//<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.0/css/all.css" integrity="" crossorigin="">
    |
    |if you want to show tooltip you nee the jquery and tooltip you need these js and toottipscript javascript or use my function toolTipScript
    |
    |//jquery
    |// <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    |//popper js
    |// <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
    |//bootstrap js
    |// <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
    |
    |usage
    |option1:
    |tableActionButtons(url()->full(),$item->id,$item->name);
    |this will generate all the buttons
    |
    |option2:
    |tableActionButtons(url()->full(),$item->id,$item->name,['edit',delete]);
    |this will generate edit and delete the buttons
    |
    |option3:
    |tableActionButtons(url()->full(),$item->id,$item->name,['edit',delete,delete],'group');
    |this will generate all the buttons with button grouping
    |
    |option4:
    |tableActionButtons(url()->full(),$item->id,$item->name,['show','edit','delete'],'dropdown');
    |this will generate all the buttons with button dropdown
    |
    */

    public static  function tableActionButtonsWithGate($gateKey='',$fullUrl,$id,$titleValue,$buttonActions = ['show', 'edit', 'delete'],$buttonOptions='')
    {


            //Value of the post Method
            $postMethod = 'POST';
            //if the application is laravel then csrf is used
            if (function_exists('csrf_token'))
            {
              $token = csrf_token();
            }elseif (!function_exists('csrf_token'))
            //else if the mcrypt id is used if the function exits
                {
                    if (function_exists('mcrypt_create_iv'))
                    {
                        // if the mcrypt_create_iv id is used if the function exits the set the token
                        $token = bin2hex(mcrypt_create_iv(32, MCRYPT_DEV_URANDOM));
                    }
                    else{
                        // elseopenssl_random_pseudo_bytes is used if the function exits the set the token
                        $token = bin2hex(openssl_random_pseudo_bytes(32));
                    }
                }

            //action button Value
            //(url()->full()) will pass the current browser url to the function[only aplicable in laravel]
            $urlWithId  =$fullUrl.'/'.$id;
            //Charset UsedByFrom
            $charset = 'UTF-8';

            // Start Delete Button Arguments
            //title for delete functions
            $deleteFunctionTitle = 'Delete';
            //class name for the deletebutton
            $deleteButtonClass = 'btn-delete btn btn-xs btn-danger';
            //Icon for the delete Button
            $deleteButtonIcon = 'fa fa-trash flat-shadow';
            //text for the delete button
            $deleteButtonText  = ' ';
            //dialog Which needs to be displayes while deleting the record
            $deleteConfirmationDialog = 'Are You Sure you wnat to delete '.$titleValue;

            $deleteButtonTooltopPostion = 'top';
            // End Delete Button Arguments


             // Start Edit Button Arguments
            //title for Edit functions
            $editFunctionTitle = 'Edit';
            $editButtonClass = 'btn-delete btn btn-xs btn-primary';
            //Icon for the Edit Button
            $editButtonIcon = 'fa fa-edit flat-shadow';
            //text for the Edit button
            $editButtonText  = '';
            $editButtonTooltopPostion = 'top';
            // End Edit Button Arguments


            // Start Show Button Arguments
            //title for Edit functions
            $showFunctionTitle = 'Show';
            $showButtonClass = 'btn-delete btn btn-xs btn-primary';
            //Icon for the Show Button
            $showButtonIcon = 'fa fa-eye flat-shadow';
            //text for the Show button
            $showButtonText  = '';
            $showButtonTooltopPostion = 'top';
            // End Show Button Arguments
            //Start Arguments for DropDown Buttons
            $dropDownButtonName = 'Actions';
            //End Arguments for DropDown Buttons

            //Start Defining the gate Keys
            $showGate  = 'view.'.$gateKey;
            $editGate  = 'edit.'.$gateKey;
            $deleteGate  = 'delete.'.$gateKey;
            //End Defining the gate Keys

            // <!-- <button type="button" class="btn btn-success">
            //             <i class="fa fa-bookmark flat-shadow"></i> Bookmark
            //           </button> -->




            $showButton = '';
            $showButton .='
                <a href="'.$fullUrl.'/'.$id.'"class="'.$showButtonClass.'"data-toggle="tooltip"data-placement="'.$showButtonTooltopPostion.'"title="'.$showFunctionTitle.'-'.$titleValue.'">
                    <i class="'.$showButtonIcon.'"></i> '.$showButtonText.'
                </a>
            ';

            $editButton ='';
            $editButton .='
                    <a href="'.$urlWithId.'/edit'.'"class="'.$editButtonClass.'"data-toggle="tooltip"data-placement="'.$editButtonTooltopPostion.'" title="'.$editFunctionTitle.'-'.$titleValue.'">
                        <i class="'.$editButtonIcon.'"></i> '.$editButtonText.'
                    </a>
                ';

            $deleteButton='';
            $deleteButton .='
                    <form id="form-delete-row' . $id . '"  method="'.$postMethod.'" action="'.$urlWithId.'" accept-charset="'.$charset.'"style="display: inline" onSubmit="return confirm(&quot;'.$deleteConfirmationDialog.'&quot;)">
                        <input name="_method" type="hidden" value="DELETE">
                        <input name="_token" type="hidden" value="'.$token.'">
                        <input name="_id" type="hidden" value="'.$id.'">
                        <button type="submit"class="'.$deleteButtonClass.'"data-toggle="tooltip"data-placement="'.$deleteButtonTooltopPostion.'" title="'.$deleteFunctionTitle.'-'.$titleValue.'">
                            <i class="'.$deleteButtonIcon.'"></i>'.$deleteButtonText.'
                        </button>
                    </form>
                ';

            $actionButtons = '';

            foreach ($buttonActions as $buttonAction)
            {
                if ($buttonAction == 'show' && Gate::allows($showGate))
                {
                    $actionButtons .= $showButton;
                }
                if ($buttonAction == 'edit' && Gate::allows($editGate))
                {
                    $actionButtons .= $editButton;
                }
                if ($buttonAction == 'delete' && Gate::allows($deleteGate))
                {

                    $actionButtons .= $deleteButton;

                }
            }
            if (empty($buttonOptions))
            {
                return  $actionButtons;
            }
            elseif (!empty($buttonOptions))
            {
                if ($buttonOptions == 'group')
                {

                    $buttonGroup = '<div class="btn-group" role="group" aria-label="">
                    '.$actionButtons.'
                    </div>';
                    return $buttonGroup;
                }elseif($buttonOptions == 'dropdown')
                {
                    $dropDownButton  =
                        '<div class="dropdown">
                          <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            '.$dropDownButtonName.'
                          </button>
                          <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                          '.$actionButtons.'
                          </div>
                        </div>
                        ';
                        return $dropDownButton;
                }else
                {
                    return  'only <code>group</code> and <code>dropdown</code> is Available ';
                }

            }
        }

        public static function toolTipScript()
        {
            $script='';
            $script='
            <script>
            $(function () {
                $('."'[data-toggle=".''.'"tooltip"]'.''."').tooltip()".'
            })
            </script>';
            return $script;
        }
        public static function getLabelForStatus($statusValue='')
        {


           $activeLabel = '';
           $activeLabel .= '<span class="label label-success ">'.ucfirst($statusValue).'</span>';

           $inActiveLabel = '';
           $inActiveLabel .= '<span class="label label-danger">'.ucfirst($statusValue).'</span>';

           $onHoldLabel = '';
           $onHoldLabel .= '<span class="label label-warning">'.ucfirst($statusValue).'</span>';

           $cancelledLabel = '';
           $cancelledLabel .= '<span class="label label-primary">'.ucfirst($statusValue).'</span>';



           if ($statusValue == 'Active' || $statusValue == 'Approved')
           {
               return $activeLabel;
           }elseif ($statusValue == 'InActive' || $statusValue == 'Rejected')
           {
               return $inActiveLabel;
           }elseif ($statusValue == 'OnHold' || $statusValue == 'Pending')
           {
                return $onHoldLabel;
           }elseif ($statusValue == 'Cancelled')
           {
               return $cancelledLabel;
           }

        }

        public static function showAsFormattedDate($date='')
        {

            $date = strtotime($date);

            $newformat = 'd-M-Y';

            return date($newformat, $date);

        }

        public static function getDifferenceOfTwoDates($fromDate='',$toDate='')
        {
            $dateArgOne=date_create($fromDate);
            $dateArgTwo=date_create($toDate);
            $diff=date_diff($dateArgOne,$dateArgTwo);
            return $diff->format("%a") ;
        }

     public static   function removeUnderScoreAndFromat($string='')
        {
            $toArray = explode('_',$string);
            $ucFirst  = array_map('ucfirst', $toArray);
            $result = implode(" ", $ucFirst);
            return $result;
        }
        public static function getLeaveStatus($value='')
        {
            if ($value == null || empty($value)) {
                return '';
            }
            else
            {
                switch ($value)
                {
                    case '1':
                        return 'Forenoon';
                        break;
                    case '2':
                        return 'Afternoon';
                        break;

                    default:
                        return '';
                        break;
                }
            }
        }

      public static  function getTimeDifference($formTime='',$toTime='')
        {
                $hoursText = ' Hours';
                $minutesText  = ' Minutes';
                $hourText = ' Hour';
                $minuteText  = ' Minute';

            $seconds  = strtotime($toTime) - strtotime($formTime);

            $secondsToMinutes = $seconds/60;

            if ($secondsToMinutes >= 60)
            {
                $hours  = round($secondsToMinutes/60);
                $minutes  = $secondsToMinutes%60;
                $result = $hours.($hours <= 1 ? $hourText : $hoursText ).(empty($minutes) ? '' :' and '.$minutes.($minutes <= 1 ? $minuteText : $minutesText));
                return $result;
            }else
            {
                return $secondsToMinutes.($secondsToMinutes <= 1 ? $minuteText : $minutesText);
            }
        }

        public static function generateEmployeeSalary($grossSalary='')
                            {

                                if(empty($grossSalary) || $grossSalary == null || !is_numeric($grossSalary))
                                {
                                    $finalResultArray =
                                                [
                                                    'basicPay' => '',
                                                    'hra' => '',
                                                    'cca' => '',
                                                    'conveyanceAllowance'=> '',
                                                    'pf' => '',
                                                    'esi' => '',
                                                    'professional_tax' => '',
                                                ];

                                $resultObj = (object)$finalResultArray;



                                return $resultObj;
                                }
                                else
                                {
                                    dd('hai');

                                $basicPayConstant = 50/100;



                                $basciPayResult  = $grossSalary * $basicPayConstant;

                                $hRAConstant = 50/100;


                                $hRAResult  = $basciPayResult * $hRAConstant;

                                $cCAConstant = 10/100;

                                $cCAResult  = $basciPayResult * $cCAConstant;


                                $conveyanceAllowanceConstant = 800;

                                $conveyanceAllowanceResult = $conveyanceAllowanceConstant;

                                $providentFundConstant = 12/100;

                                $providentFundResult = $basciPayResult * $providentFundConstant;



                                $esiConstantSALBEL_21000 = 1.75 / 100;

                                if ($grossSalary < 21000)
                                {
                                    $esiResult = $grossSalary * $esiConstantSALBEL_21000;

                                }




                                $professionalTaxConstantSALBEL_5000 = 16.50;

                                $professionalTaxConstantSALBEL_5000_7500 = 30;

                                $professionalTaxConstantSALBEL_7501_10000 = 85;

                                $professionalTaxConstantSALBEL_10001_12500 = 126.50;

                                $professionalTaxConstantSALBEL_12501 = 182.50;


                                if ($grossSalary <= 5000)
                                {
                                    $professionalTaxResult = $professionalTaxConstantSALBEL_5000;
                                }

                                if ($grossSalary > 5000 && $grossSalary <= 7500)
                                {
                                    $professionalTaxResult = $professionalTaxConstantSALBEL_5000_7500;
                                }

                                if ($grossSalary >= 7501 && $grossSalary <= 10000)
                                {
                                    $professionalTaxResult = $professionalTaxConstantSALBEL_7501_10000;
                                }

                                if ($grossSalary >= 10001 && $grossSalary <= 12500)
                                {
                                    $professionalTaxResult = $professionalTaxConstantSALBEL_10001_12500;
                                }

                                if ($grossSalary >= 12501)
                                {
                                    $professionalTaxResult = $professionalTaxConstantSALBEL_12501;
                                }













                                $finalResultArray =
                                                [
                                                    'basicPay' => $basciPayResult,
                                                    'hra' => $hRAResult,
                                                    'cca' => $cCAResult,
                                                    'conveyanceAllowance'=> $conveyanceAllowanceResult,
                                                    'pf' => $providentFundResult,
                                                    'esi' => $esiResult,
                                                    'professional_tax' =>$professionalTaxResult,
                                                ];

                                $resultObj = (object)$finalResultArray;



                                return $resultObj;
                                }
                            }

                            public static function generateStatusChangeButtons($className='')
                            {
                                
                            }

                            public static function generateStatusButtons($actionValue='',$buttonArray='',$id='')
                            {                               
                                

                                  foreach ($buttonArray as   $buttonArrayValue) 
                                    {
                                        $toObject = (object)$buttonArrayValue;

                                        $form[] ='<form action="'.route($toObject->routeName, $id).'" method="POST">
                                                        '.csrf_field().'

                                                        <button type="submit">'.$toObject->DisplayText.'</button>
                                                        '.method_field('PUT').'
                                                        <input type="hidden" id="DbStatus" name="DbStatus" value="'.$toObject->DbStatus.'">
                                                        <input type="hidden" id="DbFieldName" name="DbFieldName" value="'.$toObject->DbFieldName.'">
                                                    </form>
                                                ';                                      

                                    }
                                return implode("", $form);
                                
                            }

                            public static function generateStatusButtonsAlt($buttonValue='',$modelName='',$fieldName='',$id='',$buttonArray='')
                            {                               
                                

                                  foreach ($buttonArray as   $buttonArrayValue) 
                                    {
                                        $toObject = (object)$buttonArrayValue;

                                        $form[] ='<form action="'.route($toObject->routeName, $id).'" method="POST">
                                                        '.csrf_field().'

                                                        <button type="submit">'.$toObject->DisplayText.'</button>
                                                        '.method_field('PUT').'
                                                        <input type="hidden" id="DbStatus" name="DbStatus" value="'.$toObject->DbStatus.'">
                                                        <input type="hidden" id="DbFieldName" name="DbFieldName" value="'.$fieldName.'">
                                                        <input type="hidden" id="ModelName" name="ModelName" value="'.$modelName.'">
                                                        <input type="hidden" id="updateFiledId" name="updateFiledId" value="'.$id.'">
                                                    </form>
                                                ';                                      

                                    }
                                    return implode("", $form);
                            }


                            public static function showBirthDayDate($date='')
                            {
                                $date = strtotime($date);

                                $newformat = 'd-M';

                                return date($newformat, $date);
                            }

                            public static function generateFullAddress($employeeId='')
                            {

                                $selectArray = ['employee_door_no','employee_street_name','employee_city','employee_zip_code'];
                                

                                $compactNamesObject  = DB::table('hrms_employees')->where('id',$employeeId)
                                                ->select($selectArray)->first();

                                $doorNo = $compactNamesObject->employee_door_no;
                                $streetName  = $compactNamesObject->employee_street_name;
                                $city  = $compactNamesObject->employee_city;
                                $zipCode = $compactNamesObject->employee_zip_code;
                                
                                $result = $doorNo.','.$streetName.','.$city.'-'.$zipCode;
                                return $result;
                            }

                            public static function generateSeperateTags($skills='')
                            {
                                if ($skills == null || empty($skills))
                                    {
                                        return"";
                                    }

                                $toarray = explode(',', $skills);
                                
                                foreach ($toarray as $toarrayValue) 
                                {
                                 $html[] = '<span class="badge badge-primary">'.$toarrayValue.' </span>';
                                }
                                return implode("", $html);
                            }
// formatWheres()
                        public static function getTimeLineForPg($employeeId='')
                        {

                            if ($employeeId == null || empty($employeeId))
                                {
                                    return"";
                                }
                            $pgEduArr = HrmsEmployeePgEducation::where('employee_id',$employeeId)->orderBy('employee_pg_year_of_passing', 'ASC')->get();

                            if ($pgEduArr->isEmpty()) 
                            {
                             return"";   
                            }


                            foreach ($pgEduArr as $pgEduArrValue)
                            {
                                $pgArray[] ='
                                    <li>
                                        <a>'.$pgEduArrValue->employee_pg_college_name.'</a>
                                            <h5>'.$pgEduArrValue->employee_pg_degree_name.$pgEduArrValue->employee_pg_percentage_of_marks.'</h5>
                                                <p>'.$pgEduArrValue->employee_pg_year_of_passing.'</p>
                                    </li>';
                               
                            }
                            return implode('',$pgArray);
                        }

                        

                        public static function getTimeLineforEmployeeExperience($employeeId='')
                        {

                            if($employeeId == null || empty($employeeId)){return"";}

                            $expArr = HrmsEmployeeExperience::where('employee_id',$employeeId)->orderBy('employee_previous_employment_end_date', 'ASC')->get();

                            if ($expArr->isEmpty())
                            {
                                return"";
                            }else
                            {
                                foreach ($expArr as $expArrValue)
                                {
                                    $expArray[] ='
                                        <li>
                                            <a>'.$expArrValue->employee_previous_designation."--".$expArrValue->employee_previous_company_name.'</a>
                                            <h5>'.static::showAsFormattedDate($expArrValue->employee_previous_employment_start_date)." -- ".static::showAsFormattedDate($expArrValue->employee_previous_employment_end_date).'</h5>
                                        </li>';
                                   
                                }

                            }
                            return implode('',$expArray);


                        }


                        public static function getActiveMenuArray($masterName='')
                        {
                            $masterName = strtoupper($masterName);
                            switch ($masterName) {
                                case 'BASEMASTER':
                                    $menuItems = ['departments','leavetypes','designations','salarysettings','salarygradesettings','employeegrades'];
                                    return $menuItems;
                                    break;
                                case 'EMPLOYEEMASTER':
                                    $menuItems = ['leavetypes','designations','salarysettings'];
                                    return $menuItems;
                                    break;
                                case 'PAYROLLMASTER':
                                    $menuItems = ['employeegrades','salarygradesettings'];
                                    return $menuItems;
                                    break;
                                default:
                                    # code...
                                    break;
                            }
                            
                        }
                           






}

?>
