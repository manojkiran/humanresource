<?php

namespace App;
use App\HrmsBaseModel;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use App\Models\Comman\Traits\UserStampsTrait;

/**
 * Class HrmsSalary
 *
 * @package App
*/

class HrmsSalary extends HrmsBaseModel
{
	use SoftDeletes;
    use UserStampsTrait;

    protected $guarded = ['id'];

     /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'hrms_salaries';

    /**
     * The primary key associated with the model.
     *
     * @var integer
     */

    protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
                            'employee_id',
                            'employee_grade',
                            'employee_netsalary',
                            'employee_salary_basic_pay',
                            'employee_salary_hra',
                            'employee_salary_cca',
                            'employee_salary_medical_reimbursement',
                            'employee_salary_mobile_phone_allowance',
                            'employee_salary_field_allowance',
                            'employee_salary_monthly_reimbursement',
                            'employee_provident_fund',
                            'employee_salary_esi',
                            'employee_salary_tds',
                            'employee_salary_salary_advance',
                            'employee_salary_income_tax',
                            'employee_salary_professional_tax',
                            'employee_salary_other_deduction',
                            'employee_total_earnings',
                            'employee_total_deductions',
                            'employee_net_salary',
                            ];

     /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [ ];

    /**
     * @var array
     */
    protected $dates = ['created_at','updated_at','deleted_at'];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * The storage format of the model's date columns.
     *
     * @var string
     */
    protected $dateFormat ='';

    /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection ='';


}
