<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Crypt;
use App\HrmsApplyLeave;

class HrmsBaseModel extends Model
{
   public static  function redirectForm($SubmitButtonName='',$edittingId='',$resourceName='',$eventType='')
    {

        switch($SubmitButtonName)
        {
            case 'Save':
                    return redirect()
                            ->route($resourceName.'.index')
                            ->with('success',ucfirst($resourceName).' '.$eventType.' '.'successfully');
                break;

            case 'Save And Edit':
                    return redirect()
                            ->route($resourceName.'.edit', Crypt::encrypt($edittingId))
                            ->with('success',ucfirst($resourceName).' '.$eventType.' '.'successfully And Redirected To Edit'.' '.$resourceName);
                break;

            case 'Save And Add New':
                    return redirect()
                            ->route($resourceName.'.create')
                            ->with('success',ucfirst($resourceName).' '.$eventType.' '."successfully And Redirected To Create");
                break;
        }
    }

    public static  function toDropDown($tableName='',$nameField='',$idField='',$defaultNullText='--Select--')
    {
        if( $idField==null || empty($idField))
            {
                $idField="id";
            }
        $listFiledValues = DB::table($tableName)->select($idField,$nameField)->get();

        $selectArray=[];
        $selectArray[null] = $defaultNullText;
        foreach ($listFiledValues as $listFiledValue)
        {
            $selectArray[$listFiledValue->$idField] = $listFiledValue->$nameField;
        }
        return $selectArray;
    }

    public static  function toDropDownWithoutNull($tableName='',$nameField='',$idField='')
    {
        if( $idField==null || empty($idField))
            {
                $idField="id";
            }
        $listFiledValues = DB::table($tableName)->select($idField,$nameField)->get();

        $selectArray=[];
        foreach ($listFiledValues as $listFiledValue)
        {
            $selectArray[$listFiledValue->$idField] = $listFiledValue->$nameField;
        }
        return $selectArray;
    }

    public static function autoIncrementWithPrefix($tableName = '', $autogenField = '', $autogenStart = '', $autogenPrefix = '')
    {
        $listFiledValues = DB::table($tableName)->orderBy($autogenField, 'desc')->select($autogenField)->get();
        



        if ($listFiledValues->isNotEmpty())
        {
            foreach($listFiledValues as $listFiledValue)
            {
                $eachListarray = $listFiledValue->$autogenField;
                $totalListArrays[] = $eachListarray;
            }

            foreach($totalListArrays as $totalListArray)
            {
                $stringRemovedEachListArray = substr($totalListArray,strlen($autogenPrefix));
                $stringRemovedTotalListArray[] = $stringRemovedEachListArray;
            }

            $maximumValue = max($stringRemovedTotalListArray);

            $generatedAutogen = $autogenPrefix.++$maximumValue;

            return $generatedAutogen;
        }
        elseif ($listFiledValues->isEmpty())
        {
            $generatedAutogen = $autogenPrefix.$autogenStart;
            return $generatedAutogen;
        }
    }

    public static  function dropDownTwoColumns($tableName='',$nameFieldOne='',$nameFieldTwo='',$idField='',$defaultNullText='--Select--')
    {
        if( $idField == null || empty($idField)){$idField="id";}

        $listFiledValues = DB::table($tableName)->select($idField,$nameFieldOne,$nameFieldTwo)->get();

        $selectArray=[];
        $selectArray[null] = $defaultNullText;
        foreach ($listFiledValues as $listFiledValue)
        {
            $selectArray[$listFiledValue->$idField] = $listFiledValue->$nameFieldOne.'('.$listFiledValue->$nameFieldTwo.')';
        }
        return $selectArray;
    }

    public static  function dropDownThreeColumns($tableName='',$nameFieldOne='',$nameFieldTwo='',$nameFieldThree='',$idField='',$defaultNullText='--Select--')
    {
        if( $idField == null || empty($idField)){$idField="id";}

        $listFiledValues = DB::table($tableName)->select($idField,$nameFieldOne,$nameFieldTwo,$nameFieldThree)->get();

        $selectArray=[];
        $selectArray[null] = $defaultNullText;
        foreach ($listFiledValues as $listFiledValue)
        {
            $selectArray[$listFiledValue->$idField] = $listFiledValue->$nameFieldOne.$listFiledValue->$nameFieldTwo.'.'.$listFiledValue->$nameFieldThree;
        }
        return $selectArray;
    }

    public static function getLastField($tableName='',$filedName='')
    {
     return    DB::table($tableName)->orderBy($filedName, 'desc')->select($filedName)->get();
    }

    public static function changeTableStatus($modelName='',$fieldName='',$toChangeStatus='')
    {
        

        $controlVariable = 'App\\'.$modelName::find($id);
        dd($controlVariable);

        $leaveType->leave_type_status='InActive';
        $leaveType->save();
        return redirect()->route('leavetypes.index')->with('success','HrmsDepartment Created Successfully');
    }

    public static function convertVariableToModelName($modelName='',$nameSpace='')
        {
            if (empty($nameSpace) || is_null($nameSpace) || $nameSpace === "") 
            {                
               $modelNameWithNameSpace = "App".'\\'.$modelName;
                return app($modelNameWithNameSpace);    
            }

            if (is_array($nameSpace)) 
            {
                $nameSpace = implode('\\', $nameSpace);
                $modelNameWithNameSpace = $nameSpace.'\\'.$modelName;
                return app($modelNameWithNameSpace);    
            }elseif (!is_array($nameSpace)) 
            {
                $modelNameWithNameSpace = $nameSpace.'\\'.$modelName;
                return app($modelNameWithNameSpace);    
            }
        }






}
