<?php

namespace App;

use App\HrmsBaseModel;
use App\HrmsDepartment;
use App\Models\User;
use App\Models\Role;
use App\Models\Permission;
use App\HrmsLeaveType;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

/**
 * Class HrmsEmployee
 *
 * @package App
 * @property int $id
 * @property int $created_by
 * @property int $updated_by
 * @property string $employee_first_name
 * @property string $employee_middle_name
 * @property string $employee_last_name
 * @property string $employee_code
 * @property string $employee_date_of_birth
 * @property string $employee_gender
 * @property string $employee_aadhar_number
 * @property string $employee_marital_status
 * @property string $employee_nationality
 * @property string $employee_door_no
 * @property string $employee_street_name
 * @property string $employee_city
 * @property string $employee_state
 * @property string $employee_country
 * @property string $employee_zip_code
 * @property string $employee_home_telephone
 * @property string $employee_mobile_number
 * @property string $employee_email_id
 * @property string $employee_employeement_type
 * @property string $employee_job_category
 * @property string $employee_sub_unit
 * @property string $employee_work_shift_id
 * @property string $employee_contract_start_date
 * @property string $employee_contract_end_date
 * @property string $employee_years_of_experience
 * @property string $employee_primary_skills
 * @property string $employee_previous_company_name
 * @property string $employee_previous_designation
 * @property string $employee_previous_employment_start_date
 * @property string $employee_previous_employment_end_date
 * @property string $employee_previous_employment_reason_for_leaving
 * @property string $employee_previous_employment_contact_person_name
 * @property string $employee_previous_employment_contact_person_email
 * @property string $employee_previous_employment_contact_person_phone
 * @property string $employee_documents_collected
 * @property string $created_at
 * @property string $updated_at
 * @property string $deleted_at
 * @property User $user
 * @property User $user
*/

class HrmsEmployee extends HrmsBaseModel
{
	use SoftDeletes;

    protected $guarded = ['id'];

     /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'hrms_employees';

    /**
     * The primary key associated with the model.
     *
     * @var integer
     */

    protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable =
                        [
                            'user_id',
                            'employee_department_id',
                            'employee_reporting_to',
                            'employee_first_name',
                            'employee_middle_name',
                            'employee_last_name',
                            'employee_code',
                            'employee_date_of_birth',
                            'employee_gender',
                            'employee_aadhar_number',
                            'employee_marital_status',
                            'employee_nationality',
                            'employee_recent_photo',
                            'employee_door_no',
                            'employee_street_name',
                            'employee_city',
                            'employee_state',
                            'employee_country',
                            'employee_zip_code',
                            'employee_home_telephone',
                            'employee_mobile_number',
                            'employee_email_id',
                            'employee_employment_type_id',
                            // 'employee_job_category',
                            // 'employee_sub_unit',
                            'employee_work_shift_id',
                            'employee_designation',
                            // 'employee_contract_start_date',
                            // 'employee_contract_end_date',
                            // 'employee_years_of_experience',
                            // 'employee_months_of_experience',
                            'employee_primary_skills',
                            // 'employee_secondary_skills',
                            // 'employee_previous_company_name',
                            // 'employee_previous_designation',
                            // 'employee_previous_employment_start_date',
                            // 'employee_previous_employment_end_date',
                            // 'employee_previous_employment_reason_for_leaving',
                            // 'employee_previous_employment_contact_person_name',
                            // 'employee_previous_employment_contact_person_email',
                            // 'employee_previous_employment_contact_person_phone',
                            // 'employee_documents_collected',
                            // 'employee_relieving_letter',
                            // 'employee_experience_letter',
                            // 'employee_payslips',
                            'created_at',
                            'updated_at',
                            'created_by',
                            'updated_by',

                        ];

     /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = ['password','password_confirmation'];

    /**
     * @var array
     */
    protected $dates = ['created_at','updated_at','deleted_at'];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * The storage format of the model's date columns.
     *
     * @var string
     */
    protected $dateFormat ='';

    /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection ='';

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function createdBy()
    {
        return $this->belongsTo(User::class, 'created_by');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function updatedBy()
    {
        return $this->belongsTo(User::class, 'updated_by');
    }
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function departmentName()
    {
        return $this->belongsTo(HrmsDepartment::class,'employee_department_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function userName()
    {
        return $this->belongsTo(User::class,'user_id');
    }





    public function scopeStatus($query='',$value='')
    {
        return $query->where('employee_status', $value);
    }


}
