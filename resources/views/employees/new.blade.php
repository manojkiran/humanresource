@extends('layouts.app')
@section('title', 'Create Employee')
@section('breadcrumb')
    <div class="breadcrumb-wrapper">
        <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-home"></i>Home</a></li>
        <li class="active">Employees Create</li>
        </ol>
    </div><!-- /.breadcrumb-wrapper -->
@endsection
@section('content')

<div class="row">
   <div class="col-sm-12">
      <div class="panel panel-default-light border-default">

         <div class="panel-heading">
            <div class="panel-title">
               <i class="fa fa-cog m-right-5"></i> Basic Validation
            </div>
            <div class="panel-tools panel-action">
               <button class="btn btn-close"></button>
               <button class="btn btn-min"></button>
               <button class="btn btn-expand"></button>
            </div>
         </div>

         <div class="panel-body">

            {!! Form::open(['route' => ['employees.store'],'autocomplete' => 'off','files' => 'true','enctype'=>'multipart/form-data','class'=>"" ]) !!}


@include('employees._form')
            {!! Form::close()!!}
         </div>
      </div>
   </div>
</div>
@endsection
