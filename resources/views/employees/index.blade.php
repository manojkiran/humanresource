@extends('layouts.app')
@section('title', 'Permission')
@section('breadcrumb')
    <div class="breadcrumb-wrapper">
        <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-home"></i>Home</a></li>
        <li class="active">Employees List</li>
        </ol>
    </div><!-- /.breadcrumb-wrapper -->
@endsection
@section('content')
@section('pagetitle')
<div class="page-title-wrapper">
          <h2 class="page-title">List Of Employees</h2>
        </div><!-- /.page-titile-wrapper -->
@endsection
<div class="row">
   <div class="col-md-5">
      <h3 class="modal-title">{{ $employees->total() }} {{ str_plural('Employee', $employees->count()) }} </h3>
   </div>
   <div class="col-md-7 page-action text-right">
      @can('create.employees')
      <a href="{{ route('employees.create') }}" class="btn btn-primary btn-sm"> <i class="glyphicon glyphicon-plus-sign"></i> Create</a>
      @endcan
   </div>
</div>
<div class="result-set">
   <table class="table table-bordered table-striped table-hover" id="data-table">
      <thead>
         <tr>
            <th>#</th>
            <th>Full Name</th>
            <th>Email</th>
            <th>Mobile No</th>
            <th class="text-center">Actions</th>
         </tr>
      </thead>
      <tbody>
         @foreach($employees as $item)
         <tr>
            <td>{{ $loop->iteration }}</td>
            <td>{{ BladeHelper::generateFullName($item->id) }}</td>
            <td>{{ $item->employee_email_id }}</td>
            <td>{{ $item->employee_mobile_number }}</td>
            <td>{!!BladeHelper::tableActionButtonsWithGate('employees',url()->full(),$item->id,BladeHelper::generateFullName($item->id),['show','edit','delete'])!!}</td>
         </tr>
         @endforeach
      </tbody>
      </thead>
   </table>
   <div class="text-center">
      {{ $employees->links('layouts.pagination') }}
   </div>
</div>
@endsection
