<div class="form-layout-footer">
                <div class="form-group text-right">
                  <div class="col-sm-12">
                    <button type="submit" class="btn btn-info rounded">Submit</button>
                    <button type="button" class="btn btn-default rounded">Cancel</button>
                  </div>
                </div>
                <!-- /.form-group -->
              </div>

               <div class="form-layout">
                  <div class="form-layout-body">

                    {{-- Row 0 Starts --}}
                    <div class="panel-title">
                        <i class="fas fa-building m-right-5"></i> Official
                    </div>
                    <br><br>
                     <div class="row">

                       <div class="col-sm-4">

                  <div class="form-group @if ($errors->has('employee_department_id')) has-error @endif">
                                        {!! Form::label('employee_department_id','Department Name') !!}
                                        {{ Form::select('employee_department_id',$departments, null,  ['class' =>'form-control rounded','id' =>'employee_work_shift_id']) }}
                                            @if ($errors->has('employee_department_id')) <p class="help-block">{{ $errors->first('employee_department_id') }}</p> @endif  
                </div>

                                </div>

                        <div class="col-sm-4">
                            <div class="form-group @if ($errors->has('employee_reporting_to')) has-error @endif">
                              {!! Form::label('employee_reporting_to','Reporting To') !!}
                              {!! Form::text('employee_reporting_to',old('employee_reporting_to'),['placeholder'=>'Enter Employee Reporting to','class' =>'form-control rounded','id' =>'employee_middle_name']) !!}
                                @if ($errors->has('employee_reporting_to')) <p class="help-block">{{ $errors->first('employee_reporting_to') }}</p> @endif
                           </div>
                        </div>

                        <div class="col-sm-4">

                  <div class="form-group @if ($errors->has('employee_employeement_type')) has-error @endif">
                                        {!! Form::label('employee_employeement_type','Employeement Type') !!}
                                        {!! Form::text('employee_employeement_type',old('employee_employeement_type'),['placeholder'=>'Enter Employee Employeement Type','class' =>'form-control rounded','id' =>'employee_employeement_status']) !!}
                                            @if ($errors->has('employee_employeement_type')) <p class="help-block">{{ $errors->first('employee_employeement_type') }}</p> @endif
                                    </div>

                                </div>

                     </div>
                     {{-- Row 00 Ends --}}

                     <div class="row">
                                {{-- <div class="col-sm-4">

                                    <div class="form-group @if ($errors->has('employee_job_category')) has-error @endif">
                                        {!! Form::label('employee_job_category','Job Category') !!}
                                        {!! Form::text('employee_job_category',old('employee_job_category'),['placeholder'=>'Enter Employee Job Category','class' =>'form-control rounded','id' =>'employee_job_category']) !!}
                                            @if ($errors->has('employee_job_category')) <p class="help-block">{{ $errors->first('employee_job_category') }}</p> @endif
                                    </div>

                                </div>
                                <div class="col-sm-4">

                                    <div class="form-group @if ($errors->has('employee_sub_unit')) has-error @endif">
                                        {!! Form::label('employee_sub_unit','Sub Unit') !!}
                                          {!! Form::text('employee_sub_unit',old('employee_sub_unit'),['placeholder'=>'Enter Employee Sub Unit','class' =>'form-control rounded','id' =>'employee_sub_unit']) !!}
                                            @if ($errors->has('employee_sub_unit')) <p class="help-block">{{ $errors->first('employee_sub_unit') }}</p> @endif
                                    </div>
                                </div> --}}

                                <div class="col-sm-4">

                  <div class="form-group @if ($errors->has('employee_work_shift_id')) has-error @endif">
                                        {!! Form::label('employee_work_shift_id','Work Shift') !!}
                                        {{ Form::select('employee_work_shift_id',$shiftTimings, null,  ['class' =>'form-control rounded','id' =>'employee_work_shift_id']) }}
                                            @if ($errors->has('employee_work_shift_id')) <p class="help-block">{{ $errors->first('employee_work_shift_id') }}</p> @endif                                    </div>

                                </div>

                            </div>

                      {{-- Row 00 Ends --}}

                      {{-- Row Starts --}}

                      <div class="row">

                      <div class="col-sm-4">

                                    <div class="form-group @if ($errors->has('employee_contract_start_date')) has-error @endif">
                                        {!! Form::label('employee_contract_start_date','Contract Start Date') !!}
                                        {!! Form::text('employee_contract_start_date',old('employee_contract_start_date'),['placeholder'=>'Enter Employee Contract Start Date','class' =>'form-control rounded','id' =>'employee_contract_start_date']) !!}
                                            @if ($errors->has('employee_contract_start_date')) <p class="help-block">{{ $errors->first('employee_contract_start_date') }}</p> @endif                                    </div>

                                </div>
                                <div class="col-sm-4">

                                    <div class="form-group @if ($errors->has('employee_contract_end_date')) has-error @endif">
                                        {!! Form::label('employee_contract_end_date','Contract End Date') !!}
                                        {!! Form::text('employee_contract_end_date',old('employee_contract_end_date'),['placeholder'=>'Enter Employee Contract End Date','class' =>'form-control rounded','id' =>'employee_contract_end_date']) !!}
                                            @if ($errors->has('employee_contract_end_date')) <p class="help-block">{{ $errors->first('employee_contract_end_date') }}</p> @endif                                    </div>
                                </div>

                                <div class="col-sm-4">

                  <div class="form-group @if ($errors->has('employee_primary_skills')) has-error @endif">
                                        {!! Form::label('employee_primary_skills','Primary Skills') !!}
                                        {!! Form::text('employee_primary_skills',old('employee_primary_skills'),['placeholder'=>'','class' =>'form-control rounded','id' =>'employee_primary_skills']) !!}
                                            @if ($errors->has('employee_primary_skills')) <p class="help-block">{{ $errors->first('employee_primary_skills') }}</p> @endif
                                    </div>

                                </div>

                      </div>


                      {{-- Row Ends --}}

                      {{-- Row Starts --}}

                      <div class="row">

                      <div class="col-sm-4">

                                    <div class="form-group @if ($errors->has('employee_secondary_skills')) has-error @endif">
                                        {!! Form::label('employee_secondary_skills','Secondary Skills') !!}
                                        {!! Form::text('employee_secondary_skills',old('employee_secondary_skills'),['placeholder'=>'','class' =>'form-control rounded','id' =>'employee_secondary_skills']) !!}
                                            @if ($errors->has('employee_secondary_skills')) <p class="help-block">{{ $errors->first('employee_secondary_skills') }}</p> @endif
                                    </div>

                                </div>
                      </div>


                      {{-- Row Ends --}}




                    {{-- Row 1 Starts --}}
                    <div class="panel-title">
                        <i class="fa fa-user m-right-5"></i> Personal Details
                    </div>
                    <br><br>
                     <div class="row">

                        <div class="col-sm-4">
                           <div class="form-group @if ($errors->has('employee_first_name')) has-error @endif">
                              {!! Form::label('employee_first_name','First Name') !!}
                              {!! Form::text('employee_first_name',old('employee_first_name'),['placeholder'=>'Enter Employee First Name','class' =>'form-control rounded','id' =>'employee_first_name']) !!}
                                @if ($errors->has('employee_first_name')) <p class="help-block">{{ $errors->first('employee_first_name') }}</p> @endif
                           </div>
                        </div>

                        <div class="col-sm-4">
                            <div class="form-group @if ($errors->has('employee_middle_name')) has-error @endif">
                              {!! Form::label('employee_middle_name','Middle Name') !!}
                              {!! Form::text('employee_middle_name',old('employee_middle_name'),['placeholder'=>'Enter Employee Middle Name','class' =>'form-control rounded','id' =>'employee_middle_name']) !!}
                                @if ($errors->has('employee_middle_name')) <p class="help-block">{{ $errors->first('employee_middle_name') }}</p> @endif
                           </div>
                        </div>

                        <div class="col-sm-4">
                            <div class="form-group @if ($errors->has('employee_last_name')) has-error @endif">
                              {!! Form::label('employee_last_name','Last Name') !!}
                              {!! Form::text('employee_last_name',old('employee_last_name'),['placeholder'=>'Enter Employee Last Name','class' =>'form-control rounded','id' =>'employee_last_name']) !!}
                                @if ($errors->has('employee_last_name')) <p class="help-block">{{ $errors->first('employee_last_name') }}</p> @endif
                           </div>
                        </div>

                     </div>
                     {{-- Row 1 Ends --}}


                     {{-- Row 2 Starts --}}
                     <div class="row">
                        <div class="col-sm-4">
                           <div class="form-group @if ($errors->has('employee_code')) has-error @endif">
                              {!! Form::label('employee_code','Employee Code') !!}
                              {!! Form::text('employee_code',$autoIncrement,['placeholder'=>'Enter Employee Code','class' =>'form-control rounded','id' =>'employee_code']) !!}
                                @if ($errors->has('employee_code')) <p class="help-block">{{ $errors->first('employee_code') }}</p> @endif
                           </div>
                        </div>

                        <div class="col-sm-4">
                            <div class="form-group @if ($errors->has('employee_date_of_birth')) has-error @endif">
                              {!! Form::label('employee_date_of_birth','Date of birth') !!}
                              {!! Form::text('employee_date_of_birth',old('employee_date_of_birth'),['placeholder'=>'Enter Employee Date Of Birth','class' =>'form-control rounded','id' =>'employee_date_of_birth','id'=>'employee_date_of_birth']) !!}
                                @if ($errors->has('employee_date_of_birth')) <p class="help-block">{{ $errors->first('employee_date_of_birth') }}</p> @endif
                           </div>
                        </div>

                        <div class="col-sm-4">
                            <div class="form-group @if ($errors->has('employee_gender')) has-error @endif">
                              {!! Form::label('employee_gender','Gender') !!}
                  <div class="col-sm-12">
                                        <label class="radio-inline">
                                            {!! Form::radio('employee_gender', 'Male',null, ['class' =>'icheck-minimal-grey']) !!}
                                         Male
                                        </label>
                                        <label class="radio-inline">
                                            {!! Form::radio('employee_gender', 'Female',null, ['class' =>'icheck-minimal-grey']) !!}
                                         Female
                                        </label>
                  </div>

                                @if ($errors->has('employee_gender')) <p class="help-block">{{ $errors->first('employee_gender') }}</p> @endif
                           </div>
                        </div>

                     </div>
                     {{-- Row 2 Ends --}}

                     {{-- Row 3 Starts --}}
                     <div class="row">

                        <div class="col-sm-4">
                           <div class="form-group @if ($errors->has('employee_aadhar_number')) has-error @endif">
                                {!! Form::label('employee_aadhar_number','Aadhar Number') !!}
                                {!! Form::text('employee_aadhar_number',old('employee_aadhar_number'),['placeholder'=>'Enter Employee Aadhar Number','class' =>'form-control rounded','id' =>'employee_aadhar_number']) !!}
                                @if ($errors->has('employee_aadhar_number')) <p class="help-block">{{ $errors->first('employee_aadhar_number') }}</p> @endif
                           </div>
                        </div>

                        <div class="col-sm-4">
                            <div class="form-group @if ($errors->has('employee_marital_status')) has-error @endif">
                              {!! Form::label('employee_marital_status','Marital Status') !!}
                              {{ Form::select('employee_marital_status',$maritalStatus, null,  ['class' =>'form-control rounded','id' =>'employee_marital_status']) }}
                                @if ($errors->has('employee_marital_status')) <p class="help-block">{{ $errors->first('employee_marital_status') }}</p> @endif
                           </div>
                        </div>

                        <div class="col-sm-4">
                            <div class="form-group @if ($errors->has('employee_nationality')) has-error @endif">
                              {!! Form::label('employee_nationality','Nationality') !!}
                              {{ Form::select('employee_nationality',$nationality, null,  ['class' =>'form-control rounded','id' =>'employee_nationality']) }}
                                @if ($errors->has('employee_nationality')) <p class="help-block">{{ $errors->first('employee_nationality') }}</p> @endif
                           </div>
                        </div>


                     </div>
                     {{-- Row 3 Ends --}}

                     {{-- Row 4 Starts --}}
                    <div class="panel-title">
                        <i class="fa fa-map-marker m-right-5"></i> Address And Location
                    </div>
                    <br><br>
                     <div class="row">

                        <div class="col-sm-4">
                           <div class="form-group @if ($errors->has('employee_door_no')) has-error @endif">
                              {!! Form::label('employee_door_no','Door No') !!}
                              {!! Form::text('employee_door_no',old('employee_door_no'),['placeholder'=>'Enter Employee Door No','class' =>'form-control rounded','id' =>'employee_door_no']) !!}
                                @if ($errors->has('employee_door_no')) <p class="help-block">{{ $errors->first('employee_door_no') }}</p> @endif
                           </div>
                        </div>

                        <div class="col-sm-4">
                            <div class="form-group @if ($errors->has('employee_street_name')) has-error @endif">
                              {!! Form::label('employee_street_name','Street Name') !!}
                              {!! Form::text('employee_street_name',old('employee_street_name'),['placeholder'=>'Enter Employee Street Name','class' =>'form-control rounded','id' =>'employee_street_name']) !!}
                                @if ($errors->has('employee_street_name')) <p class="help-block">{{ $errors->first('employee_street_name') }}</p> @endif
                           </div>
                        </div>

                        <div class="col-sm-4">
                            <div class="form-group @if ($errors->has('employee_city')) has-error @endif">
                              {!! Form::label('employee_city','City') !!}
                              {!! Form::text('employee_city',old('employee_city'),['placeholder'=>'Enter Employee City','class' =>'form-control rounded','id' =>'employee_city']) !!}
                                @if ($errors->has('employee_city')) <p class="help-block">{{ $errors->first('employee_city') }}</p> @endif
                           </div>
                        </div>

                     </div>
                     {{-- Row 4 Ends --}}

                     {{--  Row 5 Starts  --}}

                     <div class="row">

                <div class="col-sm-4">

                  <div class="form-group @if ($errors->has('employee_state')) has-error @endif">
                                        {!! Form::label('employee_state','State') !!}
                                        {!! Form::text('employee_state',old('employee_state'),['placeholder'=>'Enter Employee Sate','class' =>'form-control rounded','id' =>'employee_state']) !!}
                                            @if ($errors->has('employee_state')) <p class="help-block">{{ $errors->first('employee_state') }}</p> @endif
                                    </div>

                                </div>

                                <div class="col-sm-4">

                                    <div class="form-group @if ($errors->has('employee_country')) has-error @endif">
                                        {!! Form::label('employee_country','Country') !!}
                                        {!! Form::text('employee_country',old('employee_country'),['placeholder'=>'Enter Employee Contry Name','class' =>'form-control rounded','id' =>'employee_street_name']) !!}
                                            @if ($errors->has('employee_country')) <p class="help-block">{{ $errors->first('employee_country') }}</p> @endif
                                    </div>

                                </div>
                                <div class="col-sm-4">

                                    <div class="form-group @if ($errors->has('employee_zip_code')) has-error @endif">
                                        {!! Form::label('employee_zip_code','ZipCode') !!}
                                        {!! Form::text('employee_zip_code',old('employee_zip_code'),['placeholder'=>'Enter Employee ZipCode','class' =>'form-control rounded','id' =>'employee_zip_code']) !!}
                                            @if ($errors->has('employee_zip_code')) <p class="help-block">{{ $errors->first('employee_zip_code') }}</p> @endif
                                    </div>
                                </div>

                            </div>

                      {{-- Row 5 Ends --}}

                       {{--  Row 6 Starts  --}}

                        <div class="panel-title">
                        <i class="fa fa-phone m-right-5"></i> Contact Details
                    </div>
                    <br><br>

                       <div class="row">

                <div class="col-sm-4">

                  <div class="form-group @if ($errors->has('employee_home_telephone')) has-error @endif">
                                        {!! Form::label('employee_home_telephone','Telephone No') !!}
                                        {!! Form::text('employee_home_telephone',old('employee_home_telephone'),['placeholder'=>'Enter Employee Telephone number','class' =>'form-control rounded','id' =>'employee_home_telephone']) !!}
                                            @if ($errors->has('employee_home_telephone')) <p class="help-block">{{ $errors->first('employee_home_telephone') }}</p> @endif
                                    </div>

                                </div>

                                <div class="col-sm-4">

                                    <div class="form-group @if ($errors->has('employee_mobile_number')) has-error @endif">
                                        {!! Form::label('employee_mobile_number','Mobile No') !!}
                                        {!! Form::text('employee_mobile_number',old('employee_mobile_number'),['placeholder'=>'Enter Employee Mobile Number','class' =>'form-control rounded','id' =>'employee_mobile_number']) !!}
                                            @if ($errors->has('employee_mobile_number')) <p class="help-block">{{ $errors->first('employee_mobile_number') }}</p> @endif
                                    </div>

                                </div>

                                <div class="col-sm-4">

                                    <div class="form-group @if ($errors->has('employee_email_id')) has-error @endif">
                                        {!! Form::label('employee_email_id','Email Id') !!}
                                        {!! Form::text('employee_email_id',old('employee_email_id'),['placeholder'=>'Enter Employee Email Id','class' =>'form-control rounded','id' =>'employee_email_id']) !!}
                                            @if ($errors->has('employee_email_id')) <p class="help-block">{{ $errors->first('employee_email_id') }}</p> @endif
                                    </div>
                                </div>

                            </div>

                      {{-- Row 6 Ends --}}

                      {{--  Row 7 Starts  --}}

                      {{-- Row 7 Ends --}}

                      {{--  Row 8 Starts  --}}


                      {{-- Row 8 Ends --}}

                      {{--  Row 9 Starts  --}}

                      <div class="row">
                          <div class="panel-title">
                        <i class="fas fa-user-tag m-right-5"></i> Employeement Details
                    </div>
                    <br>
                    <br>




                                <div class="col-sm-4">

                                    <div class="form-group @if ($errors->has('employee_previous_company_name')) has-error @endif">
                                        {!! Form::label('employee_previous_company_name','Previous Company Name') !!}
                                        {!! Form::text('employee_previous_company_name',old('employee_previous_company_name'),['placeholder'=>'Previous Company Name','class' =>'form-control rounded','id' =>'employee_previous_company_name']) !!}
                                            @if ($errors->has('employee_previous_company_name')) <p class="help-block">{{ $errors->first('employee_previous_company_name') }}</p> @endif                                    </div>

                                </div>
                                <div class="col-sm-4">

                                    <div class="form-group @if ($errors->has('employee_previous_designation')) has-error @endif">
                                        {!! Form::label('employee_previous_designation','Designation') !!}
                                        {!! Form::text('employee_previous_designation',old('employee_previous_designation'),['placeholder'=>'Enter Employee Previous Designation','class' =>'form-control rounded','id' =>'employee_previous_designation']) !!}
                                            @if ($errors->has('employee_previous_designation')) <p class="help-block">{{ $errors->first('employee_previous_designation') }}</p> @endif                                    </div>
                                </div>

                            </div>


                      {{-- Row 9 Ends --}}

                      {{--  Row 10 Starts  --}}

                      <div class="row">

                <div class="col-sm-4">

                  <div class="form-group @if ($errors->has('employee_previous_employment_start_date')) has-error @endif">
                                        {!! Form::label('employee_previous_employment_start_date','Start Date') !!}
                                            {!! Form::text('employee_previous_employment_start_date',old('employee_previous_employment_start_date'),['placeholder'=>'Enter Start Date','class' =>'form-control rounded','id' =>'employee_previous_employment_start_date']) !!}
                                            @if ($errors->has('employee_previous_employment_start_date')) <p class="help-block">{{ $errors->first('employee_previous_employment_start_date') }}</p> @endif                                    </div>

                                </div>

                                <div class="col-sm-4">

                                    <div class="form-group @if ($errors->has('employee_previous_employment_end_date')) has-error @endif">
                                        {!! Form::label('employee_previous_employment_end_date','End Date') !!}
                                            {!! Form::text('employee_previous_employment_end_date',old('employee_previous_employment_end_date'),['placeholder'=>'End Date','class' =>'form-control rounded','id' =>'employee_previous_employment_end_date']) !!}
                                            @if ($errors->has('employee_previous_employment_end_date')) <p class="help-block">{{ $errors->first('employee_previous_employment_end_date') }}</p> @endif
                                    </div>

                                </div>

                                <div class="col-sm-4">

                                    <div class="form-group @if ($errors->has('employee_previous_employment_reason_for_leaving')) has-error @endif">
                                        {!! Form::label('employee_previous_employment_reason_for_leaving','Reason For Leaving') !!}
                                        {!! Form::textarea('employee_previous_employment_reason_for_leaving',old('employee_previous_employment_reason_for_leaving'),['class' =>'form-control rounded','placeholder' =>'Detailed Description as reason for leaving', 'rows' => 5, 'cols' => 80]) !!}
                                        @if ($errors->has('employee_previous_employment_reason_for_leaving')) <p class="help-block">{{ $errors->first('employee_previous_employment_reason_for_leaving') }}</p> @endif
                                    </div>
                                </div>


                            </div>


                      {{-- Row 10 Ends --}}

                      {{--  Row 11 Starts  --}}

                      <div class="row">

                <div class="col-sm-4">

                  <div class="form-group @if ($errors->has('employee_previous_employment_contact_person_name')) has-error @endif">
                                        {!! Form::label('employee_previous_employment_contact_person_name','Contact Person Name') !!}
                                        {!! Form::text('employee_previous_employment_contact_person_name',old('employee_previous_employment_contact_person_name'),['placeholder'=>'Contact Person Name','class' =>'form-control rounded','id' =>'employee_previous_employment_contact_person_name']) !!}
                                            @if ($errors->has('employee_previous_employment_contact_person_name')) <p class="help-block">{{ $errors->first('employee_previous_employment_contact_person_name') }}</p> @endif
                                    </div>

                                </div>

                                <div class="col-sm-4">

                                    <div class="form-group @if ($errors->has('employee_previous_employment_contact_person_email')) has-error @endif">
                                        {!! Form::label('employee_previous_employment_contact_person_email','Contact Person Email') !!}
                                        {!! Form::text('employee_previous_employment_contact_person_email',old('employee_previous_employment_contact_person_email'),['placeholder'=>'Contact Person Email','class' =>'form-control rounded','id' =>'employee_previous_employment_start_date']) !!}
                                            @if ($errors->has('employee_previous_employment_contact_person_email')) <p class="help-block">{{ $errors->first('employee_previous_employment_contact_person_email') }}</p> @endif
                                    </div>

                                </div>


                                <div class="col-sm-4">

                                    <div class="form-group @if ($errors->has('employee_previous_employment_contact_person_phone')) has-error @endif">
                                        {!! Form::label('employee_previous_employment_contact_person_phone','Contact Person Phone Number') !!}
                                        {!! Form::text('employee_previous_employment_contact_person_phone',old('employee_previous_employment_contact_person_phone'),['placeholder'=>'Contact Person Phone Number','class' =>'form-control rounded','id' =>'employee_previous_employment_start_date']) !!}
                                            @if ($errors->has('employee_previous_employment_contact_person_phone')) <p class="help-block">{{ $errors->first('employee_previous_employment_contact_person_phone') }}</p> @endif
                                    </div>
                                </div>



                      {{-- Row 11 Ends --}}

                      {{--  Row 12 Starts  --}}

                      <div class="row">




                        <div class="col-sm-4">

                                    <div class="form-group @if ($errors->has('employee_years_of_experience')) has-error @endif">
                                        {!! Form::label('employee_years_of_experience','Total Years of Experience') !!}
                                        {!! Form::text('employee_years_of_experience',old('employee_years_of_experience'),['placeholder'=>'Contact Person Email','class' =>'form-control rounded','id' =>'employee_previous_employment_start_date']) !!}
                                            @if ($errors->has('employee_years_of_experience')) <p class="help-block">{{ $errors->first('employee_years_of_experience') }}</p> @endif
                                    </div>

                                </div>












                            </div>

                      {{-- Row 12 Ends --}}

                      {{--  Row 13 Starts  --}}

                      <div class="row">

                      <div class="col-sm-8">
                            <div class="form-group @if ($errors->has('employee_documents_collected')) has-error @endif">
                              {!! Form::label('documentscollected','Documents Collected') !!}
                  <div class="col-sm-12">
                                        @foreach ($documentCollectedArray as $documentCollectedArrayKey => $documentCollectedArrayValue)
                                            <label class="checkbox-inline">
                                            {!! Form::checkbox('employee_documents_collected[]', $documentCollectedArrayValue, null,['class'=>"icheck-minimal-grey form-control"]) !!}
                                            {{ ucfirst($documentCollectedArrayValue) }}
                                            </label>
                                        @endforeach
                  </div>

                                @if ($errors->has('employee_documents_collected')) <p class="help-block">{{ $errors->first('employee_documents_collected') }}</p> @endif
                           </div>
                        </div>

                      </div>

                      {{-- Row 13 Ends --}}







                  </div>
               </div>