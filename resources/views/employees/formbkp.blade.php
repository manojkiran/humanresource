<div class="row">
          <div class="col-sm-12">
            <div class="panel panel-default-light border-default">
              <div class="panel-heading">
                <div class="panel-title">
                  <i class="fa fa-cog m-right-5"></i> Bootstrap Wizard
                </div><!-- /.panel-title -->
                <div class="panel-tools panel-action">
                  <button class="btn btn-close"></button>
                  <button class="btn btn-min"></button>
                  <button class="btn btn-expand"></button>
                </div><!-- /.panel-tools -->
              </div><!-- /.panel-heading -->

              <div class="panel-body">


                <hr>
                <h4 class="m-top-30 m-bottom-30">Step wizard example #2</h4>
                <form class="form-horizontal" id="wizard-rect">
                  <ul class="list-steps list-steps-success">
                    <li class="col-sm-4 step-item-rect active">
                      <a href="#rect-one" data-toggle="tab">
                        <span>1</span>
                        <span>Product one</span>
                      </a>
                    </li>
                    <li class="col-sm-4 step-item-rect">
                      <a href="#rect-two" data-toggle="tab">
                        <span>2</span>
                        <span>Product two</span>
                      </a>
                    </li>
                    <li class="col-sm-4 step-item-rect">
                      <a href="#rect-three" data-toggle="tab">
                        <span>3</span>
                        <span>Product three</span>
                      </a>
                    </li>
                  </ul>

                  <div class="row">
                    <div class="tab-content col-sm-6 col-sm-offset-2">
                      <div class="tab-pane active" id="rect-one">
                        <div class="form-group">
                          <label for="inputName2" class="control-label col-sm-4">Full Name</label>
                          <div class="col-sm-8">
                            <input type="text" class="form-control" id="inputName2" name="inputName2">
                          </div>
                        </div><!-- /.form-group -->
                        <div class="form-group">
                          <label for="inputEmail2" class="control-label col-sm-4">Email Address</label>
                          <div class="col-sm-8">
                            <input type="email" class="form-control" id="inputEmail2" name="inputEmail2">
                          </div>
                        </div><!-- /.form-group -->
                        <div class="form-group">
                          <label for="inputPassword2" class="control-label col-sm-4">Password</label>
                          <div class="col-sm-8">
                            <input type="password" class="form-control" id="inputPassword2" name="inputPassword2">
                          </div>
                        </div><!-- /.form-group -->
                      </div><!-- /.tab-pane -->

                      <div class="tab-pane" id="rect-two">
                        <div class="form-group">
                          <label for="inputAddress2" class="control-label col-sm-4">Address</label>
                          <div class="col-sm-8">
                            <input type="text" class="form-control" id="inputAddress2" name="inputAddress2">
                          </div>
                        </div><!-- /.form-group -->
                        <div class="form-group">
                          <label for="inputCity2" class="control-label col-sm-4">City</label>
                          <div class="col-sm-8">
                            <input type="text" class="form-control" id="inputCity2" name="inputCity2">
                          </div>
                        </div><!-- /.form-group -->
                        <div class="form-group">
                          <label class="control-label col-sm-4">Country</label>
                          <div class="col-sm-8">
                            <select class="form-control" name="country2" style="width: 100%;">
                              <option>United States</option>
                              <option>United Kingdom</option>
                              <option>Andorra</option>
                              <option>United Arab Emirates</option>
                              <option>Afghanistan</option>
                              <option>Antigua and Barbuda</option>
                              <option>Albania</option>
                              <option>Argentina</option>
                              <option>Belgium</option>
                              <option>Bulgaria</option>
                            </select>
                          </div>
                        </div><!-- /.form-group -->
                        <div class="form-group">
                          <label for="inputZip2" class="control-label col-sm-4">ZIP</label>
                          <div class="col-sm-8">
                            <input type="text" class="form-control" id="inputZip2" name="inputZip2">
                          </div>
                        </div><!-- /.form-group -->
                      </div><!-- /.tab-pane -->

                      <div class="tab-pane" id="rect-three">
                        <div class="form-group">
                          <label for="inputCreditCard2" class="control-label col-sm-4">Credit card number</label>
                          <div class="col-sm-8">
                            <input type="text" class="form-control" id="inputCreditCard2" name="inputCreditCard2">
                          </div>
                        </div><!-- /.form-group -->
                        <div class="form-group">
                          <label class="control-label col-sm-4">Expirateion year</label>
                          <div class="col-sm-8">
                            <select class="form-control" name="year2" style="width: 100%;">
                              <option>2020</option>
                              <option>2019</option>
                              <option>2018</option>
                              <option>2017</option>
                              <option>2016</option>
                            </select>
                          </div>
                        </div><!-- /.form-group -->
                        <div class="form-group">
                          <label for="terms2" class="control-label col-sm-4">Checkboxes</label>
                          <div class="col-sm-8">
                            <div class="checkbox">
                              <label>
                                <input type="checkbox" class="icheck-minimal-grey" id="terms2" name="terms2">
                                I agree with the Terms Of Uses
                              </label>
                            </div>
                          </div>
                        </div><!-- /.form-group -->
                      </div><!-- /.tab-pane -->
                    </div><!-- /.tab-content -->
                  </div><!-- /.row -->

                  <div class="row">
                    <div class="col-sm-4 col-sm-offset-4">
                      <div class="row">
                        <div class="col-sm-6">
                          <button type="button" class="btn btn-block btn-warning-outline btn-prev">Previous Step</button>
                        </div><!-- /.col-sm-6 -->

                        <div class="col-sm-6">
                          <button type="button" class="btn btn-block btn-info btn-next">Next Step</button>
                          <button type="button" class="btn btn-block btn-success btn-finish none">Finish</button>
                        </div><!-- /.col-sm-6 -->
                      </div><!-- /.row -->
                    </div><!-- /.col-sm-12 -->
                  </div><!-- /.row -->
                </form>

                <hr>


                <hr>

              </div><!-- /.panel-body -->
            </div><!--/.panel-->
          </div><!-- /.col-sm-12 -->
        </div><!-- /.row -->
