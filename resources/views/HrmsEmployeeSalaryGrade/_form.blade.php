<div class="form-layout">
<div class="form-layout-body">

   {{-- Row 0 Starts --}}
   <div class="row">

      <div class="col-sm-4">
         <div class="form-group @if ($errors->has('employee_grade_id')) has-error @endif">
            {!! Form::label('employee_grade_id','Grade Name') !!}
            {{ Form::select('employee_grade_id',$grades, null,  ['class' =>'form-control rounded','id' =>'employee_leave_type']) }}
            @if ($errors->has('employee_grade_id'))
            <p class="help-block">{{ $errors->first('employee_grade_id') }}</p>
            @endif
         </div>
      </div>

      <div class="col-sm-4">
         <div class="form-group @if ($errors->has('employee_grade_salary_medical_reimbursement')) has-error @endif">
            {!! Form::label('employee_grade_salary_medical_reimbursement','Medical Reimbursement') !!}
            {!! Form::text('employee_grade_salary_medical_reimbursement',old('employee_grade_salary_medical_reimbursement'),['placeholder'=>'Enter Medical Reimbursement','class' =>'form-control rounded','id' =>'employee_grade_salary_medical_reimbursement']) !!}
            @if ($errors->has('employee_grade_salary_medical_reimbursement'))
            <p class="help-block">{{ $errors->first('employee_grade_salary_medical_reimbursement') }}</p>
            @endif
         </div>
      </div>

      <div class="col-sm-4">
         <div class="form-group @if ($errors->has('employee_grade_salary_mobile_phone_allowance')) has-error @endif">
            {!! Form::label('employee_grade_salary_mobile_phone_allowance',BladeHelper::removeUnderScoreAndFromat('mobile_phone_allowance')) !!}
            {!! Form::text('employee_grade_salary_mobile_phone_allowance',old('employee_grade_salary_mobile_phone_allowance'),['placeholder'=>'Enter Mobile Phone Allowance','class' =>'form-control rounded','id' =>'employee_grade_salary_mobile_phone_allowance']) !!}
            @if ($errors->has('employee_grade_salary_mobile_phone_allowance'))
            <p class="help-block">{{ $errors->first('employee_grade_salary_mobile_phone_allowance') }}</p>
            @endif
         </div>
      </div>

   </div>

   {{-- Row  Ends --}}

   {{-- Row 0 Starts --}}
   <div class="row">

      <div class="col-sm-4">
         <div class="form-group @if ($errors->has('employee_grade_salary_field_allowance')) has-error @endif">
            {!! Form::label('employee_grade_salary_field_allowance','Field Allowance') !!}
            {!! Form::text('employee_grade_salary_field_allowance',old('employee_grade_salary_field_allowance'),['placeholder'=>'Enter Field Allowance','class' =>'form-control rounded','id' =>'employee_grade_salary_field_allowance']) !!}
            @if ($errors->has('employee_grade_salary_field_allowance'))
            <p class="help-block">{{ $errors->first('employee_grade_salary_field_allowance') }}</p>
            @endif
         </div>
      </div>

      <div class="col-sm-4">
         <div class="form-group @if ($errors->has('employee_grade_salary_monthly_reimbursement')) has-error @endif">
            {!! Form::label('employee_grade_salary_monthly_reimbursement','Monthly Reimbursement') !!}
            {!! Form::text('employee_grade_salary_monthly_reimbursement',old('employee_grade_salary_monthly_reimbursement'),['placeholder'=>'Enter Monthly Reimbursement','class' =>'form-control rounded','id' =>'employee_grade_salary_monthly_reimbursement']) !!}
            @if ($errors->has('employee_grade_salary_monthly_reimbursement'))
            <p class="help-block">{{ $errors->first('employee_grade_salary_monthly_reimbursement') }}</p>
            @endif
         </div>
      </div>

   </div>

   {{-- Row  Ends --}}

   

</div>
</div>
