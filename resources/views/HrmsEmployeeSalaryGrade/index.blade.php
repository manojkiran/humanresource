@extends('layouts.app')
@section('title', 'HrmsEmployeeSalaryGrade')
@section('breadcrumb')
    <div class="breadcrumb-wrapper">
        <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-home"></i>Home</a></li>
        <li class="active">Grades Salary List</li>
        </ol>
    </div><!-- /.breadcrumb-wrapper -->
@endsection
@section('content')
@section('pagetitle')
<div class="page-title-wrapper">
          <h2 class="page-title">List Of Grades Salary</h2>
        </div><!-- /.page-titile-wrapper -->
@endsection
<div class="row">
   <div class="col-md-5">
      <h3 class="modal-title">{{ $hrmsemployeesalarygrades->total() }} {{ str_plural('Grades Salary', $hrmsemployeesalarygrades->count()) }} </h3>
   </div>
   <div class="col-md-7 page-action text-right">

      <a href="{{ route('salarygradesettings.create') }}" class="btn btn-primary btn-sm"> <i class="glyphicon glyphicon-plus-sign"></i> Create</a>

   </div>
</div>
<div class="result-set">
   <table class="table table-bordered table-striped table-hover" id="data-table">
      <thead>
         <tr>
            <th>Id</th>
            <th>{{BladeHelper::removeUnderScoreAndFromat('salary_grade_name')}}</th>
            <th> Medical Reimbursement</th>
            <th>Mobile Phone Allowance</th>
            <th>Field Allowance</th>
            <th>Monthly Reimbursement</th>
           
            <th class="text-center">Actions</th>
         </tr>
      </thead>
      <tbody>
         @forelse($hrmsemployeesalarygrades as $item)
         <tr>
            <td>{{ $loop->iteration }}</td>
            <td>{{ $item->gradeName['employee_grade_name'] }}</td>
            <td>{{ $item->employee_grade_salary_medical_reimbursement }}</td>
            <td>{{ $item->employee_grade_salary_mobile_phone_allowance }}</td>
            <td>{{ $item->employee_grade_salary_field_allowance }}</td>
            <td>{{ $item->employee_grade_salary_monthly_reimbursement }}</td>
            <td>{!!BladeHelper::tableActionButtonsWithGate('hrmsemployeesalarygrades',url()->full(),$item->id,$item->id,['show','edit','delete'])!!}</td>
            @empty
      </tbody>
      <tfoot>
         <center>
            <h3><code>There are no Grades Salary to display!</code></h3>
         </center>
      </tfoot>
      @endforelse
   </table>
   <div class="text-center">
      {{ $hrmsemployeesalarygrades->links('layouts.pagination') }}
   </div>
</div>
@endsection

