@extends('layouts.app')
@section('title', 'HrmsDepartment')
@section('breadcrumb')
    <div class="breadcrumb-wrapper">
        <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-home"></i>Home</a></li>
        <li class="active">Department List</li>
        </ol>
    </div><!-- /.breadcrumb-wrapper -->
@endsection
@section('content')
@section('pagetitle')
<div class="page-title-wrapper">
          <h2 class="page-title">List Of Departments</h2>
        </div><!-- /.page-titile-wrapper -->
@endsection
<div class="row">
   <div class="col-md-5">
      <h3 class="modal-title">{{ $hrmsdepartments->total() }} {{ str_plural('Employee', $hrmsdepartments->count()) }} </h3>
   </div>
   <div class="col-md-7 page-action text-right">
      
      <a href="{{ route('departments.create') }}" class="btn btn-primary btn-sm"> <i class="glyphicon glyphicon-plus-sign"></i> Create</a>
      
   </div>
</div>
<div class="result-set">
   <table class="table table-bordered table-striped table-hover" id="data-table">
      <thead>
         <tr>
            <th>Id</th>
            <th>Department Name</th>
            <th>Department ShortCode</th>
            <th>Department Supervisor</th>
            <th class="text-center">Actions</th>
         </tr>
      </thead>
      <tbody>
         @forelse($hrmsdepartments as $item)
         <tr>
            <td>{{ $loop->iteration }}</td>
            <td>{{$item->department_name}}</td>
            <td>{{$item->department_prefix}}</td>
            <td>{{$item->department_supervisor}}</td>
            <td>{!!BladeHelper::tableActionButtons(url()->full(),$item->id,$item->id,['show','edit','delete'])!!}</td>
            @empty
      </tbody>
      <tfoot>
         <center>
            <h3><code>There are no HrmsDepartment to display!</code></h3>
         </center>
      </tfoot>
      @endforelse
   </table>
   <div class="text-center">
      {{ $hrmsdepartments->links('layouts.pagination') }}
   </div>
</div>
@endsection
