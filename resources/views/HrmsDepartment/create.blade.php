@extends('layouts.app')
@section('title', 'Create Employee')
@section('breadcrumb')
    <div class="breadcrumb-wrapper">
        <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-home"></i>Home</a></li>
        <li class="active">Employees Create</li>
        </ol>
    </div><!-- /.breadcrumb-wrapper -->
@endsection
@section('content')
<div class="row">
   <div class="col-sm-12">
      <div class="panel panel-default-light border-default">

         <div class="panel-heading">
            <div class="panel-title">
               <i class="fa fa-cog m-right-5"></i> Basic Validation
            </div>
            <div class="panel-tools panel-action">
               <button class="btn btn-close"></button>
               <button class="btn btn-min"></button>
               <button class="btn btn-expand"></button>
            </div>
         </div>

         <div class="panel-body">
            {!! Form::open(['route' => ['departments.store'],'autocomplete' => 'off','files' => 'true','enctype'=>'multipart/form-data' ]) !!}
              @include('HrmsDepartment._form')

              <div class="form-layout-footer">
   <div class="form-group text-right">
      <div class="col-sm-12">

{{ Form::button('<i class="fa fa-save"></i> Save',['class' => 'btn btn-success','type' => 'submit']) }}

{{ Form::button('<i class="fa fa-ban"></i> Clear',['class' => 'btn btn-danger','type' => 'reset']) }}

      </div>
   </div>
   <!-- /.form-group -->
</div>

            {!! Form::close() !!}
         </div>
      </div>
   </div>
</div>
@endsection
