<div class="form-layout">
<div class="form-layout-body">

   {{-- Row 0 Starts --}}
   <div class="row">
      
      <div class="col-sm-5">
         <div class="form-group @if ($errors->has('department_name')) has-error @endif">
            {!! Form::label('department_name','Department Name') !!}
            {!! Form::text('department_name',old('department_name'),['placeholder'=>'Enter Department Name','class' =>'form-control rounded','id' =>'department_name']) !!}
            @if ($errors->has('department_name'))
            <p class="help-block">{{ $errors->first('department_name') }}</p>
            @endif
         </div>
      </div>

      <div class="col-sm-5">
         <div class="form-group @if ($errors->has('department_supervisor')) has-error @endif">
            {!! Form::label('department_supervisor','Department Supervisor') !!}
            {!! Form::text('department_supervisor',old('department_supervisor'),['placeholder'=>'Enter Department Supervisor','class' =>'form-control rounded','id' =>'department_supervisor']) !!}
            @if ($errors->has('department_supervisor'))
            <p class="help-block">{{ $errors->first('department_supervisor') }}</p>
            @endif
         </div>
      </div>

   </div>

   {{-- Row  Ends --}}

   {{-- Row 0 Starts --}}
   <div class="row">

      
      
      <div class="col-sm-5">
         <div class="form-group @if ($errors->has('company_id')) has-error @endif">
            {!! Form::label('company_id','Comapany Name') !!}
             {{ Form::select('company_id',$companies, null,  ['class' =>'form-control rounded','id' =>'company_id']) }}
            @if ($errors->has('company_id'))
            <p class="help-block">{{ $errors->first('company_id') }}</p>
            @endif
         </div>
      </div>

      <div class="col-sm-5">
         <div class="form-group @if ($errors->has('department_prefix')) has-error @endif">
            {!! Form::label('department_prefix','Department Prefix') !!}
            {!! Form::text('department_prefix',old('department_prefix'),['placeholder'=>'Enter Department Prefix','class' =>'form-control rounded','id' =>'department_prefix']) !!}
            @if ($errors->has('department_prefix'))
            <p class="help-block">{{ $errors->first('department_prefix') }}</p>
            @endif
         </div>
      </div>

   </div>

   {{-- Row  Ends --}}

      

   


   

   


   
</div>
</div>
