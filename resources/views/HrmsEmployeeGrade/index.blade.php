@extends('layouts.app')
@section('title', 'HrmsEmployeeGrade')
@section('breadcrumb')
    <div class="breadcrumb-wrapper">
        <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-home"></i>Home</a></li>
        <li class="active">HrmsEmployeeGrade List</li>
        </ol>
    </div><!-- /.breadcrumb-wrapper -->
@endsection
@section('content')
@section('pagetitle')
<div class="page-title-wrapper">
          <h2 class="page-title">List Of HrmsEmployeeGrade</h2>
        </div><!-- /.page-titile-wrapper -->
@endsection
<div class="row">
   <div class="col-md-5">
      <h3 class="modal-title">{{ $hrmsemployeegrades->total() }} {{ str_plural('Employee', $hrmsemployeegrades->count()) }} </h3>
   </div>
   <div class="col-md-7 page-action text-right">
      <a href="{{ route('employeegrades.create') }}" class="btn btn-primary btn-sm"> <i class="glyphicon glyphicon-plus-sign"></i> Create</a>
   </div>
</div>
<div class="result-set">
   <table class="table table-bordered table-striped table-hover" id="data-table">
      <thead>
         <tr>
            <th>Id</th>
            <th>Name</th>
            <th class="text-center">Actions</th>
         </tr>
      </thead>
      <tbody>
         @forelse($hrmsemployeegrades as $item)
         <tr>
            <td>{{ $loop->iteration }}</td>
            <td>{{ $item->employee_grade_name }}</td>
            
            <td>{!!BladeHelper::tableActionButtons(url()->full(),$item->id,$item->employee_grade_name,['show','edit','delete'])!!}</td>
            @empty
      </tbody>
      <tfoot>
         <center>
            <h3><code>There are no HrmsEmployeeGrade to display!</code></h3>
         </center>
      </tfoot>
      @endforelse
   </table>
   <div class="text-center">
      {{ $hrmsemployeegrades->links('layouts.pagination') }}
   </div>
</div>
@endsection

