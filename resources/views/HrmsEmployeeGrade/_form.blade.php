<div class="form-layout">
<div class="form-layout-body">

   {{-- Row 0 Starts --}}
   <div class="row">
      
      <div class="col-sm-5">
         <div class="form-group @if ($errors->has('employee_grade_name')) has-error @endif">
            {!! Form::label('employee_grade_name','Grade Name') !!}
            {!! Form::text('employee_grade_name',old('employee_grade_name'),['placeholder'=>'Enter Grade Name','class' =>'form-control rounded','id' =>'employee_grade_name']) !!}
            @if ($errors->has('employee_grade_name'))
            <p class="help-block">{{ $errors->first('employee_grade_name') }}</p>
            @endif
         </div>
      </div>


   </div>

   {{-- Row  Ends --}}
</div>
</div>
