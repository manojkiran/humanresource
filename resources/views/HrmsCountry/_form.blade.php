<div class="form-group">
	{{ Form::label('foo', 'foo') }}
	{{ Form::text('foo', null, array('class' => 'form-control')) }}
</div>

<div class="form-group">
	{{ Form::label('foomenu', 'foomenu') }}
	{{ Form::select('foomenu', array('0' => 'Select a Level', '1' => 'Sees Sunlight', '2' => 'Foosball Fanatic', '3' => 'Basement Dweller'), null, array('class' => 'form-control')) }}
</div>