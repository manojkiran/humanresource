@extends('layouts.app')
@section('title', 'HrmsCountry')
@section('breadcrumb')
@stop
@section('content')
<div class="row">
   <div class="col-md-5">
      <h3 class="modal-title">{{ $hrmscountries->total() }} {{ str_plural('Permission', $hrmscountries->count()) }} </h3>
   </div>
   <div class="col-md-7 page-action text-right">
      @can('create.hrmscountries')
      <a href="{{ route('hrmscountries.create') }}" class="btn btn-primary btn-sm"> <i class="glyphicon glyphicon-plus-sign"></i> Create</a>
      @endcan
   </div>
</div>
<div class="result-set">
   <table  class="table table-responsive">
      <thead>
         <tr>
            <th>Id</th>
         </tr>
      </thead>
      <tbody>
         @forelse($hrmscountries as $item)
         <tr>
            <td>{{ $loop->iteration }}</td>
            @empty
      </tbody>
      <tfoot>
         <center>
            <h3><code>There are no HrmsCountry to display!</code></h3>
         </center>
      </tfoot>
      @endforelse
   </table>
   <div class="text-center">
      {{ $hrmscountries->links() }}
   </div>
</div>
@endsection