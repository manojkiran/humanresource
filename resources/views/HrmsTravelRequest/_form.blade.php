<div class="form-layout">
   <div class="form-layout-body">
    
   <div class="panel-title">
      <i class="fa fa-user m-right-5"></i> Employee Details
   </div>
      
      
      {{-- Row  Starts --}}
      <div class="row">

         <div class="col-sm-4">
            <div class="form-group @if ($errors->has('employee_id')) has-error @endif">
               {!! Form::label('employee_id','Employee Id') !!}
               {!! Form::text('employee_id',old('employee_id'),['placeholder'=>'Enter Employee Id','class' =>'form-control rounded','id' =>'employee_id']) !!}
               @if ($errors->has('employee_id')) 
               <p class="help-block">{{ $errors->first('employee_id') }}</p>
               @endif                                   
            </div>
         </div>

          <div class="col-sm-4">
            <div class="form-group @if ($errors->has('employee_name')) has-error @endif">
               {!! Form::label('employee_name','Employee Name') !!}
               {!! Form::text('employee_name',old('employee_name'),['placeholder'=>'Enter Employee Name','class' =>'form-control rounded','id' =>'employee_name']) !!}
               @if ($errors->has('employee_name')) 
               <p class="help-block">{{ $errors->first('employee_name') }}</p>
               @endif                                   
            </div>
         </div>

         <div class="col-sm-4">
            <div class="form-group @if ($errors->has('employee_grade')) has-error @endif">
               {!! Form::label('employee_grade','Grade') !!}
               {!! Form::text('employee_grade',old('employee_grade'),['placeholder'=>'Enter Grade','class' =>'form-control rounded','id' =>'employee_grade']) !!}
               @if ($errors->has('employee_grade')) 
               <p class="help-block">{{ $errors->first('employee_grade') }}</p>
               @endif                                   
            </div>
         </div>

      </div>
      {{-- Row  Ends --}}

      {{-- Row  Starts --}}
      <div class="row">

         <div class="col-sm-4">
            <div class="form-group @if ($errors->has('employee_designation')) has-error @endif">
               {!! Form::label('employee_designation','Designation') !!}
               {!! Form::text('employee_designation',old('employee_designation'),['placeholder'=>'Enter Designation','class' =>'form-control rounded','id' =>'employee_designation']) !!}
               @if ($errors->has('employee_designation')) 
               <p class="help-block">{{ $errors->first('employee_designation') }}</p>
               @endif                                   
            </div>
         </div>

          <div class="col-sm-4">
            <div class="form-group @if ($errors->has('employee_location')) has-error @endif">
               {!! Form::label('employee_location','Location') !!}
               {!! Form::text('employee_location',old('employee_location'),['placeholder'=>'Enter Location','class' =>'form-control rounded','id' =>'employee_location']) !!}
               @if ($errors->has('employee_location')) 
               <p class="help-block">{{ $errors->first('employee_location') }}</p>
               @endif                                   
            </div>
         </div>

         <div class="col-sm-4">
            <div class="form-group @if ($errors->has('employee_department')) has-error @endif">
               {!! Form::label('employee_department','Department') !!}
               {!! Form::text('employee_department',old('employee_department'),['placeholder'=>'Enter Department','class' =>'form-control rounded','id' =>'employee_department']) !!}
               @if ($errors->has('employee_department')) 
               <p class="help-block">{{ $errors->first('employee_department') }}</p>
               @endif                                   
            </div>
         </div>

      </div>
      {{-- Row  Ends --}}

      {{-- Row  Starts --}}
      <div class="row">

         <div class="col-sm-4">
            <div class="form-group @if ($errors->has('employee_reporting_to')) has-error @endif">
               {!! Form::label('employee_reporting_to','Reporting To') !!}
               {!! Form::text('employee_reporting_to',old('employee_reporting_to'),['placeholder'=>'Enter Reporting To','class' =>'form-control rounded','id' =>'employee_reporting_to']) !!}
               @if ($errors->has('employee_reporting_to')) 
               <p class="help-block">{{ $errors->first('employee_reporting_to') }}</p>
               @endif                                   
            </div>
         </div>

      </div>
      {{-- Row  Ends --}}
      <hr>
      <div class="panel-title">
      <i class="fa fa-book m-right-5"></i>Booking Details
   </div>
      

      

      

      
   </div>
</div>

<div class="form-layout-footer">
                <div class="form-group text-right">
                  <div class="col-sm-12">
                    <button type="submit" class="btn btn-info rounded">Submit</button>
                    <button type="button" class="btn btn-default rounded">Cancel</button>
                  </div>
                </div>
                <!-- /.form-group -->
              </div>