<div class="form-layout">
   <div class="form-layout-body">
      
      
      {{-- Row 2 Starts --}}
      <div class="row">
         <div class="col-sm-4">
            <div class="form-group @if ($errors->has('permission_date')) has-error @endif">
               {!! Form::label('permission_date','Date of Permission') !!}
               {!! Form::text('permission_date',old('permission_date'),['placeholder'=>'Choose  Date','class' =>'form-control rounded','id' =>'permission_date']) !!}
               @if ($errors->has('permission_date')) 
               <p class="help-block">{{ $errors->first('permission_date') }}</p>
               @endif                                   
            </div>
         </div>


        
         </div>

      </div>
      {{-- Row 2 Ends --}}
      {{-- Row  Starts --}}
<div class="row">
	 <div class="col-sm-4">
            <div class="form-group @if ($errors->has('permissions_time_from')) has-error @endif">
               {!! Form::label('permissions_time_from','From') !!}
               {!! Form::text('permissions_time_from',old('permissions_time_from'),['placeholder'=>'Choose to Date','class' =>'form-control rounded','id' =>'permissions_time_from']) !!}
               @if ($errors->has('permissions_time_from')) 
               <p class="help-block">{{ $errors->first('permissions_time_from') }}</p>
               @endif
            </div>
         </div>

         <div class="col-sm-4">
            <div class="form-group @if ($errors->has('permissions_time_to')) has-error @endif">
               {!! Form::label('permissions_time_to','To') !!}
               {!! Form::text('permissions_time_to',old('permissions_time_to'),['placeholder'=>'Choose to Date','class' =>'form-control rounded','id' =>'permissions_time_to']) !!}
               @if ($errors->has('permissions_time_to')) 
               <p class="help-block">{{ $errors->first('permissions_time_to') }}</p>
               @endif
            </div>
        </div>
</div>

     
    <br>

      {{-- Row  Ends --}}


      

      {{-- Row 3 Starts --}}
      <div class="row">
        <div class="col-sm-8">
         <div class="form-group @if ($errors->has('permissions_reson')) has-error @endif">
            {!! Form::label('permissions_reson','Reason For Permission') !!}
            {!! Form::textarea('permissions_reson',old('permissions_reson'),['class' =>'form-control rounded','placeholder' =>'Detailed Description as reason for Permission','rows'=>'4', 'cols'=>'50']) !!}
            @if ($errors->has('permissions_reson')) 
            <p class="help-block">{{ $errors->first('permissions_reson') }}</p>
            @endif
         </div>
      </div>
    </div>
      {{-- Row 3 Ends --}}
   </div>
</div>

<div class="form-layout-footer">
   <div class="form-group text-right">
      <div class="col-sm-12">
         

{{ Form::button('<i class="fa fa-save"></i> Save',['class' => 'btn btn-success','type' => 'submit']) }}

{{ Form::button('<i class="fa fa-ban"></i> Clear',['class' => 'btn btn-danger','type' => 'reset']) }}

         
      </div>
   </div>
   <!-- /.form-group -->
</div>