@extends('layouts.app')
@section('title', 'HrmsApplyPermission')
@section('breadcrumb')
    <div class="breadcrumb-wrapper">
        <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-home"></i>Home</a></li>
        <li class="active">AppliedPermission List</li>
        </ol>
    </div><!-- /.breadcrumb-wrapper -->
@endsection
@section('content')
@section('pagetitle')
<div class="page-title-wrapper">
          <h2 class="page-title">List Of AppliedPermission</h2>
        </div><!-- /.page-titile-wrapper -->
@endsection
<div class="row">
   <div class="col-md-5">
      <h3 class="modal-title">{{ $hrmsapplypermissions->total() }} {{ str_plural('AppliedPermission', $hrmsapplypermissions->count()) }} </h3>
   </div>
   <div class="col-md-7 page-action text-right">
      
      <a href="{{ route('applypermissions.create') }}" class="btn btn-primary btn-sm"> <i class="glyphicon glyphicon-plus-sign"></i> Create</a>
      
   </div>
</div>
<div class="result-set">
   <table class="table table-bordered table-striped table-hover" id="data-table">
      <thead>
         <tr>
            <th>Id</th>
            <th>Name</th>
            <th>Date</th>
            <th>From</th>
            <th>To</th>
            <th>Time Length</th>
            <th>Status</th>
            <!-- <th class="text-center">Actions</th> -->
         </tr>
      </thead>
      <tbody>
         @forelse($hrmsapplypermissions as $item)
         <tr>
            <td>{{ $loop->iteration }}</td>
            <td>{{ BladeHelper::generateFullName($item->user_id) }}</td>
            <td>{{ BladeHelper::showAsFormattedDate($item->permission_date) }}</td>
            <td>{{ $item->permissions_time_from }}</td>
            <td>{{ $item->permissions_time_to }}</td>
            <td>{!! BladeHelper::getTimeDifference($item->permissions_time_from,$item->permissions_time_to) !!}</td>
            <td>{!! BladeHelper::getLabelForStatus($item->permission_status) !!}</td>
            
           <!--  <td>{!!BladeHelper::tableActionButtonsWithGate('hrmsapplypermissions',url()->full(),$item->id,$item->id,['show','edit','delete'])!!}</td> -->
            @empty
      </tbody>
      <tfoot>
         <center>
            <h3><code>There are no PermissionsApplied to display!</code></h3>
         </center>
      </tfoot>
      @endforelse
   </table>
   <div class="text-center">
      {{ $hrmsapplypermissions->links('layouts.pagination') }}
   </div>
</div>
@endsection
