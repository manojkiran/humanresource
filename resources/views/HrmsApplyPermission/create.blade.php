@extends('layouts.app')
@section('title', 'Create HrmsApplyPermission')
@section('breadcrumb')
    <div class="breadcrumb-wrapper">
        <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-home"></i>Home</a></li>
        <li><a href="#"><i class="fa fa-home"></i>ApplyPermission</a></li>
        <li class="active"> New</li>
        </ol>
    </div><!-- /.breadcrumb-wrapper -->
@endsection
@section('content')
<div class="row">
   <div class="col-sm-12">
      <div class="panel panel-default-light border-default">

         <div class="panel-heading">
            <div class="panel-title">
               <i class="fa fa-cog m-right-5"></i> Apply New Permission
            </div>
            <div class="text-right">
              <a href="{{ route('applypermissions.index') }}" class="btn btn-warning"> <i class="fa fa-arrow-left"></i> Back</a>
            </div>
         </div>

         <div class="panel-body">
            {!! Form::open(['route' => ['applypermissions.store'],'autocomplete' => 'off','files' => 'true','enctype'=>'multipart/form-data' ]) !!}
              @include('HrmsApplyPermission._form')
            
            {!! Form::close() !!}
         </div>
      </div>
   </div>
</div>
@endsection
