@extends('layouts.app')
@section('title', 'HrmsEmployeeExperience')
@section('breadcrumb')
    <div class="breadcrumb-wrapper">
        <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-home"></i>Home</a></li>
        <li class="active">HrmsEmployeeExperience List</li>
        </ol>
    </div><!-- /.breadcrumb-wrapper -->
@endsection
@section('content')
@section('pagetitle')
<div class="page-title-wrapper">
          <h2 class="page-title">List Of HrmsEmployeeExperience</h2>
        </div><!-- /.page-titile-wrapper -->
@endsection
<div class="row">
   <div class="col-md-5">
      <h3 class="modal-title">{{ $hrmsemployeeexperiences->total() }} {{ str_plural('Employee', $hrmsemployeeexperiences->count()) }} </h3>
   </div>
   <div class="col-md-7 page-action text-right">
      @can('create.hrmsemployeeexperiences')
      <a href="{{ route('hrmsemployeeexperiences.create') }}" class="btn btn-primary btn-sm"> <i class="glyphicon glyphicon-plus-sign"></i> Create</a>
      @endcan
   </div>
</div>
<div class="result-set">
   <table class="table table-bordered table-striped table-hover" id="data-table">
      <thead>
         <tr>
            <th>Id</th>
            <th class="text-center">Actions</th>
         </tr>
      </thead>
      <tbody>
         @forelse($hrmsemployeeexperiences as $item)
         <tr>
            <td>{{ $loop->iteration }}</td>
            <td>{!!BladeHelper::tableActionButtonsWithGate('hrmsemployeeexperiences',url()->full(),$item->id,$item->id,['show','edit','delete'])!!}</td>
            @empty
      </tbody>
      <tfoot>
         <center>
            <h3><code>There are no HrmsEmployeeExperience to display!</code></h3>
         </center>
      </tfoot>
      @endforelse
   </table>
   <div class="text-center">
      {{ $hrmsemployeeexperiences->links('layouts.pagination') }}
   </div>
</div>
@endsection
