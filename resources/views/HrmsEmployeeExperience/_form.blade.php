<div class="form-layout">
   <div class="form-layout-body">

      {{-- Row  Starts --}}
      <div class="row">
         <div class="col-sm-5">
            <div class="form-group @if ($errors->has('designation_name')) has-error @endif">
               {!! Form::label('designation_name','Name') !!}
               {!! Form::text('designation_name',old('designation_name'),['placeholder'=>'Designation','class' =>'form-control rounded','id' =>'designation_name']) !!}
               @if ($errors->has('designation_name'))
               <p class="help-block">{{ $errors->first('designation_name') }}</p>
               @endif
            </div>
         </div>
         
      </div>
      {{-- Row  Ends --}}
   </div>
</div>

