@extends('layouts.app')
@section('title', 'HrmsSalarySetting')
@section('breadcrumb')
    <div class="breadcrumb-wrapper">
        <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-home"></i>Home</a></li>
        <li class="active">HrmsSalarySetting List</li>
        </ol>
    </div><!-- /.breadcrumb-wrapper -->
@endsection
@section('content')
@section('pagetitle')
<div class="page-title-wrapper">
          <h2 class="page-title">List Of HrmsSalarySetting</h2>
        </div><!-- /.page-titile-wrapper -->
@endsection
<div class="row">
   <div class="col-md-5">
      <h3 class="modal-title">{{ $hrmssalarysettings->total() }} {{ str_plural('Employee', $hrmssalarysettings->count()) }} </h3>
   </div>
   <div class="col-md-7 page-action text-right">

      <a href="{{ route('salarysettings.create') }}" class="btn btn-primary btn-sm"> <i class="fa fa-plus"></i> Create</a>

   </div>
</div>
<div class="result-set">
   <table class="table table-bordered table-striped table-hover" id="data-table">
      <thead>
         <tr>
            <th>Id</th>
            <th>Basic Pay</th>
            <th>HRA</th>
            <th>CCA</th>
            <th>Conveyance Allowance</th>
            <th>Provident Fund</th>
            <th>ESI</th>
            <th>Professional Tax</th>
            {{--  <th class="text-center">Actions</th>  --}}
         </tr>
      </thead>
      <tbody>
         @forelse($hrmssalarysettings as $item)
         <tr>
            <td>{{ $loop->iteration }}</td>
             <td>{{ $item->employee_salary_basic_pay }}</td>
             <td>{{ $item->employee_salary_hra }}</td>
             <td>{{ $item->employee_salary_cca }}</td>
             <td>{{ $item->employee_salary_conveyance_allowance }}</td>
             <td>{{ $item->employee_provident_fund }}</td>
             <td>{{ $item->employee_salary_esi }}</td>
             <td>{{ $item->employee_salary_professional_tax }}</td>
             
            {{--  <td>{!!BladeHelper::tableActionButtonsWithGate('hrmssalarysettings',url()->full(),$item->id,$item->id,['show','edit','delete'])!!}</td>  --}}
            @empty
      </tbody>
      <tfoot>
         <center>
            <h3><code>There are no HrmsSalarySetting to display!</code></h3>
         </center>
      </tfoot>
      @endforelse
   </table>
   <div class="text-center">
      {{ $hrmssalarysettings->links('layouts.pagination') }}
   </div>
</div>
@endsection
