<div class="form-layout">
   <div class="form-layout-body">
      <div class="row">
         <div class="col-md-6">
            <h4 class="text-primary">Earnings</h4>
            <div class="form-group @if ($errors->has('employee_salary_basic_pay')) has-error @endif">
               {!! Form::label('employee_salary_basic_pay','Basic') !!}
               {!! Form::text('employee_salary_basic_pay',old('employee_salary_basic_pay'),['placeholder'=>'Enter Net Salary','class' =>'form-control rounded','id' =>'employee_salary_basic_pay']) !!}
               @if ($errors->has('employee_salary_basic_pay')) 
               <p class="help-block">{{ $errors->first('employee_salary_basic_pay') }}</p>
               @endif
            </div>
            <div class="form-group @if ($errors->has('employee_salary_hra')) has-error @endif">
               {!! Form::label('employee_salary_hra','HRA') !!}
               {!! Form::text('employee_salary_hra',old('employee_salary_hra'),['placeholder'=>'Enter Hra','class' =>'form-control rounded','id' =>'employee_salary_hra']) !!}
               @if ($errors->has('employee_salary_hra')) 
               <p class="help-block">{{ $errors->first('employee_salary_hra') }}</p>
               @endif
            </div>
            <div class="form-group @if ($errors->has('employee_salary_cca')) has-error @endif">
               {!! Form::label('employee_salary_cca','CCA') !!}
               {!! Form::text('employee_salary_cca',old('employee_salary_cca'),['placeholder'=>'Enter CCA','class' =>'form-control rounded','id' =>'employee_salary_cca']) !!}
               @if ($errors->has('employee_salary_cca')) 
               <p class="help-block">{{ $errors->first('employee_salary_cca') }}</p>
               @endif
            </div>
            <div class="form-group @if ($errors->has('employee_salary_conveyance_allowance')) has-error @endif">
               {!! Form::label('employee_salary_conveyance_allowance','Conveyance Allowance') !!}
               {!! Form::text('employee_salary_conveyance_allowance',old('employee_salary_conveyance_allowance'),['placeholder'=>'Enter CCA','class' =>'form-control rounded','id' =>'employee_salary_conveyance_allowance']) !!}
               @if ($errors->has('employee_salary_conveyance_allowance')) 
               <p class="help-block">{{ $errors->first('employee_salary_conveyance_allowance') }}</p>
               @endif
            </div>
         </div>
         <div class="col-md-6">
            <h4 class="text-primary">Deductions</h4>
            <div class="form-group @if ($errors->has('employee_provident_fund')) has-error @endif">
               {!! Form::label('employee_provident_fund','Provident Fund') !!}
               {!! Form::text('employee_provident_fund',old('employee_provident_fund'),['placeholder'=>'Enter PF','class' =>'form-control rounded','id' =>'employee_provident_fund']) !!}
               @if ($errors->has('employee_provident_fund')) 
               <p class="help-block">{{ $errors->first('employee_provident_fund') }}</p>
               @endif
            </div>
            <div class="form-group @if ($errors->has('employee_salary_esi')) has-error @endif">
               {!! Form::label('employee_salary_esi','ESI') !!}
               {!! Form::text('employee_salary_esi',old('employee_salary_esi'),['placeholder'=>'Enter ESI','class' =>'form-control rounded','id' =>'employee_salary_esi']) !!}
               @if ($errors->has('employee_salary_esi')) 
               <p class="help-block">{{ $errors->first('employee_salary_esi') }}</p>
               @endif
            </div>
            <!--  <div class="form-group @if ($errors->has('employee_salary_tds')) has-error @endif">
               {!! Form::label('employee_salary_tds','TDS') !!}
                {!! Form::text('employee_salary_tds',old('employee_salary_tds'),['placeholder'=>'Enter ESI','class' =>'form-control rounded','id' =>'employee_salary_tds']) !!}
                @if ($errors->has('employee_salary_tds')) 
                <p class="help-block">{{ $errors->first('employee_salary_tds') }}</p>
                @endif
               </div> -->
            <div class="form-group @if ($errors->has('employee_salary_salary_advance')) has-error @endif">
               {!! Form::label('employee_salary_salary_advance','Salary Advance') !!}
               {!! Form::text('employee_salary_salary_advance',old('employee_salary_salary_advance'),['placeholder'=>'Enter ESI','class' =>'form-control rounded','id' =>'employee_salary_salary_advance']) !!}
               @if ($errors->has('employee_salary_salary_advance')) 
               <p class="help-block">{{ $errors->first('employee_salary_salary_advance') }}</p>
               @endif
            </div>
            <!-- <div class="form-group @if ($errors->has('employee_salary_income_tax')) has-error @endif">
               {!! Form::label('employee_salary_income_tax','Income Tax') !!}
                {!! Form::text('employee_salary_income_tax',old('employee_salary_income_tax'),['placeholder'=>'Enter ESI','class' =>'form-control rounded','id' =>'employee_salary_income_tax']) !!}
                @if ($errors->has('employee_salary_income_tax')) 
                <p class="help-block">{{ $errors->first('employee_salary_income_tax') }}</p>
                @endif
               </div> -->
            <div class="form-group @if ($errors->has('employee_salary_professional_tax')) has-error @endif">
               {!! Form::label('employee_salary_professional_tax','Professional Tax') !!}
               {!! Form::text('employee_salary_professional_tax',old('employee_salary_professional_tax'),['placeholder'=>'Enter ESI','class' =>'form-control rounded','id' =>'employee_salary_professional_tax']) !!}
               @if ($errors->has('employee_salary_professional_tax')) 
               <p class="help-block">{{ $errors->first('employee_salary_professional_tax') }}</p>
               @endif
            </div>
            <!-- 
               <div class="form-group @if ($errors->has('employee_salary_other_deduction')) has-error @endif">
                             {!! Form::label('employee_salary_other_deduction','Other Deductions') !!}
                              {!! Form::text('employee_salary_other_deduction',old('employee_salary_other_deduction'),['placeholder'=>'Enter ESI','class' =>'form-control rounded','id' =>'employee_salary_other_deduction']) !!}
                              @if ($errors->has('employee_salary_other_deduction')) 
                              <p class="help-block">{{ $errors->first('employee_salary_other_deduction') }}</p>
                              @endif
               </div> -->
         </div>
      </div>
   </div>
</div>
<div class="form-layout-footer">
   <div class="form-group text-right">
      <div class="col-sm-12">
         

{{ Form::button('<i class="fa fa-save"></i> Save',['class' => 'btn btn-success','type' => 'submit']) }}

{{ Form::button('<i class="fa fa-ban"></i> Clear',['class' => 'btn btn-danger','type' => 'reset']) }}

         
      </div>
   </div>
   <!-- /.form-group -->
</div>