@extends('layouts.app')
@section('title', 'Create HrmsSalarySetting')
@section('breadcrumb')
    <div class="breadcrumb-wrapper">
        <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-home"></i>Home</a></li>
        <li class="active"> Create HrmsSalarySetting Create</li>
        </ol>
    </div><!-- /.breadcrumb-wrapper -->
@endsection
@section('content')
<div class="row">
   <div class="col-sm-12">
      <div class="panel panel-default-light border-default">

         <div class="panel-heading">
            <div class="panel-title">
               <i class="fa fa-cog m-right-5"></i> Create HrmsSalarySetting
            </div>
            <div class="panel-tools panel-action">
               <button class="btn btn-close"></button>
               <button class="btn btn-min"></button>
               <button class="btn btn-expand"></button>
            </div>
         </div>

         <div class="panel-body">
            {!! Form::open(['route' => ['salarysettings.store'],'autocomplete' => 'off','files' => 'true','enctype'=>'multipart/form-data' ]) !!}
              @include('HrmsSalarySetting._form')
            {!! Form::close() !!}
         </div>
      </div>
   </div>
</div>
@endsection
