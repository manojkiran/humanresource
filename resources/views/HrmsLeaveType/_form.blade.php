<div class="form-layout">
   <div class="form-layout-body">

      {{-- Row  Starts --}}
      <div class="row">
         <div class="col-sm-5">
            <div class="form-group @if ($errors->has('leave_type')) has-error @endif">
               {!! Form::label('leave_type','Leave Type') !!}
               {!! Form::text('leave_type',old('leave_type'),['placeholder'=>'Enter Leave Type','class' =>'form-control rounded','id' =>'leave_type']) !!}
               @if ($errors->has('leave_type'))
               <p class="help-block">{{ $errors->first('leave_type') }}</p>
               @endif
            </div>
         </div>

         <div class="col-sm-5">
            <div class="form-group @if ($errors->has('leave_no_of_days')) has-error @endif">
               {!! Form::label('leave_no_of_days','No of Days') !!}
               {!! Form::text('leave_no_of_days',old('leave_no_of_days'),['placeholder'=>'Enter No of Days','class' =>'form-control rounded','id' =>'leave_no_of_days']) !!}
               @if ($errors->has('leave_no_of_days'))
               <p class="help-block">{{ $errors->first('leave_no_of_days') }}</p>
               @endif
            </div>
         </div>
         
      </div>
      {{-- Row  Ends --}}

      
   </div>
</div>

