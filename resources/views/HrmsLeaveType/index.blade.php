@extends('layouts.app')
@section('title', 'Leave Types')
@section('breadcrumb')
    <div class="breadcrumb-wrapper">
        <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-home"></i>Home</a></li>
        <li class="active">LeaveTypes List</li>
        </ol>
    </div><!-- /.breadcrumb-wrapper -->
@endsection
@section('content')
@section('pagetitle')
<div class="page-title-wrapper">
          <h2 class="page-title">List Of LeaveTypes</h2>
        </div><!-- /.page-titile-wrapper -->
@endsection
<div class="row">
   <div class="col-md-5">
      <h3 class="modal-title">{{ $hrmsleavetypes->total() }} {{ str_plural('LeaveType', $hrmsleavetypes->count()) }} </h3>
   </div>
   <div class="col-md-7 page-action text-right">
      @can('create.employees')
      <a href="{{ route('leavetypes.create') }}" class="btn btn-primary btn-sm"> <i class="glyphicon glyphicon-plus-sign"></i> Create</a>
      @endcan
   </div>
</div>
<div class="result-set">
   <table class="table table-bordered table-striped table-hover" id="data-table">
      <thead>
         <tr>
            <th>#</th>
            <th>LeaveTypes</th>
            <th>No of Days</th>
            <th>Status</th>
            <th class="text-center">Actions</th>
         </tr>
      </thead>
      <tbody>
         @foreach($hrmsleavetypes as $item)
         <tr>
            <td>{{ $loop->iteration }}</td>
            <td>{{ $item->leave_type }}</td>
            <td>{{ $item->leave_no_of_days }}</td>
            <td>{!!BladeHelper::getLabelForStatus($item->leave_type_status)!!}</td>
            <td>{!!BladeHelper::tableActionButtons(url()->full(),$item->id,$item->leave_type,['show','edit','delete'])!!}</td>
           <!--  <td>{!!BladeHelper::generateStatusButtons('ChangeStatus',
                                    [
                                      [
                                        'DbStatus' =>'Active',
                                        'DbFieldName' => 'leave_type_status',
                                        'DisplayText' =>'Activate',
                                        'routeName' => 'leavetypes.changeStatus',
                                      ],

                                      

                                    ],$item->id)!!}</td> -->

                                     <!-- <td>
                                      {!!
                                        BladeHelper::generateStatusButtonsAlt
                                        (
                                            'ChangeStatus',
                                            HrmsApplyLeave::class,
                                            'leave_type_status',
                                            $item->id,
                                            [
                                              [ 'DbStatus' =>'Active',
                                                'DisplayText' =>'Activate',
                                                'routeName' => 'leavetypes.changeStatus',
                                              ],

                                              [ 'DbStatus' =>'InActive',
                                                'DisplayText' =>'Deactivate',
                                                'routeName' => 'leavetypes.changeStatus',
                                              ],
                                            ]
                                        )
                                      !!}
                                    </td> -->
            
         </tr>
         @endforeach
      </tbody>
      </thead>
   </table>
   <div class="text-center">
      {{ $hrmsleavetypes->links('layouts.pagination') }}
   </div>
</div>
@endsection
<!-- 'Active','InActive' -->