@extends('layouts.app')
@section('title', 'Create Employee')
@section('breadcrumb')
<link href="{{asset('assets/css/page-profile.min.css')}}" rel="stylesheet" type="text/css">
<div class="breadcrumb-wrapper">
   <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-home"></i>Home</a></li>
      <li><a href="#"><i class="fa fa-home"></i>Employee Profile</a></li>
      <li class="active">{{ BladeHelper::generateFullName($hrmsemployees->id) }}</li>
   </ol>
</div>
<!-- /.breadcrumb-wrapper -->
@endsection
@section('content')
<style>
   ul.timeline{list-style-type:none;position:relative}ul.timeline:before,ul.timeline>li:before{content:' ';display:inline-block;position:absolute;z-index:400}ul.timeline:before{background:#d4d9df;left:29px;width:2px;height:100%}ul.timeline>li{margin:20px 0;padding-left:20px}ul.timeline>li:before{background:#fff;border-radius:50%;border:3px solid #22c0e8;left:20px;width:20px;height:20px}
</style>

<div class="page-title-wrapper">
   <h2 class="page-title">Profile of {{ BladeHelper::generateFullName($hrmsemployees->id) }}</h2>
</div>
<!-- /.page-titile-wrapper -->
<div class="row">
<div class="col-sm-4 col-md-3">
   <div class="profile-heading">
      <!-- {{config('app.url').'/storage/'}} -->
      <img src="https://images.pexels.com/photos/1738623/pexels-photo-1738623.jpeg" class="img-circle" alt="">
      <p class="lead m-bottom-5">{{ BladeHelper::generateFullName($hrmsemployees->id) }}</p>
      <strong>{{ $hrmsemployees->employee_designation }}</strong>
      <address>
         <i class="fa fa-calendar m-right-5"></i>{{BladeHelper::showAsFormattedDate($hrmsemployees->employee_contract_start_date)}} <br>
         <i class=" m-right-5"></i>{{ $hrmsemployees->departmentName['department_name'] }} <br>
         {{ $hrmsemployees->employee_mobile_number}}
      </address>
      <div class="btn-group circle center-block">
         <!-- <a href="#" class="btn">
            <i class="fa fa-linkedin"></i>
            </a>
            <a href="#" class="btn">
            <i class="fa fa-twitter"></i>
            </a>
            <a href="#" class="btn">
            <i class="fa fa-facebook"></i>
            </a> -->
      </div>
      <!-- /.btn-group -->
   </div>
   <!-- /.profile-heading -->
   <div class="profile-body">
      <p class="lead">Primary Skills</p>
      <ul class="list-unstyled m-bottom-15">
         {!! BladeHelper::generateSeperateTags($hrmsemployees->employee_primary_skills) !!}                
      </ul>
      <p class="lead">Secondary Skills</p>
      <ul class="list-unstyled m-bottom-15">
         {!! BladeHelper::generateSeperateTags($hrmsemployees->employee_secondary_skills) !!}                
      </ul>
   </div>
   <!-- /.profile-body -->
</div>
<!-- /.col-sm-4 -->
<div>
   <div class="row">
      <div class="row">

         <div class="col-lg-8">
            <div class="panel panel-default-light panel-activity-monitor border-default">
               <div class="panel-heading">
                  <div class="panel-title">
                     <i class="fa fa-cog m-right-5"></i> Educational Info
                  </div>
                  <!-- /.panel-title -->
                  <div class="panel-tools panel-action">
                     <button class="btn btn-min"></button>
                  </div>
                  <!-- /.panel-tools -->
               </div>
               <!-- /.panel-heading -->
               <div class="panel-body">
                  <div class="media-wrapper">
                     <ul class="timeline">
                        @foreach($hrmsEmployeeEducation as $hrmsEmployeeEduSslc)
                        <li>
                           <a>{{$hrmsEmployeeEduSslc->employee_sslc_school_name}}</a>
                           <h5>SSLC -- {{$hrmsEmployeeEduSslc->employee_sslc_percentage_of_marks}}</h5>
                           <p>{{ $hrmsEmployeeEduSslc->employee_sslc_year_of_passing }}</p>
                        </li>
                        @endforeach
                        @foreach($hrmsEmployeeEducation as $hrmsEmployeeEduHsc)
                        <li>
                           <a>{{$hrmsEmployeeEduSslc->employee_hsc_school_name}}</a>
                           <h5>HSC -- {{$hrmsEmployeeEduSslc->employee_hsc_percentage_of_marks}}</h5>
                           <p>{{ $hrmsEmployeeEduSslc->employee_hsc_year_of_passing }}</p>
                        </li>
                        @endforeach
                        @foreach($hrmsEmployeeEducation as $hrmsEmployeeEduUg)
                        <li>
                           <a>{{$hrmsEmployeeEduUg->employee_ug_college_name}}</a>
                           <h5>UG -- {{$hrmsEmployeeEduUg->employee_ug_degree_name}} {{$hrmsEmployeeEduUg->employee_ug_percentage_of_marks}}</h5>
                           <p>{{ $hrmsEmployeeEduUg->employee_ug_year_of_passing }}</p>
                        </li>
                        @endforeach
                        {!! BladeHelper::getTimeLineForPg($hrmsemployees->id) !!}
                     </ul>
                  </div>
                  <!-- /.media-wrapper -->
               </div>
               <!-- /.panel-body -->
            </div>
            <!--/.panel-->
         </div>
         <!-- /.col-lg-6 -->


         <div class="col-lg-8">
            <div class="panel panel-default-light panel-activity-monitor border-default">
               <div class="panel-heading">
                  <div class="panel-title">
                     <i class="fa fa-cog m-right-5"></i> Experience Info
                  </div>
                  <!-- /.panel-title -->
                  <div class="panel-tools panel-action">
                     <button class="btn btn-min"></button>
                  </div>
                  <!-- /.panel-tools -->
               </div>
               <!-- /.panel-heading -->
               <div class="panel-body">
                  <div class="media-wrapper">
                     <ul class="timeline">
                        {!! BladeHelper::getTimeLineforEmployeeExperience($hrmsemployees->id) !!}
                     </ul>
                  </div>
                  <!-- /.media-wrapper -->
               </div>
               <!-- /.panel-body -->
            </div>
            <!--/.panel-->
         </div>




         
      </div>
      <!-- /.col-sm-8 -->
   </div>
   <!-- /.row -->
</div>
@endsection