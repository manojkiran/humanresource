@if ($errors->any())
{{ implode('', $errors->all('
<div>:message</div>
')) }}
@endif
<div class="form-layout">
<div class="form-layout-body">





   {{-- Row Starts --}}
   <hr>
   <div class="panel-title">
      <i class="fa fa-user m-right-5"></i> Login
   </div>
   <br><br>
   <div class="row">

      <div class="col-sm-4">
         <div class="form-group @if ($errors->has('name')) has-error @endif">
            {!! Form::label('name','Name') !!}
            {!! Form::text('name',old('name'),['placeholder'=>'Enter Full Name ','class' =>'form-control rounded','id' =>'name']) !!}
            @if ($errors->has('name'))
            <p class="help-block">{{ $errors->first('name') }}</p>
            @endif
         </div>
      </div>

      <div class="col-sm-4">
         <div class="form-group @if ($errors->has('email')) has-error @endif">
            {!! Form::label('email','Email') !!}
            {!! Form::text('email',old('email'),['placeholder'=>'Enter Officail Email id','class' =>'form-control rounded','id' =>'email']) !!}
            @if ($errors->has('email'))
            <p class="help-block">{{ $errors->first('email') }}</p>
            @endif
         </div>
      </div>

      <div class="col-sm-4">
         <div class="form-group @if ($errors->has('password')) has-error @endif">
            {!! Form::label('password','Password') !!}
            {!! Form::password('password', ['class' => 'form-control', 'placeholder' => 'Password','id'=>'password']) !!}
            @if ($errors->has('password'))
            <p class="help-block">{{ $errors->first('password') }}</p>
            @endif
         </div>
      </div>

      <div class="col-sm-4">
         <div class="form-group @if ($errors->has('password_confirmation')) has-error @endif">
            {!! Form::label('password_confirmation','Confirm Password') !!}
            {!! Form::password('password_confirmation', ['class' => 'form-control', 'placeholder' => 'Confirm Password','id'=>'password-confirm']) !!}
            @if ($errors->has('password_confirmation'))
            <p class="help-block">{{ $errors->first('password_confirmation') }}</p>
            @endif
         </div>
      </div>






<div class="col-sm-4">
      <div class="form-group row @if ($errors->has('roles')) has-error @endif">
      {{ Form::label('roles', 'Roles',['class'=>'col-md-4 col-form-label text-md-right']) }}

         @foreach($roles as $role)
         <div class="checkbox">
            <div class="col-md-10">
               {{ Form::checkbox("roles[]", $role->id) }} {{ $role->description }}
            </div>
         </div>
         @endforeach

         @if ($errors->has('roles'))
         <p class="help-block danger">{{ $errors->first('roles') }}</p>
         @endif
   </div>

</div>



   </div>

   {{-- Row  Ends --}}


   {{-- Row 0 Starts --}}
   <hr>
   <div class="panel-title">
      <i class="fa fa-building m-right-5"></i> Official
   </div>
   <br><br>
   <div class="row">
      <div class="col-sm-4">
         <div class="form-group @if ($errors->has('employee_department_id')) has-error @endif">
            {!! Form::label('employee_department_id','Department Name') !!}
            {{ Form::select('employee_department_id',$departments, null,  ['class' =>'form-control rounded','id' =>'employee_department_id']) }}
            @if ($errors->has('employee_department_id'))
            <p class="help-block">{{ $errors->first('employee_department_id') }}</p>
            @endif
         </div>
      </div>
      <div class="col-sm-4">
         <div class="form-group @if ($errors->has('employee_reporting_to')) has-error @endif">
            {!! Form::label('employee_reporting_to','Reporting To') !!}
            {!! Form::text('employee_reporting_to',old('employee_reporting_to'),['placeholder'=>'Enter Employee Reporting to','class' =>'form-control rounded','id' =>'employee_reporting_to']) !!}
            @if ($errors->has('employee_reporting_to'))
            <p class="help-block">{{ $errors->first('employee_reporting_to') }}</p>
            @endif
         </div>
      </div>
      <div class="col-sm-4">
         <!-- hrmsEmploymentType -->
         <div class="form-group @if ($errors->has('employee_employment_type_id')) has-error @endif">
            {!! Form::label('employee_employment_type_id','Employeement Type') !!}
            {{ Form::select('employee_employment_type_id',$hrmsEmploymentType, null,  ['class' =>'form-control rounded','id' =>'employee_employment_type_id']) }}
            @if ($errors->has('employee_employment_type_id'))
            <p class="help-block">{{ $errors->first('employee_employment_type_id') }}</p>
            @endif
         </div>
      </div>
   </div>

   {{-- Row  Ends --}}



   {{-- Row  Starts --}}
   <div class="row">
      {{--
      <div class="col-sm-4">
         <div class="form-group @if ($errors->has('employee_job_category')) has-error @endif">
            {!! Form::label('employee_job_category','Job Category') !!}
            {!! Form::text('employee_job_category',old('employee_job_category'),['placeholder'=>'Enter Employee Job Category','class' =>'form-control rounded','id' =>'employee_job_category']) !!}
            @if ($errors->has('employee_job_category'))
            <p class="help-block">{{ $errors->first('employee_job_category') }}</p>
            @endif
         </div>
      </div>
      <div class="col-sm-4">
         <div class="form-group @if ($errors->has('employee_sub_unit')) has-error @endif">
            {!! Form::label('employee_sub_unit','Sub Unit') !!}
            {!! Form::text('employee_sub_unit',old('employee_sub_unit'),['placeholder'=>'Enter Employee Sub Unit','class' =>'form-control rounded','id' =>'employee_sub_unit']) !!}
            @if ($errors->has('employee_sub_unit'))
            <p class="help-block">{{ $errors->first('employee_sub_unit') }}</p>
            @endif
         </div>
      </div>
      --}}
      <div class="col-sm-4">
         <div class="form-group @if ($errors->has('employee_work_shift_id')) has-error @endif">
            {!! Form::label('employee_work_shift_id','Work Shift') !!}
            {{ Form::select('employee_work_shift_id',$shiftTimings, null,  ['class' =>'form-control rounded','id' =>'employee_work_shift_id']) }}
            @if ($errors->has('employee_work_shift_id'))
            <p class="help-block">{{ $errors->first('employee_work_shift_id') }}</p>
            @endif
         </div>
      </div>
      <div class="col-sm-4">
         <div class="form-group @if ($errors->has('employee_designation')) has-error @endif">
            {!! Form::label('employee_designation','Employeement Designation') !!}
            {!! Form::text('employee_designation',old('employee_designation'),['placeholder'=>'Enter Employee Designation','class' =>'form-control rounded','id' =>'employee_designation']) !!}
            @if ($errors->has('employee_designation'))
            <p class="help-block">{{ $errors->first('employee_designation') }}</p>
            @endif
         </div>
      </div>
   </div>
   {{-- Row  Ends --}}

   {{-- Row Starts --}}
   <div class="row">
      <div class="col-sm-4">
         <div class="form-group @if ($errors->has('employee_contract_start_date')) has-error @endif">
            {!! Form::label('employee_contract_start_date','Contract Start Date') !!}
            {!! Form::text('employee_contract_start_date',old('employee_contract_start_date'),['placeholder'=>'Enter Employee Contract Start Date','class' =>'form-control rounded','id' =>'employee_contract_start_date']) !!}
            @if ($errors->has('employee_contract_start_date'))
            <p class="help-block">{{ $errors->first('employee_contract_start_date') }}</p>
            @endif
         </div>
      </div>
      <div class="col-sm-4">
         <div class="form-group @if ($errors->has('employee_contract_end_date')) has-error @endif">
            {!! Form::label('employee_contract_end_date','Contract End Date') !!}
            {!! Form::text('employee_contract_end_date',old('employee_contract_end_date'),['placeholder'=>'Enter Employee Contract End Date','class' =>'form-control rounded','id' =>'employee_contract_end_date']) !!}
            @if ($errors->has('employee_contract_end_date'))
            <p class="help-block">{{ $errors->first('employee_contract_end_date') }}</p>
            @endif
         </div>
      </div>
      <div class="col-sm-4">
         <div class="form-group @if ($errors->has('employee_primary_skills')) has-error @endif">
            {!! Form::label('employee_primary_skills','Primary Skills') !!}
            {!! Form::text('employee_primary_skills',old('employee_primary_skills'),['placeholder'=>'','class' =>'form-control rounded','id' =>'employee_primary_skills']) !!}
            @if ($errors->has('employee_primary_skills'))
            <p class="help-block">{{ $errors->first('employee_primary_skills') }}</p>
            @endif
         </div>
      </div>
   </div>
   {{-- Row Ends --}}
   {{-- Row Starts --}}
   <div class="row">
      <div class="col-sm-4">
         <div class="form-group @if ($errors->has('employee_secondary_skills')) has-error @endif">
            {!! Form::label('employee_secondary_skills','Secondary Skills') !!}
            {!! Form::text('employee_secondary_skills',old('employee_secondary_skills'),['placeholder'=>'','class' =>'form-control rounded','id' =>'employee_secondary_skills']) !!}
            @if ($errors->has('employee_secondary_skills'))
            <p class="help-block">{{ $errors->first('employee_secondary_skills') }}</p>
            @endif
         </div>
      </div>
   </div>
   {{-- Row Ends --}}
   <hr>
   {{-- Row 1 Starts --}}
   <hr>
   <div class="panel-title">
      <i class="fa fa-user m-right-5"></i> Personal Details
   </div>
   <br><br>
   <div class="row">
      <div class="col-sm-4">
         <div class="form-group @if ($errors->has('employee_first_name')) has-error @endif">
            {!! Form::label('employee_first_name','First Name') !!}
            {!! Form::text('employee_first_name',old('employee_first_name'),['placeholder'=>'Enter Employee First Name','class' =>'form-control rounded','id' =>'employee_first_name']) !!}
            @if ($errors->has('employee_first_name'))
            <p class="help-block">{{ $errors->first('employee_first_name') }}</p>
            @endif
         </div>
      </div>
      <div class="col-sm-4">
         <div class="form-group @if ($errors->has('employee_middle_name')) has-error @endif">
            {!! Form::label('employee_middle_name','Middle Name') !!}
            {!! Form::text('employee_middle_name',old('employee_middle_name'),['placeholder'=>'Enter Employee Middle Name','class' =>'form-control rounded','id' =>'employee_middle_name']) !!}
            @if ($errors->has('employee_middle_name'))
            <p class="help-block">{{ $errors->first('employee_middle_name') }}</p>
            @endif
         </div>
      </div>
      <div class="col-sm-4">
         <div class="form-group @if ($errors->has('employee_last_name')) has-error @endif">
            {!! Form::label('employee_last_name','Last Name') !!}
            {!! Form::text('employee_last_name',old('employee_last_name'),['placeholder'=>'Enter Employee Last Name','class' =>'form-control rounded','id' =>'employee_last_name']) !!}
            @if ($errors->has('employee_last_name'))
            <p class="help-block">{{ $errors->first('employee_last_name') }}</p>
            @endif
         </div>
      </div>
   </div>
   {{-- Row 1 Ends --}}
   {{-- Row 2 Starts --}}
   <div class="row">
      <div class="col-sm-4">
         <div class="form-group @if ($errors->has('employee_code')) has-error @endif">
            {!! Form::label('employee_code','Employee Code') !!}
            {!! Form::text('employee_code',$autoIncrement,['placeholder'=>'Enter Employee Code','class' =>'form-control rounded','id' =>'employee_code']) !!}
            @if ($errors->has('employee_code'))
            <p class="help-block">{{ $errors->first('employee_code') }}</p>
            @endif
         </div>
      </div>
      <div class="col-sm-4">
         <div class="form-group @if ($errors->has('employee_date_of_birth')) has-error @endif">
            {!! Form::label('employee_date_of_birth','Date of birth') !!}
            {!! Form::text('employee_date_of_birth',old('employee_date_of_birth'),['placeholder'=>'Enter Employee Date Of Birth','class' =>'form-control rounded','id' =>'employee_date_of_birth','id'=>'employee_date_of_birth']) !!}
            @if ($errors->has('employee_date_of_birth'))
            <p class="help-block">{{ $errors->first('employee_date_of_birth') }}</p>
            @endif
         </div>
      </div>
      <div class="col-sm-4">
         <div class="form-group @if ($errors->has('employee_gender')) has-error @endif">
            {!! Form::label('employee_gender','Gender') !!}
            <div class="col-sm-12">
               <label class="radio-inline">
               {!! Form::radio('employee_gender', 'Male',null, ['class' =>'icheck-minimal-grey','id'=>'employee_gender_male']) !!}
               Male
               </label>
               <label class="radio-inline">
               {!! Form::radio('employee_gender', 'Female',null, ['class' =>'icheck-minimal-grey','id'=>'employee_gender_female']) !!}
               Female
               </label>
            </div>
            @if ($errors->has('employee_gender'))
            <p class="help-block">{{ $errors->first('employee_gender') }}</p>
            @endif
         </div>
      </div>
   </div>
   {{-- Row 2 Ends --}}
   {{-- Row 3 Starts --}}
   <div class="row">
      <div class="col-sm-4">
         <div class="form-group @if ($errors->has('employee_aadhar_number')) has-error @endif">
            {!! Form::label('employee_aadhar_number','Aadhar Number') !!}
            {!! Form::text('employee_aadhar_number',old('employee_aadhar_number'),['placeholder'=>'Enter Employee Aadhar Number','class' =>'form-control rounded','id' =>'employee_aadhar_number']) !!}
            @if ($errors->has('employee_aadhar_number'))
            <p class="help-block">{{ $errors->first('employee_aadhar_number') }}</p>
            @endif
         </div>
      </div>
      <div class="col-sm-4">
         <div class="form-group @if ($errors->has('employee_marital_status')) has-error @endif">
            {!! Form::label('employee_marital_status','Marital Status') !!}
            {{ Form::select('employee_marital_status',$maritalStatus, null,  ['class' =>'form-control rounded','id' =>'employee_marital_status']) }}
            @if ($errors->has('employee_marital_status'))
            <p class="help-block">{{ $errors->first('employee_marital_status') }}</p>
            @endif
         </div>
      </div>
      <div class="col-sm-4">
         <div class="form-group @if ($errors->has('employee_nationality')) has-error @endif">
            {!! Form::label('employee_nationality','Nationality') !!}
            {{ Form::select('employee_nationality',$nationality, null,  ['class' =>'form-control rounded','id' =>'employee_nationality']) }}
            @if ($errors->has('employee_nationality'))
            <p class="help-block">{{ $errors->first('employee_nationality') }}</p>
            @endif
         </div>
      </div>
   </div>
   {{-- Row 3 Ends --}}

    

   {{--  Row Starts  --}}
   <div class="row">
      <div class="col-sm-3">
            <div class="form-group @if ($errors->has('employee_recent_photo')) has-error @endif">
               {!! Form::label('employee_recent_photo','Recent Photo') !!}
               {!! Form::file('employee_recent_photo', ['class' =>'form-control','id' =>'employee_recent_photo']) !!}
               @if ($errors->has('employee_recent_photo'))
               <p class="help-block">{{ $errors->first('employee_recent_photo') }}</p>
               @endif
            </div>
         </div>
   </div>
   {{--  Row Ends  --}}



   {{--  Row Starts  --}}
<hr>
   <div class="panel-title">
      <i class="fa fa-book m-right-5"></i> Education Details
   </div>
   <h4 class="text-primary">SSLC</h4>
   <div class="row">
      <div class="col-sm-4">
         <div class="form-group @if ($errors->has('employee_sslc_school_name')) has-error @endif">
            {!! Form::label('employee_sslc_school_name','School Name') !!}
            {!! Form::text('employee_sslc_school_name',old('employee_sslc_school_name'),['placeholder'=>'Enter School Name','class' =>'form-control rounded','id' =>'employee_sslc_school_name']) !!}
            @if ($errors->has('employee_sslc_school_name'))
            <p class="help-block">{{ $errors->first('employee_sslc_school_name') }}</p>
            @endif
         </div>
      </div>
      <div class="col-sm-4">
         <div class="form-group @if ($errors->has('employee_sslc_year_of_passing')) has-error @endif">
            {!! Form::label('employee_sslc_year_of_passing','Year Of Passing') !!}
            {!! Form::text('employee_sslc_year_of_passing',old('employee_sslc_year_of_passing'),['placeholder'=>'Enter Employee Middle Name','class' =>'form-control rounded','id' =>'employee_sslc_year_of_passing']) !!}
            @if ($errors->has('employee_sslc_year_of_passing'))
            <p class="help-block">{{ $errors->first('employee_sslc_year_of_passing') }}</p>
            @endif
         </div>
      </div>
      <div class="col-sm-4">
         <div class="form-group @if ($errors->has('employee_sslc_percentage_of_marks')) has-error @endif">
            {!! Form::label('employee_sslc_percentage_of_marks','Marks Percentage') !!}
            {!! Form::text('employee_sslc_percentage_of_marks',old('employee_sslc_percentage'),['placeholder'=>'Enter Marks Percentage','class' =>'form-control rounded','id' =>'employee_sslc_percentage_of_marks']) !!}
            @if ($errors->has('employee_sslc_percentage_of_marks'))
            <p class="help-block">{{ $errors->first('employee_sslc_percentage_of_marks') }}</p>
            @endif
         </div>
      </div>
   </div>

   {{--  Row Ends  --}}

   {{--  Row Starts  --}}

   <h4 class="text-primary">HSC</h4>
   <div class="row">
      <div class="col-sm-4">
         <div class="form-group @if ($errors->has('employee_hsc_school_name')) has-error @endif">
            {!! Form::label('employee_hsc_school_name','School Name') !!}
            {!! Form::text('employee_hsc_school_name',old('employee_hsc_school_name'),['placeholder'=>'Enter School Name','class' =>'form-control rounded','id' =>'employee_hsc_school_name']) !!}
            @if ($errors->has('employee_hsc_school_name'))
            <p class="help-block">{{ $errors->first('employee_hsc_school_name') }}</p>
            @endif
         </div>
      </div>
      <div class="col-sm-4">
         <div class="form-group @if ($errors->has('employee_hsc_year_of_passing')) has-error @endif">
            {!! Form::label('employee_hsc_year_of_passing','Year Of Passing') !!}
            {!! Form::text('employee_hsc_year_of_passing',old('employee_hsc_year_of_passing'),['placeholder'=>'Enter Year Of Passing','class' =>'form-control rounded','id' =>'employee_hsc_year_of_passing']) !!}
            @if ($errors->has('employee_hsc_year_of_passing'))
            <p class="help-block">{{ $errors->first('employee_hsc_year_of_passing') }}</p>
            @endif
         </div>
      </div>
      <div class="col-sm-4">
         <div class="form-group @if ($errors->has('employee_hsc_percentage_of_marks')) has-error @endif">
            {!! Form::label('employee_hsc_percentage_of_marks','Marks Percentage') !!}
            {!! Form::text('employee_hsc_percentage_of_marks',old('employee_sslc_percentage'),['placeholder'=>'Enter Marks Percentage','class' =>'form-control rounded','id' =>'employee_hsc_percentage_of_marks']) !!}
            @if ($errors->has('employee_hsc_percentage_of_marks'))
            <p class="help-block">{{ $errors->first('employee_hsc_percentage_of_marks') }}</p>
            @endif
         </div>
      </div>
   </div>

   {{--  Row Ends  --}}

   {{--  Row Starts  --}}

   <h4 class="text-primary">UG</h4>
   <div class="row">

    <div class="col-sm-3">
         <div class="form-group @if ($errors->has('employee_ug_college_name')) has-error @endif">
            {!! Form::label('employee_ug_college_name','College Name') !!}
            {!! Form::text('employee_ug_college_name',old('employee_ug_college_name'),['placeholder'=>'Enter College Name','class' =>'form-control rounded','id' =>'employee_ug_college_name']) !!}
            @if ($errors->has('employee_ug_college_name'))
            <p class="help-block">{{ $errors->first('employee_ug_college_name') }}</p>
            @endif
         </div>
      </div>
    <div class="col-sm-3">
         <div class="form-group @if ($errors->has('employee_ug_degree_name')) has-error @endif">
            {!! Form::label('employee_ug_degree_name','Degree Name') !!}
            {!! Form::text('employee_ug_degree_name',old('employee_ug_degree_name'),['placeholder'=>'Enter Degree Name','class' =>'form-control rounded','id' =>'employee_ug_degree_name']) !!}
            @if ($errors->has('employee_ug_degree_name'))
            <p class="help-block">{{ $errors->first('employee_ug_degree_name') }}</p>
            @endif
         </div>
      </div>
      <div class="col-sm-3">
         <div class="form-group @if ($errors->has('employee_ug_year_of_passing')) has-error @endif">
            {!! Form::label('employee_ug_year_of_passing','Year Of Passing') !!}
            {!! Form::text('employee_ug_year_of_passing',old('employee_ug_year_of_passing'),['placeholder'=>'Enter Year Of Passing','class' =>'form-control rounded','id' =>'employee_ug_year_of_passing']) !!}
            @if ($errors->has('employee_ug_year_of_passing'))
            <p class="help-block">{{ $errors->first('employee_ug_year_of_passing') }}</p>
            @endif
         </div>
      </div>
      <div class="col-sm-3">
         <div class="form-group @if ($errors->has('employee_ug_percentage_of_marks')) has-error @endif">
            {!! Form::label('employee_ug_percentage_of_marks','Marks Percentage') !!}
            {!! Form::text('employee_ug_percentage_of_marks',old('employee_ug_percentage_of_marks'),['placeholder'=>'Enter Marks Percentage','class' =>'form-control rounded','id' =>'employee_ug_percentage_of_marks']) !!}
            @if ($errors->has('employee_ug_percentage_of_marks'))
            <p class="help-block">{{ $errors->first('employee_ug_percentage_of_marks') }}</p>
            @endif
         </div>
      </div>
   </div>

   {{--  Row Ends  --}}

   {{--  Row Starts  --}}

   <hr>
   <div class="panel-title">
      <i class="fa fa-book m-right-5"></i> PG
   </div>

   

   
<div id="pgEducationDiv"  data-mfield-options='{"section": ".group","btnAdd":"#btnAddPgEducation","btnRemove":".btnRemovePgEducation"}'>
         <div class="row">
            <div class="col-md-12"><button type="button" id="btnAddPgEducation" class="btn btn-primary">Add College</button></div>
         </div>
         <div class="row group">

            <div class="col-sm-3">
         <div class="form-group @if ($errors->has('employee_pg_college_name.*')) has-error @endif">
            {!! Form::label('employee_pg_college_name','College Name') !!}
            {!! Form::text('employee_pg_college_name[]',old(serialize('employee_pg_college_name')),['placeholder'=>'Enter College Name','class' =>'form-control rounded','id' =>'employee_pg_college_name']) !!}
            @if ($errors->has('employee_pg_college_name'))
            <p class="help-block">{{ $errors->first('employee_pg_college_name.*') }}</p>
            @endif
         </div>
      </div>
    <div class="col-sm-3">
         <div class="form-group @if ($errors->has('employee_pg_degree_name.*')) has-error @endif">
            {!! Form::label('employee_pg_degree_name','Degree Name') !!}
            {!! Form::text('employee_pg_degree_name[]',old(serialize('employee_pg_degree_name')),['placeholder'=>'Enter Degree Name','class' =>'form-control rounded','id' =>'employee_pg_degree_name']) !!}
            @if ($errors->has('employee_pg_degree_name'))
            <p class="help-block">{{ $errors->first('employee_pg_degree_name.*') }}</p>
            @endif
         </div>
      </div>
      <div class="col-sm-3">
         <div class="form-group @if ($errors->has('employee_pg_year_of_passing.*')) has-error @endif">
            {!! Form::label('employee_pg_year_of_passing','Year Of Passing') !!}
            {!! Form::text('employee_pg_year_of_passing[]',old(serialize('employee_pg_year_of_passing')),['placeholder'=>'Enter Year Of Passing','class' =>'form-control rounded','id' =>'employee_pg_year_of_passing']) !!}
            @if ($errors->has('employee_pg_year_of_passing'))
            <p class="help-block">{{ $errors->first('employee_pg_year_of_passing.*') }}</p>
            @endif
         </div>
      </div>
      <div class="col-sm-3">
         <div class="form-group @if ($errors->has('employee_pg_percentage_of_marks.*')) has-error @endif">
            {!! Form::label('employee_pg_percentage_of_marks','Marks Percentage') !!}
            {!! Form::text('employee_pg_percentage_of_marks[]',old(serialize('employee_pg_percentage_of_marks')),['placeholder'=>'Enter Marks Percentage','class' =>'form-control rounded','id' =>'employee_pg_percentage_of_marks']) !!}
            @if ($errors->has('employee_pg_percentage_of_marks'))
            <p class="help-block">{{ $errors->first('employee_pg_percentage_of_marks.*') }}</p>
            @endif
         </div>
      </div>


      
            <div class="col-md-3">
               <button type="button" class="btn btn-danger btnRemovePgEducation">Remove College</button>
            </div>
         </div>
      </div>


   {{--  Row Ends  --}}

   {{--  Row Starts  --}}
<hr>
   <div class="panel-title">
      <i class="fa fa-certificate m-right-5"></i> Certification Details
   </div>

   
<div id="certificationsDiv"  data-mfield-options='{"section": ".group","btnAdd":"#btnAddCertificate","btnRemove":".btnRemoveCertificate"}'>
         <div class="row">
            <div class="col-md-12"><button type="button" id="btnAddCertificate" class="btn btn-primary">Add Certficate</button></div>
         </div>
         <div class="row group">
            <div class="col-sm-3">
         <div class="form-group @if ($errors->has('employee_certification_name.*')) has-error @endif">
            {!! Form::label('employee_certification_name','Certificate Name', []) !!}
            {!! Form::text('employee_certification_name[]',old(serialize('employee_certification_name')),['placeholder'=>'Enter Certificate Name','class' =>'form-control rounded','id' =>'employee_certification_name']) !!}
            @if ($errors->has('employee_certification_name.*'))
            <p class="help-block">{{ $errors->first('employee_certification_name.*') }}</p>
            @endif
           </div>
      </div>
      <div class="col-sm-3">
         <div class="form-group @if ($errors->has('employee_certification_valid_till.*')) has-error @endif">
            {!! Form::label('employee_certification_valid_till','Valid Till', []) !!}
            {!! Form::text('employee_certification_valid_till[]',old(serialize('employee_certification_valid_till')),['placeholder'=>'Enter Certificate Valid Till','class' =>'form-control rounded','id' =>'employee_certification_valid_till']) !!}
            @if ($errors->has('employee_certification_valid_till.*'))
            <p class="help-block">{{ $errors->first('employee_certification_valid_till.*') }}</p>
            @endif
      </div>
      </div>
      <div class="col-sm-3">
         <div class="form-group @if ($errors->has('employee_certification_grade.*')) has-error @endif">

            {!! Form::label('employee_certification_grade','Grade', []) !!}
            {!! Form::text('employee_certification_grade[]',old(serialize('employee_certification_grade')),['placeholder'=>'Enter Certificate Grade','class' =>'form-control rounded','id' =>'employee_certification_grade']) !!}
            @if ($errors->has('employee_certification_grade.*'))
            <p class="help-block">{{ $errors->first('employee_certification_grade.*') }}</p>
            @endif
         </div>
      </div>

      <div class="col-sm-3">
         <div class="form-group @if ($errors->has('employee_certification_institution_name.*')) has-error @endif">
            {!! Form::label('employee_certification_institution_name','Institution Name', []) !!}
            {!! Form::text('employee_certification_institution_name[]',old(serialize('employee_certification_institution_name')),['placeholder'=>'Enter Institution Name','class' =>'form-control rounded','id' =>'employee_certification_institution_name']) !!}
            @if ($errors->has('employee_certification_institution_name.*'))
            <p class="help-block">{{ $errors->first('employee_certification_institution_name.*') }}</p>
            @endif
         </div>
      </div>
            <div class="col-md-3">
               <button type="button" class="btn btn-danger btnRemoveCertificate">Remove Certficate</button>
            </div>
         </div>
      </div>


   {{--  Row Ends  --}}



   {{-- Row 4 Starts --}}
   <hr>
   <div class="panel-title">
      <i class="fa fa-map-marker m-right-5"></i> Address And Location
   </div>
   <br><br>
   <div class="row">
      <div class="col-sm-4">
         <div class="form-group @if ($errors->has('employee_door_no')) has-error @endif">
            {!! Form::label('employee_door_no','Door No') !!}
            {!! Form::text('employee_door_no',old('employee_door_no'),['placeholder'=>'Enter Employee Door No','class' =>'form-control rounded','id' =>'employee_door_no']) !!}
            @if ($errors->has('employee_door_no'))
            <p class="help-block">{{ $errors->first('employee_door_no') }}</p>
            @endif
         </div>
      </div>
      <div class="col-sm-4">
         <div class="form-group @if ($errors->has('employee_street_name')) has-error @endif">
            {!! Form::label('employee_street_name','Street Name') !!}
            {!! Form::text('employee_street_name',old('employee_street_name'),['placeholder'=>'Enter Employee Street Name','class' =>'form-control rounded','id' =>'employee_street_name']) !!}
            @if ($errors->has('employee_street_name'))
            <p class="help-block">{{ $errors->first('employee_street_name') }}</p>
            @endif
         </div>
      </div>
      <div class="col-sm-4">
         <div class="form-group @if ($errors->has('employee_city')) has-error @endif">
            {!! Form::label('employee_city','City') !!}
            {!! Form::text('employee_city',old('employee_city'),['placeholder'=>'Enter Employee City','class' =>'form-control rounded','id' =>'employee_city']) !!}
            @if ($errors->has('employee_city'))
            <p class="help-block">{{ $errors->first('employee_city') }}</p>
            @endif
         </div>
      </div>
   </div>
   {{-- Row 4 Ends --}}
   {{--  Row 5 Starts  --}}
   <div class="row">
      <div class="col-sm-4">
         <div class="form-group @if ($errors->has('employee_state')) has-error @endif">
            {!! Form::label('employee_state','State') !!}
            {!! Form::text('employee_state',old('employee_state'),['placeholder'=>'Enter Employee Sate','class' =>'form-control rounded','id' =>'employee_state']) !!}
            @if ($errors->has('employee_state'))
            <p class="help-block">{{ $errors->first('employee_state') }}</p>
            @endif
         </div>
      </div>
      <div class="col-sm-4">
         <div class="form-group @if ($errors->has('employee_country')) has-error @endif">
            {!! Form::label('employee_country','Country') !!}
            {!! Form::text('employee_country',old('employee_country'),['placeholder'=>'Enter Employee Contry Name','class' =>'form-control rounded','id' =>'employee_country']) !!}
            @if ($errors->has('employee_country'))
            <p class="help-block">{{ $errors->first('employee_country') }}</p>
            @endif
         </div>
      </div>
      <div class="col-sm-4">
         <div class="form-group @if ($errors->has('employee_zip_code')) has-error @endif">
            {!! Form::label('employee_zip_code','ZipCode') !!}
            {!! Form::text('employee_zip_code',old('employee_zip_code'),['placeholder'=>'Enter Employee ZipCode','class' =>'form-control rounded','id' =>'employee_zip_code']) !!}
            @if ($errors->has('employee_zip_code'))
            <p class="help-block">{{ $errors->first('employee_zip_code') }}</p>
            @endif
         </div>
      </div>
   </div>
   {{-- Row 5 Ends --}}
   {{--  Row 6 Starts  --}}
   <hr>
   <div class="panel-title">
      <i class="fa fa-phone m-right-5"></i> Contact Details
   </div>
   <br><br>
   <div class="row">
      <div class="col-sm-4">
         <div class="form-group @if ($errors->has('employee_home_telephone')) has-error @endif">
            {!! Form::label('employee_home_telephone','Telephone No') !!}
            {!! Form::text('employee_home_telephone',old('employee_home_telephone'),['placeholder'=>'Enter Employee Telephone number','class' =>'form-control rounded','id' =>'employee_home_telephone']) !!}
            @if ($errors->has('employee_home_telephone'))
            <p class="help-block">{{ $errors->first('employee_home_telephone') }}</p>
            @endif
         </div>
      </div>
      <div class="col-sm-4">
         <div class="form-group @if ($errors->has('employee_mobile_number')) has-error @endif">
            {!! Form::label('employee_mobile_number','Mobile No') !!}
            {!! Form::text('employee_mobile_number',old('employee_mobile_number'),['placeholder'=>'Enter Employee Mobile Number','class' =>'form-control rounded','id' =>'employee_mobile_number']) !!}
            @if ($errors->has('employee_mobile_number'))
            <p class="help-block">{{ $errors->first('employee_mobile_number') }}</p>
            @endif
         </div>
      </div>
      <div class="col-sm-4">
         <div class="form-group @if ($errors->has('employee_email_id')) has-error @endif">
            {!! Form::label('employee_email_id','Email Id') !!}
            {!! Form::text('employee_email_id',old('employee_email_id'),['placeholder'=>'Enter Employee Email Id','class' =>'form-control rounded','id' =>'employee_email_id']) !!}
            @if ($errors->has('employee_email_id'))
            <p class="help-block">{{ $errors->first('employee_email_id') }}</p>
            @endif
         </div>
      </div>
   </div>
   {{-- Row 6 Ends --}}
   {{--  Row 7 Starts  --}}
   {{-- Row 7 Ends --}}
   {{--  Row 8 Starts  --}}
   {{-- Row 8 Ends --}}
   {{--  Row 9 Starts  --}}



   {{--  Row Starts  --}}
<hr>
   <div class="panel-title">
         <i class="fa fa-user m-right-5"></i> Employeement Details
      </div>
      
   
      <div id="employmentDetailsDiv"  data-mfield-options='{"section": ".group","btnAdd":"#btnAddEmployment","btnRemove":".btnRemoveEmployment"}'>

         <div class="row">
            <div class="col-md-12"><button type="button" id="btnAddEmployment" class="btn btn-primary">Add Company</button></div>
         </div>


         <div class="row group">
            <hr>

            <div class="col-sm-5">
         <div class="form-group @if ($errors->has('employee_previous_company_name')) has-error @endif">
            {!! Form::label('employee_previous_company_name','Previous Company Name') !!}
            {!! Form::text('employee_previous_company_name[]',old(serialize('employee_previous_company_name')),['placeholder'=>'Previous Company Name','class' =>'form-control rounded','id' =>'employee_previous_company_name']) !!}
            @if ($errors->has('employee_previous_company_name'))
            <p class="help-block">{{ $errors->first('employee_previous_company_name') }}</p>
            @endif
         </div>
      </div>
      <div class="col-sm-5">
         <div class="form-group @if ($errors->has('employee_previous_designation')) has-error @endif">
            {!! Form::label('employee_previous_designation','Designation') !!}
            {!! Form::text('employee_previous_designation[]',old(serialize('employee_previous_designation')),['placeholder'=>'Enter Employee Previous Designation','class' =>'form-control rounded','id' =>'employee_previous_designation']) !!}
            @if ($errors->has('employee_previous_designation'))
            <p class="help-block">{{ $errors->first('employee_previous_designation') }}</p>
            @endif
         </div>
      </div>

      <div class="col-sm-4">
         <div class="form-group @if ($errors->has('employee_previous_employment_start_date')) has-error @endif">
            {!! Form::label('employee_previous_employment_start_date','Start Date') !!}
            {!! Form::text('employee_previous_employment_start_date[]',old(serialize('employee_previous_employment_start_date')),['placeholder'=>'Enter Start Date','class' =>'form-control rounded','id' =>'employee_previous_employment_start_date']) !!}
            @if ($errors->has('employee_previous_employment_start_date'))
            <p class="help-block">{{ $errors->first('employee_previous_employment_start_date') }}</p>
            @endif
         </div>
      </div>
      <div class="col-sm-4">
         <div class="form-group @if ($errors->has('employee_previous_employment_end_date')) has-error @endif">
            {!! Form::label('employee_previous_employment_end_date','End Date') !!}
            {!! Form::text('employee_previous_employment_end_date[]',old(serialize('employee_previous_employment_end_date')),['placeholder'=>'End Date','class' =>'form-control rounded','id' =>'employee_previous_employment_end_date']) !!}
            @if ($errors->has('employee_previous_employment_end_date'))
            <p class="help-block">{{ $errors->first('employee_previous_employment_end_date') }}</p>
            @endif
         </div>
      </div>
      <div class="col-sm-4">
         <div class="form-group @if ($errors->has('employee_previous_employment_reason_for_leaving')) has-error @endif">
            {!! Form::label('employee_previous_employment_reason_for_leaving','Reason For Leaving') !!}
            {!! Form::textarea('employee_previous_employment_reason_for_leaving[]',old(serialize('employee_previous_employment_reason_for_leaving')),['class' =>'form-control rounded','placeholder' =>'Detailed Description as reason for leaving', 'rows' => 5, 'cols' => 80,'id'=>'employee_previous_employment_reason_for_leaving']) !!}
            @if ($errors->has('employee_previous_employment_reason_for_leaving'))
            <p class="help-block">{{ $errors->first('employee_previous_employment_reason_for_leaving') }}</p>
            @endif
         </div>
      </div>

      <div class="col-sm-4">
         <div class="form-group @if ($errors->has('employee_previous_employment_contact_person_name')) has-error @endif">
            {!! Form::label('employee_previous_employment_contact_person_name','Contact Person Name') !!}
            {!! Form::text('employee_previous_employment_contact_person_name[]',old(serialize('employee_previous_employment_contact_person_name')),['placeholder'=>'Contact Person Name','class' =>'form-control rounded','id' =>'employee_previous_employment_contact_person_name']) !!}
            @if ($errors->has('employee_previous_employment_contact_person_name'))
            <p class="help-block">{{ $errors->first('employee_previous_employment_contact_person_name') }}</p>
            @endif
         </div>
      </div>
      <div class="col-sm-4">
         <div class="form-group @if ($errors->has('employee_previous_employment_contact_person_email')) has-error @endif">
            {!! Form::label('employee_previous_employment_contact_person_email','Contact Person Email') !!}
            {!! Form::text('employee_previous_employment_contact_person_email[]',old(serialize('employee_previous_employment_contact_person_email')),['placeholder'=>'Contact Person Email','class' =>'form-control rounded','id' =>'employee_previous_employment_contact_person_email']) !!}
            @if ($errors->has('employee_previous_employment_contact_person_email'))
            <p class="help-block">{{ $errors->first('employee_previous_employment_contact_person_email') }}</p>
            @endif
         </div>
      </div>
      <div class="col-sm-4">
         <div class="form-group @if ($errors->has('employee_previous_employment_contact_person_phone')) has-error @endif">
            {!! Form::label('employee_previous_employment_contact_person_phone','Contact Person Phone Number') !!}
            {!! Form::text('employee_previous_employment_contact_person_phone[]',old(serialize('employee_previous_employment_contact_person_phone')),['placeholder'=>'Contact Person Phone Number','class' =>'form-control rounded','id' =>'employee_previous_employment_contact_person_phone']) !!}
            @if ($errors->has('employee_previous_employment_contact_person_phone'))
            <p class="help-block">{{ $errors->first('employee_previous_employment_contact_person_phone') }}</p>
            @endif
         </div>
      </div>

      <div class="col-sm-4">
         <div class="form-group @if ($errors->has('employee_years_of_experience')) has-error @endif">
            {!! Form::label('employee_years_of_experience','Years of Experience') !!}
            {{ Form::select('employee_years_of_experience[]',$experienceYears, null,  ['class' =>'form-control','id' =>'employee_years_of_experience']) }}
            @if ($errors->has('employee_years_of_experience'))
            <p class="help-block">{{ $errors->first('employee_years_of_experience') }}</p>
            @endif
         </div>
      </div>
      <div class="col-sm-4">
         <div class="form-group @if ($errors->has('employee_months_of_experience')) has-error @endif">
            {!! Form::label('employee_months_of_experience','Months of Experience') !!}
            {{ Form::select('employee_months_of_experience[]',$experienceMonths, null,  ['class' =>'form-control rounded','id' =>'employee_months_of_experience']) }}
            @if ($errors->has('employee_months_of_experience'))
            <p class="help-block">{{ $errors->first('employee_months_of_experience') }}</p>
            @endif
         </div>
      </div>

      <div class="col-sm-10">
            <div class="form-group @if ($errors->has('employee_documents_collected')) has-error @endif">
               {!! Form::label('documentscollected','Documents Collected') !!}
               <div class="col-sm-12">
                  @foreach ($documentCollectedArray as $documentCollectedArrayKey => $documentCollectedArrayValue)
                  <label class="checkbox-inline">
                  {!! Form::checkbox('employee_documents_collected[]', $documentCollectedArrayValue, null) !!}
                  {{ ucfirst($documentCollectedArrayValue) }}
                  </label>
                  @endforeach
               </div>
               @if ($errors->has('employee_documents_collected'))
               <p class="help-block">{{ $errors->first('employee_documents_collected') }}</p>
               @endif
            </div>
         </div>


         
      
<div class="form-group row">
    <label for="staticEmail" class="col-sm-2 col-form-label"></label>
    <div class="col-sm-10">
      
    </div>
  </div>
  <div class="form-group row">
    <label for="inputPassword" class="col-sm-2 col-form-label">Files</label>
    <div class="col-sm-10">
      
    </div>
  </div>
      

         <div class="col-sm-3">
            <div class="form-group @if ($errors->has('employee_relieving_letter')) has-error @endif">
               {!! Form::label('employee_relieving_letter','Relieving Letter') !!}
               {!! Form::file('employee_relieving_letter[]', ['class' =>'form-control','id' =>'employee_relieving_letter']) !!}
               @if ($errors->has('employee_relieving_letter'))
               <p class="help-block">{{ $errors->first('employee_relieving_letter') }}</p>
               @endif
            </div>
         </div>

         <div class="col-sm-3">
            <div class="form-group @if ($errors->has('employee_experience_letter')) has-error @endif">
               {!! Form::label('employee_experience_letter','Experience Letter') !!}
               {!! Form::file('employee_experience_letter[]', ['class' =>'form-control','id' =>'employee_experience_letter']) !!}
               @if ($errors->has('employee_experience_letter'))
               <p class="help-block">{{ $errors->first('employee_experience_letter') }}</p>
               @endif
            </div>
         </div>

         <div class="col-sm-3">
            <div class="form-group @if ($errors->has('employee_payslips')) has-error @endif">
               {!! Form::label('employee_payslips','Payslips') !!}
               {!! Form::file('employee_payslips[]', ['class' =>'form-control','id' =>'employee_payslips','multiple' => 'multiple']) !!}
               @if ($errors->has('employee_payslips'))
               <p class="help-block">{{ $errors->first('employee_payslips') }}</p>
               @endif
            </div>
         </div>

      





            <div class="col-md-3">
               <button type="button" class="btn btn-danger btnRemoveEmployment">Remove Company</button>
            </div>
            
         </div>

      </div>


   {{--  Row Ends  --}}

   




   



   </div>
</div>





<div class="form-layout-footer">
   <div class="form-group text-right">
      <div class="col-sm-12">

{{ Form::button('<i class="fa fa-save"></i> Save',['class' => 'btn btn-success','type' => 'submit']) }}

{{ Form::button('<i class="fa fa-ban"></i> Clear',['class' => 'btn btn-danger','type' => 'reset']) }}

      </div>
   </div>
   <!-- /.form-group -->
</div>
