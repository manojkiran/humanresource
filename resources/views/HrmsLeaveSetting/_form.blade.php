	<div class="form-layout">
<div class="form-layout-body">

   {{-- Row 0 Starts --}}
   <div class="row">

      <div class="col-sm-5">
         <div class="form-group @if ($errors->has('leave_from_date')) has-error @endif">
            {!! Form::label('leave_from_date','From Date') !!}
            {!! Form::text('leave_from_date',old('leave_from_date'),['placeholder'=>'Choose From Date','class' =>'form-control rounded','id' =>'leave_from_date']) !!}
            @if ($errors->has('leave_from_date'))
            <p class="help-block">{{ $errors->first('leave_from_date') }}</p>
            @endif
         </div>
      </div>

      <div class="col-sm-5">
         <div class="form-group @if ($errors->has('leave_to_date')) has-error @endif">
            {!! Form::label('leave_to_date','To Date') !!}
            {!! Form::text('leave_to_date',old('leave_to_date'),['placeholder'=>'Choose To Date','class' =>'form-control rounded','id' =>'leave_to_date']) !!}
            @if ($errors->has('leave_to_date'))
            <p class="help-block">{{ $errors->first('leave_to_date') }}</p>
            @endif
         </div>
      </div>

   </div>

   {{-- Row  Ends --}}

   {{-- Row 0 Starts --}}
   <br>
   <div class="row">





      <div class="col-sm-5">

         <div class="form-group @if ($errors->has('leave_repeats_annually')) has-error @endif">
            {!! Form::label('leave_repeats_annually','Repeats Annually ?') !!}
            <div class="col-sm-12">

                @foreach ($leaveRepeatsAnnually as $leaveRepeatsAnnuallyKey => $leaveRepeatsAnnuallyValue )
	                <label class="radio-inline">
	                {!! Form::radio('leave_repeats_annually', $leaveRepeatsAnnuallyKey,null, ['class' =>'icheck-minimal-grey']) !!}
	                {{ $leaveRepeatsAnnuallyValue }}
	                </label>

                @endforeach
                </div>

            @if ($errors->has('leave_repeats_annually'))
            <p class="help-block">{{ $errors->first('leave_repeats_annually') }}</p>
            @endif
         </div>
      </div>

      <div class="col-sm-5">
         <div class="form-group @if ($errors->has('leave_leave_type')) has-error @endif">
            {!! Form::label('leave_leave_type','Leave Type') !!}

            {!! Form::select('leave_leave_type', $leaveSettingsType,null,['placeholder'=>'Choose To Date','class' =>'form-control rounded','id' =>'leave_leave_type']) !!}

            @if ($errors->has('leave_leave_type'))
            <p class="help-block">{{ $errors->first('leave_leave_type') }}</p>
            @endif
         </div>
      </div>



   </div>

   {{-- Row  Ends --}}

   {{-- Row  Starts --}}
   <br>
   <div class="row">

       <div class="col-sm-5">
        <div class="form-group @if ($errors->has('leave_description')) has-error @endif">
            {!! Form::label('leave_description','Leave Description') !!}
           {!! Form::text('leave_description',old('leave_description'),['placeholder'=>'Enter Leave Description','class' =>'form-control rounded','id' =>'leave_description']) !!}
            @if ($errors->has('leave_description'))
            <p class="help-block">{{ $errors->first('leave_description') }}</p>
            @endif
         </div>
       </div>

       <div class="col-sm-5">

         <div class="form-group @if ($errors->has('leave_include_all_locations')) has-error @endif">
            {!! Form::label('leave_include_all_locations','Include All Location ?') !!}
            <div class="col-sm-12">

                @foreach ($leaveIncludeAllLocation as $leaveIncludeAllLocationKey => $leaveIncludeAllLocationValue )
	                <label class="radio-inline">
	                {!! Form::radio('leave_include_all_locations', $leaveIncludeAllLocationKey,null, ['class' =>'icheck-minimal-grey']) !!}
	                {{ $leaveIncludeAllLocationValue }}
	                </label>

                @endforeach
                </div>

            @if ($errors->has('leave_include_all_locations'))
            <p class="help-block">{{ $errors->first('leave_include_all_locations') }}</p>
            @endif
         </div>
      </div>

   </div>
   {{-- Row  Ends --}}




</div>
</div>
