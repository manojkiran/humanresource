@extends('layouts.app')
@section('title', 'Permission')
@section('content')

    <div class="row">
        <div class="col-md-5">
            <h3 class="modal-title">{{ $result->total() }} {{ str_plural('Permission', $result->count()) }} </h3>
        </div>
        <div class="col-md-7 page-action text-right">
            @can('create.permissions')
                <a href="{{ route('permissions.create') }}" class="btn btn-primary btn-sm"> <i class="glyphicon glyphicon-plus-sign"></i> Create</a>
            @endcan
        </div>
    </div>

    <div class="result-set">
        <table class="table table-bordered table-striped table-hover" id="data-table">
            <thead>
               <tr>
                        <th>Id</th>
                        <th>Name</th>
                        <th>Role Associated With  Permisions</th>
                        <th>Module Name</th>
                        @can('edit.permissions','delete.permissions')
                        <th class="text-center">Actions</th>
                        @endcan
                     </tr>
                  </thead>
                  <tbody>
                     @foreach($result as $item)
                     <tr>
                        <td>{{ $loop->iteration }}</td>
                        <td>{{ $item->name }}</td>
                        <td><label class="label label-info">{{ $item->roles->implode('description', ', ') }}</label></td>
                        <td>{{ $item->moduleName['name'] }}</td>
                        @can('edit.permissions','delete.permissions')
                        <td class="text-center">
                           @include('backend.permissions.actions', [
                           'entity' => 'permissions',
                           'id' => Crypt::encrypt($item->id)
                           ])
                        </td>
                        @endcan
                     </tr>
                     @endforeach
                  </tbody>
            </thead>
            
        </table>

        <div class="text-center">
            {{ $result->links() }}
        </div>
    </div>

@endsection