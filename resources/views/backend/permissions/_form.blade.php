<!-- Permission Form Input -->
<div class="form-group @if ($errors->has('name')) has-error @endif">
    {{ Form::label('name', 'Permission Name') }}
    {{ Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Permission name.action Eg:posts.create']) }}
    @if ($errors->has('name')) <p class="help-block">{{ $errors->first('name') }}</p> @endif
</div>
<!-- Permission Description Form Input -->
<div class="form-group @if ($errors->has('description')) has-error @endif">
    {{ Form::label('description', 'Permission Description') }}
    {{ Form::text('description', null, ['class' => 'form-control', 'placeholder' => 'Permission Description']) }}
    @if ($errors->has('description')) <p class="help-block">{{ $errors->first('description') }}</p> @endif
</div>

<!-- Roles Form Input -->
<div class="form-group @if ($errors->has('roles')) has-error @endif">
    {{ Form::label('roles', 'Roles') }}
    @foreach($roles as $role)
        <div class="checkbox">
            <label class="{{ str_contains($role->name, 'delete') ? 'text-danger' : '' }}">
              <div class="col-md-12">
                {{ Form::checkbox("roles[]", $role->id) }} {{ $role->description }}
                </div>
            </label>
        </div>
    @endforeach   
    @if ($errors->has('roles')) <p class="help-block">{{ $errors->first('roles') }}</p> @endif
</div>

<!-- Module Name Form Input -->
<div class="form-group @if ($errors->has('module_id')) has-error @endif">
    {{ Form::label('module_id', 'Module Name') }}
    {{ Form::select('module_id', $modules, null,  ['class' => 'form-control']) }}
    @if ($errors->has('module_id')) <p class="help-block">{{ $errors->first('module_id') }}</p> @endif
</div>