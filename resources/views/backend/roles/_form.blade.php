<!-- Name Form Input -->
<div class="form-group @if ($errors->has('name')) has-error @endif">
    {!! Form::label('name', 'Role Name') !!}
    {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Role Name Eg: subscriber,editor']) !!}
    @if ($errors->has('name')) <p class="help-block">{{ $errors->first('name') }}</p> @endif
</div>
<!-- Name Form Input -->
<div class="form-group @if ($errors->has('description')) has-error @endif">
    {!! Form::label('description', 'Permission Description') !!}
    {!! Form::text('description', null, ['class' => 'form-control', 'placeholder' => 'Permission Description']) !!}
    @if ($errors->has('description')) <p class="help-block">{{ $errors->first('description') }}</p> @endif
</div>
{!! Form::label('permissions', 'Permissions',['class'=>'col-md-4 col-form-label text-md-right']) !!}
@php
echo App\Models\Role::generatePermissonGrouping($modules);
@endphp


