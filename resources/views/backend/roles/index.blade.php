@extends('layouts.app')
@section('title', 'Roles')
@section('content')

    <div class="row">
        <div class="col-md-5">
            <h3 class="modal-title">{{ $result->total() }} {{ str_plural('Role', $result->count()) }} </h3>
        </div>
        <div class="col-md-7 page-action text-right">
            @can('create.roles')
                <a href="{{ route('roles.create') }}" class="btn btn-primary btn-sm"> <i class="glyphicon glyphicon-plus-sign"></i> Create</a>
            @endcan
        </div>
    </div>

    <div class="result-set">
        <table class="table table-bordered table-striped table-hover" id="data-table">
            <thead>
               <tr>
                <th>Id</th>
                <th>Name</th>
                <th>Permisions</th>
                @can('edit.roles','delete.roles')
                        <th class="text-center">Actions</th>
                @endcan
            </tr>
            </thead>
            <tbody>
            @foreach($result as $item)
                <tr>
                    <td>{{ $loop->iteration }}</td>
                    <td>{{ $item->name }}</td>
                    <td><label class="label label-info">{{ $item->permissions->implode('description', ',') }}</label></td>
                    @can('edit.roles','delete.roles')
                        <td class="text-center">
                            @if($item->name != 'Admin')
                               @include('backend.roles.actions', [
                               'entity' => 'roles',
                               'id' => Crypt::encrypt($item->id)
                               ])
                           @endif
                        </td>
                    @endcan
                </tr>
            @endforeach
            </tbody>
            </thead>
            
        </table>

        <div class="text-center">
            {{ $result->links() }}
        </div>
    </div>

@endsection