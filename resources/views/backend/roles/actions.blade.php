@can('edit.roles')
    <a href="{{ route($entity.'.edit', [str_singular($entity) => $id])  }}" class="btn btn-primary btn-sm">
        <i class="glyphicon glyphicon-edit">Edit</i></a>
@endcan

@can('delete.roles')
    {!! Form::open( ['method' => 'delete', 'url' => route($entity.'.destroy', ['roles' => $id]), 'style' => 'display: inline', 'onSubmit' => 'return confirm("Are yous sure wanted to delete it?")']) !!}
        <button type="submit" class="btn btn-danger btn-sm">
            <i class="glyphicon glyphicon-trash">Delete</i>
        </button>
    {!! Form::close() !!}
@endcan



