@extends('layouts.app')

@section('title', 'Create User')

@section('content')

    <div class="row">
        <div class="col-md-5">
            <h3>Create</h3>
        </div>
        <div class="col-md-7 page-action text-right">
            <a href="{{ route('users.index') }}" class="btn btn-danger btn-sm"> <i class="glyphicon glyphicon-arrow-left"></i> Back</a>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            {!! Form::open(['route' => ['users.store'],'autocomplete' => 'off','files' => 'true','enctype'=>'multipart/form-data' ]) !!}
                @include('backend.users._form')
                <!-- Submit Form Button -->
                {!! Form::submit( 'Save', ['class' => 'btn btn-success', 'name' => 'submitbutton', 'value' => 'Save'])!!}

               {!! Form::submit( 'Save And Edit', ['class' => 'btn btn-primary', 'name' => 'submitbutton', 'value' => 'Save And Edit']) !!}

               {!! Form::submit( 'Save And Add New', ['class' => 'btn btn-primary', 'name' => 'submitbutton', 'value' => 'Save And Add New']) !!}
            {!! Form::close() !!}
        </div>
    </div>
@endsection