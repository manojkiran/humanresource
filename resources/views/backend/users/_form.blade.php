<!-- Email Form Input -->
<div class="form-group row">
   {{ Form::label('name', 'Name',['class'=>'col-md-4 col-form-label text-md-right']) }}
      {{ Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Name of the User']) }}
      @if ($errors->has('name')) 
      <p class="help-block danger">{{ $errors->first('name') }}</p>
      @endif
</div>
<!-- Email Form Input -->
<div class="form-group row @if ($errors->has('email')) has-error @endif">
   {{ Form::label('email', 'Email',['class'=>'col-md-4 col-form-label text-md-right']) }}
      {{ Form::email('email', null, ['class' => 'form-control', 'placeholder' => 'Email of the User']) }}
   @if ($errors->has('email')) 
   <p class="help-block">{{ $errors->first('email') }}</p>
   @endif
</div>
<!-- password Form Input -->
<div class="form-group row @if ($errors->has('password')) has-error @endif">
   {{ Form::label('password', 'Create Password',['class'=>'col-md-4 col-form-label text-md-right']) }}
   {{ Form::password('password', ['class' => 'form-control', 'placeholder' => 'Password']) }}
   @if ($errors->has('password')) 
   <p class="help-block">{{ $errors->first('password') }}</p>
   @endif
</div>


<!-- Email Form Input -->
<div class="form-group row @if ($errors->has('roles')) has-error @endif">
   {{ Form::label('roles', 'Roles',['class'=>'col-md-4 col-form-label text-md-right']) }}

      @foreach($roles as $role)
      <div class="checkbox">
         <div class="col-md-12">
            {{ Form::checkbox("roles[]", $role->id) }} {{ $role->description }}
         </div>
      </div>
      @endforeach     

      @if ($errors->has('roles')) 
      <p class="help-block danger">{{ $errors->first('roles') }}</p>
      @endif
</div>
<!-- Email Form Input -->

@php
echo App\Models\Role::generatePermissonGrouping($modules);
@endphp
