@extends('layouts.app')
@section('title', 'Users')
@section('content')

    <div class="row">
        <div class="col-md-5">
            <h3 class="modal-title">{{ $result->total() }} {{ str_plural('User', $result->count()) }} </h3>
        </div>
        <div class="col-md-7 page-action text-right">
            @can('create.users')
                <a href="{{ route('users.create') }}" class="btn btn-primary btn-sm"> <i class="glyphicon glyphicon-plus-sign"></i> Create</a>
            @endcan
        </div>
    </div>

    <div class="result-set">
        <table class="table table-bordered table-striped table-hover" id="data-table">
            <tr>
                <th>Id</th>
                <th>Name</th>
                <th>Email</th>
                <th>Roles</th>
                <th>Extra Permisions</th>
                <th>Created At</th>
                @can('edit.users','delete.users')
                    <th class="text-center">Actions</th>
                @endcan
            </tr>
            </thead>
            <tbody>
            @foreach($result as $item)
                <tr>
                    <td>{{ $loop->iteration }}</td>
                    <td>{{ $item->name }}</td>
                    <td>{{ $item->email }}</td>
                    <td>{{ $item->roles->implode('description', ', ') }}</td>
                    <td>{{ $item->permissions->implode('description', ', ') }}</td>
                    <td>{{ $item->created_at->toFormattedDateString() }}</td>
                    @can('edit.users','delete.users')
                        <td class="text-center">
                           @include('backend.users.actions', [
                           'entity' => 'users',
                           'id' => Crypt::encrypt($item->id)
                           ])
                        </td>
                    @endcan
                </tr>
            @endforeach
            </tbody>
            </thead>
            
        </table>

        <div class="text-center">
            {{ $result->links() }}
        </div>
    </div>

@endsection