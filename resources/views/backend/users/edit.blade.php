@extends('layouts.app')

@section('title', 'Create Permission')

@section('content')
<div class="container">
   <div class="row justify-content-center">
      <div class="col-md-8">
         <div class="card-header">
            <div class="text-right">
               Permissions
               @can('create.permissions')
               <a href="{{ route('permissions.create') }}" class="btn btn-primary btn-sm"> <i class="glyphicon glyphicon-plus-sign"></i> Create</a>
               @endcan
            </div>
         </div>
         <div class="card">
            <div class="row">
        <div class="col-md-5">
            <h3>Create</h3>
        </div>
        <div class="col-md-7 page-action text-right">
            <a href="{{ route('users.index') }}" class="btn btn-default btn-sm"> <i class="fa fa-arrow-left"></i> Back</a>
        </div>
    </div>


    <div class="row">
        <div class="col-lg-12">
            {!! Form::model($users, ['method' => 'PUT', 'route' => ['users.update',  $users->id ],'autocomplete' => 'off','files' => 'true','enctype'=>'multipart/form-data' ]) !!}

                @include('backend.users._form')
                <!-- Submit Form Button -->



       {!! Form::submit( 'Save', ['class' => 'btn btn-success', 'name' => 'submitbutton', 'value' => 'Save'])!!}

       {!! Form::submit( 'Save And Edit', ['class' => 'btn btn-primary', 'name' => 'submitbutton', 'value' => 'Save And Edit']) !!}

       {!! Form::submit( 'Save And Add New', ['class' => 'btn btn-primary', 'name' => 'submitbutton', 'value' => 'Save And Add New']) !!}


            {!! Form::close() !!}
    </div>
         </div>
      </div>
   </div>
</div>
@endsection