@extends('layouts.app')

@section('breadcrumb')
<div class="breadcrumb-wrapper">
          <ol class="breadcrumb">
            <li><a href="dashboard1.html"><i class="fa fa-home"></i>Home</a></li>
            <li class="active">Blank Page</li>
          </ol>

@endsection
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    You are logged in!

                    @if(Auth::user()->hasRoles('admin','SuperAdmin'))
                    dfsd
                    @endif
                    @role('admin')
                    @can(view.users)
                    <td>view.users</td>
                    @endcan
                    SuperAdmin
                    @endrole
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
