@extends('layouts.app')
@section('title', 'Create HrmsEmployeeHierarchy')
@section('breadcrumb')
    <div class="breadcrumb-wrapper">
        <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-home"></i>Home</a></li>
        <li class="active"> Create EmployeeHierarchy </li>
        </ol>
    </div><!-- /.breadcrumb-wrapper -->
@endsection
@section('content')
<div class="row">
   <div class="col-sm-12">
      <div class="panel panel-default-light border-default">

         <div class="panel-heading">
            <div class="panel-title">
               <i class="fa fa-cog m-right-5"></i> Create EmployeeHierarchy
            </div>
            <div class="text-right">
              <a href="{{ route('employeehierarchies.index') }}" class="btn btn-warning"> <i class="fa fa-arrow-left"></i> Back</a>
            </div>
         </div>

         <div class="panel-body">
            {!! Form::open(['route' => ['employeehierarchies.store'],'autocomplete' => 'off','files' => 'true','enctype'=>'multipart/form-data' ]) !!}
              @include('HrmsEmployeeHierarchy._form')
           <div class="form-layout-footer">
             <div class="form-group text-right">
                <div class="col-sm-12">
                  {{ Form::button('<i class="fa fa-save"></i> Save',['class' => 'btn btn-success','type' => 'submit']) }}
                  {{ Form::button('<i class="fa fa-ban"></i> Clear',['class' => 'btn btn-danger','type' => 'reset']) }}
                </div>
             </div>
             <!-- /.form-group -->
          </div>
            {!! Form::close() !!}
         </div>
      </div>
   </div>
</div>
@endsection
