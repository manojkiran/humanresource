<div class="form-layout">
   <div class="form-layout-body">

      {{-- Row  Starts --}}
      <div class="row">

         <div class="col-sm-4">
            <div class="form-group @if ($errors->has('department_id')) has-error @endif">
               {!! Form::label('designation_name','Department Name') !!}
               {{ Form::select('department_id',$departments, null,  ['class' =>'form-control rounded','id' =>'department_id']) }}
               @if ($errors->has('designation_name'))
               <p class="help-block">{{ $errors->first('designation_name') }}</p>
               @endif
            </div>
         </div>

        <div class="col-sm-4">
         <div class="form-group @if ($errors->has('department_short_code')) has-error @endif">
            {!! Form::label('department_short_code','Short Code') !!}
            {!! Form::text('department_short_code',old('department_short_code'),['placeholder'=>'Enter Department Short Code','class' =>'form-control rounded','id' =>'department_short_code']) !!}
            @if ($errors->has('department_short_code  '))
            <p class="help-block">{{ $errors->first('department_short_code') }}</p>
            @endif
         </div>
      </div>

      <div class="col-sm-4">
         <div class="form-group @if ($errors->has('department_supervisor')) has-error @endif">
            {!! Form::label('department_supervisor','Department Supervisor') !!}
            {!! Form::text('department_supervisor',old('department_supervisor'),['placeholder'=>'Enter Department Supervisor','class' =>'form-control rounded','id' =>'department_supervisor']) !!}
            @if ($errors->has('department_supervisor  '))
            <p class="help-block">{{ $errors->first('department_supervisor') }}</p>
            @endif
         </div>
      </div>


      </div>
      {{-- Row  Ends --}}

      {{-- Row  Starts --}}
      <div class="row">

         <div class="col-sm-4">
            <div class="form-group @if ($errors->has('department_supervisor_designation')) has-error @endif">
               {!! Form::label('department_supervisor_designation','Supervisor Designation') !!}
            {!! Form::text('department_supervisor_designation',old('department_supervisor_designation'),['placeholder'=>'Enter Supervisor Designation','class' =>'form-control rounded','id' =>'department_supervisor_designation']) !!}
            @if ($errors->has('department_supervisor_designation  '))
            <p class="help-block">{{ $errors->first('department_supervisor_designation') }}</p>
            @endif
            </div>
         </div>

        <div class="col-sm-4">
         <div class="form-group @if ($errors->has('employee_name')) has-error @endif">
            {!! Form::label('employee_name','Employee Name') !!}
               {{ Form::select('employee_name',$employees, null,  ['class' =>'form-control rounded','id' =>'department_id']) }}
               @if ($errors->has('employee_name'))
               <p class="help-block">{{ $errors->first('employee_name') }}</p>
               @endif
         </div>
      </div>

      <div class="col-sm-4">
         <div class="form-group @if ($errors->has('employee_designation_id')) has-error @endif">
            {!! Form::label('employee_designation_id','Employee Designation') !!}
               {{ Form::select('employee_designation_id',$designations, null,  ['class' =>'form-control rounded','id' =>'employee_designation_id']) }}
               @if ($errors->has('employee_designation_id'))
               <p class="help-block">{{ $errors->first('employee_designation_id') }}</p>
               @endif
         </div>
      </div>


      </div>
      {{-- Row  Ends --}}

      {{-- Row  Starts --}}
      <div class="row">

         <div class="col-sm-4">
            <div class="form-group @if ($errors->has('employee_reporting_level_one')) has-error @endif">
               {!! Form::label('employee_reporting_level_one','Reporting Level One') !!}
            {!! Form::text('employee_reporting_level_one',old('employee_reporting_level_one'),['placeholder'=>'Enter Reporting Level One','class' =>'form-control rounded','id' =>'employee_reporting_level_one']) !!}
            @if ($errors->has('employee_reporting_level_one  '))
            <p class="help-block">{{ $errors->first('employee_reporting_level_one') }}</p>
            @endif
            </div>
         </div>

         <div class="col-sm-4">
            <div class="form-group @if ($errors->has('employee_reporting_level_two')) has-error @endif">
               {!! Form::label('employee_reporting_level_two','Reporting Level Two') !!}
            {!! Form::text('employee_reporting_level_two',old('employee_reporting_level_two'),['placeholder'=>'Enter Reporting Level Two','class' =>'form-control rounded','id' =>'employee_reporting_level_two']) !!}
            @if ($errors->has('employee_reporting_level_two  '))
            <p class="help-block">{{ $errors->first('employee_reporting_level_two') }}</p>
            @endif
            </div>
         </div>

         <div class="col-sm-4">
            <div class="form-group @if ($errors->has('employee_reporting_level_three')) has-error @endif">
               {!! Form::label('employee_reporting_level_three','Reporting Level Three') !!}
            {!! Form::text('employee_reporting_level_three',old('employee_reporting_level_three'),['placeholder'=>'Enter Reporting Level Three','class' =>'form-control rounded','id' =>'employee_reporting_level_three']) !!}
            @if ($errors->has('employee_reporting_level_three  '))
            <p class="help-block">{{ $errors->first('employee_reporting_level_three') }}</p>
            @endif
            </div>
         </div>

        


      </div>
      {{-- Row  Ends --}}

   </div>
</div>

