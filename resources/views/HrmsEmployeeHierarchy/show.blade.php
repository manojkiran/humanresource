@extends('layouts.app')
@section('title', 'HrmsEmployeeHierarchy')
@section('breadcrumb')
@stop
@section('content')
    
    <div class="row">
        <div class="col-md-12">
            <div class="box-header b-b">
                <div class="row">
                    <div class="col-md-5">
                        <h3> HrmsEmployeeHierarchy</h3>
                    </div>
                    <div class="col-md-7 page-action text-right">
                         <a href="{{ route('hrmsemployeehierarchies.index') }}" class="btn btn-default btn-sm"> <i class="fa fa-arrow-left"></i> Back</a>
                    </div>
                </div>
            </div>  
        </div>
    </div>
    <div class="table-responsive">
        <table class="table table-bordered table-striped table-hover">
            <tbody>

                <tr>
                    <th>Id</th>
                    <td>{{ $hrmsemployeehierarchies->id }} </td>
                </tr>
                
            </tbody>    
        </table>
    </div>

@endsection