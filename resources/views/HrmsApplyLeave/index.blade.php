@extends('layouts.app')
@section('title', 'HrmsApplyLeave')
@section('breadcrumb')
    <div class="breadcrumb-wrapper">
        <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-home"></i>Home</a></li>
        <li class="active">Leaves List</li>
        </ol>
    </div><!-- /.breadcrumb-wrapper -->
@endsection
@section('content')
@section('pagetitle')
<div class="page-title-wrapper">
          <h2 class="page-title">List Of Leaves</h2>
        </div><!-- /.page-titile-wrapper -->
@endsection
<div class="row">
   <div class="col-md-5">
      <h3 class="modal-title">{{ $hrmsapplyleaves->total() }} {{ str_plural('Leave', $hrmsapplyleaves->count()) }} </h3>
   </div>
   <div class="col-md-7 page-action text-right">
      <a href="{{ route('applyleaves.create') }}" class="btn btn-primary btn-sm"> <i class="glyphicon glyphicon-plus-sign"></i> Create</a>

   </div>
</div>
<div class="result-set">
   <table class="table table-bordered table-striped table-hover" id="data-table">
      <thead>
         <tr>

            <th>Employee Name</th>
            <th>Leave Type 1</th>
            <th>Leave Type 2</th>
            <th>Leave From</th>
            <th>Leave To</th>
            <th>No of Days</th>
            <th>Status</th>

         </tr>
      </thead>
      <tbody>
         @forelse($hrmsapplyleaves as $item)
         <tr>
            <td>{{ BladeHelper::generateFullName($item->user_id) }}</td>
            <td>{{ $item->LeaveName['leave_type'] }}</td>
            <td>{{ BladeHelper::getLeaveStatus($item->employee_leave_half_day) }}</td>
            <td>{{ BladeHelper::showAsFormattedDate($item->employee_leave_starting_from) }}</td>
            <td>{{ BladeHelper::showAsFormattedDate($item->employee_leave_ending_to) }}</td>
            <td>{{ BladeHelper::getDifferenceOfTwoDates($item->employee_leave_starting_from,$item->employee_leave_ending_to) }}</td>
            <td>{!! BladeHelper::getLabelForStatus($item->employee_leave_status) !!}</td>
            <!-- <td>{!!BladeHelper::tableActionButtons(url()->full(),$item->id,$item->id,['show','edit','delete'])!!}</td> -->
            @empty
      </tbody>
      <tfoot>
         <center>
            <h3><code>There are no AppliedLeaves to display!</code></h3>
         </center>
      </tfoot>
      @endforelse
   </table>
   <div class="text-center">
      {{ $hrmsapplyleaves->links('layouts.pagination') }}
   </div>
</div>
@endsection
