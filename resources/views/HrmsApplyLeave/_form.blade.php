<div class="form-layout">
   <div class="form-layout-body">
      {{-- Row 1 Starts --}}
      <div class="row">
        <div class="col-sm-4">
         <div class="form-group @if ($errors->has('employee_leave_type')) has-error @endif">
            {!! Form::label('employee_leave_type','Leave Type') !!}
            {{ Form::select('employee_leave_type',$leaveTypes, null,  ['class' =>'form-control rounded','id' =>'employee_leave_type']) }}
            @if ($errors->has('employee_leave_type')) 
            <p class="help-block">{{ $errors->first('employee_leave_type') }}</p>
            @endif
         </div>
       </div>
      </div>
      {{-- Row 1 Ends --}}
      
      {{-- Row 2 Starts --}}
      <div class="row">
         <div class="col-sm-4">
            <div class="form-group @if ($errors->has('employee_leave_starting_from')) has-error @endif">
               {!! Form::label('employee_leave_starting_from','From') !!}
               {!! Form::text('employee_leave_starting_from',old('employee_leave_starting_from'),['placeholder'=>'Choose from Date','class' =>'form-control rounded','id' =>'employee_leave_starting_from']) !!}
               @if ($errors->has('employee_leave_starting_from')) 
               <p class="help-block">{{ $errors->first('employee_leave_starting_from') }}</p>
               @endif                                   
            </div>
         </div>
         <div class="col-sm-4">
            <div class="form-group @if ($errors->has('employee_leave_ending_to')) has-error @endif">
               {!! Form::label('employee_leave_ending_to','To') !!}
               {!! Form::text('employee_leave_ending_to',old('employee_leave_ending_to'),['placeholder'=>'Choose to Date','class' =>'form-control rounded','id' =>'employee_leave_ending_to']) !!}
               @if ($errors->has('employee_leave_ending_to')) 
               <p class="help-block">{{ $errors->first('employee_leave_ending_to') }}</p>
               @endif
            </div>
         </div>
      </div>
      {{-- Row 2 Ends --}}
      {{-- Row  Starts --}}


      <div class="row">

      
         <div class="form-group @if ($errors->has('employee_leave_half_day')) has-error @endif">
            {!! Form::label('employee_leave_half_day','Half Day',['class'=>'col-sm-1 col-form-label']) !!}
            <div class="col-sm-4">
                @foreach ($leaveLength as $leaveLengthKey => $leaveLengthValue )
                <label class="radio-inline">
                {!! Form::radio('employee_leave_half_day', $leaveLengthKey,null, ['class' =>'icheck-minimal-grey']) !!}
                {{ $leaveLengthValue }}
                </label>
                @endforeach
            </div>
            @if ($errors->has('employee_leave_half_day'))
            <p class="help-block">{{ $errors->first('employee_leave_half_day') }}</p>
            @endif
         </div>
      
    </div>
    <br>

      {{-- Row  Ends --}}


      

      {{-- Row 3 Starts --}}
      <div class="row">
        <div class="col-sm-8">
         <div class="form-group @if ($errors->has('employee_reason_for_leave')) has-error @endif">
            {!! Form::label('employee_reason_for_leave','Reason For Leave') !!}
            {!! Form::textarea('employee_reason_for_leave',old('employee_reason_for_leave'),['class' =>'form-control rounded','placeholder' =>'Detailed Description as reason for Leave', 'rows' => 5, 'cols' => 40]) !!}
            @if ($errors->has('employee_reason_for_leave')) 
            <p class="help-block">{{ $errors->first('employee_reason_for_leave') }}</p>
            @endif
         </div>
      </div>
    </div>
      {{-- Row 3 Ends --}}
   </div>
</div>

<div class="form-layout-footer">
   <div class="form-group text-right">
      <div class="col-sm-12">
         

{{ Form::button('<i class="fa fa-save"></i> Save',['class' => 'btn btn-success','type' => 'submit']) }}

{{ Form::button('<i class="fa fa-ban"></i> Clear',['class' => 'btn btn-danger','type' => 'reset']) }}

         
      </div>
   </div>
   <!-- /.form-group -->
</div>