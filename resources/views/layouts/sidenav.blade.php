<!-- SIDEBAR STARTS -->
<div class="sidebar">
	<ul class="sidebar-nav">
		<li class="">
			<a href="#">
				<i class="fa fa-tachometer"></i>
				<span>Dashboard</span>
			</a>
		</li>
		<li class="">
			<a href="{{ route('home') }}">
				<i class="fa fa-home"></i>
				<span>Home</span>
			</a>
        </li>










        {{--
		<li class="title">Test Title</li>
		<li class="dropdown active open">
			<a href="#" data-click="prevent">
				<i class="fa fa-home"></i>
				<span>Dashboard</span>
			</a>
			<ul class="inner-nav">
				<li>
					<a href="dashboard1.html">
						<i class="fa fa-bar-chart"></i>
						<span>Dashboard 1</span>
					</a>
				</li>
				<li>
					<a href="dashboard2.html">
						<i class="fa fa-line-chart"></i>
						<span>Dashboard 2</span>
					</a>
				</li>
				<li>
					<a href="dashboard3.html">
						<i class="fa fa-area-chart"></i>
						<span>Dashboard 3</span>
					</a>
				</li>
				<li>
					<a href="dashboard4.html">
						<i class="fa fa-pie-chart"></i>
						<span>Dashboard 4</span>
					</a>
				</li>
				<li>
					<a href="dashboard5.html">
						<i class="fa fa-tasks"></i>
						<span>Dashboard 5</span>
					</a>
				</li>
				<li>
					<a href="dashboard6.html">
						<i class="fa fa-list-ol"></i>
						<span>Dashboard 6</span>
					</a>
				</li>
			</ul>
		</li>  --}}


		<li class="">
			<a href="{{route('companyprofiles.index')}}">
				<i class="fa fa-building "></i>
				<span>Organization</span>
			</a>
		</li>
		<li class="dropdown">
			<a href="#" data-click="prevent">
				<i class="fa fa-book"></i>
				<span>Directory</span>
			</a>
			<ul class="inner-nav">
				<li>
					<a href="#">
						<i class="fa fa-users"></i>
						<span>Employee Directory</span>
					</a>
				</li>
				<li>
					<a href="#">
						<i class="fa fa-address-card"></i>
						<span>Clients Directory</span>
					</a>
				</li>
			</ul>
		</li>
		<li class="{{ Aktiv::isResourceActive('employees') }}">
			<a href="{{route('employees.index')}}">
				<i class="fa fa-user"></i>
				<span>Employee</span>
			</a>
		</li>
		<li class="">
			<a href="#">
				<i class="fa fa-clock-o"></i>
				<span>Attendance</span>
			</a>
		</li>
		<li class="">
			<a href="{{route('applyleaves.index')}}">
				<i class="fa fa-sign-out"></i>
				<span>Leaves</span>
			</a>
		</li>

		<li class="">
			<a href="{{route('applypermissions.index')}}">
				<i class="fa fa-sign-out"></i>
				<span>Permissions</span>
			</a>
		</li>
		<li class="dropdown">
			<a href="#" data-click="prevent">
				<i class="fa fa-money"></i>
				<span>Payroll</span>
			</a>
			<ul class="inner-nav">
				<li>
					<a href="{{ route('salaries.index') }}">
						<i class="fa fa-inr"></i>
						<span>Employee Salary</span>
					</a>
				</li>
				<li>
					<a href="#">
						<i class="fa fa-money"></i>
						<span>Employee Pay slip</span>
					</a>
				</li>

				<li>
					<a href="{{route('salarysettings.index')}}">
						<i class="fa fa-wrench"></i>
						<span>Salary Settings</span>
					</a>
				</li>
			</ul>
		</li>
		<li class="">
			<a href="#">
				<i class="fa fa-money"></i>
				<span>Expenses</span>
			</a>
		</li>
		<li class="">
			<a href="#">
				<i class="fa fa-th-list"></i>
				<span>Assets</span>
			</a>
		</li>
		<li class="">
			<a href="#">
				<i class="fa fa-ticket"></i>
				<span>Tickets</span>
			</a>
		</li>
		<li class="">
			<a href="#">
				<i class="fa fa-check"></i>
				<span>Recruitment</span>
			</a>
		</li>
		<li class="">
			<a href="#">
				<i class="fa fa-graduation-cap"></i>
				<span>Training</span>
			</a>
		</li>
		<li class="dropdown">
			<a href="#" data-click="prevent">
				<i class="fa fa-money"></i>
				<span>Performance</span>
			</a>
			<ul class="inner-nav">
				<li>
					<a href="#">
						<i class="fa fa-chart-bar"></i>
						<span>Appraisals</span>
					</a>
				</li>
			</ul>
		</li>
		<li class="">
			<a href="#">
				<i class="fa fa-pie-chart"></i>
				<span>Reports</span>
			</a>
		</li>
		<li class="dropdown">
			<a href="#" data-click="prevent">
				<i class="fa fa-arrows-alt"></i>
				<span>More</span>
			</a>
			<ul class="inner-nav">
				<li>
					<a href="#">
						<i class="fa fa-microphone"></i>
						<span>Announcements</span>
					</a>
				</li>
				<li>
					<a href="#">
						<i class="fa fa-calendar"></i>
						<span>Events</span>
					</a>
				</li>
				<li>
					<a href="#">
						<i class="fas fa-chart-pie"></i>
						<span>Org Chart</span>
					</a>
				</li>
			</ul>
		</li>
		<li class="dropdown {{ Aktiv::areResourcesActive(BladeHelper::getActiveMenuArray('BASEMASTER'),'open active')}}">
			<a href="#" data-click="prevent">
				<i class="fab fa-megaport"></i>
				<span>Masters</span>
			</a>
			<ul class="inner-nav">
				<li class="dropdown {{ Aktiv::areResourcesActive(BladeHelper::getActiveMenuArray('EMPLOYEEMASTER'),'open active')}}">
					<a href="#" data-click="prevent">
                        Employee Masters
                        </a>
					<ul class="inner-nav">
						<li class="{{ Aktiv::isResourceActive('leavetypes')}}">
							<a href="{{ route('leavetypes.index') }}">
                                Leave Types
                            </a>
                        </li>

                        <li class="{{ Aktiv::isResourceActive('designations')}}">
							<a href="{{ route('designations.index') }}">
                                Designations
                            </a>
                        </li>

                        <li class="{{ Aktiv::isResourceActive('salarysettings')}}">
							<a href="{{ route('salarysettings.index') }}">
                                Salary Settings
                            </a>
                        </li>




					</ul>
				</li>
			</ul>
			<ul class="inner-nav">
				<li class="dropdown {{ Aktiv::areResourcesActive(['departments'],'open active')}}">
					<a href="#" data-click="prevent">
                        Department Masters
                        </a>
					<ul class="inner-nav">
						<li class="{{ Aktiv::isResourceActive('departments')}}">
							<a href="{{ route('departments.index') }}">
                                Departments
                            </a>
						</li>
					</ul>
				</li>
			</ul>
			<ul class="inner-nav">
				<li class="dropdown {{ Aktiv::areResourcesActive(BladeHelper::getActiveMenuArray('PAYROLLMASTER'),'open active')}}">
					<a href="#" data-click="prevent">
                        Payroll Masters
                        </a>
					<ul class="inner-nav">
						<li class="{{ Aktiv::isResourceActive('employeegrades')}}">
							<a href="{{route('employeegrades.index')}}">
                                 Employee Grades
                            </a>
						</li>

						<li class="{{ Aktiv::isResourceActive('salarygradesettings')}}">
							<a href="{{route('salarygradesettings.index')}}">
                                  Grade - Salary Settings
                            </a>
						</li>
					</ul>
				</li>
			</ul>
			<ul class="inner-nav">
				<li class="dropdown">
					<a href="#" data-click="prevent">
                        Assets Masters
                        </a>
					<ul class="inner-nav">
						<li>
							<a href="#">
                                Inner Link 1
                            </a>
						</li>
					</ul>
				</li>
			</ul>
		</li>
	</ul>
</div>
<!-- SIDEBAR ENDS -->
