<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>@yield('title') {{ config('app.name') }}</title>

<script src="https://code.jquery.com/jquery-1.11.1.min.js"></script>

    <!-- REQUIRED STYLES -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic" rel="stylesheet" type="text/css">
    <link href="{{asset('assets/dist/main/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css">
    <!-- REQUIRED PLUGINS -->
    <link rel="stylesheet"  href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css">
    <link href="{{ asset('assets/dist/main/plugins/bootstrap-switch/css/bootstrap-switch.min.css') }}" rel="stylesheet">
    <!-- main.min.css - WISEBOARD CORE CSS -->
    <link href="{{asset('assets/dist/main/css/main.min.css')}}" rel="stylesheet" type="text/css">
    <!-- plugins.min.css - ALL PLUGINS CUSTOMIZATIONS -->
    <link href="{{asset('assets/dist/main/css/plugins.min.css')}}" rel="stylesheet" type="text/css">
    <!-- admin.min.css - ADMIN LAYOUT -->

    <link href="{{asset('assets/css/admin.min.css')}}" rel="stylesheet" type="text/css">
    <!-- theme-default.min.css - DEFAULT THEME -->
    <link href="{{asset('assets/css/theme-default.min.css')}}" rel="stylesheet" type="text/css">
    <!-- REQUIRED PLUGINS -->
    <link href="{{ asset('assets/dist/main/plugins/icheck/css/skins/all.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('assets/dist/main/plugins/select2/css/select2.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('assets/dist/main/plugins/select2/select2-bootstrap-theme/css/select2-bootstrap.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('assets/dist/main/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('assets/dist/main/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css') }}" rel="stylesheet" type="text/css">


    <link href="{{ asset('assets/dist/main/plugins/bootstrap-tagsinput/css/bootstrap-tagsinput.min.css') }}" rel="stylesheet" type="text/css">

    <link href="{{asset('assets/dist/main/plugins/select2/css/select2.min.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('assets/dist/main/plugins/select2/select2-bootstrap-theme/css/select2-bootstrap.min.css')}}" rel="stylesheet" type="text/css">


  </head>
  <body>

    @include('layouts.topnav')

    <!-- CONTENT WRAPPER STARTS ( INCLUDES LEFT SIDEBAR AND CONTENT PART OF THE PAGE ) -->
    <div class="content-wrapper">
    @include('layouts.sidenav')
      <!-- CONTENT STARTS -->
      <div class="content">
        @yield('breadcrumb')
        @yield('pagetitle')
        @include('flash-message')
        @yield('content')
      </div>
      <!-- CONTENT ENDS -->
    </div>
    <!-- CONTENT WRAPPER ENDS -->
    <!-- FOOTER STARTS -->
    @include('layouts.footer')
    <!-- FOOTER ENDS -->
    <!-- REQUIRED SCRIPTS -->
    <script src="{{ asset('assets/dist/main/js/jquery.min.js') }}"></script>
    <script src="{{ asset('assets/dist/main/js/bootstrap.min.js') }}"></script>
    <!-- REQUIRED PLUGINS -->
    <script src="{{ asset('assets/dist/main/plugins/jquery.slimscroll/js/jquery.slimscroll.min.js') }}"></script>
    <script src="{{ asset('assets/dist/main/plugins/bootstrap-switch/js/bootstrap-switch.min.js') }}"></script>
    <script src="{{ asset('assets/dist/main/plugins/jquery.succinct/js/jquery.succinct.min.js') }}"></script>
    <script src="{{ asset('assets/dist/main/plugins/icheck/js/icheck.min.js') }}"></script>
    <script src="{{ asset('assets/dist/main/plugins/select2/js/select2.full.min.js') }}"></script>
    <script src="{{ asset('assets/dist/main/plugins/bootstrap-wizard/js/jquery.bootstrap.wizard.min.js') }}"></script>
    <script src="{{ asset('assets/dist/main/plugins/jquery.validate/js/jquery.validate.min.js') }}"></script>

    <script src="{{ asset('assets/dist/main/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}"></script>
    <!-- main.min.js - WISEBOARD CORE SCRIPT -->
    <script src="{{ asset('assets/dist/main/js/main.min.js') }}"></script>
    <!-- admin.min.js - GENERAL CONFIGURATION SCRIPT FOR THE PAGES -->
    <script src="{{ asset('assets/js/admin.min.js') }}"></script>
    {{--  <script src="{{ asset('assets/js/demo/demo-form-wizard-with-validation.js') }}"></script>  --}}
    <script src="{{ asset('assets/dist/main/plugins/bootstrap-tagsinput/js/bootstrap-tagsinput.min.js') }}"></script>

    <script src="{{ asset('assets/dist/main/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js') }}"></script>

    <script src="{{ asset('assets/js/custom/jquery.multifield.min.js') }}"></script>





    <script>

        $(document).ready(function () {

        $('#employee_date_of_birth').datepicker({format: "yyyy/mm/dd",autoclose: true});
        $('#employee_contract_start_date').datepicker({format: "yyyy/mm/dd",autoclose: true});
        $('#employee_contract_end_date').datepicker({format: "yyyy/mm/dd",autoclose: true});
        $('#employee_previous_employment_start_date').datepicker({format: "yyyy/mm/dd",autoclose: true});
        $('#employee_previous_employment_end_date').datepicker({format: "yyyy/mm/dd",autoclose: true});

        $('#employee_leave_starting_from').datepicker({format: "yyyy/mm/dd",todayHighlight: true,autoclose: true,orientation: 'bottom'});
        $('#employee_leave_ending_to').datepicker({format: "yyyy/mm/dd",todayHighlight: true,autoclose: true,orientation: 'bottom'});





        $('#employee_primary_skills').tagsinput({theme: "bootstrap"});
        $('#employee_secondary_skills').tagsinput();
        $('#permission_date').datepicker({format: "yyyy/mm/dd",todayHighlight: true,autoclose: true,orientation: 'bottom'});
        $('#permissions_time_from').timepicker();
        $('#permissions_time_to').timepicker();

        $('#leave_from_date').datepicker({format: "yyyy/mm/dd",todayHighlight: true,autoclose: true,orientation: 'bottom'});
        $('#leave_to_date').datepicker({format: "yyyy/mm/dd",todayHighlight: true,autoclose: true,orientation: 'bottom'});





});

    </script>
     <script>

         $('#certificationsDiv').multifield();
         $('#employmentDetailsDiv').multifield();
         $('#pgEducationDiv').multifield();
      </script>
      <script>
          $('#employee_nationality').select2( {theme: "bootstrap",allowClear: true, placeholder: '--Select Your Nationality--'});
          $('#employee_employment_type_id').select2( {theme: "bootstrap",allowClear: true, placeholder: '--Select Employement Type--'});
          $('#employee_department_id').select2( {theme: "bootstrap",allowClear: true, placeholder: '--Select Department--'});
           $('#company_id').select2( {theme: "bootstrap",allowClear: true, placeholder: '--Select Company--'});

          

      </script>


    <script>
        $(function () {
            // flash auto hide
            $('#flash-msg .alert').not('.alert-danger, .alert-important').delay(6000).slideUp(500);
        })
    </script>

    <script>
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};
    </script>
 @stack('scripts')

 {!!BladeHelper::toolTipScript()!!}



  </body>
</html>
