<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>@yield('title') {{ config('app.name') }}</title>

    <!-- REQUIRED STYLES -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic" rel="stylesheet" type="text/css">
    <link href="{{asset('assets/dist/main/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css">
    <!-- REQUIRED PLUGINS -->
    <link rel="stylesheet"  href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css">
    <link href="{{ asset('assets/dist/main/plugins/bootstrap-switch/css/bootstrap-switch.min.css') }}" rel="stylesheet">
    <!-- main.min.css - WISEBOARD CORE CSS -->
    <link href="{{asset('assets/dist/main/css/main.min.css')}}" rel="stylesheet" type="text/css">
    <!-- plugins.min.css - ALL PLUGINS CUSTOMIZATIONS -->
    <link href="{{asset('assets/dist/main/css/plugins.min.css')}}" rel="stylesheet" type="text/css">
    <!-- admin.min.css - ADMIN LAYOUT -->

    <link href="{{asset('assets/css/admin.min.css')}}" rel="stylesheet" type="text/css">
    <!-- theme-default.min.css - DEFAULT THEME -->
    <link href="{{asset('assets/css/theme-default.min.css')}}" rel="stylesheet" type="text/css">
  </head>
  <body>

    @include('layouts.topnav')

    <!-- CONTENT WRAPPER STARTS ( INCLUDES LEFT SIDEBAR AND CONTENT PART OF THE PAGE ) -->
    <div class="content-wrapper">
      @include('layouts.sidenav')

      <!-- CONTENT STARTS -->
      <div class="content">

        @yield('breadcrumb')

        <div class="page-title-wrapper">
          <h2 class="page-title">Blank Page</h2>
        </div><!-- /.page-titile-wrapper -->

        @yield('content')

      </div>
      <!-- CONTENT ENDS -->
    </div>
    <!-- CONTENT WRAPPER ENDS -->

   @include('layouts.footer')

    <!-- REQUIRED SCRIPTS -->

    <script>
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};
    </script>


    <script src="{{asset('assets/dist/main/js/jquery.min.js')}}"></script>
    <script src="{{asset('assets/dist/main/js/bootstrap.min.js')}}"></script>
    <!-- REQUIRED PLUGINS -->





    <script src="{{asset('assets/dist/main/plugins/jquery.slimscroll/js/jquery.slimscroll.min.js')}}"></script>
    <script src="{{asset('assets/dist/main/plugins/bootstrap-switch/js/bootstrap-switch.min.js')}}"></script>
    <script src="{{asset('assets/dist/main/plugins/jquery.succinct/js/jquery.succinct.min.js')}}"></script>
    <!-- main.min.js - WISEBOARD CORE SCRIPT -->

    <!-- <script src="{{asset('assets/dist/main/js/main.min.js')}}"></script> -->
    <!-- admin.min.js - GENERAL CONFIGURATION SCRIPT FOR THE PAGES -->
    <script src="{{asset('assets/js/admin.min.js')}}"></script>
  </body>

</html>
