<!-- TOP NAVIGATION STARTS -->
    <nav class="navbar navbar-fixed-top navbar-top">
      <div class="container-fluid">
        <div class="navbar-header">
          <a class="navbar-brand" href="dashboard1.html">

            <img src="https://www.postiefs.com/wp-content/themes/postiefs_v2.0/images/logo2.png" alt="logo" style="height: 27px; width: auto;">
          </a>
          <button type="button" class="sidebar-toggle rounded">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
        </div><!-- /.navbar-header -->

        <form class="navbar-form navbar-left" autocomplete="off">
          <div class="input-group input-group-sm rounded">
            <input type="text" class="form-control" placeholder="Search">
            <span class="input-group-btn">
              <button type="submit" class="btn">
                <i class="fa fa-search"></i>
              </button>
            </span>
          </div><!-- /input-group -->
        </form><!-- /.navbar-form -->
        
        <ul class="nav navbar-nav navbar-right">
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <i class="fa fa-bell flat-shadow"></i>
              <span class="label circle">4</span>
            </a>
            <ul class="dropdown-menu">
              <li class="dropdown-header">
                You have 3 <a href="page-profile.html">notifications.</a>
              </li><!-- /.dropdown-header -->

              <li>
                <ul class="list-dropdown-content list-dropdown-notifications">
                  <li>
                    <a href="#" data-click="prevent">
                      <div class="media">
                        <div class="media-left">
                          <span class="label label-success circle">
                            <i class="fa fa-power-off"></i>
                          </span>
                        </div>
                        <div class="media-body">
                          <i class="pull-right">1 min</i> Servers restarted successfully.
                        </div>
                      </div><!-- /.media -->
                    </a>
                  </li>
                  <li>
                    <a href="#" data-click="prevent">
                      <div class="media">
                        <div class="media-left">
                          <span class="label label-info circle">
                            <i class="fa fa-user"></i>
                          </span>
                        </div>
                        <div class="media-body">
                          <i class="pull-right">2 min</i> New user registered.
                        </div>
                      </div><!-- /.media -->
                    </a>
                  </li>
                  <li>
                    <a href="#" data-click="prevent">
                      <div class="media">
                        <div class="media-left">
                          <span class="label label-danger circle">
                            <i class="fa fa-undo"></i>
                          </span>
                        </div>
                        <div class="media-body">
                          <i class="pull-right">4 min</i> Backup error.
                        </div>
                      </div><!-- /.media -->
                    </a>
                  </li>
                  <li>
                    <a href="#" data-click="prevent">
                      <div class="media">
                        <div class="media-left">
                          <span class="label label-primary circle">
                            <i class="fa fa-gear"></i>
                          </span>
                        </div>
                        <div class="media-body">
                          <i class="pull-right">7 min</i> Settings updated.
                        </div>
                      </div><!-- /.media -->
                    </a>
                  </li>
                  <li>
                    <a href="#" data-click="prevent">
                      <div class="media">
                        <div class="media-left">
                          <span class="label label-warning circle">
                            <i class="fa fa-exclamation"></i>
                          </span>
                        </div>
                        <div class="media-body">
                          <i class="pull-right">9 min</i> Application is not responding.
                        </div>
                      </div><!-- /.media -->
                    </a>
                  </li>
                  <li>
                    <a href="#" data-click="prevent">
                      <div class="media">
                        <div class="media-left">
                          <span class="label label-success circle">
                            <i class="fa fa-cube"></i>
                          </span>
                        </div>
                        <div class="media-body">
                          <i class="pull-right">12 min</i> Weekly dump created successfully.
                        </div>
                      </div><!-- /.media -->
                    </a>
                  </li>
                </ul>
              </li>
            </ul><!-- /.dropdown-menu -->
          </li><!-- /.dropdown -->

          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <i class="fa fa-envelope flat-shadow"></i>
              <span class="label circle">3</span>
            </a>
            <ul class="dropdown-menu">
              <li class="dropdown-header">
                You have 3 new <a href="page-inbox.html">messages.</a>
              </li><!-- /.dropdown-header -->

              <li>
                <ul class="list-dropdown-content list-dropdown-messages">
                  <li>
                    <a href="#" data-click="prevent">
                      <div class="media">
                        <div class="media-left">
                          <img src="{{asset('assets/img/avatar3.png')}}" alt="avatar" class="media-object img-circle">
                        </div>
                        <div class="media-body">
                          <h6 class="media-heading">
                            <i class="pull-right">1 min</i>
                            <strong class="text-primary">Nancy Allen</strong>
                          </h6>
                          <span class="text-muted">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</span>
                        </div>
                      </div><!-- /.media -->
                    </a>
                  </li>
                  <li>
                    <a href="#" data-click="prevent">
                      <div class="media">
                        <div class="media-left">

                          <img src="{{asset('assets/img/avatar2.png')}}" alt="avatar" class="media-object img-circle">
                        </div>
                        <div class="media-body">
                          <h6 class="media-heading">
                            <i class="pull-right">2 min</i>
                            <strong class="text-primary">Michael Spencer</strong>
                          </h6>
                          <span class="text-muted">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</span>
                        </div>
                      </div><!-- /.media -->
                    </a>
                  </li>
                  <li>
                    <a href="#" data-click="prevent">
                      <div class="media">
                        <div class="media-left">
                          <img src="{{asset('assets/img/avatar1.png')}}" alt="avatar" class="media-object img-circle">
                        </div>
                        <div class="media-body">
                          <h6 class="media-heading">
                            <i class="pull-right">5 min</i>
                            <strong class="text-primary">Jessica Parker</strong>
                          </h6>
                          <span class="text-muted">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</span>
                        </div>
                      </div><!-- /.media -->
                    </a>
                  </li>
                  <li>
                    <a href="#" data-click="prevent">
                      <div class="media">
                        <div class="media-left">

                          <img src="{{asset('assets/img/avatar2.png')}}" alt="avatar" class="media-object img-circle">
                        </div>
                        <div class="media-body">
                          <h6 class="media-heading">
                            <i class="pull-right">7 min</i>
                            <strong class="text-primary">Michael Spencer</strong>
                          </h6>
                          <span class="text-muted">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</span>
                        </div>
                      </div><!-- /.media -->
                    </a>
                  </li>
                  <li>
                    <a href="#" data-click="prevent">
                      <div class="media">
                        <div class="media-left">
                          <img src="{{asset('assets/img/avatar3.png')}}" alt="avatar" class="media-object img-circle">
                        </div>
                        <div class="media-body">
                          <h6 class="media-heading">
                            <i class="pull-right">10 min</i>
                            <strong class="text-primary">Nancy Allen</strong>
                          </h6>
                          <span class="text-muted">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</span>
                        </div>
                      </div><!-- /.media -->
                    </a>
                  </li>
                </ul>
              </li>
            </ul>
          </li><!-- /.dropdown -->

          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <i class="fa fa-tasks flat-shadow"></i>
              <span class="label circle">7</span>
            </a>
            <ul class="dropdown-menu">
              <li class="dropdown-header">
                2 <a href="page-todo.html">projects</a> of 3 are not completed.
              </li><!-- /.dropdown-header -->

              <li>
                <ul class="list-dropdown-content list-dropdown-tasks">
                  <li>
                    <a href="#" data-click="prevent">
                      <h6>
                        <i class="pull-right">1 min</i>
                        <strong>Landing page design</strong>
                      </h6>
                      <p>The project includes landing page design, frontend &amp; backend development.</p>
                        
                      <div class="progress rounded">
                        <div class="progress-bar progress-bar-success progress-bar-striped" style="width: 100%">
                        </div>
                      </div>
                      <small>Project end: 03.03.2017</small>
                    </a>
                  </li>
                  <li>
                    <a href="#" data-click="prevent">
                      <h6>
                        <i class="pull-right">4 min</i>
                        <strong>Frontend app</strong>
                      </h6>
                      <p>Frontend development, responsiveness fixes, sidebar css cleanup.</p>
                        
                      <div class="progress rounded">
                        <div class="progress-bar progress-bar-primary progress-bar-striped" style="width: 70%">
                        </div>
                      </div>
                      <small>Project end: 25.02.2017</small>
                    </a>
                  </li>
                  <li>
                    <a href="#" data-click="prevent">
                      <h6>
                        <i class="pull-right">7 min</i>
                        <strong>Backend development</strong>
                      </h6>
                      <p>Backend development, database engineering, domain &amp; hosting.</p>
                        
                      <div class="progress rounded">
                        <div class="progress-bar progress-bar-warning progress-bar-striped" style="width: 90%">
                        </div>
                      </div>
                      <small>Project end: 20.02.2017</small>
                    </a>
                  </li>
                </ul>
              </li>
            </ul>
          </li><!-- /.dropdown -->

          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <img src="{{asset('assets/img/profile-sm.png')}}" alt="avatar" class="img-circle">
            </a>
            <ul class="dropdown-menu dropdown-menu-profile">
              <li class="dropdown-header">Account Details</li>
              <li>
                <div class="media">
                  <div class="media-left">
                    <img src="{{asset('assets/img/profile-lg.png')}}" alt="avatar" class="media-object img-circle">
                  </div>
                  <div class="media-body">
                    <h5 class="media-heading">Sandra Baker</h5>
                    <p>sandrabaker@gmail.com</p>
                    <a href="page-profile.html" class="btn btn-primary btn-xs">View profile</a>
                  </div>
                </div><!-- /.media -->
              </li>

              <li>
                <a href="page-inbox.html">
                  <i class="fa fa-paper-plane-o"></i> Inbox
                </a>
              </li>
              <li>
                <a href="page-timeline.html">
                  <i class="fa fa-history"></i> Timeline
                </a>
              </li>
              <li>
                <a href="plugin-fullcalendar.html">
                  <i class="fa fa-calendar-check-o"></i> Calendar
                </a>
              </li>
              <li class="divider"></li>
              <li>
                <a href="page-login1.html">
                  <i class="fa fa-sign-out"></i> Sign Out
                </a>
              </li>
            </ul>
          </li><!-- /.dropdown -->

          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <i class="fa fa-align-justify flat-shadow"></i>
            </a>
            <div class="sidebar-extra">
              <div class="sidebar-extra-scroller">
                <ul class="nav nav-tabs solid-tabs">
                  <li class="active">
                    <a href="#sidebar-timeline">Timeline</a>
                  </li>
                  <li>
                    <a href="#sidebar-mail">Mail</a>
                  </li>
                  <li>
                    <a href="#sidebar-settings">Settings</a>
                  </li>
                </ul>

                <div class="tab-content">
                  <div class="tab-pane sidebar-timeline fade in active" id="sidebar-timeline">
                    <h6>Today</h6>
                    <ul class="list-sidebar-extra">
                      <li>
                        <a href="page-timeline.html">
                          <div class="media">
                            <div class="media-left">
                              <img src="{{asset('assets/img/avatar3.png')}}" alt="avatar" class="media-object img-circle">
                            </div><!-- /.media-left -->
                            <div class="media-body">
                              <p>Nancy A. submitted the <span class="text-highlight">project.</span></p>
                            </div><!-- /.media-body -->
                            <div class="media-right">7:10PM</div>
                          </div><!-- /.media -->
                        </a>
                      </li>
                      <li>
                        <a href="page-timeline.html">
                          <div class="media">
                            <div class="media-left">
                              <img src="{{asset('assets/img/avatar2.png')}}" alt="avatar" class="media-object img-circle">
                            </div><!-- /.media-left -->
                            <div class="media-body">
                              <p>Michael S. commented on <span class="text-highlight">Hosting options: the best experiences.</span></p>
                              <div class="text-ellipsis">"No, I wouldn't recommend an unmanaged service to a novice client"</div>
                            </div><!-- /.media-body -->
                            <div class="media-right">5:35PM</div>
                          </div><!-- /.media -->
                        </a>
                      </li>
                    </ul>

                    <h6>Thursday</h6>
                    <ul class="list-sidebar-extra">
                      <li>
                        <a href="page-timeline.html">
                          <div class="media">
                            <div class="media-left">
                              <img src="{{asset('assets/img/avatar3.png')}}" alt="avatar" class="media-object img-circle">
                            </div><!-- /.media-left -->
                            <div class="media-body">
                              <p>Nancy A. created a new <span class="text-highlight">Urgent project</span> topic.</p>
                            </div><!-- /.media-body -->
                            <div class="media-right">6:00PM</div>
                          </div><!-- /.media -->
                        </a>
                      </li>
                      <li>
                        <a href="page-timeline.html">
                          <div class="media">
                            <div class="media-left">
                              <img src="{{asset('assets/img/avatar2.png')}}" alt="avatar" class="media-object img-circle">
                            </div><!-- /.media-left -->
                            <div class="media-body">
                              <p>Michael S. replied on <span class="text-highlight">Hosting options: the best experiences.</span></p>
                              <div class="text-ellipsis">"I agree with Jessica, but I think I have a brilliant idea"</div>
                            </div><!-- /.media-body -->
                            <div class="media-right">5:35PM</div>
                          </div><!-- /.media -->
                        </a>
                      </li>
                      <li>
                        <a href="page-timeline.html">
                          <div class="media">
                            <div class="media-left">
                              <img src="{{asset('assets/img/avatar1.png')}}" alt="avatar" class="media-object img-circle">
                            </div><!-- /.media-left -->
                            <div class="media-body">
                              <p>Jessica M. assigned a <span class="text-highlight">project</span> to you.</p>
                            </div><!-- /.media-body -->
                            <div class="media-right">4:30PM</div>
                          </div><!-- /.media -->
                        </a>
                      </li>
                      <li>
                        <a href="page-timeline.html">
                          <div class="media">
                            <div class="media-left">
                              <img src="{{asset('assets/img/avatar3.png')}}" alt="avatar" class="media-object img-circle">
                            </div><!-- /.media-left -->
                            <div class="media-body">
                              <p>Nancy A. submitted the <span class="text-highlight">project.</span></p>
                            </div><!-- /.media-body -->
                            <div class="media-right">4:25PM</div>
                          </div><!-- /.media -->
                        </a>
                      </li>
                    </ul>

                    <h6>Wednesday</h6>
                    <ul class="list-sidebar-extra">
                      <li>
                        <a href="page-timeline.html">
                          <div class="media">
                            <div class="media-left">
                              <img src="{{asset('assets/img/avatar1.png')}}" alt="avatar" class="media-object img-circle">
                            </div><!-- /.media-left -->
                            <div class="media-body">
                              <p>Jessica M. commented on <span class="text-highlight">Design &amp; Development.</span></p>
                              <div class="text-ellipsis">"I think this is the most beautiful design we have ever created"</div>
                            </div><!-- /.media-body -->
                            <div class="media-right">4:15PM</div>
                          </div><!-- /.media -->
                        </a>
                      </li>
                      <li>
                        <a href="page-timeline.html">
                          <div class="media">
                            <div class="media-left">
                              <img src="{{asset('assets/img/avatar3.png')}}" alt="avatar" class="media-object img-circle">
                            </div><!-- /.media-left -->
                            <div class="media-body">
                              <p>Nancy A. created a new <span class="text-highlight">Urgent project</span> topic.</p>
                            </div><!-- /.media-body -->
                            <div class="media-right">6:00PM</div>
                          </div><!-- /.media -->
                        </a>
                      </li>
                      <li>
                        <a href="page-timeline.html">
                          <div class="media">
                            <div class="media-left">
                              <img src="{{asset('assets/img/avatar2.png')}}" alt="avatar" class="media-object img-circle">
                            </div><!-- /.media-left -->
                            <div class="media-body">
                              <p>Michael S. replied on <span class="text-highlight">Hosting options: the best experiences.</span></p>
                              <div class="text-ellipsis">"I agree with Jessica, but I think I have a brilliant idea"</div>
                            </div><!-- /.media-body -->
                            <div class="media-right">5:35PM</div>
                          </div><!-- /.media -->
                        </a>
                      </li>
                      <li>
                        <a href="page-timeline.html">
                          <div class="media">
                            <div class="media-left">
                              <img src="{{asset('assets/img/avatar3.png')}}" alt="avatar" class="media-object img-circle">
                            </div><!-- /.media-left -->
                            <div class="media-body">
                              <p>Nancy A. submitted the <span class="text-highlight">project.</span></p>
                            </div><!-- /.media-body -->
                            <div class="media-right">4:25PM</div>
                          </div><!-- /.media -->
                        </a>
                      </li>
                    </ul>

                    <h6>Tuesday</h6>
                    <ul class="list-sidebar-extra">
                      <li>
                        <a href="page-timeline.html">
                          <div class="media">
                            <div class="media-left">
                              <img src="{{asset('assets/img/avatar3.png')}}" alt="avatar" class="media-object img-circle">
                            </div><!-- /.media-left -->
                            <div class="media-body">
                              <p>Nancy A. submitted the <span class="text-highlight">project.</span></p>
                            </div><!-- /.media-body -->
                            <div class="media-right">7:10PM</div>
                          </div><!-- /.media -->
                        </a>
                      </li>
                      <li>
                        <a href="page-timeline.html">
                          <div class="media">
                            <div class="media-left">
                              <img src="{{asset('assets/img/avatar2.png')}}" alt="avatar" class="media-object img-circle">
                            </div><!-- /.media-left -->
                            <div class="media-body">
                              <p>Michael S. commented on <span class="text-highlight">Hosting options: the best experiences.</span></p>
                              <div class="text-ellipsis">"No, I wouldn't recommend an unmanaged service to a novice client"</div>
                            </div><!-- /.media-body -->
                            <div class="media-right">5:35PM</div>
                          </div><!-- /.media -->
                        </a>
                      </li>
                    </ul>

                    <h6>Monday</h6>
                    <ul class="list-sidebar-extra">
                      <li>
                        <a href="page-timeline.html">
                          <div class="media">
                            <div class="media-left">
                              <img src="{{asset('assets/img/avatar3.png')}}" alt="avatar" class="media-object img-circle">
                            </div><!-- /.media-left -->
                            <div class="media-body">
                              <p>Nancy A. created a new <span class="text-highlight">Urgent project</span> topic.</p>
                            </div><!-- /.media-body -->
                            <div class="media-right">6:00PM</div>
                          </div><!-- /.media -->
                        </a>
                      </li>
                      <li>
                        <a href="page-timeline.html">
                          <div class="media">
                            <div class="media-left">
                              <img src="{{asset('assets/img/avatar2.png')}}" alt="avatar" class="media-object img-circle">
                            </div><!-- /.media-left -->
                            <div class="media-body">
                              <p>Michael S. replied on <span class="text-highlight">Hosting options: the best experiences.</span></p>
                              <div class="text-ellipsis">"I agree with Jessica, but I think I have a brilliant idea"</div>
                            </div><!-- /.media-body -->
                            <div class="media-right">5:35PM</div>
                          </div><!-- /.media -->
                        </a>
                      </li>
                      <li>
                        <a href="page-timeline.html">
                          <div class="media">
                            <div class="media-left">
                              <img src="{{asset('assets/img/avatar1.png')}}" alt="avatar" class="media-object img-circle">
                            </div><!-- /.media-left -->
                            <div class="media-body">
                              <p>Jessica M. assigned a <span class="text-highlight">project</span> to you.</p>
                            </div><!-- /.media-body -->
                            <div class="media-right">4:30PM</div>
                          </div><!-- /.media -->
                        </a>
                      </li>
                      <li>
                        <a href="page-timeline.html">
                          <div class="media">
                            <div class="media-left">
                              <img src="{{asset('assets/img/avatar3.png')}}" alt="avatar" class="media-object img-circle">
                            </div><!-- /.media-left -->
                            <div class="media-body">
                              <p>Nancy A. submitted the <span class="text-highlight">project.</span></p>
                            </div><!-- /.media-body -->
                            <div class="media-right">4:25PM</div>
                          </div><!-- /.media -->
                        </a>
                      </li>
                    </ul>
                  </div><!-- /.tab-pane -->

                  <div class="tab-pane sidebar-mail fade" id="sidebar-mail">
                    <h6>Today</h6>
                    <ul class="list-sidebar-extra">
                      <li>
                        <a href="page-inbox.html">
                          <div class="media">
                            <div class="media-left">
                              <img src="{{asset('assets/img/avatar3.png')}}" alt="avatar" class="media-object img-circle">
                            </div><!-- /.media-left -->
                            <div class="media-body">
                              <span>Nancy Allen</span>
                              <div class="text-ellipsis">Here is the code sample.</div>
                            </div><!-- /.media-body -->
                            <div class="media-right">5/30/16</div>
                          </div><!-- /.media -->
                        </a>
                      </li>
                      <li>
                        <a href="page-inbox.html">
                          <div class="media">
                            <div class="media-left">
                              <img src="{{asset('assets/img/avatar2.png')}}" alt="avatar" class="media-object img-circle">
                            </div><!-- /.media-left -->
                            <div class="media-body">
                              <span>Michael Spencer</span>
                              <div class="text-ellipsis">Re: Welcome to Wiseboard.</div>
                            </div><!-- /.media-body -->
                            <div class="media-right">5/30/16</div>
                          </div><!-- /.media -->
                        </a>
                      </li>
                      <li>
                        <a href="page-inbox.html">
                          <div class="media">
                            <div class="media-left">
                              <img src="{{asset('assets/img/avatar3.png')}}" alt="avatar" class="media-object img-circle">
                            </div><!-- /.media-left -->
                            <div class="media-body">
                              <span>Nancy Allen</span>
                              <div class="text-ellipsis">Your contract started.</div>
                            </div><!-- /.media-body -->
                            <div class="media-right">5/30/16</div>
                          </div><!-- /.media -->
                        </a>
                      </li>
                    </ul>

                    <h6>Thursday</h6>
                    <ul class="list-sidebar-extra">
                      <li>
                        <a href="page-inbox.html">
                          <div class="media">
                            <div class="media-left">
                              <img src="{{asset('assets/img/avatar3.png')}}" alt="avatar" class="media-object img-circle">
                            </div><!-- /.media-left -->
                            <div class="media-body">
                              <span>Nancy Allen</span>
                              <div class="text-ellipsis">Re: I have sent you the sketch files.</div>
                            </div><!-- /.media-body -->
                            <div class="media-right">5/29/16</div>
                          </div><!-- /.media -->
                        </a>
                      </li>
                      <li>
                        <a href="page-inbox.html">
                          <div class="media">
                            <div class="media-left">
                              <img src="{{asset('assets/img/avatar1.png')}}" alt="avatar" class="media-object img-circle">
                            </div><!-- /.media-left -->
                            <div class="media-body">
                              <span>Jessica Parker</span>
                              <div class="text-ellipsis">Here is the code sample.</div>
                            </div><!-- /.media-body -->
                            <div class="media-right">5/29/16</div>
                          </div><!-- /.media -->
                        </a>
                      </li>
                      <li>
                        <a href="page-inbox.html">
                          <div class="media">
                            <div class="media-left">
                              <img src="{{asset('assets/img/avatar2.png')}}" alt="avatar" class="media-object img-circle">
                            </div><!-- /.media-left -->
                            <div class="media-body">
                              <span>Michael Spencer</span>
                              <div class="text-ellipsis">Here are all the design materials.</div>
                            </div><!-- /.media-body -->
                            <div class="media-right">5/29/16</div>
                          </div><!-- /.media -->
                        </a>
                      </li>
                      <li>
                        <a href="page-inbox.html">
                          <div class="media">
                            <div class="media-left">
                              <img src="{{asset('assets/img/avatar1.png')}}" alt="avatar" class="media-object img-circle">
                            </div><!-- /.media-left -->
                            <div class="media-body">
                              <span>Jessica Parker</span>
                              <div class="text-ellipsis">Here are all the psd files.</div>
                            </div><!-- /.media-body -->
                            <div class="media-right">5/29/16</div>
                          </div><!-- /.media -->
                        </a>
                      </li>
                      <li>
                        <a href="page-inbox.html">
                          <div class="media">
                            <div class="media-left">
                              <img src="{{asset('assets/img/avatar3.png')}}" alt="avatar" class="media-object img-circle">
                            </div><!-- /.media-left -->
                            <div class="media-body">
                              <span>Nancy Allen</span>
                              <div class="text-ellipsis">Re: I have sent you the sketch files.</div>
                            </div><!-- /.media-body -->
                            <div class="media-right">5/29/16</div>
                          </div><!-- /.media -->
                        </a>
                      </li>
                    </ul>

                    <h6>Wednesday</h6>
                    <ul class="list-sidebar-extra">
                      <li>
                        <a href="page-inbox.html">
                          <div class="media">
                            <div class="media-left">
                              <img src="{{asset('assets/img/avatar3.png')}}" alt="avatar" class="media-object img-circle">
                            </div><!-- /.media-left -->
                            <div class="media-body">
                              <span>Nancy Allen</span>
                              <div class="text-ellipsis">Here is the code sample.</div>
                            </div><!-- /.media-body -->
                            <div class="media-right">5/28/16</div>
                          </div><!-- /.media -->
                        </a>
                      </li>
                      <li>
                        <a href="page-inbox.html">
                          <div class="media">
                            <div class="media-left">
                              <img src="{{asset('assets/img/avatar2.png')}}" alt="avatar" class="media-object img-circle">
                            </div><!-- /.media-left -->
                            <div class="media-body">
                              <span>Michael Spencer</span>
                              <div class="text-ellipsis">Re: Welcome to Wiseboard.</div>
                            </div><!-- /.media-body -->
                            <div class="media-right">5/28/16</div>
                          </div><!-- /.media -->
                        </a>
                      </li>
                      <li>
                        <a href="page-inbox.html">
                          <div class="media">
                            <div class="media-left">
                              <img src="{{asset('assets/img/avatar3.png')}}" alt="avatar" class="media-object img-circle">
                            </div><!-- /.media-left -->
                            <div class="media-body">
                              <span>Nancy Allen</span>
                              <div class="text-ellipsis">Your contract started.</div>
                            </div><!-- /.media-body -->
                            <div class="media-right">5/28/16</div>
                          </div><!-- /.media -->
                        </a>
                      </li>
                      <li>
                        <a href="page-inbox.html">
                          <div class="media">
                            <div class="media-left">
                              <img src="{{asset('assets/img/avatar1.png')}}" alt="avatar" class="media-object img-circle">
                            </div><!-- /.media-left -->
                            <div class="media-body">
                              <span>Jessica Parker</span>
                              <div class="text-ellipsis">Here is the code sample.</div>
                            </div><!-- /.media-body -->
                            <div class="media-right">5/28/16</div>
                          </div><!-- /.media -->
                        </a>
                      </li>
                    </ul>

                    <h6>Tuesday</h6>
                    <ul class="list-sidebar-extra">
                      <li>
                        <a href="page-inbox.html">
                          <div class="media">
                            <div class="media-left">
                              <img src="{{asset('assets/img/avatar3.png')}}" alt="avatar" class="media-object img-circle">
                            </div><!-- /.media-left -->
                            <div class="media-body">
                              <span>Nancy Allen</span>
                              <div class="text-ellipsis">Re: I have sent you the sketch files.</div>
                            </div><!-- /.media-body -->
                            <div class="media-right">5/27/16</div>
                          </div><!-- /.media -->
                        </a>
                      </li>
                      <li>
                        <a href="page-inbox.html">
                          <div class="media">
                            <div class="media-left">
                              <img src="{{asset('assets/img/avatar1.png')}}" alt="avatar" class="media-object img-circle">
                            </div><!-- /.media-left -->
                            <div class="media-body">
                              <span>Jessica Parker</span>
                              <div class="text-ellipsis">Here is the code sample.</div>
                            </div><!-- /.media-body -->
                            <div class="media-right">5/27/16</div>
                          </div><!-- /.media -->
                        </a>
                      </li>
                      <li>
                        <a href="page-inbox.html">
                          <div class="media">
                            <div class="media-left">
                              <img src="{{asset('assets/img/avatar2.png')}}" alt="avatar" class="media-object img-circle">
                            </div><!-- /.media-left -->
                            <div class="media-body">
                              <span>Michael Spencer</span>
                              <div class="text-ellipsis">Here are all the design materials.</div>
                            </div><!-- /.media-body -->
                            <div class="media-right">5/27/16</div>
                          </div><!-- /.media -->
                        </a>
                      </li>
                      <li>
                        <a href="page-inbox.html">
                          <div class="media">
                            <div class="media-left">
                              <img src="{{asset('assets/img/avatar1.png')}}" alt="avatar" class="media-object img-circle">
                            </div><!-- /.media-left -->
                            <div class="media-body">
                              <span>Jessica Parker</span>
                              <div class="text-ellipsis">Here are all the psd files.</div>
                            </div><!-- /.media-body -->
                            <div class="media-right">5/27/16</div>
                          </div><!-- /.media -->
                        </a>
                      </li>
                      <li>
                        <a href="page-inbox.html">
                          <div class="media">
                            <div class="media-left">
                              <img src="{{asset('assets/img/avatar3.png')}}" alt="avatar" class="media-object img-circle">
                            </div><!-- /.media-left -->
                            <div class="media-body">
                              <span>Nancy Allen</span>
                              <div class="text-ellipsis">Re: I have sent you the sketch files.</div>
                            </div><!-- /.media-body -->
                            <div class="media-right">5/27/16</div>
                          </div><!-- /.media -->
                        </a>
                      </li>
                    </ul>

                    <h6>Monday</h6>
                    <ul class="list-sidebar-extra">
                      <li>
                        <a href="page-inbox.html">
                          <div class="media">
                            <div class="media-left">
                              <img src="{{asset('assets/img/avatar3.png')}}" alt="avatar" class="media-object img-circle">
                            </div><!-- /.media-left -->
                            <div class="media-body">
                              <span>Nancy Allen</span>
                              <div class="text-ellipsis">Here is the code sample.</div>
                            </div><!-- /.media-body -->
                            <div class="media-right">5/26/16</div>
                          </div><!-- /.media -->
                        </a>
                      </li>
                      <li>
                        <a href="page-inbox.html">
                          <div class="media">
                            <div class="media-left">
                              <img src="{{asset('assets/img/avatar2.png')}}" alt="avatar" class="media-object img-circle">
                            </div><!-- /.media-left -->
                            <div class="media-body">
                              <span>Michael Spencer</span>
                              <div class="text-ellipsis">Re: Welcome to Wiseboard.</div>
                            </div><!-- /.media-body -->
                            <div class="media-right">5/26/16</div>
                          </div><!-- /.media -->
                        </a>
                      </li>
                      <li>
                        <a href="page-inbox.html">
                          <div class="media">
                            <div class="media-left">
                              <img src="{{asset('assets/img/avatar3.png')}}" alt="avatar" class="media-object img-circle">
                            </div><!-- /.media-left -->
                            <div class="media-body">
                              <span>Nancy Allen</span>
                              <div class="text-ellipsis">Your contract started.</div>
                            </div><!-- /.media-body -->
                            <div class="media-right">5/26/16</div>
                          </div><!-- /.media -->
                        </a>
                      </li>
                    </ul>
                  </div><!-- /.tab-pane -->

                  <div class="tab-pane sidebar-settings fade" id="sidebar-settings">
                    <h6>Settings</h6>
                    <ul class="list-sidebar-extra">
                      <li>
                        Enable notifications
                        <input type="checkbox" class="bs-switch" data-size="small" checked>
                      </li>
                      <li>
                        Enable reports
                        <input type="checkbox" class="bs-switch" data-size="small" checked>
                      </li>
                      <li>
                        Enable alerts
                        <input type="checkbox" class="bs-switch" data-size="small">
                      </li>
                      <li>
                        Enable Cookies
                        <input type="checkbox" class="bs-switch" data-size="small" checked>
                      </li>
                      <li>
                        Enable SMTP
                        <input type="checkbox" class="bs-switch" data-size="small">
                      </li>
                      <li>
                        Enable upload
                        <input type="checkbox" class="bs-switch" data-size="small">
                      </li>
                      <li>
                        Enable backup
                        <input type="checkbox" class="bs-switch" data-size="small">
                      </li>
                      <li>
                        Enable mail
                        <input type="checkbox" class="bs-switch" data-size="small">
                      </li>
                      <li>
                        Enable chat
                        <input type="checkbox" class="bs-switch" data-size="small" checked>
                      </li>
                    </ul>
                  </div><!-- /.tab-pane -->
                </div>
              </div><!-- /.sidebar-extra-scroller -->
            </div><!-- /.sidebar-extra -->
          </li><!-- /.dropdown -->
        </ul><!-- /.navbar-nav -->
      </div>
    </nav>
    <!-- TOP NAVIGATION ENDS -->