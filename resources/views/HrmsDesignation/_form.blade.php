<div class="form-layout">
   <div class="form-layout-body">

      {{-- Row  Starts --}}
      <div class="row">
         <div class="col-sm-5">
            <div class="form-group @if ($errors->has('designation_name')) has-error @endif">
               {!! Form::label('designation_name','Name') !!}
               {!! Form::text('designation_name',old('designation_name'),['placeholder'=>'Designation','class' =>'form-control rounded','id' =>'designation_name']) !!}
               @if ($errors->has('designation_name'))
               <p class="help-block">{{ $errors->first('designation_name') }}</p>
               @endif
            </div>
         </div>
         <div class="col-sm-5">
            <div class="form-group @if ($errors->has('department_id')) has-error @endif">
               {!! Form::label('department_id','Department Name') !!}
               {{ Form::select('department_id',$departments, null,  ['class' =>'form-control rounded','id' =>'department_id']) }}
               @if ($errors->has('department_id'))
               <p class="help-block">{{ $errors->first('department_id') }}</p>
               @endif
            </div>
         </div>
      </div>
      {{-- Row  Ends --}}

      {{-- Row  Starts --}}
      <div class="row">
         <div class="col-sm-5">
            <div class="form-group @if ($errors->has('reporting_to')) has-error @endif">
               {!! Form::label('reporting_to','Reporting To') !!}
               {!! Form::text('reporting_to',old('reporting_to'),['placeholder'=>'Reporting To','class' =>'form-control rounded','id' =>'reporting_to']) !!}
               @if ($errors->has('reporting_to'))
               <p class="help-block">{{ $errors->first('reporting_to') }}</p>
               @endif
            </div>
         </div>
         <div class="col-sm-5">
            <div class="form-group @if ($errors->has('company_id')) has-error @endif">
               {!! Form::label('company_id','Company Name') !!}
               {{ Form::select('company_id',$companies, null,  ['class' =>'form-control rounded','id' =>'department_id']) }}
               @if ($errors->has('company_id'))
               <p class="help-block">{{ $errors->first('company_id') }}</p>
               @endif
            </div>
         </div>
      </div>
      {{-- Row  Ends --}}
   </div>
</div>

<div class="form-layout-footer">
                <div class="form-group text-right">
                  <div class="col-sm-12">
                    <button type="submit" class="btn btn-info rounded">Submit</button>
                    <button type="button" class="btn btn-default rounded">Cancel</button>
                  </div>
                </div>
                <!-- /.form-group -->
              </div>
