@extends('layouts.app')
@section('title', 'HrmsEmploymentType-')
@section('breadcrumb')
@stop
@section('content')
<div class="row">
   <div class="col-xs-12">
      <div class="box">
         <div class="col-md-5">
            <h3>New HrmsEmploymentType</h3>
         </div>
         <div class="text-right">
            <a href="{{ route('hrmsemploymenttypes.index') }}" class="btn bg-purple btn-flat margin"> <i class="fa fa-arrow-left"></i> Back</a>
         </div>
         <div class="box-body">
            {!! Form::open(['route' => ['hrmsemploymenttypes.store'],'autocomplete' => 'off','files' => 'true','enctype'=>'multipart/form-data' ]) !!}
            @include('HrmsEmploymentType._form')
            <div class="box-footer">
               {!! Form::submit('Create', ['class' => 'btn btn-primary']) !!}
            </div>
            {!! Form::close() !!}
         </div>
      </div>
   </div>
</div>
@stop