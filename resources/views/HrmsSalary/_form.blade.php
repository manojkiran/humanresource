<div class="form-layout">
   <div class="form-layout-body">
      
      {{-- Row 1 Starts --}}
      <div class="row">
         <div class="col-sm-6">
             <div class="form-group @if ($errors->has('employee_id')) has-error @endif">
                                        {!! Form::label('employee_id','Employe Name') !!}
                                        {{ Form::select('employee_id',$employees, null,  ['class' =>'form-control rounded','id' =>'employee_id']) }}
                                            @if ($errors->has('employee_id')) <p class="help-block">{{ $errors->first('employee_id') }}</p> @endif
                </div>
         </div>

         <div class="col-sm-6">
            <div class="form-group @if ($errors->has('employee_grade')) has-error @endif">
               {!! Form::label('employee_grade','Employee Grade') !!}
               {{ Form::select('employee_grade',$employeeGrade, null,  ['class' =>'form-control rounded','id' =>'employee_grade','onChange'=>'getSelectedGradeId()']) }}
               @if ($errors->has('employee_grade')) 
               <p class="help-block">{{ $errors->first('employee_grade') }}</p>
               @endif
            </div>
         </div>
         
      </div>
      {{-- Row 1 Ends --}}

      {{-- Row  Starts --}}

      <div id="rslt2">
</div>
      <div class="row">
        
      

      <div class="col-sm-6">
            <div class="form-group @if ($errors->has('employee_netsalary')) has-error @endif">
               {!! Form::label('employee_netsalary','Net Salary') !!}
               {!! Form::text('employee_netsalary',old('employee_netsalary'),['placeholder'=>'Enter Net Salary','class' =>'form-control rounded','id' =>'employee_netsalary','onkeyup'=>'calculateSalary()']) !!}
               @if ($errors->has('employee_netsalary')) 
               <p class="help-block">{{ $errors->first('employee_netsalary') }}</p>
               @endif
            </div>
         </div>
        

         </div>

      {{-- Row 1 Ends --}}
      {{-- Row Starts --}}
      
<div class="row">
       <div class="col-md-6"> 
         
         {{ Form::button('<i class="fa fa-calculator"></i> Calculate Salary',['class' => 'btn btn-success','type' => 'submit']) }}
       </div>
     </div>
     {{-- Row Ends --}}
</div>
</div>