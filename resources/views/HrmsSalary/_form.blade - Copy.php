<div class="form-layout">
   <div class="form-layout-body">
      
      {{-- Row 1 Starts --}}
      <div class="row">
         <div class="col-sm-6">
             <div class="form-group @if ($errors->has('employee_id')) has-error @endif">
                                        {!! Form::label('employee_id','Employe Name') !!}
                                        {{ Form::select('employee_id',$employees, null,  ['class' =>'form-control rounded','id' =>'employee_id']) }}
                                            @if ($errors->has('employee_id')) <p class="help-block">{{ $errors->first('employee_id') }}</p> @endif
                </div>
         </div>

         <div class="col-sm-6">
            <div class="form-group @if ($errors->has('employee_grade')) has-error @endif">
               {!! Form::label('employee_grade','Employee Grade') !!}
               {{ Form::select('employee_grade',$employeeGrade, null,  ['class' =>'form-control rounded','id' =>'employee_grade','onChange'=>'getSelectedGradeId()']) }}
               @if ($errors->has('employee_grade')) 
               <p class="help-block">{{ $errors->first('employee_grade') }}</p>
               @endif
            </div>
         </div>
         
      </div>
      {{-- Row 1 Ends --}}

      {{-- Row  Starts --}}

      <div id="rslt2">
</div>
      <div class="row">
        
      

      <div class="col-sm-6">
            <div class="form-group @if ($errors->has('employee_netsalary')) has-error @endif">
               {!! Form::label('employee_netsalary','Net Salary') !!}
               {!! Form::text('employee_netsalary',old('employee_netsalary'),['placeholder'=>'Enter Net Salary','class' =>'form-control rounded','id' =>'employee_netsalary','onkeyup'=>'calculateSalary()']) !!}
               @if ($errors->has('employee_netsalary')) 
               <p class="help-block">{{ $errors->first('employee_netsalary') }}</p>
               @endif
            </div>
         </div>
        

         </div>

      {{-- Row 1 Ends --}}
      {{-- Row Starts --}}
      
<div class="row">
       <div class="col-md-6"> 
         
         {{ Form::button('<i class="fa fa-calculator"></i> Calculate Salary',['class' => 'btn btn-success','type' => 'submit']) }}
       </div>
     </div>
     {{-- Row Ends --}}

                <div class="row"> 

                  <div class="col-md-6"> 

                    <h4 class="text-primary">Earnings</h4>


                    <div class="form-group @if ($errors->has('employee_salary_basic_pay')) has-error @endif">
                              {!! Form::label('employee_salary_basic_pay','Basic') !!}
                               {!! Form::text('employee_salary_basic_pay',old('employee_salary_basic_pay'),['placeholder'=>'Enter Net Salary','class' =>'form-control rounded','id' =>'employee_salary_basic_pay']) !!}
                               @if ($errors->has('employee_salary_basic_pay')) 
                               <p class="help-block">{{ $errors->first('employee_salary_basic_pay') }}</p>
                               @endif
                </div>

                    

                <div class="form-group @if ($errors->has('employee_salary_hra')) has-error @endif">
                              {!! Form::label('employee_salary_hra','HRA') !!}
                               {!! Form::text('employee_salary_hra',old('employee_salary_hra'),['placeholder'=>'Enter Hra','class' =>'form-control rounded','id' =>'employee_salary_hra']) !!}
                               @if ($errors->has('employee_salary_hra')) 
                               <p class="help-block">{{ $errors->first('employee_salary_hra') }}</p>
                               @endif
                </div>

                <div class="form-group @if ($errors->has('employee_salary_cca')) has-error @endif">
                              {!! Form::label('employee_salary_cca','CCA') !!}
                               {!! Form::text('employee_salary_cca',old('employee_salary_cca'),['placeholder'=>'Enter CCA','class' =>'form-control rounded','id' =>'employee_salary_cca']) !!}
                               @if ($errors->has('employee_salary_cca')) 
                               <p class="help-block">{{ $errors->first('employee_salary_cca') }}</p>
                               @endif
                </div>


                <div class="form-group @if ($errors->has('employee_salary_medical_reimbursement')) has-error @endif">
                              {!! Form::label('employee_salary_medical_reimbursement','Medical Reimbursement') !!}
                               {!! Form::text('employee_salary_medical_reimbursement',old('employee_salary_medical_reimbursement'),['placeholder'=>'Enter Medical Reimbursement','class' =>'form-control rounded','id' =>'employee_salary_medical_reimbursement']) !!}
                               @if ($errors->has('employee_salary_medical_reimbursement')) 
                               <p class="help-block">{{ $errors->first('employee_salary_medical_reimbursement') }}</p>
                               @endif
                </div>

                <div class="form-group @if ($errors->has('employee_salary_mobile_phone_allowance')) has-error @endif">
                              {!! Form::label('employee_salary_mobile_phone_allowance','Mobile Phone Allowance') !!}
                               {!! Form::text('employee_salary_mobile_phone_allowance',old('employee_salary_mobile_phone_allowance'),['placeholder'=>'Enter Mobile Phone Allowance','class' =>'form-control rounded','id' =>'employee_salary_mobile_phone_allowance']) !!}
                               @if ($errors->has('employee_salary_mobile_phone_allowance')) 
                               <p class="help-block">{{ $errors->first('employee_salary_mobile_phone_allowance') }}</p>
                               @endif
                </div>

                <div class="form-group @if ($errors->has('employee_salary_field_allowance')) has-error @endif">
                              {!! Form::label('employee_salary_field_allowance','Field Allowance') !!}
                               {!! Form::text('employee_salary_field_allowance',old('employee_salary_field_allowance'),['placeholder'=>'Enter Field Allowance','class' =>'form-control rounded','id' =>'employee_salary_field_allowance']) !!}
                               @if ($errors->has('employee_salary_field_allowance')) 
                               <p class="help-block">{{ $errors->first('employee_salary_field_allowance') }}</p>
                               @endif
                </div>

                <div class="form-group @if ($errors->has('employee_salary_monthly_reimbursement')) has-error @endif">
                              {!! Form::label('employee_salary_monthly_reimbursement','Monthly Reimbursement') !!}
                               {!! Form::text('employee_salary_monthly_reimbursement',old('employee_salary_monthly_reimbursement'),['placeholder'=>'Enter Field Allowance','class' =>'form-control rounded','id' =>'employee_salary_monthly_reimbursement']) !!}
                               @if ($errors->has('employee_salary_monthly_reimbursement')) 
                               <p class="help-block">{{ $errors->first('employee_salary_monthly_reimbursement') }}</p>
                               @endif
                </div>

                


                

                

                


                   

                    

                  </div>

                  <div class="col-md-6">  

                    <h4 class="text-primary">Deductions</h4>

                    <div class="form-group @if ($errors->has('employee_provident_fund')) has-error @endif">
                              {!! Form::label('employee_provident_fund','Provident Fund') !!}
                               {!! Form::text('employee_provident_fund',old('employee_provident_fund'),['placeholder'=>'Enter PF','class' =>'form-control rounded','id' =>'employee_provident_fund']) !!}
                               @if ($errors->has('employee_provident_fund')) 
                               <p class="help-block">{{ $errors->first('employee_provident_fund') }}</p>
                               @endif
                </div>

                <div class="form-group @if ($errors->has('employee_salary_esi')) has-error @endif">
                              {!! Form::label('employee_salary_esi','ESI') !!}
                               {!! Form::text('employee_salary_esi',old('employee_salary_esi'),['placeholder'=>'Enter ESI','class' =>'form-control rounded','id' =>'employee_salary_esi']) !!}
                               @if ($errors->has('employee_salary_esi')) 
                               <p class="help-block">{{ $errors->first('employee_salary_esi') }}</p>
                               @endif
                </div>

                <div class="form-group @if ($errors->has('employee_salary_tds')) has-error @endif">
                              {!! Form::label('employee_salary_tds','TDS') !!}
                               {!! Form::text('employee_salary_tds',old('employee_salary_tds'),['placeholder'=>'Enter ESI','class' =>'form-control rounded','id' =>'employee_salary_tds']) !!}
                               @if ($errors->has('employee_salary_tds')) 
                               <p class="help-block">{{ $errors->first('employee_salary_tds') }}</p>
                               @endif
                </div>

                <div class="form-group @if ($errors->has('employee_salary_salary_advance')) has-error @endif">
                              {!! Form::label('employee_salary_salary_advance','Salary Advance') !!}
                               {!! Form::text('employee_salary_salary_advance',old('employee_salary_salary_advance'),['placeholder'=>'Enter ESI','class' =>'form-control rounded','id' =>'employee_salary_salary_advance']) !!}
                               @if ($errors->has('employee_salary_salary_advance')) 
                               <p class="help-block">{{ $errors->first('employee_salary_salary_advance') }}</p>
                               @endif
                </div>

                <div class="form-group @if ($errors->has('employee_salary_income_tax')) has-error @endif">
                              {!! Form::label('employee_salary_income_tax','Salary Advance') !!}
                               {!! Form::text('employee_salary_income_tax',old('employee_salary_income_tax'),['placeholder'=>'Enter ESI','class' =>'form-control rounded','id' =>'employee_salary_income_tax']) !!}
                               @if ($errors->has('employee_salary_income_tax')) 
                               <p class="help-block">{{ $errors->first('employee_salary_income_tax') }}</p>
                               @endif
                </div>

                <div class="form-group @if ($errors->has('employee_salary_professional_tax')) has-error @endif">
                              {!! Form::label('employee_salary_professional_tax','Professional Tax') !!}
                               {!! Form::text('employee_salary_professional_tax',old('employee_salary_professional_tax'),['placeholder'=>'Enter ESI','class' =>'form-control rounded','id' =>'employee_salary_professional_tax']) !!}
                               @if ($errors->has('employee_salary_professional_tax')) 
                               <p class="help-block">{{ $errors->first('employee_salary_professional_tax') }}</p>
                               @endif
                </div>




                <div class="form-group @if ($errors->has('employee_salary_other_deduction')) has-error @endif">
                              {!! Form::label('employee_salary_other_deduction','Other Deductions') !!}
                               {!! Form::text('employee_salary_other_deduction',old('employee_salary_other_deduction'),['placeholder'=>'Enter ESI','class' =>'form-control rounded','id' =>'employee_salary_other_deduction']) !!}
                               @if ($errors->has('employee_salary_other_deduction')) 
                               <p class="help-block">{{ $errors->first('employee_salary_other_deduction') }}</p>
                               @endif
                </div>




                
{{-- {{BladeHelper::generateEmployeeSalary('15000')}} --}}
                

                

                
                   
                    

                    

                    
                   

                   

                  </div>
                </div>
                
   </div>
</div>


<script>
  function getEnteredGrossSalary() 
  {
    var netSalary = document.getElementById('employee_netsalary').value;
    return netSalary;
  }

  function getSelectedGradeId() 
  {
    var gradeID = document.getElementById('employee_grade').value;
    return gradeID;
    
  }

  function isEmpty(controlVariable)
  {
    return (controlVariable === undefined || controlVariable == null || controlVariable.length <= 0) ? true : false;
  }



 



  function calculateSalary()
  {
    if(isEmpty(getSelectedGradeId())) 
    {
    alert('Choose the grade');
    var emptyValue = '';
    document.getElementById('employee_netsalary').value = emptyValue;
    }
    else
    {
      

      

    document.getElementById("employee_salary_basic_pay").value = calculateBasicPay();
    document.getElementById("employee_salary_hra").value = calculateHra();
    document.getElementById("employee_salary_cca").value = calculateCCA();
    document.getElementById("employee_provident_fund").value = calculateProvidentFund();
    document.getElementById("employee_salary_esi").value = calculateEsi();

    }

    


    
    
    
    
    

    
    

 
  }

 function calculateBasicPay() 
 {
  var  basicPayConstant = 50/100;
  var basciPayResult = getEnteredGrossSalary() *  basicPayConstant;
  return   basciPayResult;                           

}
function calculateHra() 
{
    var hRAConstant = 50/100;
    var hRAResult  = calculateBasicPay() * hRAConstant;
    return hRAResult;
}
function calculateCCA() 
{
  var cCAConstant = 10/100;
  var  cCAResult  = calculateBasicPay() * cCAConstant;
  return cCAResult;
}

function calculateConveyanceAllowance() 
{
  var conveyanceAllowanceConstant = 800;
  var conveyanceAllowanceResult = conveyanceAllowanceConstant;
  return conveyanceAllowanceResult;
}

function calculateProvidentFund() 
{
                    var providentFundConstant = 12/100;


                                var providentFundResult = calculateBasicPay() * providentFundConstant;
                                
                                return providentFundResult;
}

function calculateEsi()
{

  var  esiConstantSALBEL_21000 = 1.75 / 100;

    if (getEnteredGrossSalary() < 21000) 
    {
        var esiResult = getEnteredGrossSalary() * esiConstantSALBEL_21000;
        return esiResult;
    }  
}

  

</script>




 <!-- <?php $test_id = \App\Models\User::findOrFail(1); ?>

 var test_id = <?= $test_id ?>; -->

<div class="form-layout-footer">
                <div class="form-group text-right">
                  <div class="col-sm-12">
                    <button type="submit" class="btn btn-info rounded">Submit</button>
                    <button type="button" class="btn btn-default rounded">Cancel</button>
                  </div>
                </div>
                <!-- /.form-group -->
              </div>

              <!-- employees -->