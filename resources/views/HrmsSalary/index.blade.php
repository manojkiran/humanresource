@extends('layouts.app')
@section('title', 'HrmsSalary')
@section('breadcrumb')
    <div class="breadcrumb-wrapper">
        <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-home"></i>Home</a></li>
        <li class="active">Salaries List</li>
        </ol>
    </div><!-- /.breadcrumb-wrapper -->
@endsection
@section('content')
@section('pagetitle')
<div class="page-title-wrapper">
          <h2 class="page-title">List Of HrmsSalary</h2>
        </div><!-- /.page-titile-wrapper -->
@endsection
<div class="row">
   <div class="col-md-5">
      <h3 class="modal-title">{{ $hrmssalaries->total() }} {{ str_plural('Salary', $hrmssalaries->count()) }} </h3>
   </div>
   <div class="col-md-7 page-action text-right">
      
      <a href="{{ route('salaries.create') }}" class="btn btn-primary btn-sm"> <i class="glyphicon glyphicon-plus-sign"></i> Create</a>
      
   </div>
</div>
<div class="result-set">
   <table class="table table-bordered table-striped table-hover" id="data-table">
      <thead>
         <tr>
            <th>Id</th>
            <th>Employee Name</th>
            <th class="text-center">Actions</th>
         </tr>
      </thead>
      <tbody>
         @forelse($hrmssalaries as $item)
         <tr>
            <td>{{ $loop->iteration }}</td>
            <td>{{ BladeHelper::generateFullName($item->employee_id) }}</td>
            <td>{!!BladeHelper::tableActionButtons(url()->full(),$item->id,$item->id,['show','edit','delete'])!!}</td>
            @empty
      </tbody>
      <tfoot>
         <center>
            <h3><code>There are no HrmsSalary to display!</code></h3>
         </center>
      </tfoot>
      @endforelse
   </table>
   <div class="text-center">
      {{ $hrmssalaries->links('layouts.pagination') }}
   </div>
</div>
@endsection
