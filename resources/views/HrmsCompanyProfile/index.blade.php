@extends('layouts.app')
@section('title', 'HrmsCompanyProfile')
@section('breadcrumb')
    <div class="breadcrumb-wrapper">
        <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-home"></i>Home</a></li>
        <li class="active">Organization Profile List</li>
        </ol>
    </div><!-- /.breadcrumb-wrapper -->
@endsection
@section('content')
@section('pagetitle')
<div class="page-title-wrapper">
          <h2 class="page-title">List Of CompanyProfile</h2>
        </div><!-- /.page-titile-wrapper -->
@endsection
<div class="row">
   <div class="col-md-5">
      <h3 class="modal-title">{{ $hrmscompanyprofiles->total() }} {{ str_plural('CompanyProfile', $hrmscompanyprofiles->count()) }} </h3>
   </div>
   <div class="col-md-7 page-action text-right">
      
      <a href="{{ route('companyprofiles.create') }}" class="btn btn-primary btn-sm"> <i class="fa fa-plus"></i> Create</a>
      
   </div>
</div>
<div class="result-set">
   <table class="table table-bordered table-striped table-hover" id="data-table">
      <thead>
         <tr>
            <th>Id</th>
            <th>Company Name</th>
            <th>State</th>
            <th>City</th>
            <th>Phone Number</th>
            <th>Authorized Person</th>
            
            <th class="text-center">Actions</th>
         </tr>
      </thead>
      <tbody>
         @forelse($hrmscompanyprofiles as $item)
         <tr>
            <td>{{ $loop->iteration }}</td>
            <td>{{ $item->company_name }}</td>
            <td>{{ $item->company_address_state }}</td>
            <td>{{ $item->company_address_city }}</td>
            <td>{{ $item->company_phone_number }}</td>   
            <td>{{ $item->company_contact_person_full_name }}</td>   
                     
            <td>{!!BladeHelper::tableActionButtons(url()->full(),$item->id,$item->company_name,['edit','show'])!!}</td>
            @empty
      </tbody>
      <tfoot>
         <center>
            <h3><code>There are no CompanyProfile to display!</code></h3>
         </center>
      </tfoot>
      @endforelse
   </table>
   <div class="text-center">
      {{ $hrmscompanyprofiles->links('layouts.pagination') }}
   </div>
</div>
@endsection
