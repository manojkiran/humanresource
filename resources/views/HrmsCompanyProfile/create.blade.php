@extends('layouts.app')
@section('title', 'Create Employee')
@section('breadcrumb')
    <div class="breadcrumb-wrapper">
        <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-home"></i>Home</a></li>
        <li><a href="#"><i class="fa fa-building"></i>Organization Profile</a></li>
        <li class="active">Create</li>
        </ol>
    </div><!-- /.breadcrumb-wrapper -->
@endsection
@section('content')
<div class="row">
   <div class="col-sm-12">
      <div class="panel panel-default-light border-default">

         <div class="panel-heading">
            <div class="panel-title">
               <i class="fa fa-cog m-right-5"></i> Create New Company Profile
            </div>
            <div class="text-right">
              <a href="{{ route('companyprofiles.index') }}" class="btn btn-warning rounded"> <i class="fa fa-arrow-left"></i> Back</a>
            </div>
         </div>

         <div class="panel-body">
            {!! Form::open(['route' => ['companyprofiles.store'],'autocomplete' => 'off','files' => 'true','enctype'=>'multipart/form-data' ]) !!}
            @include('HrmsCompanyProfile._form')
               
            {!! Form::close()!!}
         </div>
      </div>
   </div>
</div>
@endsection
