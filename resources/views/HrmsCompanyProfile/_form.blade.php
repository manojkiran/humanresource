<div class="form-layout">
<div class="form-layout-body">

   {{-- Row 0 Starts --}}
   <hr>
   <div class="panel-title">
      <i class="fa fa-building m-right-5"></i> General Information
   </div>
   <br><br>
   <div class="row">

      <div class="col-sm-4">
         <div class="form-group @if ($errors->has('company_name')) has-error @endif">
            {!! Form::label('company_name','Company Name') !!}
            {!! Form::text('company_name',old('company_name'),['placeholder'=>'Enter Company Name','class' =>'form-control rounded','id' =>'company_name']) !!}
            @if ($errors->has('company_name'))
            <p class="help-block">{{ $errors->first('company_name') }}</p>
            @endif
         </div>
      </div>

      <div class="col-sm-4">
         <div class="form-group @if ($errors->has('company_registration_number')) has-error @endif">
            {!! Form::label('company_registration_number','Company Registration Number(CIN)') !!}
            {!! Form::text('company_registration_number',old('company_registration_number'),['placeholder'=>'Enter Registration Number','class' =>'form-control rounded','id' =>'company_registration_number']) !!}
            @if ($errors->has('company_registration_number'))
            <p class="help-block">{{ $errors->first('company_registration_number') }}</p>
            @endif
         </div>
      </div>

      <div class="col-sm-4">
         <div class="form-group @if ($errors->has('company_gst_in')) has-error @endif">
            {!! Form::label('company_gst_in','Company GST No.') !!}
            {!! Form::text('company_gst_in',old('company_gst_in'),['placeholder'=>'Enter Registration Number','class' =>'form-control rounded','id' =>'company_gst_in']) !!}
            @if ($errors->has('company_gst_in'))
            <p class="help-block">{{ $errors->first('company_gst_in') }}</p>
            @endif
         </div>
      </div>
   </div>

   {{-- Row  Ends --}}

      {{-- Row  Starts --}}
      <div class="panel-title">
      <i class="fa fa-phone m-right-5"></i> Contact Details
   </div>
   <br><br>
   <div class="row">


      <div class="col-sm-4">
         <div class="form-group @if ($errors->has('company_address_line_one')) has-error @endif">
            {!! Form::label('company_address_line_one','Address Line 1') !!}
            {!! Form::text('company_address_line_one',old('company_address_line_one'),['placeholder'=>'Enter Company Name','class' =>'form-control rounded','id' =>'company_address_line_one']) !!}
            @if ($errors->has('company_address_line_one'))
            <p class="help-block">{{ $errors->first('company_address_line_one') }}</p>
            @endif
         </div>
      </div>

      <div class="col-sm-4">
         <div class="form-group @if ($errors->has('company_address_line_two')) has-error @endif">
            {!! Form::label('company_address_line_two','Address Line 2') !!}
            {!! Form::text('company_address_line_two',old('company_address_line_two'),['placeholder'=>'Enter Company Name','class' =>'form-control rounded','id' =>'company_address_line_two']) !!}
            @if ($errors->has('company_address_line_two'))
            <p class="help-block">{{ $errors->first('company_address_line_two') }}</p>
            @endif
         </div>
      </div>


      <div class="col-sm-4">
         <div class="form-group @if ($errors->has('company_address_city')) has-error @endif">
            {!! Form::label('company_address_city','City') !!}
            {!! Form::text('company_address_city',old('company_address_city'),['placeholder'=>'Enter City','class' =>'form-control rounded','id' =>'company_address_city']) !!}
            @if ($errors->has('company_address_city'))
            <p class="help-block">{{ $errors->first('company_address_city') }}</p>
            @endif
         </div>
      </div>


   </div>

   {{-- Row  Ends --}}

       {{-- Row  Starts --}}

   <div class="row">

      <div class="col-sm-4">
         <div class="form-group @if ($errors->has('company_address_state')) has-error @endif">
            {!! Form::label('company_address_state','State') !!}
            {!! Form::text('company_address_state',old('company_address_state'),['placeholder'=>'Enter State','class' =>'form-control rounded','id' =>'company_address_state']) !!}
            @if ($errors->has('company_address_state'))
            <p class="help-block">{{ $errors->first('company_address_state') }}</p>
            @endif
         </div>
      </div>

      <div class="col-sm-4">
         <div class="form-group @if ($errors->has('company_address_country')) has-error @endif">
            {!! Form::label('company_address_country',' Country') !!}
            {!! Form::text('company_address_country',old('company_address_country'),['placeholder'=>'Enter Country','class' =>'form-control rounded','id' =>'company_address_country']) !!}
            @if ($errors->has('company_address_country'))
            <p class="help-block">{{ $errors->first('company_address_country') }}</p>
            @endif
         </div>
      </div>

      <div class="col-sm-4">
         <div class="form-group @if ($errors->has('company_address_zip_code')) has-error @endif">
            {!! Form::label('company_address_zip_code','Zip Code') !!}
            {!! Form::text('company_address_zip_code',old('employee_reporting_to'),['placeholder'=>'Enter City','class' =>'form-control rounded','id' =>'company_address_zip_code']) !!}
            @if ($errors->has('company_address_zip_code'))
            <p class="help-block">{{ $errors->first('company_address_zip_code') }}</p>
            @endif
         </div>
      </div>


   </div>

   {{-- Row  Ends --}}


       {{-- Row  Starts --}}

   <div class="row">

      <div class="col-sm-4">
         <div class="form-group @if ($errors->has('company_email')) has-error @endif">
            {!! Form::label('company_email','Email') !!}
            {!! Form::text('company_email',old('company_email'),['placeholder'=>'Enter Email id','class' =>'form-control rounded','id' =>'company_email']) !!}
            @if ($errors->has('company_email'))
            <p class="help-block">{{ $errors->first('company_email') }}</p>
            @endif
         </div>
      </div>

      <div class="col-sm-4">
         <div class="form-group @if ($errors->has('company_phone_number')) has-error @endif">
            {!! Form::label('company_phone_number',' Phone Number') !!}
            {!! Form::text('company_phone_number',old('company_phone_number'),['placeholder'=>'Enter Phone Number','class' =>'form-control rounded','id' =>'company_phone_number']) !!}
            @if ($errors->has('company_phone_number'))
            <p class="help-block">{{ $errors->first('company_phone_number') }}</p>
            @endif
         </div>
      </div>

     <!--  <div class="col-sm-4">
         <div class="form-group @if ($errors->has('company_mobile_number')) has-error @endif">
            {!! Form::label('company_mobile_number','Mobile Number') !!}
            {!! Form::text('company_mobile_number',old('company_mobile_number'),['placeholder'=>'Enter Mobile Number','class' =>'form-control rounded','id' =>'company_mobile_number']) !!}
            @if ($errors->has('company_mobile_number'))
            <p class="help-block">{{ $errors->first('company_mobile_number') }}</p>
            @endif
         </div>
      </div> -->


   </div>

   {{-- Row  Ends --}}


       {{-- Row  Starts --}}

   <div class="row">

      <div class="col-sm-4">
         <div class="form-group @if ($errors->has('company_fax_number')) has-error @endif">
            {!! Form::label('company_fax_number','Fax') !!}
            {!! Form::text('company_fax_number',old('company_fax_number'),['placeholder'=>'Enter Fax No','class' =>'form-control rounded','id' =>'company_fax_number']) !!}
            @if ($errors->has('company_fax_number'))
            <p class="help-block">{{ $errors->first('company_fax_number') }}</p>
            @endif
         </div>
      </div>

      <div class="col-sm-4">
         <div class="form-group @if ($errors->has('company_website_url')) has-error @endif">
            {!! Form::label('company_website_url',' Website Url') !!}
            {!! Form::text('company_website_url',old('company_website_url'),['placeholder'=>'Enter WebSite Url','class' =>'form-control rounded','id' =>'company_website_url']) !!}
            @if ($errors->has('company_website_url'))
            <p class="help-block">{{ $errors->first('company_website_url') }}</p>
            @endif
         </div>
      </div>


   </div>

   {{-- Row  Ends --}}

   {{-- Row 0 Starts --}}
   <hr>
   <div class="panel-title">
      <i class="fas fa-user-tie m-right-5"></i> Authorized Signatory
   </div>
   <br><br>
   <div class="row">

      <div class="col-sm-4">
         <div class="form-group @if ($errors->has('company_contact_person_full_name')) has-error @endif">
            {!! Form::label('company_contact_person_full_name','Full Name') !!}
            {!! Form::text('company_contact_person_full_name',old('company_contact_person_full_name'),['placeholder'=>'Enter Full  Name','class' =>'form-control rounded','id' =>'company_contact_person_full_name']) !!}
            @if ($errors->has('company_contact_person_full_name'))
            <p class="help-block">{{ $errors->first('company_contact_person_full_name') }}</p>
            @endif
         </div>
      </div>

      <div class="col-sm-4">
         <div class="form-group @if ($errors->has('company_contact_person_designation')) has-error @endif">
            {!! Form::label('company_contact_person_designation','Designation') !!}
            {!! Form::text('company_contact_person_designation',old('company_contact_person_designation'),['placeholder'=>'Enter Designation','class' =>'form-control rounded','id' =>'company_contact_person_designation']) !!}
            @if ($errors->has('company_contact_person_designation'))
            <p class="help-block">{{ $errors->first('company_contact_person_designation') }}</p>
            @endif
         </div>
      </div>


   </div>

   {{-- Row  Ends --}}

    {{-- Row 0 Starts --}}
   <div class="row">


      <div class="col-sm-4">
         <div class="form-group @if ($errors->has('company_contact_person_email_id')) has-error @endif">
            {!! Form::label('company_contact_person_email_id','Email Id') !!}
            {!! Form::text('company_contact_person_email_id',old('company_contact_person_email_id'),['placeholder'=>'Enter Email Id','class' =>'form-control rounded','id' =>'company_contact_person_email_id']) !!}
            @if ($errors->has('company_contact_person_email_id'))
            <p class="help-block">{{ $errors->first('company_contact_person_email_id') }}</p>
            @endif
         </div>
      </div>

      <div class="col-sm-4">
         <div class="form-group @if ($errors->has('company_contact_person_phone_number')) has-error @endif">
            {!! Form::label('company_contact_person_phone_number','Mobile Number') !!}
            {!! Form::text('company_contact_person_phone_number',old('company_contact_person_phone_number'),['placeholder'=>'Enter Mobile Number','class' =>'form-control rounded','id' =>'company_contact_person_phone_number']) !!}
            @if ($errors->has('company_contact_person_phone_number'))
            <p class="help-block">{{ $errors->first('company_contact_person_phone_number') }}</p>
            @endif
         </div>
      </div>


   </div>

   {{-- Row  Ends --}}

   <div class="form-layout-footer">
   <div class="form-group text-right">
      <div class="col-sm-12">
         <button type="button" class="btn btn-success">
                        <i class="fa fa-cube"></i> Button
                      </button>

{{ form::button('<i class="fa fa-cube"></i> Button',['class' => 'btn btn-success']) }}

         <button type="submit" class="btn btn-info rounded">Submit</button>
         <button type="reset" class="btn btn-danger rounded">Clear</button>
      </div>
   </div>
   <!-- /.form-group -->
</div>








</div>
</div>
