@extends('layouts.app')
@section('title', 'HrmsShift-')
@section('breadcrumb')
@stop
@section('content')
<div class="row">
   <div class="col-xs-12">
      <div class="box">
         <div class="col-md-5">
            <h3>New HrmsShift</h3>
         </div>
         <div class="text-right">
            <a href="{{ route('hrmsshifts.index') }}" class="btn bg-purple btn-flat margin"> <i class="fa fa-arrow-left"></i> Back</a>
         </div>
         <div class="box-body">
            {!! Form::open(['route' => ['hrmsshifts.store'],'autocomplete' => 'off','files' => 'true','enctype'=>'multipart/form-data' ]) !!}
            @include('HrmsShift._form')
            <div class="box-footer">
               {!! Form::submit('Create', ['class' => 'btn btn-primary']) !!}
            </div>
            {!! Form::close() !!}
         </div>
      </div>
   </div>
</div>
@stop