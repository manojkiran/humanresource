<?php

use Illuminate\Database\Seeder;
use App\Models\Permission;
use App\Models\Role;
use App\Models\User;
use Illuminate\Support\Facades\DB;

class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        if (DB::table('permissions')->get()->count() == 0) {

            DB::table('permissions')->insert([

                [
                    'name' => 'view.users',
                    'description'  =>'View Users',
                    'module_id' =>'1',
                    'created_at' => date('Y-m-d H:i:s'),
                    'created_by' =>'1',

                ],
                [
                    'name' => 'create.users',
                    'description'  =>'Create Users',
                    'module_id' =>'1',
                    'created_at' => date('Y-m-d H:i:s'),
                    'created_by' =>'1',

                ],
                [
                    'name' => 'edit.users',
                    'description'  =>'Edit Users',
                    'module_id' =>'1',
                    'created_at' => date('Y-m-d H:i:s'),
                    'created_by' =>'1',
                ],
                [
                    'name' => 'delete.users',
                    'description'  =>'Delete Users',
                    'module_id' =>'1',
                    'created_at' => date('Y-m-d H:i:s'),
                    'created_by' =>'1',
                ],
                [
                    'name' => 'view.roles',
                    'description'  =>'View Roles',
                    'module_id' =>'2',
                    'created_at' => date('Y-m-d H:i:s'),
                    'created_by' =>'1',

                ],
                [
                    'name' => 'create.roles',
                    'description'  =>'Create Roles',
                    'module_id' =>'2',
                    'created_at' => date('Y-m-d H:i:s'),
                    'created_by' =>'1',

                ],
                [
                    'name' => 'edit.roles',
                    'description'  =>'Edit Roles',
                    'module_id' =>'2',
                    'created_at' => date('Y-m-d H:i:s'),
                    'created_by' =>'1',
                ],
                [
                    'name' => 'delete.roles',
                    'description'  =>'Delete Roles',
                    'module_id' =>'2',
                    'created_at' => date('Y-m-d H:i:s'),
                    'created_by' =>'1',
                ],
                [
                    'name' => 'view.permissions',
                    'description'  =>'View Permissions',
                    'module_id' =>'3',
                    'created_at' => date('Y-m-d H:i:s'),
                    'created_by' =>'1',

                ],
                [
                    'name' => 'create.permissions',
                    'description'  =>'Create Permissions',
                    'module_id' =>'3',
                    'created_at' => date('Y-m-d H:i:s'),
                    'created_by' =>'1',

                ],
                [
                    'name' => 'edit.permissions',
                    'description'  =>'Edit Permissions',
                    'module_id' =>'3',
                    'created_at' => date('Y-m-d H:i:s'),
                    'created_by' =>'1',
                ],
                [
                    'name' => 'delete.permissions',
                    'description'  =>'Delete Permissions',
                    'module_id' =>'3',
                    'created_at' => date('Y-m-d H:i:s'),
                    'created_by' =>'1',
                ],
                [
                    'name' => 'view.employees',
                    'description'  =>'View Employees',
                    'module_id' =>'4',
                    'created_at' => date('Y-m-d H:i:s'),
                    'created_by' =>'1',

                ],
                [
                    'name' => 'create.employees',
                    'description'  =>'Create Employees',
                    'module_id' =>'4',
                    'created_at' => date('Y-m-d H:i:s'),
                    'created_by' =>'1',

                ],
                [
                    'name' => 'edit.employees',
                    'description'  =>'Edit Employees',
                    'module_id' =>'4',
                    'created_at' => date('Y-m-d H:i:s'),
                    'created_by' =>'1',
                ],
                [
                    'name' => 'delete.employees',
                    'description'  =>'Delete Permissions',
                    'module_id' =>'4',
                    'created_at' => date('Y-m-d H:i:s'),
                    'created_by' =>'1',
                ],

            ]);

        } else {
            echo "Permission Table is not empty";
        }

        Role::find(1)->permissions()->sync([1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16]);
        Role::find(2)->permissions()->sync([1,2,3,4,5,6,7,8,9,10,11,12]);
        Role::find(3)->permissions()->sync([1,5,9]);
        User::find(1)->roles()->sync([1]);
        User::find(2)->roles()->sync([2]);
        User::find(3)->roles()->sync([3]);
        User::find(4)->roles()->sync([3]);
        // User::find(5)->roles()->sync([4]);
        User::find(4)->permissions()->sync([2,6,10]);

    }
}
