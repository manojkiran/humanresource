<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        Schema::disableForeignKeyConstraints();
        $this->truncateAllTables();
        Schema::enableForeignKeyConstraints();

        $this->call(UsersTableSeeder::class);
        $this->call(RolesTableSeeder::class);
        $this->call(ModulesTableSeeder::class);
        $this->call(PermissionsTableSeeder::class);
        $this->call(HrmsCompanyProfilesTableSeeder::class);
        $this->call(HrmsCountriesTableSeeder::class);

        $this->call(HrmsDepartmentsTableSeeder::class);
        $this->call(HrmsShiftsTableSeeder::class);
        $this->call(HrmsLeaveTypesTableSeeder::class);
        $this->call(HrmsEmploymentTypesTableSeeder::class);

        // $this->call(HrmsEmployeesTableSeeder::class);
        
        //$this->call(HrmsApplyLeavesTableSeeder::class);
        $this->call(HrmsEmployeeGradesTableSeeder::class);
        $this->call(HrmsEmployeeSalaryGradesTableSeeder::class);
        $this->call(HrmsLeaveSettingLeaveTypesTableSeeder::class);
    }

    private function truncateAllTables()
    {
        $tableNames = Schema::getConnection()->getDoctrineSchemaManager()->listTableNames();

        foreach ($tableNames as $name)
        {
            //if you don't want to truncate migrations
            if ($name == 'migrations')
            {
                continue;
            }
            else
            {
                $this->command->warn('Truncating Table '.$name);
                DB::table($name)->truncate();
                $this->command->warn('Truncated Table '.$name);
            }
            

        }
    }
}

