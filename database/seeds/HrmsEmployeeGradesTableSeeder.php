<?php

use Illuminate\Database\Seeder;

class HrmsEmployeeGradesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('hrms_employee_grades')->delete();
        
        \DB::table('hrms_employee_grades')->insert(array (
            0 => 
            array (
                'id' => 1,
                'employee_grade_name' => 'Top Management',
                'created_by' => 1,
                'updated_by' => NULL,
                'created_at' => '2019-01-03 15:38:22',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'employee_grade_name' => 'Directors / President',
                'created_by' => 1,
                'updated_by' => NULL,
                'created_at' => '2019-01-03 15:39:26',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'employee_grade_name' => 'Vice President',
                'created_by' => 1,
                'updated_by' => NULL,
                'created_at' => '2019-01-03 15:39:36',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            3 => 
            array (
                'id' => 4,
                'employee_grade_name' => 'General Manager / Senior Manager',
                'created_by' => 1,
                'updated_by' => NULL,
                'created_at' => '2019-01-03 15:39:44',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            4 => 
            array (
                'id' => 5,
                'employee_grade_name' => 'Assistant General Manager',
                'created_by' => 1,
                'updated_by' => NULL,
                'created_at' => '2019-01-03 15:39:52',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            5 => 
            array (
                'id' => 6,
                'employee_grade_name' => 'Project / Program Manager',
                'created_by' => 1,
                'updated_by' => NULL,
                'created_at' => '2019-01-03 15:40:01',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            6 => 
            array (
                'id' => 7,
                'employee_grade_name' => 'Team / Tech Lead / Supervisor',
                'created_by' => 1,
                'updated_by' => NULL,
                'created_at' => '2019-01-03 15:40:24',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            7 => 
            array (
                'id' => 8,
                'employee_grade_name' => 'BDM / Marketing',
                'created_by' => 1,
                'updated_by' => NULL,
                'created_at' => '2019-01-03 15:40:43',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            8 => 
            array (
                'id' => 9,
                'employee_grade_name' => 'Senior Engineer / Senior Associate',
                'created_by' => 1,
                'updated_by' => NULL,
                'created_at' => '2019-01-03 15:40:55',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            9 => 
            array (
                'id' => 10,
                'employee_grade_name' => 'Engineer / Associate',
                'created_by' => 1,
                'updated_by' => NULL,
                'created_at' => '2019-01-03 15:41:03',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            10 => 
            array (
                'id' => 11,
                'employee_grade_name' => 'Trainee / Junior Associate',
                'created_by' => 1,
                'updated_by' => NULL,
                'created_at' => '2019-01-03 15:41:11',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            11 => 
            array (
                'id' => 12,
                'employee_grade_name' => 'Contractor / Consultant',
                'created_by' => 1,
                'updated_by' => NULL,
                'created_at' => '2019-01-03 15:41:54',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            12 => 
            array (
                'id' => 13,
                'employee_grade_name' => 'Bussiness Development Executives',
                'created_by' => 1,
                'updated_by' => NULL,
                'created_at' => '2019-01-07 14:04:21',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
        ));
        
        
    }
}