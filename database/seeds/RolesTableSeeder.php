<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (DB::table('roles')->get()->count() == 0) {

            DB::table('roles')->insert([

                [
                    'name' => config('Roles.dafaultSeeds.roles.name.superAdmin'),
                    'description' => config('Roles.dafaultSeeds.roles.description.superAdmin'),
                    'created_at' => date('Y-m-d H:i:s'),
                    'created_by' =>'1',

                ],
                [
                    'name' => config('Roles.dafaultSeeds.roles.name.admin'),
                    'description' => config('Roles.dafaultSeeds.roles.description.admin'),
                    'description'  =>'Admin Role',
                    'created_at' => date('Y-m-d H:i:s'),
                    'created_by' =>'1',

                ],
                [
                    'name' => config('Roles.dafaultSeeds.roles.name.user'),
                    'description' => config('Roles.dafaultSeeds.roles.description.user'),
                    'created_at' => date('Y-m-d H:i:s'),
                    'created_by' =>'1',
                ],

                [
                    'name' => config('Roles.dafaultSeeds.roles.name.employee'),
                    'description' => config('Roles.dafaultSeeds.roles.description.employee'),
                    'created_at' => date('Y-m-d H:i:s'),
                    'created_by' => '1',
                ],

                [
                    'name' => config('Roles.dafaultSeeds.roles.name.manager'),
                    'description' => config('Roles.dafaultSeeds.roles.description.manager'),
                    'created_at' => date('Y-m-d H:i:s'),
                    'created_by' => '1',
                ],

            ]);

        } else {
            echo "Roles Table is not empty";
        }
    }
}
