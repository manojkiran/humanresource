<?php

use Illuminate\Database\Seeder;

class HrmsLeaveSettingLeaveTypesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('hrms_leave_setting_leave_types')->delete();
        
        \DB::table('hrms_leave_setting_leave_types')->insert(array (
            0 => 
            array (
                'id' => 1,
                'leave_setting_name' => 'Central Government',
                'created_by' => 1,
                'updated_by' => NULL,
                'created_at' => '2019-01-08 11:11:39',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'leave_setting_name' => 'State Government',
                'created_by' => 1,
                'updated_by' => NULL,
                'created_at' => '2019-01-08 11:12:04',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'leave_setting_name' => 'Festival Leave',
                'created_by' => 1,
                'updated_by' => NULL,
                'created_at' => '2019-01-08 11:12:30',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
        ));
        
        
    }
}