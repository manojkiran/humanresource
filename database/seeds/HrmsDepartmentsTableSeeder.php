<?php

use Illuminate\Database\Seeder;

class HrmsDepartmentsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('hrms_departments')->delete();
        
        \DB::table('hrms_departments')->insert(array (
            0 => 
            array (
                'id' => 1,
                'department_name' => 'Software Development',
                'company_id' => 1,
                'department_prefix' => 'SWDE-',
                'created_by' => 1,
                'updated_by' => NULL,
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
        ));
        
        
    }
}