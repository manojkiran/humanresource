<?php

use Illuminate\Database\Seeder;

class HrmsApplyLeavesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('hrms_apply_leaves')->delete();
        
        \DB::table('hrms_apply_leaves')->insert(array (
            0 => 
            array (
                'id' => 1,
                'user_id' => 1,
                'employee_leave_type' => 1,
                'employee_leave_starting_from' => '2018/12/31',
                'employee_leave_ending_to' => '2019/01/02',
                'employee_reason_for_leave' => 'test leave',
                'employee_leave_status' => 'Pending',
                'created_by' => 1,
                'updated_by' => NULL,
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
        ));
        
        
    }
}