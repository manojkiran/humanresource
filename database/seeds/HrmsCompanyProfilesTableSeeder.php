<?php

use Illuminate\Database\Seeder;

class HrmsCompanyProfilesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('hrms_company_profiles')->delete();
        
        \DB::table('hrms_company_profiles')->insert(array (
            0 => 
            array (
                'id' => 1,
                'company_name' => 'Postiefs Technologies P Ltd',
                'company_registration_number' => 'UT88347936437964',
                'company_gst_in' => 'UT88347936437964',
                'company_address_line_one' => 'Adyar',
                'company_address_line_two' => 'Adyar',
                'company_address_city' => 'Chennai',
                'company_address_state' => 'TamilNadu',
                'company_address_country' => 'India',
                'company_address_zip_code' => '600020',
                'company_email' => 'contact@postiefs.com',
                'company_phone_number' => '04443533747',
                'company_mobile_number' => NULL,
                'company_fax_number' => NULL,
                'company_website_url' => 'https://www.postiefs.com/',
                'company_contact_person_full_name' => 'Rozario Lavaniyan',
                'company_contact_person_designation' => 'Technical Director',
                'company_contact_person_email_id' => 'rozario@postiefs.com',
                'company_contact_person_phone_number' => '9940425460',
                'created_by' => 1,
                'updated_by' => NULL,
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
        ));
        
        
    }
}