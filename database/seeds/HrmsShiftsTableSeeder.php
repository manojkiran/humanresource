<?php

use Illuminate\Database\Seeder;

class HrmsShiftsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('hrms_shifts')->delete();
        
        \DB::table('hrms_shifts')->insert(array (
            0 => 
            array (
                'id' => 1,
                'shift_name' => '1st Shift',
                'shift_timing' => '7.00 AM to 4.00 PM',
                'created_by' => 1,
                'updated_by' => NULL,
                'created_at' => '2018-12-25 11:09:09',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'shift_name' => '2nd Shift',
                'shift_timing' => '9.00 AM to 6.00 PM',
                'created_by' => 1,
                'updated_by' => NULL,
                'created_at' => '2018-12-25 11:09:09',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'shift_name' => '3rd Shift',
                'shift_timing' => '1.00 PM to 10.00 PM',
                'created_by' => 1,
                'updated_by' => NULL,
                'created_at' => '2018-12-25 11:09:09',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
        ));
        
        
    }
}