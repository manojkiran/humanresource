<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ModulesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         if (DB::table('modules')->get()->count() == 0) {

            DB::table('modules')->insert([

                [
                    'name' =>'User',
                    'created_at' => date('Y-m-d H:i:s'),
                    'created_by' =>'1',

                ],
                [
                    'name' =>'Role',
                    'created_at' => date('Y-m-d H:i:s'),
                    'created_by' =>'1',

                ],
                [
                    'name' =>'Permission',
                    'created_at' => date('Y-m-d H:i:s'),
                    'created_by' =>'1',

                ],
                [
                    'name' =>'Employee',
                    'created_at' => date('Y-m-d H:i:s'),
                    'created_by' =>'1',

                ],

            ]);

        } else {
            echo "Modules Table is not empty";
        }
    }
}
