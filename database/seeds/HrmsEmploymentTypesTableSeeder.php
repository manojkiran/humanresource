<?php

use Illuminate\Database\Seeder;

class HrmsEmploymentTypesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('hrms_employment_types')->delete();
        
        \DB::table('hrms_employment_types')->insert(array (
            0 => 
            array (
                'id' => 1,
                'employment_type' => 'Permanent or Fixed Term',
                'created_by' => 1,
                'updated_by' => NULL,
                'created_at' => '2018-12-27 11:20:06',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'employment_type' => 'Part Time',
                'created_by' => 1,
                'updated_by' => NULL,
                'created_at' => '2018-12-27 11:20:27',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'employment_type' => 'Apprentices or Trainees',
                'created_by' => 1,
                'updated_by' => NULL,
                'created_at' => '2018-12-27 11:20:47',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            3 => 
            array (
                'id' => 4,
                'employment_type' => 'External Consultants',
                'created_by' => 1,
                'updated_by' => NULL,
                'created_at' => '2018-12-27 11:21:03',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            4 => 
            array (
                'id' => 5,
                'employment_type' => 'Consultants',
                'created_by' => 1,
                'updated_by' => NULL,
                'created_at' => '2018-12-27 11:21:18',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            5 => 
            array (
                'id' => 6,
                'employment_type' => 'Contract',
                'created_by' => 1,
                'updated_by' => NULL,
                'created_at' => '2018-12-27 11:21:31',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
        ));
        
        
    }
}