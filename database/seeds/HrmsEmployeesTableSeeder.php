<?php

use Illuminate\Database\Seeder;

class HrmsEmployeesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        
        

        \DB::table('hrms_employees')->delete();
        
        \DB::table('hrms_employees')->insert(array (
            0 => 
            array (
                'id' => 1,
                'user_id' => 5,
                'employee_department_id' => 6,
                'employee_status' => 'Active',
                'employee_reporting_to' => 'Sathish.R',
                'employee_work_shift_id' => 2,
                'employee_designation' => 'Junior Developer',
                'employee_first_name' => 'Manojkiran',
                'employee_middle_name' => NULL,
                'employee_last_name' => 'Appathurai',
                'employee_code' => 'Emp-1001',
                'employee_date_of_birth' => '1998/03/10',
                'employee_gender' => 'Male',
                'employee_aadhar_number' => '835630341732',
                'employee_marital_status' => 'Married',
                'employee_nationality' => 103,
                
                'employee_door_no' => '10/33',
                'employee_street_name' => 'Odakkattur',
                'employee_city' => 'Salem',
                'employee_state' => 'Tamil Nadu',
                'employee_country' => 'India',
                'employee_zip_code' => '637107',
                'employee_home_telephone' => NULL,
                'employee_mobile_number' => '7373637312',
                'employee_email_id' => 'manojkiran.dhurai@gmail.com',
                'employee_employment_type_id' => 1,
                'employee_job_category' => NULL,
                'employee_sub_unit' => NULL,
                'employee_contract_start_date' => '2018-07-04 09:00:00',
                'employee_contract_end_date' => NULL,
                'employee_years_of_experience' => '0',
                'employee_months_of_experience' => '0',
                'employee_primary_skills' => 'php,laravel,mysql,vb.net',
                'employee_secondary_skills' => NULL,
                'employee_previous_company_name' => NULL,
                'employee_previous_designation' => NULL,
                'employee_previous_employment_start_date' => NULL,
                'employee_previous_employment_end_date' => NULL,
                'employee_previous_employment_reason_for_leaving' => NULL,
                'employee_previous_employment_contact_person_name' => NULL,
                'employee_previous_employment_contact_person_email' => NULL,
                'employee_previous_employment_contact_person_phone' => NULL,
                'employee_documents_collected' => 'null',
                'created_by' => 1,
                'updated_by' => NULL,
                'created_at' => '2019-01-07 15:42:27',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
        ));
        
        
    }
}