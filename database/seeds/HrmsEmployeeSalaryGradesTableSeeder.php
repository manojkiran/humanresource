<?php

use Illuminate\Database\Seeder;

class HrmsEmployeeSalaryGradesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('hrms_employee_salary_grades')->delete();
        
        \DB::table('hrms_employee_salary_grades')->insert(array (
            0 => 
            array (
                'id' => 1,
                'employee_grade_id' => 1,
                'employee_grade_salary_medical_reimbursement' => '1250',
                'employee_grade_salary_mobile_phone_allowance' => '3000',
                'employee_grade_salary_field_allowance' => '3000',
                'employee_grade_salary_monthly_reimbursement' => '16000',
                'created_by' => 1,
                'updated_by' => NULL,
                'created_at' => '2019-01-04 11:53:29',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'employee_grade_id' => 2,
                'employee_grade_salary_medical_reimbursement' => '1250',
                'employee_grade_salary_mobile_phone_allowance' => '2500',
                'employee_grade_salary_field_allowance' => '2500',
                'employee_grade_salary_monthly_reimbursement' => '8000',
                'created_by' => 1,
                'updated_by' => NULL,
                'created_at' => '2019-01-07 13:50:48',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'employee_grade_id' => 6,
                'employee_grade_salary_medical_reimbursement' => '0',
                'employee_grade_salary_mobile_phone_allowance' => '0',
                'employee_grade_salary_field_allowance' => '0',
                'employee_grade_salary_monthly_reimbursement' => '5000',
                'created_by' => 1,
                'updated_by' => NULL,
                'created_at' => '2019-01-07 13:51:52',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            3 => 
            array (
                'id' => 4,
                'employee_grade_id' => 9,
                'employee_grade_salary_medical_reimbursement' => '0',
                'employee_grade_salary_mobile_phone_allowance' => '0',
                'employee_grade_salary_field_allowance' => '0',
                'employee_grade_salary_monthly_reimbursement' => '5000',
                'created_by' => 1,
                'updated_by' => NULL,
                'created_at' => '2019-01-07 13:53:09',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            4 => 
            array (
                'id' => 5,
                'employee_grade_id' => 7,
                'employee_grade_salary_medical_reimbursement' => '0',
                'employee_grade_salary_mobile_phone_allowance' => '1500',
                'employee_grade_salary_field_allowance' => '0',
                'employee_grade_salary_monthly_reimbursement' => '5000',
                'created_by' => 1,
                'updated_by' => NULL,
                'created_at' => '2019-01-07 13:54:08',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            5 => 
            array (
                'id' => 6,
                'employee_grade_id' => 11,
                'employee_grade_salary_medical_reimbursement' => '0',
                'employee_grade_salary_mobile_phone_allowance' => '0',
                'employee_grade_salary_field_allowance' => '0',
                'employee_grade_salary_monthly_reimbursement' => '1500',
                'created_by' => 1,
                'updated_by' => NULL,
                'created_at' => '2019-01-07 13:54:48',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            6 => 
            array (
                'id' => 7,
                'employee_grade_id' => 10,
                'employee_grade_salary_medical_reimbursement' => '0',
                'employee_grade_salary_mobile_phone_allowance' => '0',
                'employee_grade_salary_field_allowance' => '0',
                'employee_grade_salary_monthly_reimbursement' => '0',
                'created_by' => 1,
                'updated_by' => NULL,
                'created_at' => '2019-01-07 14:00:39',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            7 => 
            array (
                'id' => 8,
                'employee_grade_id' => 8,
                'employee_grade_salary_medical_reimbursement' => '0',
                'employee_grade_salary_mobile_phone_allowance' => '0',
                'employee_grade_salary_field_allowance' => '1500',
                'employee_grade_salary_monthly_reimbursement' => '5000',
                'created_by' => 1,
                'updated_by' => NULL,
                'created_at' => '2019-01-07 14:02:45',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            8 => 
            array (
                'id' => 9,
                'employee_grade_id' => 13,
                'employee_grade_salary_medical_reimbursement' => '0',
                'employee_grade_salary_mobile_phone_allowance' => '0',
                'employee_grade_salary_field_allowance' => '1500',
                'employee_grade_salary_monthly_reimbursement' => '0',
                'created_by' => 1,
                'updated_by' => NULL,
                'created_at' => '2019-01-07 14:05:04',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
        ));
        
        
    }
}