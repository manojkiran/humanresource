<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (DB::table('users')->get()->count() == 0) {

            DB::table('users')->insert([

                [
                    'name' => config('Roles.dafaultSeeds.users.name.superAdmin'),
                    'email' => config('Roles.dafaultSeeds.users.email.superAdmin'),
                    'email_verified_at' => now(),
                    'password' => bcrypt(config('Roles.dafaultSeeds.users.password.superAdmin')),
                    'created_at' =>now(),
                    'remember_token' => str_random(10),

                ],

                [
                    'name' => config('Roles.dafaultSeeds.users.name.admin'),
                    'email' => config('Roles.dafaultSeeds.users.email.admin'),
                    'email_verified_at' => now(),
                    'password' => bcrypt(config('Roles.dafaultSeeds.users.password.admin')),
                    'created_at' =>now(),
                    'remember_token' => str_random(10),

                ],

                [
                    'name' => config('Roles.dafaultSeeds.users.name.user'),
                    'email' => config('Roles.dafaultSeeds.users.email.user'),
                    'email_verified_at' => now(),
                    'password' => bcrypt(config('Roles.dafaultSeeds.users.password.user')),
                    'created_at' =>now(),
                    'remember_token' => str_random(10),
                ],

                [
                    'name' => config('Roles.dafaultSeeds.users.name.specialUser'),
                    'email' => config('Roles.dafaultSeeds.users.email.specialUser'),
                    'email_verified_at' => now(),
                    'password' => bcrypt(config('Roles.dafaultSeeds.users.password.specialUser')),
                    'created_at' =>now(),
                    'remember_token' => str_random(10),
                ],

                // [
                //     'name' => 'Manojkiran',
                //     'email' => 'manojkiran.a@postiefs.com',
                //     'email_verified_at' => now(),
                //     'password' => '$2y$10$T3EtuicyKRd9INv06XnRpuByzwhhf1Fz3rzYBtdUru5.T4WcqLToq',
                //     'created_at' => '2019-01-07 15:42:27',
                //     'remember_token' => str_random(10),
                // ],



            ]);

        } else
        {
            echo "Users Table is not empty";
        }
    }
}
