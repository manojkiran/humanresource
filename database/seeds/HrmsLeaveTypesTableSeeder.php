<?php

use Illuminate\Database\Seeder;

class HrmsLeaveTypesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('hrms_leave_types')->delete();
        
        \DB::table('hrms_leave_types')->insert(array (
            0 => 
            array (
                'id' => 1,
                'leave_type' => 'Sick Leave',
                'leave_no_of_days' => '12',
                'created_by' => 1,
                'updated_by' => NULL,
                'created_at' => '2018-12-26 13:17:57',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'leave_type' => 'Casual Leave',
                'leave_no_of_days' => '12',
                'created_by' => 1,
                'updated_by' => NULL,
                'created_at' => '2018-12-26 13:17:56',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            2 => 
            array (
                'id' => 4,
                'leave_type' => 'Maternity Leave',
                'leave_no_of_days' => '60',
                'created_by' => 1,
                'updated_by' => NULL,
                'created_at' => '2019-01-08 12:22:45',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            3 => 
            array (
                'id' => 5,
                'leave_type' => 'Loss of Pay',
                'leave_no_of_days' => '10',
                'created_by' => 1,
                'updated_by' => NULL,
                'created_at' => '2019-01-08 12:23:12',
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
        ));
        
        
    }
}