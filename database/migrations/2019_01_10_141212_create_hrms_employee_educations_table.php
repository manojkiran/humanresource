<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHrmsEmployeeEducationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hrms_employee_educations', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('employee_id');
            $table->foreign('employee_id')->references('id')->on('hrms_employees');
            $table->string('employee_sslc_school_name' , 191);
            $table->string('employee_sslc_year_of_passing' , 191);
            $table->string('employee_sslc_percentage_of_marks' , 191);

            $table->string('employee_hsc_school_name' , 191);
            $table->string('employee_hsc_year_of_passing' , 10);
            $table->string('employee_hsc_percentage_of_marks' , 5);


            $table->string('employee_ug_degree_name' , 20);
            $table->string('employee_ug_college_name' , 191);
            $table->string('employee_ug_year_of_passing' , 10);
            $table->string('employee_ug_percentage_of_marks' , 10);
            $table->integer('created_by')->unsigned();
			$table->integer('updated_by')->unsigned()->nullable();
			$table->timestamps();
            $table->softDeletes();
            $table->foreign('created_by')->references('id')->on('users');
            $table->foreign('updated_by')->references('id')->on('users')->default(NULL);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hrms_employee_educations');
    }
}
