<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHrmsSalarySettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hrms_salary_settings', function (Blueprint $table) {
            $table->increments('id');
            $table->string('employee_salary_basic_pay', 5)->nullable();
            $table->string('employee_salary_hra', 5)->nullable();
            $table->string('employee_salary_cca', 5)->nullable();
            $table->string('employee_salary_conveyance_allowance', 5)->nullable();
            $table->string('employee_provident_fund', 5)->nullable();
            $table->string('employee_salary_esi', 5)->nullable();
            $table->string('employee_salary_tds', 5)->nullable();
            $table->string('employee_salary_salary_advance', 5)->nullable();
            $table->string('employee_salary_income_tax', 5)->nullable();
            $table->string('employee_salary_professional_tax', 5)->nullable();
            $table->string('employee_salary_other_deduction', 5)->nullable();
            $table->integer('created_by')->unsigned();
			$table->integer('updated_by')->unsigned()->nullable();
			$table->timestamps();
            $table->softDeletes();
            $table->foreign('created_by')->references('id')->on('users');
            $table->foreign('updated_by')->references('id')->on('users')->default(NULL);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hrms_salary_settings');
    }
}
