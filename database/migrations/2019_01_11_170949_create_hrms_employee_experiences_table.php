<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHrmsEmployeeExperiencesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hrms_employee_experiences', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('employee_id');
            $table->foreign('employee_id')->references('id')->on('hrms_employees');
            $table->string('employee_previous_company_name', 191)->nullable();
            $table->string('employee_previous_designation', 191)->nullable();
            $table->string('employee_previous_employment_start_date', 191)->nullable();
            $table->string('employee_previous_employment_end_date', 191)->nullable();
            $table->text('employee_previous_employment_reason_for_leaving', 65535)->nullable();
            $table->string('employee_previous_employment_contact_person_name', 191)->nullable();
            $table->string('employee_previous_employment_contact_person_email', 191)->nullable();
            $table->string('employee_previous_employment_contact_person_phone', 191)->nullable();
            $table->text('employee_documents_collected', 2000)->nullable();
            $table->string('employee_relieving_letter', 191)->nullable();
            $table->string('employee_experience_letter', 191)->nullable();
            $table->text('employee_payslips', 2000)->nullable();
            $table->integer('created_by')->unsigned();
			$table->integer('updated_by')->unsigned()->nullable();
			$table->timestamps();
            $table->softDeletes();
            $table->foreign('created_by')->references('id')->on('users');
            $table->foreign('updated_by')->references('id')->on('users')->default(NULL);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hrms_employee_experiences');
    }
}
