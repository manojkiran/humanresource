<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHrmsEmployeeHierarchiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hrms_employee_hierarchies', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('department_id');
            $table->string('department_short_code',20); 
            $table->string('department_supervisor',100); 
            $table->string('department_supervisor_designation',100); 
            $table->string('employee_name',100);
            $table->unsignedInteger('employee_designation_id');
            $table->string('employee_reporting_level_one',100);
            $table->string('employee_reporting_level_two',100);
            $table->string('employee_reporting_level_three',100);

            $table->string('employee_reporting_level_one_email',100);
            $table->string('employee_reporting_level_two_email',100);
            $table->string('employee_reporting_level_three_email',100);


            $table->integer('created_by')->unsigned();
			$table->integer('updated_by')->unsigned()->nullable();
			$table->timestamps();
            $table->softDeletes();
            $table->foreign('employee_designation_id')->references('id')->on('hrms_designations');
            $table->foreign('department_id')->references('id')->on('hrms_departments');
            $table->foreign('created_by')->references('id')->on('users');
            $table->foreign('updated_by')->references('id')->on('users')->default(NULL);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hrms_employee_hierarchies');
    }
}

