<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePermissionTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $config = config('Roles.laravelRoles.table_names');

            //Roles Table
            Schema::create($config['roles'], function (Blueprint $table) 
            {
                $table->increments('id');
                $table->string('name')->unique();
                $table->string('description')->nullable();
                $table->unsignedInteger('created_by');
                $table->unsignedInteger('updated_by')->nullable();
                $table->timestamps();
                $table->softDeletes();
                $table->foreign('created_by')->references('id')->on('users');
                $table->foreign('updated_by')->references('id')->on('users')->nullable()->default(NULL);
            });
            //Modules Table
            Schema::create($config['modules'], function (Blueprint $table) {
                $table->increments('id');
                $table->string('name')->unique();
                $table->unsignedInteger('created_by');
                $table->unsignedInteger('updated_by')->nullable();
                $table->timestamps();
                $table->softDeletes();
                $table->foreign('created_by')->references('id')->on('users');
                $table->foreign('updated_by')->references('id')->on('users')->nullable()->default(NULL);
            });
            //Permissions Table
            Schema::create($config['permissions'], function (Blueprint $table) {
                $table->increments('id');
                $table->string('name')->unique();
                $table->string('description')->nullable();
                $table->integer('module_id')->unsigned();
                $table->foreign('module_id')->references('id')->on('modules')->onDelete('cascade');
                $table->unsignedInteger('created_by');
                $table->unsignedInteger('updated_by')->nullable();
                $table->timestamps();
                $table->softDeletes();
                $table->foreign('created_by')->references('id')->on('users');
                $table->foreign('updated_by')->references('id')->on('users')->nullable()->default(NULL);
            });
            // permission_user Table
            Schema::create($config['user_has_permissions'], function (Blueprint $table) use ($config) {
                $table->integer('user_id')->unsigned();
                $table->integer('permission_id')->unsigned();
                $table->foreign('permission_id')->references('id')->on($config['permissions'])->onDelete('cascade');
                $table->foreign('user_id')->references('id')->on($config['users'])->onDelete('cascade');
                $table->primary(['permission_id','user_id']);
            });
            // role_user Table
            Schema::create($config['user_has_roles'], function (Blueprint $table) use ($config) {
                $table->integer('role_id')->unsigned();
                $table->integer('user_id')->unsigned();
                $table->foreign('role_id')->references('id')->on($config['roles'])->onDelete('cascade');
                $table->foreign('user_id')->references('id')->on($config['users'])->onDelete('cascade');
                $table->primary(['role_id', 'user_id']);
            });
            // permission_role Table
            Schema::create($config['role_has_permissions'], function (Blueprint $table) use ($config) {
                $table->integer('permission_id')->unsigned();
                $table->integer('role_id')->unsigned();
                $table->foreign('permission_id')->references('id')->on($config['permissions'])->onDelete('cascade');
                $table->foreign('role_id')->references('id')->on($config['roles'])->onDelete('cascade');
                $table->primary(['permission_id', 'role_id']);
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $config = config('Roles.laravelRoles.table_names');
        
        Schema::drop($config['role_has_permissions']);
        Schema::drop($config['user_has_roles']);
        Schema::drop($config['user_has_permissions']);
        Schema::drop($config['roles']);
        Schema::drop($config['permissions']);
    }
}
