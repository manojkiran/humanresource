<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHrmsEmployeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
    	Schema::disableForeignKeyConstraints();

        Schema::create('hrms_employees', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id')->nullable();   
            $table->unsignedInteger('employee_department_id');    
            $table->enum('employee_status', ['Active', 'InActive','OnHold'])->default('Active');	
            $table->string('employee_reporting_to', 191);
            $table->unsignedInteger('employee_work_shift_id');
            $table->string('employee_designation', 191);
			$table->string('employee_first_name', 191);
			$table->string('employee_middle_name', 191)->nullable();
			$table->string('employee_last_name', 191);
			$table->string('employee_code', 191);
			$table->string('employee_date_of_birth', 191);
			$table->string('employee_gender', 191);
			$table->string('employee_aadhar_number', 191);
			$table->string('employee_marital_status', 191);
			$table->unsignedInteger('employee_nationality');
			$table->string('employee_recent_photo', 191)->nullable();
			
			$table->string('employee_door_no', 191);
			$table->string('employee_street_name', 191);
			$table->string('employee_city', 191);
			$table->string('employee_state', 191);
			$table->string('employee_country', 191);
			$table->string('employee_zip_code', 191);
			$table->string('employee_home_telephone', 191)->nullable();
			$table->string('employee_mobile_number', 191);
			$table->string('employee_email_id', 191);
			$table->unsignedInteger('employee_employment_type_id');
			$table->string('employee_job_category', 191)->nullable();
			$table->string('employee_sub_unit', 191)->nullable();
			$table->string('employee_contract_start_date', 191)->nullable();
			$table->string('employee_contract_end_date', 191)->nullable();
			$table->string('employee_years_of_experience', 5)->nullable();
			$table->string('employee_months_of_experience', 5)->nullable();
            $table->string('employee_primary_skills', 191);
            $table->string('employee_secondary_skills', 191)->nullable();
			
			$table->unsignedInteger('created_by');
            $table->unsignedInteger('updated_by')->nullable();
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('created_by')->references('id')->on('users');
            $table->foreign('updated_by')->references('id')->on('users')->nullable()->default(NULL);
			$table->foreign('employee_department_id')->references('id')->on('hrms_departments');
			$table->foreign('employee_work_shift_id')->references('id')->on('hrms_shifts');
			$table->foreign('employee_employment_type_id')->references('id')->on('hrms_employment_types');
			$table->foreign('employee_nationality')->references('id')->on('hrms_countries');
			$table->timestamps();
			$table->softDeletes();
        });

        Schema::enableForeignKeyConstraints();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hrms_employees');
    }
}
