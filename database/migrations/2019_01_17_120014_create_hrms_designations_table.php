<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHrmsDesignationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hrms_designations', function (Blueprint $table) {
            $table->increments('id');
            $table->string('designation_name');
            $table->unsignedInteger('department_id');
            $table->string('reporting_to');
            $table->unsignedInteger('company_id');
            $table->integer('created_by')->unsigned();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('created_by')->references('id')->on('users');
            $table->foreign('department_id')->references('id')->on('hrms_departments');
            $table->foreign('company_id')->references('id')->on('hrms_company_profiles');
            $table->foreign('updated_by')->references('id')->on('users')->default(null);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hrms_designations');
    }
}
