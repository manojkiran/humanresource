<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHrmsEmployeeSalaryGradesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('hrms_employee_salary_grades', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('employee_grade_id');
            $table->string('employee_grade_salary_medical_reimbursement', 5)->nullable();
            $table->string('employee_grade_salary_mobile_phone_allowance', 5)->nullable();
            $table->string('employee_grade_salary_field_allowance', 5)->nullable();
            $table->string('employee_grade_salary_monthly_reimbursement', 5)->nullable();
            $table->integer('created_by')->unsigned();
			$table->integer('updated_by')->unsigned()->nullable();
			$table->timestamps();
            $table->softDeletes();
            $table->foreign('employee_grade_id')->references('id')->on('hrms_employee_grades');
            $table->foreign('created_by')->references('id')->on('users');
            $table->foreign('updated_by')->references('id')->on('users')->default(NULL);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hrms_employee_salary_grades');
    }
}
