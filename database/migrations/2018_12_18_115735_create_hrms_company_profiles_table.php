<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHrmsCompanyProfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hrms_company_profiles', function (Blueprint $table) {
            $table->increments('id');
            $table->text('company_name',250);
            $table->string('company_registration_number',191);
            $table->string('company_gst_in',191);
            $table->text('company_address_line_one', 300);
            $table->text('company_address_line_two', 300)->nullable();
            $table->string('company_address_city', 191);
            $table->string('company_address_state', 191);
            $table->string('company_address_country', 191);
            $table->string('company_address_zip_code', 191);
            $table->string('company_email',191);
            $table->string('company_phone_number',15);
            $table->string('company_mobile_number',15)->nullable();
            $table->string('company_fax_number',15)->nullable();
            $table->string('company_website_url',50);

            $table->string('company_contact_person_full_name',191);
            $table->string('company_contact_person_designation',191);
            $table->string('company_contact_person_email_id',191);
            $table->string('company_contact_person_phone_number',25);
            
            $table->integer('created_by')->unsigned();
			$table->integer('updated_by')->unsigned()->nullable();
			$table->timestamps();
            $table->softDeletes();
            $table->foreign('created_by')->references('id')->on('users');
            $table->foreign('updated_by')->references('id')->on('users')->default(NULL);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hrms_company_profiles');
    }
}
