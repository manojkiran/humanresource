<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHrmsApplyPermissionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hrms_apply_permissions', function (Blueprint $table) {
            $table->increments('id');
            $table->string('permission_date');
            $table->string('permissions_time_from');
            $table->string('permissions_time_to');
            $table->text('permissions_reson');
            $table->integer('user_id')->unsigned();
            $table->enum('permission_status', ['Pending','Rejected','Cancelled','Approved'])->default('Pending');
            
            $table->integer('created_by')->unsigned();
			$table->integer('updated_by')->unsigned()->nullable();
			$table->timestamps();
            $table->softDeletes();
            $table->foreign('created_by')->references('id')->on('users');
            $table->foreign('user_id')->references('id')->on('users');
            
            $table->foreign('updated_by')->references('id')->on('users')->default(NULL);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hrms_apply_permissions');
    }
}

