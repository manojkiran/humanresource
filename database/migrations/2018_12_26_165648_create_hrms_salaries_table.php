<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHrmsSalariesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hrms_salaries', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('employee_id')->nullable();
            $table->unsignedInteger('employee_grade')->nullable;
            $table->string('employee_netsalary', 191)->nullable();

            $table->string('employee_salary_basic_pay', 10)->nullable();
            $table->string('employee_salary_hra', 10)->nullable();
            $table->string('employee_salary_cca', 10)->nullable();
            $table->string('employee_salary_medical_reimbursement', 10)->nullable();
            $table->string('employee_salary_mobile_phone_allowance', 10)->nullable();
            $table->string('employee_salary_field_allowance', 10)->nullable();
            $table->string('employee_salary_monthly_reimbursement', 10)->nullable();
            $table->string('employee_provident_fund', 10)->nullable();
            $table->string('employee_salary_esi', 10)->nullable();
            $table->string('employee_salary_tds', 10)->nullable();
            $table->string('employee_salary_salary_advance', 10)->nullable();
            $table->string('employee_salary_income_tax', 10)->nullable();
            $table->string('employee_salary_professional_tax', 10)->nullable();
            $table->string('employee_salary_other_deduction', 10)->nullable();

            $table->string('employee_total_earnings', 10)->nullable();

            $table->string('employee_total_deductions', 10)->nullable();

            $table->string('employee_net_salary', 10)->nullable();

            

            $table->integer('created_by')->unsigned();
			$table->integer('updated_by')->unsigned()->nullable();
			$table->timestamps();
            $table->softDeletes();
            $table->foreign('created_by')->references('id')->on('users');
            $table->foreign('updated_by')->references('id')->on('users')->default(NULL);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hrms_salaries');
    }
}
