<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::resource('employees', 'HrmsEmployeeController');
Route::resource('hrmscountries', 'HrmsCountryController');
Route::resource('hrmsdepartments', 'HrmsDepartmentController');
Route::resource('hrmsshifts', 'HrmsShiftController');
Route::resource('hrmsleavetypes', 'HrmsLeaveTypeController');
Route::resource('hrmsapplyleaves', 'HrmsApplyLeaveController');
Route::resource('hrmsapplyleaves', 'HrmsApplyLeaveController');
Route::resource('hrmsapplyleaves', 'HrmsApplyLeaveController');
Route::resource('hrmssalaries', 'HrmsSalaryController');
Route::resource('hrmsemploymenttypes', 'HrmsEmploymentTypeController');
Route::resource('hrmscompanyprofiles', 'HrmsCompanyProfileController');
Route::resource('hrmssamples', 'HrmsSampleController');
Route::resource('hrmsdepartments', 'HrmsDepartmentController');
Route::resource('hrmsdesignations', 'HrmsDesignationController');
Route::resource('hrmssalarysettings', 'HrmsSalarySettingController');
Route::resource('hrmsapplypermissions', 'HrmsApplyPermissionController');
Route::resource('hrmsexpensetypes', 'HrmsExpenseTypeController');
Route::resource('hrmstravelrequests', 'HrmsTravelRequestController');
Route::resource('hrmsemployeegrades', 'HrmsEmployeeGradeController');
Route::resource('hrmsemployeesalarygrades', 'HrmsEmployeeSalaryGradeController');
Route::resource('hrmsemployeesalarysettings', 'HrmsEmployeeSalarySettingController');
Route::resource('hrmsleavesettings', 'HrmsLeaveSettingController');
Route::resource('hrmsleavesettingleavetypes', 'HrmsLeaveSettingLeaveTypeController');
Route::resource('hrmsleavetypes', 'HrmsLeaveTypeController');
Route::resource('hrmsemployeecertificates', 'HrmsEmployeeCertificateController');
Route::resource('hrmsemployeeeducations', 'HrmsEmployeeEducationController');
Route::resource('hrmsemployeepgeducations', 'HrmsEmployeePgEducationController');
Route::resource('hrmsemployeepgeducations', 'HrmsEmployeePgEducationController');
Route::resource('hrmsemployeeexperiences', 'HrmsEmployeeExperienceController');
Route::resource('hrmsemployeehierarchies', 'HrmsEmployeeHierarchyController');
