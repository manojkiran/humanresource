<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

// Route::get('/', function (\Illuminate\Http\Request $request)
// {
//     $user = $request->user();
//     dd($user->hasRole('SuperAdmin'));
// });

// Route::get('/', function (\Illuminate\Http\Request $request) {
//     $user = $request->user();
//     dd($user->can('delete'));
// });
// Route::get('/', function (\Illuminate\Http\Request $request) {
//     $user = $request->user();
//     $user->givePermission('view.users');
// });

// Route::get('/', function (\Illuminate\Http\Request $request) {
//     $user = $request->user();
//     $user->removePermission('view.users');
// });

// Route::get('/', function (\Illuminate\Http\Request $request) {
//     $user = $request->user();
//     $user->givePermission('view.users');
// });


// Route::get('/', function (\Illuminate\Http\Request $request) {
//     $user = $request->user();
//     $user->modifyPermission('view.users','view.roles','view.permissions','delete.permissions');
// });
Route::get('/give', 'HomeController@give')->name('give');


Route::get('/', function () {
    return view('welcome');
});

Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');

Route::prefix('admin')->middleware(['web', 'auth'])->group(function (){


	Route::get('/permissions', 'PermissionController@index')->name('permissions.index');
	Route::get('/permissions/create', 'PermissionController@create')->name('permissions.create');
	Route::post('/permissions', 'PermissionController@store')->name('permissions.store');
	Route::get('/permissions/{permission}', 'PermissionController@show')->name('permissions.show');
	Route::get('/permissions/{permission}/edit', 'PermissionController@edit')->name('permissions.edit');
	Route::put('/permissions/{permission}', 'PermissionController@update')->name('permissions.update');
	Route::delete('/permissions/{permission}', 'PermissionController@destroy')->name('permissions.destroy');


	Route::get('roles', 'RoleController@index')->name('roles.index');
	Route::get('roles/create', 'RoleController@create')->name('roles.create');
	Route::post('roles', 'RoleController@store')->name('roles.store');
	Route::get('roles/{role}', 'RoleController@show')->name('roles.show');
	Route::get('roles/{role}/edit', 'RoleController@edit')->name('roles.edit');
	Route::put('roles/{role}', 'RoleController@update')->name('roles.update');
	Route::delete('roles/{role}', 'RoleController@destroy')->name('roles.destroy');


	Route::get('users', 'UserController@index')->name('users.index');
	Route::get('users/create', 'UserController@create')->name('users.create');
	Route::post('users', 'UserController@store')->name('users.store');
	Route::get('users/{user}', 'UserController@show')->name('users.show');
	Route::get('users/{user}/edit', 'UserController@edit')->name('users.edit');
	Route::put('users/{user}', 'UserController@update')->name('users.update');
	Route::delete('users/{user}', 'UserController@destroy')->name('users.destroy');

});

Route::resource('employees','HrmsEmployeeController');


//Start Routes For HrmsEmployee
Route::get('/employees',  'HrmsEmployeeController@index')->name('employees.index');
Route::get('/employees/create',  'HrmsEmployeeController@create')->name('employees.create');
Route::post('/employees',  'HrmsEmployeeController@store')->name('employees.store');
Route::get('/employees/{employee}',  'HrmsEmployeeController@show')->name('employees.show');
Route::get('/employees/{employee}/edit',  'HrmsEmployeeController@edit')->name('employees.edit');
Route::put('/employees/{employee}',  'HrmsEmployeeController@update')->name('employees.update');
Route::delete('/employees/{employee}',  'HrmsEmployeeController@destroy')->name('employees.destroy');
//End Routes For HrmsEmployee

// Route::get('/employees', 'HrmsEmployeeController@index')->name('employees.index');
// Route::get('/employees/create', 'HrmsEmployeeController@create')->name('employees.create');
// Route::post('employees', 'HrmsEmployeeController@store')->name('employees.store');
// //Start Routes For HrmsCountry
// Route::get('/hrmscountries',  'HrmsCountryController@index')->name('hrmscountries.index');
// Route::get('/hrmscountries/create',  'HrmsCountryController@create')->name('hrmscountries.create');
// Route::post('/hrmscountries',  'HrmsCountryController@store')->name('hrmscountries.store');
// Route::get('/hrmscountries/{hrmscountry}',  'HrmsCountryController@show')->name('hrmscountries.show');
// Route::get('/hrmscountries/{hrmscountry}/edit',  'HrmsCountryController@edit')->name('hrmscountries.edit');
// Route::put('/hrmscountries/{hrmscountry}',  'HrmsCountryController@update')->name('hrmscountries.update');
// Route::delete('/hrmscountries/{hrmscountry}',  'HrmsCountryController@destroy')->name('hrmscountries.destroy');
// //End Routes For HrmsCountry

//Start Routes For department
Route::get('/departments',  'HrmsDepartmentController@index')->name('departments.index');
Route::get('/departments/create',  'HrmsDepartmentController@create')->name('departments.create');
Route::post('/departments',  'HrmsDepartmentController@store')->name('departments.store');
Route::get('/departments/{department}',  'HrmsDepartmentController@show')->name('departments.show');
Route::get('/departments/{department}/edit',  'HrmsDepartmentController@edit')->name('departments.edit');
Route::put('/departments/{department}',  'HrmsDepartmentController@update')->name('departments.update');
Route::delete('/departments/{department}',  'HrmsDepartmentController@destroy')->name('departments.destroy');
//End Routes For department

//Start Routes For HrmsShift
Route::get('/hrmsshifts',  'HrmsShiftController@index')->name('hrmsshifts.index');
Route::get('/hrmsshifts/create',  'HrmsShiftController@create')->name('hrmsshifts.create');
Route::post('/hrmsshifts',  'HrmsShiftController@store')->name('hrmsshifts.store');
Route::get('/hrmsshifts/{hrmsshift}',  'HrmsShiftController@show')->name('hrmsshifts.show');
Route::get('/hrmsshifts/{hrmsshift}/edit',  'HrmsShiftController@edit')->name('hrmsshifts.edit');
Route::put('/hrmsshifts/{hrmsshift}',  'HrmsShiftController@update')->name('hrmsshifts.update');
Route::delete('/hrmsshifts/{hrmsshift}',  'HrmsShiftController@destroy')->name('hrmsshifts.destroy');
//End Routes For HrmsShift

//Start Routes For leavetype
Route::get('/leavetypes',  'HrmsLeaveTypeController@index')->name('leavetypes.index');
Route::get('/leavetypes/create',  'HrmsLeaveTypeController@create')->name('leavetypes.create');
Route::post('/leavetypes',  'HrmsLeaveTypeController@store')->name('leavetypes.store');
Route::get('/leavetypes/{leavetype}',  'HrmsLeaveTypeController@show')->name('leavetypes.show');
Route::get('/leavetypes/{leavetype}/edit',  'HrmsLeaveTypeController@edit')->name('leavetypes.edit');
Route::put('/leavetypes/{leavetype}',  'HrmsLeaveTypeController@update')->name('leavetypes.update');
Route::delete('/leavetypes/{leavetype}',  'HrmsLeaveTypeController@destroy')->name('leavetypes.destroy');
//End Routes For leavetype
//Start Routes For applyleave
Route::get('/applyleaves',  'HrmsApplyLeaveController@index')->name('applyleaves.index');
Route::get('/applyleaves/create',  'HrmsApplyLeaveController@create')->name('applyleaves.create');
Route::post('/applyleaves',  'HrmsApplyLeaveController@store')->name('applyleaves.store');
Route::get('/applyleaves/{applyleave}',  'HrmsApplyLeaveController@show')->name('applyleaves.show');
Route::get('/applyleaves/{applyleave}/edit',  'HrmsApplyLeaveController@edit')->name('applyleaves.edit');
Route::put('/applyleaves/{applyleave}',  'HrmsApplyLeaveController@update')->name('applyleaves.update');
Route::delete('/applyleaves/{applyleave}',  'HrmsApplyLeaveController@destroy')->name('applyleaves.destroy');
//End Routes For applyleave
//Start Routes For HrmsSalary
Route::get('/salaries',  'HrmsSalaryController@index')->name('salaries.index');
Route::get('/salaries/create',  'HrmsSalaryController@create')->name('salaries.create');
Route::post('/salaries',  'HrmsSalaryController@store')->name('salaries.store');
Route::get('/salaries/{salary}',  'HrmsSalaryController@show')->name('salaries.show');
Route::get('/salaries/{salary}/edit',  'HrmsSalaryController@edit')->name('salaries.edit');
Route::put('/salaries/{salary}',  'HrmsSalaryController@update')->name('salaries.update');
Route::delete('/salaries/{salary}',  'HrmsSalaryController@destroy')->name('salaries.destroy');
Route::post('/salaries/calculate',  'HrmsSalaryController@calculateSalary')->name('salaries.calculate');


//End Routes For HrmsSalary

//Start Routes For HrmsEmploymentType
Route::get('/hrmsemploymenttypes',  'HrmsEmploymentTypeController@index')->name('hrmsemploymenttypes.index');
Route::get('/hrmsemploymenttypes/create',  'HrmsEmploymentTypeController@create')->name('hrmsemploymenttypes.create');
Route::post('/hrmsemploymenttypes',  'HrmsEmploymentTypeController@store')->name('hrmsemploymenttypes.store');
Route::get('/hrmsemploymenttypes/{hrmsemploymenttype}',  'HrmsEmploymentTypeController@show')->name('hrmsemploymenttypes.show');
Route::get('/hrmsemploymenttypes/{hrmsemploymenttype}/edit',  'HrmsEmploymentTypeController@edit')->name('hrmsemploymenttypes.edit');
Route::put('/hrmsemploymenttypes/{hrmsemploymenttype}',  'HrmsEmploymentTypeController@update')->name('hrmsemploymenttypes.update');
Route::delete('/hrmsemploymenttypes/{hrmsemploymenttype}',  'HrmsEmploymentTypeController@destroy')->name('hrmsemploymenttypes.destroy');
//End Routes For HrmsEmploymentType
//Start Routes For companyprofile
Route::get('/companyprofiles',  'HrmsCompanyProfileController@index')->name('companyprofiles.index');
Route::get('/companyprofiles/create',  'HrmsCompanyProfileController@create')->name('companyprofiles.create');
Route::post('/companyprofiles',  'HrmsCompanyProfileController@store')->name('companyprofiles.store');
Route::get('/companyprofiles/{companyprofile}',  'HrmsCompanyProfileController@show')->name('companyprofiles.show');
Route::get('/companyprofiles/{companyprofile}/edit',  'HrmsCompanyProfileController@edit')->name('companyprofiles.edit');
Route::put('/companyprofiles/{companyprofile}',  'HrmsCompanyProfileController@update')->name('companyprofiles.update');
Route::delete('/companyprofiles/{companyprofile}',  'HrmsCompanyProfileController@destroy')->name('companyprofiles.destroy');
//End Routes For companyprofile
//Start Routes For designation
Route::get('/designations',  'HrmsDesignationController@index')->name('designations.index');
Route::get('/designations/create',  'HrmsDesignationController@create')->name('designations.create');
Route::post('/designations',  'HrmsDesignationController@store')->name('designations.store');
Route::get('/designations/{designation}',  'HrmsDesignationController@show')->name('designations.show');
Route::get('/designations/{designation}/edit',  'HrmsDesignationController@edit')->name('designations.edit');
Route::put('/designations/{designation}',  'HrmsDesignationController@update')->name('designations.update');
Route::delete('/designations/{designation}',  'HrmsDesignationController@destroy')->name('designations.destroy');
//End Routes For designation
//Start Routes For salarysetting
Route::get('/salarysettings',  'HrmsSalarySettingController@index')->name('salarysettings.index');
Route::get('/salarysettings/create',  'HrmsSalarySettingController@create')->name('salarysettings.create');
Route::post('/salarysettings',  'HrmsSalarySettingController@store')->name('salarysettings.store');
Route::get('/salarysettings/{salarysetting}',  'HrmsSalarySettingController@show')->name('salarysettings.show');
Route::get('/salarysettings/{salarysetting}/edit',  'HrmsSalarySettingController@edit')->name('salarysettings.edit');
Route::put('/salarysettings/{salarysetting}',  'HrmsSalarySettingController@update')->name('salarysettings.update');
Route::delete('/salarysettings/{salarysetting}',  'HrmsSalarySettingController@destroy')->name('salarysettings.destroy');
//End Routes For salarysetting
//Start Routes For applypermission
Route::get('/applypermissions',  'HrmsApplyPermissionController@index')->name('applypermissions.index');
Route::get('/applypermissions/create',  'HrmsApplyPermissionController@create')->name('applypermissions.create');
Route::post('/applypermissions',  'HrmsApplyPermissionController@store')->name('applypermissions.store');
Route::get('/applypermissions/{applypermission}',  'HrmsApplyPermissionController@show')->name('applypermissions.show');
Route::get('/applypermissions/{applypermission}/edit',  'HrmsApplyPermissionController@edit')->name('applypermissions.edit');
Route::put('/applypermissions/{applypermission}',  'HrmsApplyPermissionController@update')->name('applypermissions.update');
Route::delete('/applypermissions/{applypermission}',  'HrmsApplyPermissionController@destroy')->name('applypermissions.destroy');
//End Routes For applypermission
// applyleaves//Start Routes For HrmsExpenseType
Route::get('/hrmsexpensetypes',  'HrmsExpenseTypeController@index')->name('hrmsexpensetypes.index');
Route::get('/hrmsexpensetypes/create',  'HrmsExpenseTypeController@create')->name('hrmsexpensetypes.create');
Route::post('/hrmsexpensetypes',  'HrmsExpenseTypeController@store')->name('hrmsexpensetypes.store');
Route::get('/hrmsexpensetypes/{hrmsexpensetype}',  'HrmsExpenseTypeController@show')->name('hrmsexpensetypes.show');
Route::get('/hrmsexpensetypes/{hrmsexpensetype}/edit',  'HrmsExpenseTypeController@edit')->name('hrmsexpensetypes.edit');
Route::put('/hrmsexpensetypes/{hrmsexpensetype}',  'HrmsExpenseTypeController@update')->name('hrmsexpensetypes.update');
Route::delete('/hrmsexpensetypes/{hrmsexpensetype}',  'HrmsExpenseTypeController@destroy')->name('hrmsexpensetypes.destroy');
//End Routes For HrmsExpenseType
//Start Routes For travelrequest
Route::get('/travelrequests',  'HrmsTravelRequestController@index')->name('travelrequests.index');
Route::get('/travelrequests/create',  'HrmsTravelRequestController@create')->name('travelrequests.create');
Route::post('/travelrequests',  'HrmsTravelRequestController@store')->name('travelrequests.store');
Route::get('/travelrequests/{travelrequest}',  'HrmsTravelRequestController@show')->name('travelrequests.show');
Route::get('/travelrequests/{travelrequest}/edit',  'HrmsTravelRequestController@edit')->name('travelrequests.edit');
Route::put('/travelrequests/{travelrequest}',  'HrmsTravelRequestController@update')->name('travelrequests.update');
Route::delete('/travelrequests/{travelrequest}',  'HrmsTravelRequestController@destroy')->name('travelrequests.destroy');
//End Routes For travelrequest
//Start Routes For employeegrade
Route::get('/employeegrades',  'HrmsEmployeeGradeController@index')->name('employeegrades.index');
Route::get('/employeegrades/create',  'HrmsEmployeeGradeController@create')->name('employeegrades.create');
Route::post('/employeegrades',  'HrmsEmployeeGradeController@store')->name('employeegrades.store');
Route::get('/employeegrades/{employeegrade}',  'HrmsEmployeeGradeController@show')->name('employeegrades.show');
Route::get('/employeegrades/{employeegrade}/edit',  'HrmsEmployeeGradeController@edit')->name('employeegrades.edit');
Route::put('/employeegrades/{employeegrade}',  'HrmsEmployeeGradeController@update')->name('employeegrades.update');
Route::delete('/employeegrades/{employeegrade}',  'HrmsEmployeeGradeController@destroy')->name('employeegrades.destroy');
//End Routes For employeegrade
//Start Routes For salarygradesetting
Route::get('/salarygradesettings',  'HrmsEmployeeSalaryGradeController@index')->name('salarygradesettings.index');
Route::get('/salarygradesettings/create',  'HrmsEmployeeSalaryGradeController@create')->name('salarygradesettings.create');
Route::post('/salarygradesettings',  'HrmsEmployeeSalaryGradeController@store')->name('salarygradesettings.store');
Route::get('/salarygradesettings/{salarygradesetting}',  'HrmsEmployeeSalaryGradeController@show')->name('salarygradesettings.show');
Route::get('/salarygradesettings/{salarygradesetting}/edit',  'HrmsEmployeeSalaryGradeController@edit')->name('salarygradesettings.edit');
Route::put('/salarygradesettings/{salarygradesetting}',  'HrmsEmployeeSalaryGradeController@update')->name('salarygradesettings.update');
Route::delete('/salarygradesettings/{salarygradesetting}',  'HrmsEmployeeSalaryGradeController@destroy')->name('salarygradesettings.destroy');
//End Routes For salarygradesetting
//Start Routes For employeesalarysetting
Route::get('/employeesalarysettings',  'HrmsEmployeeSalarySettingController@index')->name('employeesalarysettings.index');
Route::get('/employeesalarysettings/create',  'HrmsEmployeeSalarySettingController@create')->name('employeesalarysettings.create');
Route::post('/employeesalarysettings',  'HrmsEmployeeSalarySettingController@store')->name('employeesalarysettings.store');
Route::get('/employeesalarysettings/{employeesalarysetting}',  'HrmsEmployeeSalarySettingController@show')->name('employeesalarysettings.show');
Route::get('/employeesalarysettings/{employeesalarysetting}/edit',  'HrmsEmployeeSalarySettingController@edit')->name('employeesalarysettings.edit');
Route::put('/employeesalarysettings/{employeesalarysetting}',  'HrmsEmployeeSalarySettingController@update')->name('employeesalarysettings.update');
Route::delete('/employeesalarysettings/{employeesalarysetting}',  'HrmsEmployeeSalarySettingController@destroy')->name('employeesalarysettings.destroy');
//End Routes For employeesalarysetting


//Start Routes For HrmsLeaveSetting
Route::get('/leavesettings',  'HrmsLeaveSettingController@index')->name('leavesettings.index');
Route::get('/leavesettings/create',  'HrmsLeaveSettingController@create')->name('leavesettings.create');
Route::post('/leavesettings',  'HrmsLeaveSettingController@store')->name('leavesettings.store');
Route::get('/leavesettings/{leavesetting}',  'HrmsLeaveSettingController@show')->name('leavesettings.show');
Route::get('/leavesettings/{leavesetting}/edit',  'HrmsLeaveSettingController@edit')->name('leavesettings.edit');
Route::put('/leavesettings/{leavesetting}',  'HrmsLeaveSettingController@update')->name('leavesettings.update');
Route::delete('/leavesettings/{leavesetting}',  'HrmsLeaveSettingController@destroy')->name('leavesettings.destroy');
//End Routes For HrmsLeaveSetting
//Start Routes For leavesettingleavetype
Route::get('/leavesettingleavetypes',  'HrmsLeaveSettingLeaveTypeController@index')->name('leavesettingleavetypes.index');
Route::get('/leavesettingleavetypes/create',  'HrmsLeaveSettingLeaveTypeController@create')->name('leavesettingleavetypes.create');
Route::post('/leavesettingleavetypes',  'HrmsLeaveSettingLeaveTypeController@store')->name('leavesettingleavetypes.store');
Route::get('/leavesettingleavetypes/{leavesettingleavetype}',  'HrmsLeaveSettingLeaveTypeController@show')->name('leavesettingleavetypes.show');
Route::get('/leavesettingleavetypes/{leavesettingleavetype}/edit',  'HrmsLeaveSettingLeaveTypeController@edit')->name('leavesettingleavetypes.edit');
Route::put('/leavesettingleavetypes/{leavesettingleavetype}',  'HrmsLeaveSettingLeaveTypeController@update')->name('leavesettingleavetypes.update');
Route::delete('/leavesettingleavetypes/{leavesettingleavetype}',  'HrmsLeaveSettingLeaveTypeController@destroy')->name('leavesettingleavetypes.destroy');
//End Routes For leavesettingleavetype



Route::put('leavetypes/{leavetype}/changeStatus',  'HomeController@changeStatus')->name('leavetypes.changeStatus');

//Start Routes For HrmsEmployeeCertificate
Route::get('/hrmsemployeecertificates',  'HrmsEmployeeCertificateController@index')->name('hrmsemployeecertificates.index');
Route::get('/hrmsemployeecertificates/create',  'HrmsEmployeeCertificateController@create')->name('hrmsemployeecertificates.create');
Route::post('/hrmsemployeecertificates',  'HrmsEmployeeCertificateController@store')->name('hrmsemployeecertificates.store');
Route::get('/hrmsemployeecertificates/{hrmsemployeecertificate}',  'HrmsEmployeeCertificateController@show')->name('hrmsemployeecertificates.show');
Route::get('/hrmsemployeecertificates/{hrmsemployeecertificate}/edit',  'HrmsEmployeeCertificateController@edit')->name('hrmsemployeecertificates.edit');
Route::put('/hrmsemployeecertificates/{hrmsemployeecertificate}',  'HrmsEmployeeCertificateController@update')->name('hrmsemployeecertificates.update');
Route::delete('/hrmsemployeecertificates/{hrmsemployeecertificate}',  'HrmsEmployeeCertificateController@destroy')->name('hrmsemployeecertificates.destroy');
//End Routes For HrmsEmployeeCertificate
//Start Routes For HrmsEmployeeEducation
Route::get('/hrmsemployeeeducations',  'HrmsEmployeeEducationController@index')->name('hrmsemployeeeducations.index');
Route::get('/hrmsemployeeeducations/create',  'HrmsEmployeeEducationController@create')->name('hrmsemployeeeducations.create');
Route::post('/hrmsemployeeeducations',  'HrmsEmployeeEducationController@store')->name('hrmsemployeeeducations.store');
Route::get('/hrmsemployeeeducations/{hrmsemployeeeducation}',  'HrmsEmployeeEducationController@show')->name('hrmsemployeeeducations.show');
Route::get('/hrmsemployeeeducations/{hrmsemployeeeducation}/edit',  'HrmsEmployeeEducationController@edit')->name('hrmsemployeeeducations.edit');
Route::put('/hrmsemployeeeducations/{hrmsemployeeeducation}',  'HrmsEmployeeEducationController@update')->name('hrmsemployeeeducations.update');
Route::delete('/hrmsemployeeeducations/{hrmsemployeeeducation}',  'HrmsEmployeeEducationController@destroy')->name('hrmsemployeeeducations.destroy');
//End Routes For HrmsEmployeeEducation
//Start Routes For HrmsEmployeePgEducation
Route::get('/hrmsemployeepgeducations',  'HrmsEmployeePgEducationController@index')->name('hrmsemployeepgeducations.index');
Route::get('/hrmsemployeepgeducations/create',  'HrmsEmployeePgEducationController@create')->name('hrmsemployeepgeducations.create');
Route::post('/hrmsemployeepgeducations',  'HrmsEmployeePgEducationController@store')->name('hrmsemployeepgeducations.store');
Route::get('/hrmsemployeepgeducations/{hrmsemployeepgeducation}',  'HrmsEmployeePgEducationController@show')->name('hrmsemployeepgeducations.show');
Route::get('/hrmsemployeepgeducations/{hrmsemployeepgeducation}/edit',  'HrmsEmployeePgEducationController@edit')->name('hrmsemployeepgeducations.edit');
Route::put('/hrmsemployeepgeducations/{hrmsemployeepgeducation}',  'HrmsEmployeePgEducationController@update')->name('hrmsemployeepgeducations.update');
Route::delete('/hrmsemployeepgeducations/{hrmsemployeepgeducation}',  'HrmsEmployeePgEducationController@destroy')->name('hrmsemployeepgeducations.destroy');
//End Routes For HrmsEmployeePgEducation
//Start Routes For HrmsEmployeePgEducation
Route::get('/hrmsemployeepgeducations',  'HrmsEmployeePgEducationController@index')->name('hrmsemployeepgeducations.index');
Route::get('/hrmsemployeepgeducations/create',  'HrmsEmployeePgEducationController@create')->name('hrmsemployeepgeducations.create');
Route::post('/hrmsemployeepgeducations',  'HrmsEmployeePgEducationController@store')->name('hrmsemployeepgeducations.store');
Route::get('/hrmsemployeepgeducations/{hrmsemployeepgeducation}',  'HrmsEmployeePgEducationController@show')->name('hrmsemployeepgeducations.show');
Route::get('/hrmsemployeepgeducations/{hrmsemployeepgeducation}/edit',  'HrmsEmployeePgEducationController@edit')->name('hrmsemployeepgeducations.edit');
Route::put('/hrmsemployeepgeducations/{hrmsemployeepgeducation}',  'HrmsEmployeePgEducationController@update')->name('hrmsemployeepgeducations.update');
Route::delete('/hrmsemployeepgeducations/{hrmsemployeepgeducation}',  'HrmsEmployeePgEducationController@destroy')->name('hrmsemployeepgeducations.destroy');
//End Routes For HrmsEmployeePgEducation
//Start Routes For HrmsEmployeeExperience
Route::get('/hrmsemployeeexperiences',  'HrmsEmployeeExperienceController@index')->name('hrmsemployeeexperiences.index');
Route::get('/hrmsemployeeexperiences/create',  'HrmsEmployeeExperienceController@create')->name('hrmsemployeeexperiences.create');
Route::post('/hrmsemployeeexperiences',  'HrmsEmployeeExperienceController@store')->name('hrmsemployeeexperiences.store');
Route::get('/hrmsemployeeexperiences/{hrmsemployeeexperience}',  'HrmsEmployeeExperienceController@show')->name('hrmsemployeeexperiences.show');
Route::get('/hrmsemployeeexperiences/{hrmsemployeeexperience}/edit',  'HrmsEmployeeExperienceController@edit')->name('hrmsemployeeexperiences.edit');
Route::put('/hrmsemployeeexperiences/{hrmsemployeeexperience}',  'HrmsEmployeeExperienceController@update')->name('hrmsemployeeexperiences.update');
Route::delete('/hrmsemployeeexperiences/{hrmsemployeeexperience}',  'HrmsEmployeeExperienceController@destroy')->name('hrmsemployeeexperiences.destroy');
//End Routes For HrmsEmployeeExperience
//Start Routes For employeehierarchy
Route::get('/employeehierarchies',  'HrmsEmployeeHierarchyController@index')->name('employeehierarchies.index');
Route::get('/employeehierarchies/create',  'HrmsEmployeeHierarchyController@create')->name('employeehierarchies.create');
Route::post('/employeehierarchies',  'HrmsEmployeeHierarchyController@store')->name('employeehierarchies.store');
Route::get('/employeehierarchies/{employeehierarchy}',  'HrmsEmployeeHierarchyController@show')->name('employeehierarchies.show');
Route::get('/employeehierarchies/{employeehierarchy}/edit',  'HrmsEmployeeHierarchyController@edit')->name('employeehierarchies.edit');
Route::put('/employeehierarchies/{employeehierarchy}',  'HrmsEmployeeHierarchyController@update')->name('employeehierarchies.update');
Route::delete('/employeehierarchies/{employeehierarchy}',  'HrmsEmployeeHierarchyController@destroy')->name('employeehierarchies.destroy');
//End Routes For employeehierarchy
